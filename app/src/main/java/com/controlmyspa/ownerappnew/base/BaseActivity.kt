package com.controlmyspa.ownerappnew.base

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.DialogProgressMailBinding
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

const val REQUEST_CODE_UPDATE = 1001

@RuntimePermissions
abstract class BaseActivity : AppCompatActivity() {
    var connectGatt: BluetoothGatt? = null
    var mBluetoothAdapter: BluetoothAdapter? = null
    var dialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialiseMailProgressDialog()
    }

    open fun getLocalBitmapUri(bmp: Bitmap): Uri? {
        var bmpUri: Uri? = null
        try {
            val file = File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "control_my_spa_dealer.png"
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            val uriForFile = FileProvider.getUriForFile(
                this,
                applicationContext
                    .packageName + ".provider", file
            )
            bmpUri = uriForFile
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }

    @SuppressLint("ServiceCast")
    fun initialiseBluetooth() {
        if (mBluetoothAdapter == null) {
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
            mBluetoothAdapter = bluetoothManager?.adapter
        }
        bluetoothInitialised(mBluetoothAdapter != null)
    }

    abstract fun bluetoothInitialised(hasBluetooth: Boolean)


    fun isPermissionGranted() {
        locationPermissionGrantedWithPermissionCheck()
    }

    fun isBlePermissionRequiredWithAndroidVersionS() {
        isBlePermissionRequiredWithPermissionCheck()
    }

    @RequiresApi(Build.VERSION_CODES.S)
    @NeedsPermission(
        Manifest.permission.BLUETOOTH_CONNECT,
        Manifest.permission.BLUETOOTH_SCAN
    )
    fun isBlePermissionRequired() {
        locationPermissionGrantedWithPermissionCheck()
    }

    fun signInAction() {
        signInRequestSuccessWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    abstract fun signInRequestSuccess()

    @NeedsPermission(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    abstract fun locationPermissionGranted()

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun checkForUpdates() {
        val updateManager = AppUpdateManagerFactory.create(this)
        updateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                it.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                try {
                    updateManager.startUpdateFlowForResult(
                        it,
                        AppUpdateType.IMMEDIATE,
                        this,
                        REQUEST_CODE_UPDATE
                    )
                } catch (ex: IntentSender.SendIntentException) {
                    ex.printStackTrace()
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkForUpdates()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_UPDATE && resultCode != Activity.RESULT_OK) {
            /*If user cancelled the update flow*/
        }
    }

    private fun initialiseMailProgressDialog() {
        dialog = Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        val mBinding: DialogProgressMailBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.dialog_progress_mail,
            null,
            false
        )
        dialog?.setContentView(mBinding.root)
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
    }

    fun showMailProgress() {
        if (!dialog?.isShowing!!) dialog?.show()
    }

    fun hideMailProgress() {
        if (dialog?.isShowing!!) dialog?.dismiss()
    }


}