package com.controlmyspa.ownerappnew.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.controlmyspa.ownerappnew.api.ApiClient
import com.controlmyspa.ownerappnew.api.ApiInterface
import com.controlmyspa.ownerappnew.api.BEARER
import com.controlmyspa.ownerappnew.helper.SPA_STATE_UPDATED
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.TokenManager
import com.controlmyspa.ownerappnew.service.UserManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

abstract class BaseFragment<T : ViewDataBinding> : Fragment() {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    protected lateinit var binding: T

    private val mSpaStateUpdatedReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            loadData()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getFragmentView(), container, false)
        registerBroadcastReceiver()
        return binding.root
    }

    private fun registerBroadcastReceiver() {
        activity?.let { mActivity ->
            LocalBroadcastManager.getInstance(mActivity).registerReceiver(
                    mSpaStateUpdatedReceiver,
                    IntentFilter(SPA_STATE_UPDATED)
            )
        }
    }

    abstract fun getFragmentView(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loadApiData()
    }

    override fun onStart() {
        super.onStart()
        if (compositeDisposable.size() == 0) loadApiData()
    }

    private fun loadApiData() {
        activity?.runOnUiThread {
            val disposable = Observable.interval(0, 20,
                    TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ loadData() }) { throwable: Throwable -> onError(throwable) }
            compositeDisposable.add(disposable)
        }
    }

    fun onError(throwable: Throwable) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    private fun loadData() {
        val apiService = ApiClient.getClient()?.create(ApiInterface::class.java)
        val observable = apiService?.getSpaDetails(UserManager.strUsername, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: CzDetails -> result }
                ?.subscribe({ czDetails: CzDetails -> handleResults(czDetails) }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    abstract fun handleResults(czDetails: CzDetails)

    abstract fun handleError(throwable: Throwable)

    override fun onDetach() {
        super.onDetach()
        compositeDisposable.clear()
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }


    fun getDealerData() {
        if (UserManager.strDealerId.isEmpty()) {
            return
        }
        UserManager.getDealerInfo(object : RestAPICallback {
            override fun onSuccess(result: String) {
                onSuccessUserData(result)
            }

            override fun onFailure(result: String) {
                onFailedUserData(result)
            }
        })
    }

    abstract fun onSuccessUserData(result: String)

    abstract fun onFailedUserData(result: String)

    override fun onDestroy() {
        super.onDestroy()
        unregisterBroadcastReceiver()
    }

    private fun unregisterBroadcastReceiver() {
        activity?.let { mActivity ->
            LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mSpaStateUpdatedReceiver)
        }
    }

}