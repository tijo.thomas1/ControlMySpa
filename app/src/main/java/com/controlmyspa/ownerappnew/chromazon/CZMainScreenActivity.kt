package com.controlmyspa.ownerappnew.chromazon

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.controlmyspa.ownerappnew.OwnerApplication
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.api.*
import com.controlmyspa.ownerappnew.api.ApiClient.getClient
import com.controlmyspa.ownerappnew.databinding.ActCzmainscreenBinding
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.Utility
import com.controlmyspa.ownerappnew.model.*
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.TokenManager
import com.controlmyspa.ownerappnew.service.UserManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class CZMainScreenActivity : AppCompatActivity(), View.OnClickListener, ChromazonButtonClickedListener {
    private var zoneValue: String? = null
    private var zonesList: List<TzlZones> = ArrayList()
    private val tzlGroupStates: MutableList<TzlGroupStates> = ArrayList()
    private var tzlZoneFunctions: List<TzlZoneFunctions> = ArrayList()
    private var tzlColors: MutableList<TzlColors> = ArrayList()
    private lateinit var binding: ActCzmainscreenBinding
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        compositeDisposable.dispose()
        super.onBackPressed()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    private val compositeDisposable = CompositeDisposable()
    private var colorAdapter: ColorAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.act_czmainscreen)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        binding.colorRecyclerView.apply {
            layoutManager = GridLayoutManager(this@CZMainScreenActivity, 4)
            setHasFixedSize(true)
        }

        binding.isVisibleStatus = false
        binding.isVisibleViews = false
        binding.isSingleColorMode = false
        binding.isProgress = true
        STATUS_PRO = getString(R.string.status_pro)
        ZONE = getString(R.string.zone)
        GROUP = getString(R.string.group)
        GROUP_AB = "AB"
        GROUP_BC = "BC"
        GROUP_AC = "AC"
        GROUP_ABC = "ABC"
        GROUP_DE = "DE"
        GROUP_EF = "EF"
        GROUP_DF = "DF"
        GROUP_DEF = "DEF"
        binding.ivLtrang.setOnClickListener(this)
        binding.ivRtrang.setOnClickListener(this)
        binding.btnColorWheel.setOnClickListener(this)
        binding.btnIntensity.setOnClickListener(this)
        binding.btnParty.setOnClickListener(this)
        binding.btnPower.setOnClickListener(this)
        binding.btnRelax.setOnClickListener(this)
        binding.btnSpeed.setOnClickListener(this)
        //setButtonBackgroundColor(-1)
        runOnUiThread {
            val disposable = Observable.interval(0, 5,
                    TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ loadData() }) { throwable: Throwable -> onError(throwable) }
            compositeDisposable.add(disposable)
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun onError(throwable: Throwable) {
        Log.e("ERROR", "" + throwable.message)
        SpaManager.authFailRedirectToRefreshAppData(this)
    }

    private var groupPosition = 0
    private var zonePosition = 0
    override fun onClick(v: View) {
        try {
            val viewId = v.id
            zoneValue = binding.tvZoneValue.text.toString()
            when (viewId) {
                R.id.iv_ltrang -> {
                    val name: String
                    if (zoneValue!!.contains(ZONE)) {
                        if (zonePosition == 0) {
                            if (tzlGroupStates.size > 0) {
                                groupPosition = tzlGroupStates.size - 1
                                name = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
                            } else {
                                zonePosition = zonesList.size - 1
                                name = ZONE + " " + zonesList[zonePosition].zoneName
                            }
                        } else {
                            zonePosition--
                            name = ZONE + " " + zonesList[zonePosition].zoneName
                        }
                    } else {
                        if (tzlGroupStates.size > 0) {
                            if (groupPosition != 0) {
                                groupPosition--
                                name = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
                            } else {
                                zonePosition = zonesList.size - 1
                                name = ZONE + " " + zonesList[zonePosition].zoneName
                            }
                        } else {
                            zonePosition = zonesList.size - 1
                            name = ZONE + " " + zonesList[zonePosition].zoneName
                        }
                    }
                    binding.tvZoneValue.text = name
                    setUpData()
                }
                R.id.iv_rtrang -> {
                    val name: String
                    if (zoneValue!!.contains(ZONE)) {
                        if (zonePosition == zonesList.size - 1) {
                            if (tzlGroupStates.size > 0) {
                                groupPosition = 0
                                name = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
                            } else {
                                zonePosition = 0
                                name = ZONE + " " + zonesList[zonePosition].zoneName
                            }
                        } else {
                            zonePosition++
                            name = ZONE + " " + zonesList[zonePosition].zoneName
                        }
                    } else {
                        if (tzlGroupStates.size > 0) {
                            if (groupPosition != tzlGroupStates.size - 1) {
                                groupPosition++
                                name = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
                            } else {
                                zonePosition = 0
                                name = ZONE + " " + zonesList[zonePosition].zoneName
                            }
                        } else {
                            zonePosition = 0
                            name = ZONE + " " + zonesList[zonePosition].zoneName
                        }
                    }
                    binding.tvZoneValue.text = name
                    setUpData()
                }
                R.id.btn_intensity -> {
                    isUpdateNeeded = false
                    if (intensity_value == 0) intensity_value = 8 else intensity_value--
                    setIntensityValue(intensity_value)
                    setIntensityImage(intensity_value)
                }
                R.id.btn_speed -> {
                    isUpdateNeeded = false
                    if (speed_value == 0) speed_value = 5 else speed_value--
                    setSpeedValue(speed_value)
                    setLightSpeedImage(speed_value)
                }
                R.id.btn_party -> {
                    isUpdateNeeded = false
                    setLightState(state = PARTY)
                    setPartyRelaxWheelValue(PARTY)
                }
                R.id.btn_power -> {
                    isUpdateNeeded = false
                    power_state = if (power_state == 0) 1 else 0
                    setPowerState(power_state)
                    setPowerStateImage(power_state)
                }
                R.id.btn_color_wheel -> {
                    isUpdateNeeded = false
                    color_wheel_value = if (color_wheel_value == 0) 1 else 0
                    setLightState(state = WHEEL)
                    setPartyRelaxWheelValue(WHEEL)
                }
                R.id.btn_relax -> {
                    isUpdateNeeded = false
                    relax_value = if (relax_value == 0) 1 else 0
                    setLightState(state = RELAX)
                    setPartyRelaxWheelValue(RELAX)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            groupPosition = 0

        }
    }

    private fun setPowerStateImage(powerState: Int) {
        power_state = powerState
        if (powerState == 0) binding.btnPower.setImageResource(R.drawable.ic_off_button) else binding.btnPower.setImageResource(R.drawable.ic_on_button)
    }

    private fun handleData() {
        binding.progressbar.visibility = View.GONE
        needsToUpdateAgain()
    }

    private val isGroupOrZone: String
        get() = if (zoneValue!!.contains(GROUP)) TZL_GROUP else TZL_ZONE


    private fun setLightSpeedImage(speedValue: Int) {
        speed_value = speedValue
        when (speedValue) {
            5 -> binding.btnSpeed.setImageResource(R.drawable.speed_5)
            4 -> binding.btnSpeed.setImageResource(R.drawable.speed_4)
            3 -> binding.btnSpeed.setImageResource(R.drawable.speed_3)
            2 -> binding.btnSpeed.setImageResource(R.drawable.speed_2)
            1 -> binding.btnSpeed.setImageResource(R.drawable.speed_1)
            0 -> binding.btnSpeed.setImageResource(R.drawable.speed_0)
        }
    }

    private fun setIntensityImage(intensityValue: Int) {
        intensity_value = intensityValue
        when (intensityValue) {
            7 -> binding.btnIntensity.setImageResource(R.drawable.intensity_7)
            6 -> binding.btnIntensity.setImageResource(R.drawable.intensity_6)
            5 -> binding.btnIntensity.setImageResource(R.drawable.intensity_5)
            4 -> binding.btnIntensity.setImageResource(R.drawable.intensity_4)
            3 -> binding.btnIntensity.setImageResource(R.drawable.intensity_3)
            2 -> binding.btnIntensity.setImageResource(R.drawable.intensity_2)
            1 -> binding.btnIntensity.setImageResource(R.drawable.intensity_1)
            0 -> binding.btnIntensity.setImageResource(R.drawable.intensity_0)
            8 -> binding.btnIntensity.setImageResource(R.drawable.intensity_8)
            else -> binding.btnIntensity.setImageResource(R.drawable.intensity_8)
        }
    }

    private fun setColor(color: Int) {
        binding.progressbar.visibility = View.VISIBLE
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        jsonObject.addProperty(isGroupOrZone, isGroupIdOrZoneId)
        jsonObject.addProperty(DESIRED_STATE, color.toString())
        val observable = apiInterface?.setColor(SpaManager.m_ID, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: PowerState? -> result }
                ?.subscribe({ _: PowerState? -> handleData() }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun setPartyRelaxWheelValue(partyValue: String) {
        binding.progressbar.visibility = View.VISIBLE
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        jsonObject.addProperty(isGroupOrZone, isGroupIdOrZoneId)
        jsonObject.addProperty(DESIRED_STATE, partyValue)
        val observable = apiInterface?.setState(SpaManager.m_ID, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: PowerState? -> result }
                ?.subscribe({ handleData() }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun setIntensityValue(intensityValue: Int) {
        binding.progressbar.visibility = View.VISIBLE
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        jsonObject.addProperty(isGroupOrZone, isGroupIdOrZoneId)
        jsonObject.addProperty(DESIRED_STATE, intensityValue.toString())
        val observable = apiInterface?.setCzIntensity(SpaManager.m_ID, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: PowerState? -> result }
                ?.subscribe({ handleData() }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun setPowerState(powerState: Int) {
        if (powerState == 0) {
            binding.btnPower.setImageResource(R.drawable.ic_off_button)
            setLightState(state = ZONE_FN_OFF)
        } else binding.btnPower.setImageResource(R.drawable.ic_on_button)
        binding.progressbar.visibility = View.VISIBLE
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        val desiredState: String = if (powerState == 0) "OFF" else "ON"
        jsonObject.addProperty(DESIRED_STATE, desiredState)
        val observable = apiInterface?.setPowerState(SpaManager.m_ID, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: PowerState? -> result }
                ?.subscribe({ handleData() }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun setSpeedValue(stateValue: Int) {
        binding.progressbar.visibility = View.VISIBLE
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        jsonObject.addProperty(isGroupOrZone, isGroupIdOrZoneId)
        jsonObject.addProperty(DESIRED_STATE, stateValue.toString())
        val observable = apiInterface?.setSpeed(SpaManager.m_ID, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: PowerState? -> result }
                ?.subscribe({ handleData() }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private val isGroupIdOrZoneId: String
        get() = if (zoneValue!!.contains(GROUP)) tzlGroupStates[groupPosition].groupId else zonesList[zonePosition].zoneId


    private fun loadData() {
        val apiService = getClient()?.create(ApiInterface::class.java)
        val observable = apiService?.getSpaDetails(UserManager.strUsername, BEARER + " " + TokenManager.accessToken)
        val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                ?.map { result: CzDetails -> result }
                ?.subscribe({ czDetails: CzDetails -> handleResults(czDetails) }) { throwable: Throwable -> handleError(throwable) }
        disposable?.let { compositeDisposable.add(it) }
    }

    private fun handleError(throwable: Throwable) {
        binding.isProgress = false
        needsToUpdateAgain()
        SpaManager.authFailRedirectToRefreshAppData(this)
        Log.e("ERROR", "" + throwable.message)
    }

    private var isUpdateNeeded = true

    private fun needsToUpdateAgain() {
        Handler(Looper.getMainLooper()).postDelayed({
            isUpdateNeeded = true
        }, 7000)
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    private fun handleResults(czDetails: CzDetails) {
        if (isUpdateNeeded) {
            if (czDetails.currentState?.primaryTZLStatus.equals("TZL_COMMLOST", ignoreCase = true)) {
                Snackbar.make(binding.root, getString(R.string.error_later), Snackbar.LENGTH_INDEFINITE).show()
                onBackPressed()
            }
            val isOnline = czDetails.currentState?.online ?: false
            if (!isOnline) Snackbar.make(binding.root, getString(R.string.your_spa_is_offline), Snackbar.LENGTH_LONG).show()
            if (binding.rlParent.visibility == View.GONE) {
                binding.isProgress = false
                binding.isVisibleStatus = false
                binding.isVisibleViews = true
                binding.tvZoneValue.text = ZONE + " " + czDetails.tzlstate.tzlLightStatus.tzlZones[0].zoneName
                zoneValue = binding.tvZoneValue.text.toString()
            }
            val tzlConfiguration = czDetails.tzlstate.tzlConfiguration
            val tzlColorSettings = czDetails.tzlstate.tzlColorSettings
            tzlZoneFunctions = tzlConfiguration.tzlZoneFunctions
            val tzlLightStatus = czDetails.tzlstate.tzlLightStatus
            zonesList = tzlLightStatus.tzlZones
            tzlColors = tzlColorSettings.tzlColors
            colorAdapter = ColorAdapter(tzlColors, -1, this)
            binding.colorRecyclerView.adapter = colorAdapter
            val groupStates: List<TzlGroupStates> = tzlConfiguration.tzlGroupStates
            tzlGroupStates.clear()
            if (groupStates.isNotEmpty()) for (j in groupStates.indices) {
                if (!groupStates[j].state.equals("OFF", ignoreCase = true)) {
                    tzlGroupStates.add(groupStates[j])
                }
            }
            if (isGroupOrZone == TZL_GROUP) {
                if (groupPosition == tzlGroupStates.size)
                    groupPosition = 0

                if (tzlGroupStates.size == 0)
                    binding.tvZoneValue.text = ZONE + " " + zonesList[zonePosition].zoneName
                else
                    binding.tvZoneValue.text = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
            }
            setUpData()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setUpData() {
        zoneValue = binding.tvZoneValue.text.toString()
        if (zoneValue!!.contains(ZONE)) {
            binding.tvZoneValue.text = ZONE + " " + zonesList[zonePosition].zoneName
            setUpApplyValues(zonePosition)

        } else {
            binding.tvZoneValue.text = GROUP + " ${tzlGroupStates[groupPosition].groupId} : " + tzlGroupStates[groupPosition].state
            var zoneIdFromZoneValue = 0
            if (zoneValue!!.contains(GROUP_AB)) zoneIdFromZoneValue = 0
            else if (zoneValue!!.contains(GROUP_BC)) zoneIdFromZoneValue = 1
            else if (zoneValue!!.contains(GROUP_AC) || zoneValue!!.contains(GROUP_ABC)) zoneIdFromZoneValue = 0
            else if (zoneValue!!.contains(GROUP_DE)) zoneIdFromZoneValue = 3
            else if (zoneValue!!.contains(GROUP_EF)) zoneIdFromZoneValue = 4
            else if (zoneValue!!.contains(GROUP_DF) || zoneValue!!.contains(GROUP_DEF)) zoneIdFromZoneValue = 3
            else if (zoneValue!!.contains(GROUP_A)) zoneIdFromZoneValue = 0
            else if (zoneValue!!.contains(GROUP_B)) zoneIdFromZoneValue = 1
            else if (zoneValue!!.contains(GROUP_C)) zoneIdFromZoneValue = 2
            else if (zoneValue!!.contains(GROUP_D)) zoneIdFromZoneValue = 3
            else if (zoneValue!!.contains(GROUP_E)) zoneIdFromZoneValue = 4
            else if (zoneValue!!.contains(GROUP_F)) zoneIdFromZoneValue = 5

            setUpApplyValues(zoneIdFromZoneValue)
        }
    }

    private fun setUpApplyValues(zoneIdFromZoneValue: Int) {
        val setter: MutableSet<String> = HashSet()
        for (j in zonesList.indices) {
            if (zonesList[j].state != "OFF") {
                setter.add(zonesList[j].state)
            }
        }
        if (setter.size == 0) setPowerStateImage(0) else setPowerStateImage(1)
        setLightState(zonesList[zoneIdFromZoneValue].state)

        /**@author TIJO THOMAS
         * @issue color code check from server to update the list drawable
         * @since 26-8-2021*/
        tzlColors.forEachIndexed { index, colors ->
            if (zonesList[zoneIdFromZoneValue].red == colors.red &&
                    zonesList[zoneIdFromZoneValue].blue == colors.blue &&
                    zonesList[zoneIdFromZoneValue].green == colors.green) {
                colorAdapter?.updatePosition(index)
            }
        }

        setIntensityImage(zonesList[zoneIdFromZoneValue].intensity)
        setLightSpeedImage(zonesList[zoneIdFromZoneValue].speed)
        when (tzlZoneFunctions[zoneIdFromZoneValue].zoneFunction) {
            ZONE_FN_NORMAL -> {
                binding.isVisibleViews = true
                binding.isVisibleStatus = false
                binding.isSingleColorMode = false
            }
            ZONE_FN_WATER_TEMP -> {
                binding.isVisibleViews = false
                binding.isVisibleStatus = true
                binding.isSingleColorMode = false
                binding.tvZoneBC.text = WATER_TEMP
            }
            ZONE_FN_STATUS_PRO -> {
                binding.isVisibleViews = false
                binding.isVisibleStatus = true
                binding.isSingleColorMode = false
                binding.tvZoneBC.text = STATUS_PRO
            }
            ZONE_FN_SINGLE_COLOR_MODE -> {
                binding.isVisibleViews = true
                binding.isVisibleStatus = false
                binding.isSingleColorMode = true
                //setLightState(state = ZONE_FN_SINGLE_COLOR_MODE)
            }
        }

        println("FUNCTION : ${tzlZoneFunctions[zoneIdFromZoneValue].zoneFunction}")
    }

    private fun setLightState(state: String) {
        when (state) {
            WHEEL -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_active)
                binding.btnParty.setImageResource(R.drawable.party_inactive)
                binding.btnRelax.setImageResource(R.drawable.relax_inactive)
                colorAdapter?.updatePosition(-1)
            }
            PARTY -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_inactive)
                binding.btnParty.setImageResource(R.drawable.party_active)
                binding.btnRelax.setImageResource(R.drawable.relax_inactive)
                colorAdapter?.updatePosition(-1)
            }
            RELAX -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_inactive)
                binding.btnParty.setImageResource(R.drawable.party_inactive)
                binding.btnRelax.setImageResource(R.drawable.relax_active)
                colorAdapter?.updatePosition(-1)
            }
            ZONE_FN_OFF -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_inactive)
                binding.btnParty.setImageResource(R.drawable.party_inactive)
                binding.btnRelax.setImageResource(R.drawable.relax_inactive)
                colorAdapter?.updatePosition(-1)
                setIntensityImage(0)
                setLightSpeedImage(0)
            }
            ZONE_FN_SINGLE_COLOR_MODE -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_inactive)
                binding.btnParty.setImageResource(R.drawable.party_inactive)
                binding.btnRelax.setImageResource(R.drawable.relax_inactive)
                colorAdapter?.updatePosition(-1)
                setLightSpeedImage(0)
            }
            ZONE_FN_NORMAL -> {
                binding.btnColorWheel.setImageResource(R.drawable.colorwheel_inactive)
                binding.btnParty.setImageResource(R.drawable.party_inactive)
                binding.btnRelax.setImageResource(R.drawable.relax_inactive)
            }
        }
    }


    companion object {
        private const val PARTY = "PARTY"
        private const val RELAX = "RELAX"
        private const val WHEEL = "WHEEL"
        private const val WATER_TEMP = "Water Temp"
        private var STATUS_PRO: String = ""
        private const val ZONE_FN_NORMAL = "NORMAL"
        private const val ZONE_FN_OFF = "OFF"
        private const val ZONE_FN_STATUS_PRO = "STATUS_PRO"
        private const val ZONE_FN_WATER_TEMP = "WATER_TEMP"
        private const val ZONE_FN_SINGLE_COLOR_MODE = "SINGLE_COLOR"
        private var ZONE: String = ""
        private var GROUP: String = ""
        private var GROUP_AB: String = ""
        private var GROUP_BC: String = ""
        private var GROUP_AC: String = ""
        private var GROUP_ABC: String = ""
        private var GROUP_DE: String = ""
        private var GROUP_EF: String = ""
        private var GROUP_DF: String = ""
        private var GROUP_DEF: String = ""
        private var GROUP_A: String = "A"
        private var GROUP_B: String = "B"
        private var GROUP_C: String = "C"
        private var GROUP_D: String = "D"
        private var GROUP_E: String = "E"
        private var GROUP_F: String = "F"
        private var intensity_value = 8
        private var speed_value = 5
        private var color_wheel_value = 0
        private var relax_value = 0
        private var power_state = 0
    }

    override fun onClicked(position: Int) {
        colorAdapter?.updatePosition(position)
        isUpdateNeeded = false
        setLightState(state = ZONE_FN_NORMAL)
        setColor(tzlColors[position].colorId)
    }
}