package com.controlmyspa.ownerappnew.chromazon

interface ChromazonButtonClickedListener {
    fun onClicked (position:Int)
}