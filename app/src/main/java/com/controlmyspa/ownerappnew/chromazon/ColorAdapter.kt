package com.controlmyspa.ownerappnew.chromazon

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.AdapterColorCzBinding
import com.controlmyspa.ownerappnew.model.TzlColors

class ColorAdapter(private val tzlColors: MutableList<TzlColors>, private var changedPosition: Int, private val listener: ChromazonButtonClickedListener) : RecyclerView.Adapter<ColorAdapter.MyViewHolder>() {
    class MyViewHolder(val binding: AdapterColorCzBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: AdapterColorCzBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_color_cz, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val lightZone = tzlColors[position]
        val colors = intArrayOf(R.color.Gray, R.color.Black)
        val border = GradientDrawable()
        border.setColor(Color.rgb(lightZone.red, lightZone.green, lightZone.blue))
        border.cornerRadius = 8f
        if (changedPosition == position)
            border.setStroke(8, ContextCompat.getColor(holder.itemView.context, colors[1]))
        else border.setStroke(8, ContextCompat.getColor(holder.itemView.context, colors[0]))
        holder.binding.btnColor.background = border
        holder.binding.btnColor.setOnClickListener {
            listener.onClicked(holder.adapterPosition)
        }
    }

    override fun getItemCount(): Int = tzlColors.count()

    fun updatePosition(position: Int) {
        this.changedPosition = position
        notifyDataSetChanged()
    }
}