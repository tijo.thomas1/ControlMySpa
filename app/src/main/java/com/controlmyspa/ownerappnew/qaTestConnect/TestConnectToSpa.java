package com.controlmyspa.ownerappnew.qaTestConnect;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;
import com.controlmyspa.ownerappnew.spasetup.cmscode.CmsCodeSetupOne;

public class TestConnectToSpa extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_activity_connect_to_wifi);

        Toolbar toolbar = findViewById(R.id.toolbar);
        Button btn_new_spa_owner = findViewById(R.id.btn_new_spa_owner);
        Button btn_spa_already_have = findViewById(R.id.btn_spa_already_have);
        Button btn_spa_salesperson_dealer = findViewById(R.id.btn_spa_salesperson_dealer);

        btn_new_spa_owner.setOnClickListener(this);
        btn_spa_already_have.setOnClickListener(this);
        btn_spa_salesperson_dealer.setOnClickListener(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_new_spa_owner) {

            startActivity(new Intent(TestConnectToSpa.this,
                    TestNewSpaOwner.class));


        } else if (v.getId() == R.id.btn_spa_already_have) {

            startActivity(new Intent(TestConnectToSpa.this,
                    GetCloseToSpaWithin10Feet.class));

        } else if (v.getId() == R.id.btn_spa_salesperson_dealer) {

            startActivity(new Intent(TestConnectToSpa.this,
                    CmsCodeSetupOne.class));

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
