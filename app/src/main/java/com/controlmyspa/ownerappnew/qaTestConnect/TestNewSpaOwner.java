package com.controlmyspa.ownerappnew.qaTestConnect;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;
import com.controlmyspa.ownerappnew.spasetup.NoneOfTheAboveActivity;
import com.controlmyspa.ownerappnew.spasetup.cmscode.CmsCodeSetupOne;

public class TestNewSpaOwner extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_new_spa_owner);

        Toolbar toolbar = findViewById(R.id.toolbar);
        Button btn_email_with_password = findViewById(R.id.btn_email_with_password);
        Button btn_cms_code = findViewById(R.id.btn_cms_code);
        Button btn_none_of_these = findViewById(R.id.btn_none_of_these);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        btn_email_with_password.setOnClickListener(this);
        btn_cms_code.setOnClickListener(this);
        btn_none_of_these.setOnClickListener(this);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_email_with_password) {


            startActivity(new Intent(TestNewSpaOwner.this,
                    GetCloseToSpaWithin10Feet.class));

        } else if (v.getId() == R.id.btn_cms_code) {

            startActivity(new Intent(TestNewSpaOwner.this,
                    CmsCodeSetupOne.class));


        } else if (v.getId() == R.id.btn_none_of_these) {

            startActivity(new Intent(TestNewSpaOwner.this,
                    NoneOfTheAboveActivity.class));

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
