package com.controlmyspa.ownerappnew;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.base.BaseActivity;
import com.controlmyspa.ownerappnew.databinding.ActivityLoginBinding;
import com.controlmyspa.ownerappnew.gatewayultra.BluetoothRequiredActivity;
import com.controlmyspa.ownerappnew.helper.LanguageHelper;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.AccountSetupActivity;
import com.controlmyspa.ownerappnew.qaTestConnect.TestConnectToSpa;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.service.RestAPICallback1;
import com.controlmyspa.ownerappnew.service.SpaManager;
import com.controlmyspa.ownerappnew.service.TokenManager;
import com.controlmyspa.ownerappnew.service.UserManager;
import com.controlmyspa.ownerappnew.utils.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.onetrust.otpublisherssdk.OTPublishersSDK;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends BaseActivity {

    private final static String TAG = "LoginActivity";
    public static boolean signal_strength = false;
    private SharedPreferences preferences, LNG_prefs;
    private SharedPreferences.Editor loginPrefsEditor;
    private ActivityLoginBinding binding;

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        if (getConnectGatt() != null) getConnectGatt().disconnect();
        super.onResume();

    }

    public static Intent getStartIntent(Activity mActivity) {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    String[] mLanguageArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        // binding.loginProgressBar.setIndeterminate(true);
        binding.loginProgressBar.setVisibility(View.GONE);
        mLanguageArray = getResources().getStringArray(R.array.array_lng);
        preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        LNG_prefs = getSharedPreferences(Constants.PREFS_LANGUAGE_STORE, MODE_PRIVATE);

        //Change Application level locale
        String mLanguageCode = LNG_prefs.getString(Constants.PREF_COUNTRY_CODE, "");

        /*
         * German "DE"
         * French "FR"
         * Italian "IT"
         * Sweden  "SE"
         * English "EN"
         * */


        if (!mLanguageCode.equals("")) {

            if (mLanguageCode.equalsIgnoreCase("en")) binding.tvLng4.setText(mLanguageArray[0]);
            if (mLanguageCode.equalsIgnoreCase("de")) binding.tvLng4.setText(mLanguageArray[1]);
            if (mLanguageCode.equalsIgnoreCase("fr")) binding.tvLng4.setText(mLanguageArray[2]);
            if (mLanguageCode.equalsIgnoreCase("it")) binding.tvLng4.setText(mLanguageArray[3]);
            if (mLanguageCode.equalsIgnoreCase("se")) binding.tvLng4.setText(mLanguageArray[4]);

            LanguageHelper.setLocale(LoginActivity.this, mLanguageCode);

        } else {
            LanguageHelper.setLocale(LoginActivity.this, "en");
            binding.tvLng4.setText(mLanguageArray[0]);

        }
        SpaManager.COUNTRY_CODE = LNG_prefs.getString(Constants.PREF_COUNTRY_CODE, "");


        boolean saveLogin = preferences.getBoolean(Constants.PREF_SAVE_LOGIN, false);
        if (saveLogin) {
            UserManager.strUsername = preferences.getString(Constants.PREF_USERNAME, "");
            UserManager.strPassword = preferences.getString(Constants.PREF_PASSWORD, "");
            binding.loginTxtUsername.setText(UserManager.strUsername);
            binding.loginTxtPassword.setText(UserManager.strPassword);
            binding.chkRemember.setChecked(true);


            Bundle bundle = getIntent().getExtras();
            if (bundle == null) {
                if (!preferences.getBoolean(Constants.PREF_IS_LOGOUT, false) &&
                        Utility.isInternetConnectivity(this)) {
                    signInAction();
                }
            }
        }

        if (UserManager.strDeviceToken == null || UserManager.strDeviceToken.isEmpty())
            getDeviceFCMToken();


        binding.tvLng4.setOnClickListener(v -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle(R.string.select_language)
                    .setItems(R.array.array_lng, (dialog, position) -> {

                        if (position == 0) {
                            Log.d(TAG, "EN");
                            loginPrefsEditor = LNG_prefs.edit();
                            loginPrefsEditor.putString(Constants.PREF_COUNTRY_CODE, "en");
                            loginPrefsEditor.apply();
                            binding.tvLng4.setText(mLanguageArray[position]);
                        } else if (position == 1) {
                            Log.d(TAG, "de");
                            loginPrefsEditor = LNG_prefs.edit();
                            loginPrefsEditor.putString(Constants.PREF_COUNTRY_CODE, "de");
                            loginPrefsEditor.apply();
                            binding.tvLng4.setText(mLanguageArray[position]);

                        } else if (position == 2) {
                            Log.d(TAG, "FR");
                            loginPrefsEditor = LNG_prefs.edit();
                            loginPrefsEditor.putString(Constants.PREF_COUNTRY_CODE, "fr");
                            loginPrefsEditor.apply();
                            binding.tvLng4.setText(mLanguageArray[position]);

                        } else if (position == 3) {
                            Log.d(TAG, "IT");
                            loginPrefsEditor = LNG_prefs.edit();
                            loginPrefsEditor.putString(Constants.PREF_COUNTRY_CODE, "it");
                            loginPrefsEditor.apply();
                            binding.tvLng4.setText(mLanguageArray[position]);

                        } else if (position == 4) {
                            Log.d(TAG, "SE");
                            loginPrefsEditor = LNG_prefs.edit();
                            loginPrefsEditor.putString(Constants.PREF_COUNTRY_CODE, "se");
                            loginPrefsEditor.apply();
                            binding.tvLng4.setText(mLanguageArray[position]);
                        }
                        /*refresh Current Activity*/
                        recreate();
                    });
            builder.create();
            builder.show();


        });


        //method to initialise OneTrust Publishers SDK with parameters as url and application ID
        new OTPublishersSDK(this).initializeOneTrustPublishersSDK(
                "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js", "32029a75-22b8-444e-9e2d-81a5dcdfd5a9");
       //method to load Banner/Preference Center based on boolean flag
        startActivityForResult(new OTPublishersSDK(this).loadPreferenceCenter(false), 1);
    }


    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void getDeviceFCMToken() {

        //finish firebase on complete
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        UserManager.strDeviceToken = preferences.getString(Constants.PREF_TOKEN,
                                null);

                        return;
                    }

                    String token = task.getResult().getToken();

                    Log.d(TAG, "token received:" + token);
                    UserManager.strDeviceToken = token;
                    // Get new Instance ID token

                });
    }

    public void onClickSignIn(View view) {
        binding.btnLogin.setEnabled(false);

        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> binding.btnLogin.setEnabled(true));
            }
        }, 10000);


        final String strUsername = binding.loginTxtUsername.getText().toString();
        final String strPassword = binding.loginTxtPassword.getText().toString();

        if (strUsername.isEmpty()) {
            Toast.makeText(this, R.string.please_enter_username, Toast.LENGTH_SHORT).show();
            return;
        }
        if (strPassword.isEmpty()) {
            Toast.makeText(this, R.string.please_enter_password, Toast.LENGTH_SHORT).show();
            return;
        }

        UserManager.strUsername = strUsername;
        UserManager.strPassword = strPassword;
        signInAction();
    }


    public void onClickNetworkSettings(View view) {

        if (Constants.TESTING_NEW_ONLINE) {
            Intent intent = new Intent(LoginActivity.this, TestConnectToSpa.class);
            startActivity(intent);
            return;
        }


        if (binding.loginProgressBar.getVisibility() != View.VISIBLE) {
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();
            loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, false);
            loginPrefsEditor.apply();

            Intent intent = new Intent(this, AccountSetupActivity.class);
            startActivity(intent);

        }
    }

    public void onClickBluetoothSetup(View view) {
        signal_strength = false;
        isPermissionGranted();
    }

    private void goToBluetoothRequiredActivity() {
        Intent intent = new Intent(LoginActivity.this, BluetoothRequiredActivity.class);
        startActivity(intent);
    }

    public void onClickSignalStrengthSetup(View view) {
        signal_strength = true;
        isPermissionGranted();
    }

    private void setUpUiInteractions(boolean value) {
        binding.btnLogin.setEnabled(value);
        binding.chkRemember.setEnabled(value);
        binding.tvLng4.setEnabled(value);
        binding.mForgotPassword.setEnabled(value);
        binding.mSetup.setEnabled(value);
        binding.mSignalStrength.setEnabled(value);
        binding.loginTxtPassword.setEnabled(value);
        binding.loginTxtUsername.setEnabled(value);
    }

    void signIn() {
        binding.loginProgressBar.setVisibility(View.VISIBLE);
        setUpUiInteractions(false);
        final String strUsername = UserManager.strUsername;
        final String strPassword = UserManager.strPassword;

        TokenManager.getTokenEndpoint(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {

                TokenManager.getAccessToken(strUsername, strPassword, new RestAPICallback() {
                    @Override
                    public void onSuccess(String result) {
                        UserManager.getWhoAmI(new RestAPICallback() {
                            @Override
                            public void onSuccess(String result) {

                                if (UserManager.strOemId != null) {
                                    if (binding.chkRemember.isChecked()) {
                                        loginPrefsEditor = preferences.edit();
                                        loginPrefsEditor.putBoolean(Constants.PREF_SAVE_LOGIN, true);
                                        loginPrefsEditor.apply();
                                    }
                                }

                                if (UserManager.strDeviceToken != null) {

                                    if (UserManager.strDeviceToken.length() > 0) {
                                        UserManager.setDeviceToken(LoginActivity.this, new RestAPICallback() {
                                            @Override
                                            public void onSuccess(String result) {
                                                Log.d(TAG, "device token set success:" + result);
                                            }

                                            @Override
                                            public void onFailure(String result) {
                                                Log.d(TAG, "device token failed:" + result);
                                            }
                                        });
                                    } else {
                                        Log.d(TAG, "device token length is 0");
                                    }
                                } else {
                                    Log.d(TAG, "device token is null");
                                }

                                UserManager.findCurrentUserAgreement(new RestAPICallback() {
                                    @SuppressLint("CommitPrefEdits")
                                    @Override
                                    public void onSuccess(String result) {

                                        binding.loginProgressBar.setVisibility(View.GONE);

                                        loginPrefsEditor = preferences.edit();
                                        if (binding.chkRemember.isChecked()) {
                                            loginPrefsEditor.putBoolean(Constants.PREF_SAVE_LOGIN, true);
                                            loginPrefsEditor.putString(Constants.PREF_USERNAME, strUsername);
                                            loginPrefsEditor.putString(Constants.PREF_PASSWORD, strPassword);
                                        } else {
                                            loginPrefsEditor.clear();// uncommented this line
                                        }
                                        loginPrefsEditor.apply();


                                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();
                                        loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, true);
                                        loginPrefsEditor.apply();


                                        Intent intent = new Intent(LoginActivity.this,
                                                MainActivity.class);
                                        startActivity(intent);
                                        LoginActivity.this.finish();
                                    }

                                    @Override
                                    public void onFailure(String result) {

                                        binding.loginProgressBar.setVisibility(View.GONE);
                                        Intent intent = new Intent(LoginActivity.this,
                                                TermsAndConditionsActivity.class);
                                        startActivity(intent);
                                        LoginActivity.this.finish();
                                    }
                                });


                                UserManager.getOEMUrl(UserManager.strOemLogoUrl, new RestAPICallback1() {
                                    @SuppressLint("CommitPrefEdits")
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        InputStream inputStream = new ByteArrayInputStream(bytes);
                                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                        getLocalBitmapUri(bitmap);
                                    }

                                    @Override
                                    public void onFailure(String s) {
                                        binding.loginProgressBar.setVisibility(View.GONE);
                                    }
                                });


                            }

                            @Override
                            public void onFailure(String result) {

                                binding.loginProgressBar.setVisibility(View.GONE);
                                Log.d(TAG, result);

                                if (result.equals("500"))
                                    Toast.makeText(LoginActivity.this, getString(R.string.server_error),
                                            Toast.LENGTH_SHORT).show();
                                else if (result.equals("401")) {
                                    Toast.makeText(LoginActivity.this, getString(R.string.check_login_credentials),
                                            Toast.LENGTH_SHORT).show();
                                }
                                setUpUiInteractions(true);
                            }
                        });
                    }

                    @Override
                    public void onFailure(String result) {

                        binding.loginProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, getString(R.string.check_login_credentials),
                                Toast.LENGTH_SHORT).show();

                        setUpUiInteractions(true);
                    }
                });
            }

            @Override
            public void onFailure(String result) {
                binding.loginProgressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, getString(R.string.login_error),
                        Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Couldn't get token endpoint.");
                setUpUiInteractions(true);
            }
        });

    }

    public void onClickForgotPassword(View view) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(Constants.FORGOT_PASSWORD_URL));
        startActivity(browserIntent);
    }

    @Override
    public void locationPermissionGranted() {
        goToBluetoothRequiredActivity();
    }


    @Override
    public void bluetoothInitialised(boolean hasBluetooth) {

    }

    @Override
    public void signInRequestSuccess() {
        signIn();
    }
}
