package com.controlmyspa.ownerappnew.utils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.MySSLSocketFactory;

import java.security.KeyStore;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {

    public static final String PREFS_LANGUAGE_STORE = "spa_language_settings";
    public static final String CURRENT_COUNTRY_NAME_URL = "http://ip-api.com/json";
    public static final String NETWORK_SETTINGS_URL = "http://192.168.78.15:8080/networkSettings";
    public static final String NETWORK_WIFI_URL = "http://192.168.78.15:8080/wifiScan";
    public static final String REGISTER_ENTRY_URL = "http://192.168.78.15:8080/registerUserToSpa";


    //QA

    /*public static final String BASE_URL = "https://iotqa.controlmyspa.com/mobile";
    public static final String ENTRY_URL = "https://iotqa.controlmyspa.com/idm/tokenEndpoint";
    public static final String FORGOT_PASSWORD_URL = "https://idmqa.controlmyspa.com/identity/person/passwordReminder.htm";
    public static final String AUTO_BUILD_URL = "https://iotqa.controlmyspa.com/autoBuild/";*/

    //Production
    public static final String BASE_URL = "https://iot.controlmyspa.com/mobile";
    public static final String ENTRY_URL = "https://iot.controlmyspa.com/idm/tokenEndpoint";
    public static final String FORGOT_PASSWORD_URL = "https://idm.controlmyspa.com/identity/person/passwordReminder.htm";
    public static final String AUTO_BUILD_URL = "https://iot.controlmyspa.com/autoBuild/";
    public static final String PRIVACY_POLICY = "http://www.balboawater.com/Privacy-Policy";
    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5";
    public static final String WEATHER_API_KEY = "4a3c5aac25a8f8d2b99a836f9a7bbe93";
    public static final float MAX_TEMPERATURE = 104;
    public static final float MIN_TEMPERATURE = 50;
    public static final int MAX_TEMPERATURE_CELSIUS = 40;
    public static final int MIN_TEMPERATURE_CELSIUS = 10;

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final int REFRESH_TIME_INTERVAL = 30;
    public static final int REFRESH_TIME_INTERVAL_FOR_CONTROLS = 5;
    public static final int REFRESH_TIME_INTERVAL_FOR_TEMPERATURE = 14;
    public static final int REFRESH_TIME_INTERVAL_FILTER_CYCLE = 20;
    public static final DateFormat sdf12 = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    public static final DateFormat sdf24 = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    public static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static final String PREFS_NAME = "ControlMySpaPrefs";
    public static final String PREF_COUNTRY_CODE = "countryCode";
    public static final String PREFS_FILTER = "FilterPrefs";
    public static final String PREF_SAVE_LOGIN = "SaveLogin";
    public static final String PREF_IS_LOGOUT = "isLogout";
    public static final String PREF_USERNAME = "username";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_TIME_MODE = "TimeMode";
    public static final String PREF_TOKEN = "DeviceToken";
    public static final String PREF_MAIN_PAGE = "fromMainPage";
    public static final String PREF_IS_FAHRENHEIT = "isFahrenheit";
    public static final String SERIAL_NUMBER = "serialNumber";
    public static final String FILTER_TWO_ENABLE = "filterTwoEnable";
    public static final String STR_OEM_ID = "strOemId";
    private static final String PACKAGE_NAME = "com.controlmyspa.ownerappnew";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static Boolean TESTING_OFFLINE = false;
    /*make true if you are making QA build */
    public static Boolean TESTING_NEW_ONLINE = true;

    public static AsyncHttpClient getUnsafeHttpClient(){
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            client.setSSLSocketFactory(sf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

}
