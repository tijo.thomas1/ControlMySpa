package com.controlmyspa.ownerappnew.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.api.ApiClient.getClient
import com.controlmyspa.ownerappnew.api.ApiInterface
import com.controlmyspa.ownerappnew.api.BEARER
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.DialogProgressbarBinding
import com.controlmyspa.ownerappnew.databinding.FragmentDetailsBinding
import com.controlmyspa.ownerappnew.helper.KeyBoardHideListener
import com.controlmyspa.ownerappnew.helper.Utility
import com.controlmyspa.ownerappnew.helper.accountData
import com.controlmyspa.ownerappnew.model.AccountData
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.TokenManager
import com.controlmyspa.ownerappnew.service.UserManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class AccountFragment : BaseFragment<FragmentDetailsBinding>() {
    private var keyBoardHideListener: KeyBoardHideListener? = null
    private var progressDialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        keyBoardHideListener = activity as KeyBoardHideListener?
    }

    override fun getFragmentView() = R.layout.fragment_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgressDialog()
        binding.fragAcc.btnUpdateAccount.setOnClickListener {
            if (!Utility.isValidEmail(binding.fragAcc.accountEmail.text)) {
                Toast.makeText(context, getString(R.string.enter_valid_email), Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            } else if (UserManager.strId.isEmpty()) {
                return@setOnClickListener
            }
            UserManager.strFirstName = binding.fragAcc.accountFirstName.text.toString()
            UserManager.strLastName = binding.accountLastName.text.toString()
            UserManager.strEmail = binding.fragAcc.accountEmail.text.toString()
            UserManager.strPhone = binding.fragAcc.accountPhone.text.toString()
            UserManager.strAddress1 = binding.fragAcc.accountAddress1.text.toString()
            UserManager.strAddress2 = binding.fragAcc.accountAddress2.text.toString()
            UserManager.strCity = binding.fragAcc.accountCity.text.toString()
            UserManager.strState = binding.fragAcc.accountState.text.toString()
            UserManager.strZipcode = binding.fragAcc.accountZipcode.text.toString()
            UserManager.strCountry = binding.fragAcc.accountCountry.text.toString()
            showProgressDialog()
            keyBoardHideListener?.hideKeyBoardIfNeeded()
            UserManager.requestUpdateAccount(context, object : RestAPICallback {
                override fun onSuccess(result: String) {
                    hideProgressDialog()
                    Toast.makeText(context, R.string.success_update, Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(result: String) {
                    hideProgressDialog()
                    Toast.makeText(context, R.string.error_update, Toast.LENGTH_SHORT).show()
                }
            })
        }
        binding.fragAcc.btnUnregisterSpa.text = getString(R.string.request_service)
        binding.fragAcc.btnUnregisterSpa.setOnClickListener { requestService() }
        accountData?.let { handleResults(it) }
    }

    private fun initProgressDialog() {
        progressDialog = Dialog(binding.root.context)
        val dialogProgressBarBinding: DialogProgressbarBinding = DataBindingUtil.inflate(
            LayoutInflater.from(binding.root.context),
            R.layout.dialog_progressbar,
            null,
            false
        )
        progressDialog?.setContentView(dialogProgressBarBinding.root)
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
    }

    private fun showProgressDialog() {
        progressDialog?.show()
    }

    private fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

    abstract fun requestService()
    override fun onAttach(context: Context) {
        super.onAttach(context)
        showAccountInfo()
    }

    private fun showAccountInfo() {
        val apiService = getClient()?.create(ApiInterface::class.java)
        val whoAmI = apiService?.getWhoAmI(BEARER + " " + TokenManager.accessToken)
        whoAmI?.enqueue(object : Callback<AccountData?> {
            override fun onResponse(call: Call<AccountData?>, response: Response<AccountData?>) {
                accountData = response.body()
                accountData?.let { handleResults(it) }
            }

            override fun onFailure(call: Call<AccountData?>, t: Throwable) {
                Toast.makeText(
                    requireContext(),
                    requireContext().getString(R.string.error_later),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun handleResults(accountData: AccountData?) {
        try {
            binding.fragAcc.accountFirstName.setText(accountData?.firstName)
            binding.accountLastName.setText(accountData?.lastName)
            binding.fragAcc.accountEmail.text = accountData?.email ?: ""
            binding.fragAcc.accountPhone.setText(accountData?.phone)
            binding.fragAcc.accountAddress1.setText(accountData?.address?.address1)
            binding.fragAcc.accountAddress2.setText(accountData?.address?.address2)
            binding.fragAcc.accountCity.setText(accountData?.address?.city)
            binding.fragAcc.accountState.setText(accountData?.address?.state)
            binding.fragAcc.accountZipcode.setText(accountData?.address?.zip)
            binding.fragAcc.accountCountry.setText(accountData?.address?.country)
        } catch (ee: Exception) {
            ee.printStackTrace()
        }
    }

    abstract fun handleErrorThrown(throwable: Throwable)
    override fun handleError(throwable: Throwable) {
        handleErrorThrown(throwable)
    }
}