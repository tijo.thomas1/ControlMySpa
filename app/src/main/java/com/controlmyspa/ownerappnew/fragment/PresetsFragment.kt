package com.controlmyspa.ownerappnew.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.controlmyspa.ownerappnew.EditPresetActivity
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.SchedulePresetActivity
import com.controlmyspa.ownerappnew.adapter.PresetListAdapter
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.DialogPromptBinding
import com.controlmyspa.ownerappnew.databinding.FragmentPresetsBinding
import com.controlmyspa.ownerappnew.helper.baseCzDetails
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import java.util.concurrent.TimeUnit

class PresetsFragment : BaseFragment<FragmentPresetsBinding>() {
    private var mAdapter: PresetListAdapter? = null
    private val autoUpdateDisposable = CompositeDisposable()
    override fun getFragmentView(): Int = R.layout.fragment_presets

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presetsProgressBar.isIndeterminate = true
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.setHasFixedSize(true)
        mAdapter?.setHasStableIds(true)
        mAdapter = PresetListAdapter(SpaManager.m_PresetList, object : PresetListAdapter.OnPresetActionListener {
            override fun onPlay(position: Int) {
                binding.presetsProgressBar.visibility = View.VISIBLE
                val presetID = SpaManager.m_PresetList[position].strId
                SpaManager.requestRunSpaRecipe(presetID, object : RestAPICallback {
                    override fun onSuccess(result: String) {
                        binding.presetsProgressBar.visibility = View.GONE
                        val adb = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
                        adb.setMessage(getString(R.string.schedule_start_msg1) + " " + SpaManager.m_PresetList[position].strName + " " + getString(R.string.schedule_start_msg2))
                        adb.setPositiveButton(R.string.ok) { _: DialogInterface?, _: Int ->
                            baseCzDetails?.let { loadData(it.id) }
                        }
                        adb.show()
                    }

                    override fun onFailure(result: String) {
                        binding.presetsProgressBar.visibility = View.GONE
                        Snackbar.make(binding.root, getString(R.string.unable_to_run_a_preset), Snackbar.LENGTH_LONG).show()
                    }
                })
            }

            override fun onDelete(position: Int) {
                AlertDialog.Builder(context!!, R.style.AlertDialogTheme)
                        .setMessage(R.string.Are_you_sure)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                            binding.presetsProgressBar.visibility = View.VISIBLE
                            SpaManager.requestDeleteSpaPreset(position, object : RestAPICallback {
                                override fun onSuccess(result: String) {
                                    binding.presetsProgressBar.visibility = View.GONE
                                    mAdapter?.notifyDataSetChanged()
                                    showNoSchedules()
                                }

                                override fun onFailure(result: String) {
                                    binding.presetsProgressBar.visibility = View.GONE
                                    Log.d(TAG, result)
                                    Snackbar.make(binding.root, getString(R.string.unable_to_delete_a_preset), Snackbar.LENGTH_LONG).show()
                                }
                            })
                        }
                        .setNegativeButton(android.R.string.cancel, null).show()
            }

            override fun onSchedule(position: Int) {
                val intent = Intent(this@PresetsFragment.context,
                        SchedulePresetActivity::class.java)
                intent.putExtra("index", position)
                startActivity(intent)
            }

            override fun onEdit(position: Int) {
                val intent = Intent(this@PresetsFragment.context,
                        EditPresetActivity::class.java)
                intent.putExtra("index", position)
                startActivity(intent)
            }
        })
        binding.recyclerView.adapter = mAdapter
        binding.presetsBtnAddNew.setOnClickListener {
            baseCzDetails?.let {
                if (it.id.isEmpty()) {
                    binding.offData = getString(R.string.spa_not_found)
                    binding.visible = false
                    return@setOnClickListener
                }
            }
            baseCzDetails?.let {
                val dialog = Dialog(binding.root.context)
                val mBinding: DialogPromptBinding = DataBindingUtil.inflate(LayoutInflater.from(binding.root.context), R.layout.dialog_prompt, null, false)
                dialog.setContentView(mBinding.root)
                dialog.setCancelable(false)
                dialog.setCanceledOnTouchOutside(false)
                mBinding.mCancel.setOnClickListener {
                    dialog.dismiss()
                }
                mBinding.mOk.setOnClickListener {
                    val mData = mBinding.promptTxtname.text.toString().trim()
                    if (!TextUtils.isEmpty(mData)) {
                        mBinding.promptTxtname.error = null
                        val intent = Intent(this@PresetsFragment.context,
                                EditPresetActivity::class.java)
                        intent.putExtra("index", -1) //New preset
                        intent.putExtra("name", mData)
                        startActivity(intent)
                        dialog.dismiss()
                    } else mBinding.promptTxtname.error = getString(R.string.required)
                }
                dialog.show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        baseCzDetails?.let { autoUpdateData(it.id) }
        binding.visible = true
    }

    private fun autoUpdateData(mId: String) {
        binding.presetsProgressBar.visibility = View.VISIBLE
        val disposable = Observable.interval(0, 10,
                TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loadData(mId) }) { throwable: Throwable -> onError(throwable) }
        autoUpdateDisposable.add(disposable)
    }

    override fun onPause() {
        super.onPause()
        autoUpdateDisposable.clear()
    }

    private fun loadData(mId: String) {
        if (mId.isEmpty()) {
            binding.offData = getString(R.string.spa_not_found)
            binding.visible = false
            binding.presetsProgressBar.visibility = View.GONE
            return
        }
        SpaManager.requestGetSpaPresets(object : RestAPICallback {
            override fun onSuccess(result: String) {
                binding.presetsProgressBar.visibility = View.GONE
                mAdapter?.notifyDataSetChanged()
                binding.visible = true
                showNoSchedules()
            }

            override fun onFailure(result: String) {
                binding.presetsProgressBar.visibility = View.GONE
                showNoSchedules()
            }
        })
    }

    private fun showNoSchedules() {
        if (SpaManager.m_PresetList.isEmpty()) {
            activity?.let {
                binding.offData = it.getString(R.string.no_schedules)
            }
            binding.visible = false
        }
    }

    companion object {
        private const val TAG = "PresetsFragment"

        @JvmStatic
        fun newInstance(): PresetsFragment {
            return PresetsFragment()
        }
    }

    override fun handleResults(czDetails: CzDetails) {
        baseCzDetails = czDetails
    }

    override fun handleError(throwable: Throwable) {
        binding.presetsProgressBar.visibility = View.GONE
        if (throwable is HttpException) {
            if (throwable.code() == 404) {
                binding.offData = getString(R.string.spa_not_found)
            } else {
                binding.offData = getString(R.string.could_not_get_spa_info)
                SpaManager.authFailRedirectToRefreshAppData(requireContext())
            }
            binding.visible = false
        } else {
            binding.offData = getString(R.string.could_not_get_spa_info)
            SpaManager.authFailRedirectToRefreshAppData(requireContext())
        }
    }

    override fun onSuccessUserData(result: String) {

    }

    override fun onFailedUserData(result: String) {

    }
}