package com.controlmyspa.ownerappnew.fragment

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.adapter.ControlsAdapter
import com.controlmyspa.ownerappnew.api.ApiClient.getClient
import com.controlmyspa.ownerappnew.api.ApiInterface
import com.controlmyspa.ownerappnew.api.BEARER
import com.controlmyspa.ownerappnew.api.DESIRED_STATE
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.chromazon.CZMainScreenActivity
import com.controlmyspa.ownerappnew.databinding.FragmentControlsBinding
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.model.PowerState
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.TokenManager
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.*
import kotlin.collections.ArrayList

private const val TAG = "NewControlsFragment"

class NewControlsFragment : BaseFragment<FragmentControlsBinding>(), ControlsAdapter.OnControlClickListener {
    private var mComponentList: MutableList<Components> = ArrayList()
    private var mSpaId = ""
    private val mCompositeDisposable = CompositeDisposable()
    private val mAdapter: ControlsAdapter by lazy {
        ControlsAdapter(this, mComponentList)
    }

    override fun getFragmentView() = R.layout.fragment_controls

    override fun handleResults(czDetails: CzDetails) {
        baseCzDetails = czDetails
        baseCzDetails?.let { showData(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val spanCount = resources.getInteger(R.integer.ctrl_span_count)
        val metrics = context?.resources?.displayMetrics
        val ctrlWidth = resources.getDimension(R.dimen.ctrl_width).toInt()
        val pxWidth = metrics?.widthPixels?.toFloat()
        val spacing = (pxWidth?.minus(ctrlWidth * spanCount))!!.toInt() / (spanCount + 1)
        val mGridLayoutManager = GridLayoutManager(this.context, spanCount)
        binding.recyclerView.layoutManager = mGridLayoutManager
        binding.recyclerView.setHasFixedSize(true)
        mAdapter.setHasStableIds(true)
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount,
                spacing, true))
        baseCzDetails?.let { showData(it) }
    }

    private fun showData(czDetails: CzDetails) {
        Log.d(TAG, "showData ++++")
        val online = czDetails.currentState?.online?:false
        Log.d(TAG, "online = $online")
        Log.d(TAG, "mAdapter.getIsSomethingInProgress() = ${mAdapter.getIsSomethingInProgress()}")
        if (online) {
            if (mAdapter.getIsSomethingInProgress()) {
                Log.d(TAG, "online && mAdapter.getIsSomethingInProgress()")
                binding.recyclerView.visibility = View.VISIBLE
                binding.tvSpaOffline.visibility = View.GONE
                mSpaId = czDetails.id
                val currentState = czDetails.currentState
                val components = currentState?.components
                mComponentList.clear()
                if (components != null) {
                    for (component in components) {
                        if (!component.availableValues.isNullOrEmpty() && component.componentType != "FILTER" && component.componentType != "OZONE" && !TextUtils.isEmpty(component.registeredTimestamp)) {
                            component.sortOrder = when (component.componentType) {
                                "PUMP" -> 1
                                "LIGHT" -> 2
                                else -> 3
                            }
                            mComponentList.add(component)
                        }
                    }
                }
                mComponentList.sortWith { ob1, ob2 -> ob1.sortOrder - ob2.sortOrder }

                val panelLock = currentState?.panelLock?:false
                val panelValue = if (panelLock) LOCK_PANEL else UNLOCK_PANEL
                val panelComponent = Components(getString(R.string.CMS_panel_name),
                        "",
                        "",
                        panelValue,
                        Collections.emptyList(),
                        "",
                        getString(R.string.CMS_panel_name),
                        "",
                        0,
                        0,
                        0,
                        "",
                        "", null, 3)
                mComponentList.add(panelComponent)

                val primaryTZLStatus = currentState?.primaryTZLStatus
                val secondaryTZLStatus = currentState?.secondaryTZLStatus
                if (primaryTZLStatus == TZL_CONNECTED || secondaryTZLStatus == TZL_CONNECTED) {
                    val component = Components("",
                            "",
                            "",
                            getString(R.string.lighting),
                            Collections.emptyList(),
                            "",
                            CHROMAZONE,
                            "",
                            0,
                            0,
                            0,
                            "",
                            "", null, 3)
                    mComponentList.add(component)
                }
                mAdapter.notifyDataSetChanged()
            }
        } else {
            Log.d(TAG, "else .... ")
            binding.recyclerView.visibility = View.GONE
            binding.tvSpaOffline.visibility = View.VISIBLE
            binding.tvSpaOffline.text = getString(R.string.your_spa_is_offline)
        }
        Log.d(TAG, "showData ----")
    }

    class GridSpacingItemDecoration internal constructor(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column
            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)
                if (position < spanCount) { // top edge
                    outRect.top = spacing / 2
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }

    }

    override fun handleError(throwable: Throwable) {
        if (throwable is HttpException) {
            if (throwable.code() == 404) {
                binding.recyclerView.visibility = View.GONE
                binding.tvSpaOffline.visibility = View.VISIBLE
                binding.tvSpaOffline.text = getString(R.string.spa_not_found)
            } else {
                binding.recyclerView.visibility = View.GONE
                binding.tvSpaOffline.visibility = View.VISIBLE
                binding.tvSpaOffline.text = getString(R.string.could_not_get_spa_info)
                SpaManager.authFailRedirectToRefreshAppData(requireContext())
            }
        } else {
            binding.tvSpaOffline.text = getString(R.string.could_not_get_spa_info)
            SpaManager.authFailRedirectToRefreshAppData(requireContext())
        }
    }

    override fun onSuccessUserData(result: String) {

    }

    override fun onFailedUserData(result: String) {

    }

    override fun setControlState(position: Int, holder: ControlsAdapter.MyViewHolder, spaComponent: Components) {
        val components = mComponentList[position]
        components.value = spaComponent.targetValue
        val jsonObject = JsonObject()
        jsonObject.addProperty("deviceNumber", spaComponent.port)
        jsonObject.addProperty(DESIRED_STATE, spaComponent.targetValue)
        val componentType = when (spaComponent.componentType) {
            "PUMP" ->
                "Jet"

            "BLOWER" ->
                "Blower"

            "LIGHT" ->
                "Light"

            "MISTER" ->
                "Mister"

            "AUX" ->
                "Aux"

            "MICROSILK" ->
                "Microsilk"

            "CIRCULATION_PUMP" ->
                "CircPump"

            "OZONE" ->
                "Ozone"
            else -> ""
        }
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val observable = apiInterface?.setControlsState(mSpaId, componentType, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable!!.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { result: PowerState -> result }
                .subscribe({ powerState: PowerState -> handleControlsResults(powerState, holder) }) { handleControlsError(holder) }
        mCompositeDisposable.add(disposable)
    }

    override fun setPanelLockMode(state: String, holder: ControlsAdapter.MyViewHolder) {
        holder.binding.llControls.animate().alpha(0.5f)
        val apiInterface = getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        if (state == LOCK_PANEL) jsonObject.addProperty(DESIRED_STATE, UNLOCK_PANEL) else jsonObject.addProperty(DESIRED_STATE, LOCK_PANEL)
        val observable = apiInterface?.setPanelLockMode(mSpaId, jsonObject, BEARER + " " + TokenManager.accessToken)
        val disposable = observable!!.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { result: PowerState -> result }
                .subscribe({ powerState: PowerState -> handleControlsResults(powerState, holder) }) { handleControlsError(holder) }
        mCompositeDisposable.add(disposable)
    }

    private fun handleControlsError(holder: ControlsAdapter.MyViewHolder) {
        holder.binding.llControls.animate().alpha(1.0f)
        notifyAfterApiCallDone()
    }

    private fun handleControlsResults(powerState: PowerState, holder: ControlsAdapter.MyViewHolder) {
        holder.binding.llControls.animate().alpha(1.0f)
        notifyAfterApiCallDone()
        println(powerState.values.desiredState)
    }

    private fun notifyAfterApiCallDone() {
        Handler(Looper.getMainLooper()).postDelayed({
            mAdapter.isSomethingInProgress(false)
            mAdapter.notifyDataSetChanged()
        }, 3000)
    }

    override fun openCzPage() {
        val intent = Intent(activity, CZMainScreenActivity::class.java)
        startActivity(intent)
    }

    override fun onDetach() {
        mCompositeDisposable.clear()
        super.onDetach()
    }
}