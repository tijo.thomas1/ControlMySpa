package com.controlmyspa.ownerappnew.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.controlmyspa.ownerappnew.FilterCycleTimerActivity
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.FragmentFilterCycleBinding
import com.controlmyspa.ownerappnew.helper.baseCzDetails
import com.controlmyspa.ownerappnew.materialdatetimepicker.time.TimePickerDialog
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.utils.Constants
import java.util.*

class FilterCycleFragment : BaseFragment<FragmentFilterCycleBinding>() {
    private var startTime1: Date = Date()
    private var startTime2: Date = Date()
    private var duration1 = 1
    private var duration2 = 1
    private var flag = false
    private var isToggleClicked = true
    private var is24Hour = false
    private var preferences: SharedPreferences? = null
    private val componentList = ArrayList<Components>()
    private var mComponentsList: List<Components> = ArrayList()
    override fun handleResults(czDetails: CzDetails) {
        baseCzDetails = czDetails
        baseCzDetails?.let { showData(it) }
    }

    private fun showData(czDetails: CzDetails) {
        mComponentsList = czDetails.currentState?.components ?: arrayListOf()
        is24Hour = czDetails.currentState?.military ?: false
        val online = czDetails.currentState?.online?:false
        if (online) {
            binding.llFilter.visibility = View.VISIBLE
            binding.tvSpaOffline.visibility = View.GONE
        } else {
            binding.llFilter.visibility = View.GONE
            binding.tvSpaOffline.visibility = View.VISIBLE
        }
        if (!flag) updateFilterValues()
    }

    override fun handleError(throwable: Throwable) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    override fun onSuccessUserData(result: String) {

    }

    override fun onFailedUserData(result: String) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = this.activity!!.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.filterCycleProgressBar.isIndeterminate = true
        binding.filterCycleProgressBar.visibility = View.GONE
        setFilterTwoEnable(false)
        binding.scFilterCycle.setOnClickListener {
            val b = binding.scFilterCycle.isChecked
            if (componentList.isEmpty()) return@setOnClickListener
            val loginPrefsEditor = preferences!!.edit()
            flag = true
            Handler(Looper.getMainLooper()).postDelayed({ flag = false }, 20000)
            if (b) {
                loginPrefsEditor.putBoolean(Constants.FILTER_TWO_ENABLE, true)
                setFilterTwoEnable(true)
                if (componentList[0].value.equals("ON", ignoreCase = true)) {
                    binding.filterStatus.text = "F"
                    binding.filterStatusValue.text = "+"
                } else {
                    binding.filterStatus.text = "F"
                    binding.filterStatusValue.text = "2"
                }
            } else {
                setFilterTwoEnable(false)
                loginPrefsEditor.putBoolean(Constants.FILTER_TWO_ENABLE, false)
                if (componentList[0].value.equals("ON", ignoreCase = true)) {
                    binding.filterStatus.text = "F"
                    binding.filterStatusValue.text = "1"
                } else {
                    binding.filterStatus.text = ""
                    binding.filterStatusValue.text = ""
                }
            }
            loginPrefsEditor.apply()
            loginPrefsEditor.commit()
            updateFilterCycleToggle(b)
        }
        binding.tvChooseTimer1.setOnClickListener {
            if (componentList.isEmpty()) return@setOnClickListener
            flag = true
            val intent = Intent(activity, FilterCycleTimerActivity::class.java)
            startActivityForResult(intent, 101)
        }
        binding.tvChooseTimer2.setOnClickListener {
            if (componentList.isEmpty()) return@setOnClickListener
            flag = true
            val intent = Intent(activity, FilterCycleTimerActivity::class.java)
            startActivityForResult(intent, 102)
        }
        binding.filterCycleTxtstart1.setOnClickListener {
            if (componentList.isEmpty()) return@setOnClickListener
            flag = true
            val now = Calendar.getInstance()
            now.time = startTime1

            val military = baseCzDetails?.currentState?.military

            val hour = now[Calendar.HOUR_OF_DAY]
            val minute = now[Calendar.MINUTE]
            val dpd = TimePickerDialog.newInstance(
                    { _: TimePickerDialog?, hourOfDay: Int, minute_: Int, _: Int ->
                        now[Calendar.HOUR_OF_DAY] = hourOfDay
                        now[Calendar.MINUTE] = minute_
                        startTime1 = now.time
                        flag = true
                        setFilterOneTime(hourOfDay, minute_)
                        updateEndTime()
                    },
                    hour,
                    minute, military!!
            )
            dpd.isCancelable = false
            dpd.accentColor = ContextCompat.getColor(activity!!, R.color.colorPrimaryDark)
            dpd.setTimeInterval(1, 15)
            dpd.title = getString(R.string.Select_Time)
            dpd.show(childFragmentManager, "TimePickerDialog")
        }
        binding.filterCycleTxtstart2.setOnClickListener {
            if (componentList.isEmpty()) return@setOnClickListener
            flag = true
            val now = Calendar.getInstance()
            now.time = startTime2
            val military = baseCzDetails?.currentState?.military
            val hour = now[Calendar.HOUR_OF_DAY]
            val minute = now[Calendar.MINUTE]
            val dpd = TimePickerDialog.newInstance(
                    { _: TimePickerDialog?, hourOfDay: Int, minute_: Int, _: Int ->
                        now[Calendar.HOUR_OF_DAY] = hourOfDay
                        now[Calendar.MINUTE] = minute_
                        startTime2 = now.time
                        flag = true
                        setFilterTwoTime(hourOfDay, minute_)
                        updateEndTime()
                    },
                    hour,
                    minute, military!!
            )
            dpd.isCancelable = false
            dpd.accentColor = ContextCompat.getColor(activity!!, R.color.colorPrimaryDark)
            dpd.setTimeInterval(1, 15)
            dpd.title = getString(R.string.Select_Time)
            dpd.show(childFragmentManager, "TimePickerDialog")
        }
        binding.filterCycleBtnsave.setOnClickListener {
            if (componentList.isEmpty()) return@setOnClickListener
            binding.filterCycleProgressBar.visibility = View.VISIBLE
            SpaManager.requestSetFilterCycleIntervalsSchedule(this@FilterCycleFragment.context, 0, Constants.sdf24.format(startTime1), duration1, object : RestAPICallback {
                override fun onSuccess(result: String) {
                    Log.d(TAG, result)
                    if (!preferences!!.getBoolean(Constants.FILTER_TWO_ENABLE, false)) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, resources.getString(R.string.success_save), Toast.LENGTH_SHORT).show()
                        needToUpdateAgain()
                    }
                }

                override fun onFailure(result: String) {
                    Log.d(TAG, result)
                    if (!preferences!!.getBoolean(Constants.FILTER_TWO_ENABLE, false)) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, resources.getString(R.string.error_save), Toast.LENGTH_SHORT).show()
                        needToUpdateAgain()
                    }
                }
            })
            if (preferences!!.getBoolean(Constants.FILTER_TWO_ENABLE, false)) {
                SpaManager.requestSetFilterCycleIntervalsSchedule(this@FilterCycleFragment.context, 1, Constants.sdf24.format(startTime2), duration2, object : RestAPICallback {
                    override fun onSuccess(result: String) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, getString(R.string.success_save), Toast.LENGTH_SHORT).show()
                        Log.d(TAG, result)
                        needToUpdateAgain()
                    }

                    override fun onFailure(result: String) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, getString(R.string.error_save), Toast.LENGTH_SHORT).show()
                        Log.d(TAG, result)
                        needToUpdateAgain()
                    }
                })
            }
        }

        baseCzDetails?.let { showData(it) }
    }


    private fun needToUpdateAgain() {
        Handler(Looper.getMainLooper()).postDelayed({ flag = false }, 5000)
    }

    @SuppressLint("SetTextI18n")
    private fun setFilterOneTime(hr: Int, min: Int) {
        val minute: String = if (min < 10) {
            "0$min"
        } else {
            min.toString()
        }
        if (!is24Hour) if (hr < 12) {
            binding.filterCycleTxtstart1.text = (if (hr == 0) "12" else hr.toString()).toString() + ":" + minute + " AM"
        } else {
            binding.filterCycleTxtstart1.text = (if (hr > 12) hr - 12 else hr).toString() + ":" + minute + " PM"
        } else binding.filterCycleTxtstart1.text = "$hr:$minute"
    }

    @SuppressLint("SetTextI18n")
    private fun setFilterTwoTime(hr: Int, min: Int) {
        val minute: String = if (min < 10) {
            "0$min"
        } else {
            min.toString()
        }
        if (!is24Hour) if (hr < 12) {
            binding.filterCycleTxtstart2.text = (if (hr == 0) "12" else hr.toString()).toString() + ":" + minute + " AM"
        } else {
            binding.filterCycleTxtstart2.text = (if (hr > 12) hr - 12 else hr).toString() + ":" + minute + " PM"
        } else binding.filterCycleTxtstart2.text = "$hr:$minute"
    }

    private fun updateFilterCycleToggle(b: Boolean) {
        binding.filterCycleProgressBar.visibility = View.GONE
        SpaManager.requestSetFilterCycleToggle(this@FilterCycleFragment.context, if (b) "1" else "0",
                object : RestAPICallback {
                    override fun onSuccess(result: String) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, getString(R.string.success_save), Toast.LENGTH_SHORT).show()
                    }

                    override fun onFailure(result: String) {
                        binding.filterCycleProgressBar.visibility = View.GONE
                        Toast.makeText(activity, getString(R.string.error_save), Toast.LENGTH_SHORT).show()
                    }
                })
    }

    @SuppressLint("SetTextI18n")
    private fun updateFilterValues() {
        componentList.clear()
        for (i in mComponentsList.indices) {
            if (mComponentsList[i].componentType == "FILTER") {
                componentList.add(mComponentsList[i])
            }
        }
        if (componentList.isNotEmpty())
            if (componentList.size == 2) {
                if (componentList[0].componentType == "FILTER" && componentList[1].componentType == "FILTER") {
                    if (componentList[0].value.equals("OFF", ignoreCase = true) && componentList[1].value.equals("OFF", ignoreCase = true)) {
                        binding.filterStatus.text = ""
                        binding.filterStatusValue.text = ""
                    } else if (componentList[0].value.equals("OFF", ignoreCase = true) && componentList[1].value.equals("ON", ignoreCase = true)) {
                        binding.filterStatus.text = "F"
                        binding.filterStatusValue.text = "2"
                    } else if (componentList[0].value.equals("ON", ignoreCase = true) && componentList[1].value.equals("OFF", ignoreCase = true)) {
                        binding.filterStatus.text = "F"
                        binding.filterStatusValue.text = "1"
                    } else if (componentList[0].value.equals("ON", ignoreCase = true) && componentList[1].value.equals("ON", ignoreCase = true)) {
                        binding.filterStatus.text = "F"
                        binding.filterStatusValue.text = "+"
                    }
                }
                for (i in componentList.indices) {
                    if (componentList[i].componentType == "FILTER" && componentList[i].port == "0") {
                        val calendar = Calendar.getInstance()
                        calendar[Calendar.HOUR_OF_DAY] = componentList[i].hour
                        calendar[Calendar.MINUTE] = componentList[i].minute
                        if (componentList[i].durationMinutes < 60) {
                            binding.tvChooseTimer1.text = componentList[i].durationMinutes.toString() + " " + getString(R.string.mins)
                        } else if (componentList[i].durationMinutes % 60 == 0) {
                            if (componentList[i].durationMinutes / 60 == 1) binding.tvChooseTimer1.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hour) else binding.tvChooseTimer1.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hours)
                        } else {
                            binding.tvChooseTimer1.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hours) + " " + componentList[i].durationMinutes % 60 + " " + getString(R.string.mins)
                        }
                        duration1 = componentList[i].durationMinutes / 15
                        startTime1 = calendar.time
                        setFilterOneTime(componentList[i].hour, componentList[i].minute)
                        updateEndTime()
                    }
                    if (componentList[i].componentType == "FILTER" && componentList[i].port == "1") {
                        val calendar = Calendar.getInstance()
                        calendar[Calendar.HOUR_OF_DAY] = componentList[i].hour
                        calendar[Calendar.MINUTE] = componentList[i].minute
                        if (componentList[i].durationMinutes < 60) {
                            binding.tvChooseTimer2.text = componentList[i].durationMinutes.toString() + " " + getString(R.string.mins)
                        } else if (componentList[i].durationMinutes % 60 == 0) {
                            if (componentList[i].durationMinutes / 60 == 1) binding.tvChooseTimer2.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hour) else binding.tvChooseTimer2.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hours)
                        } else {
                            binding.tvChooseTimer2.text = (componentList[i].durationMinutes / 60).toString() + " " + getString(R.string.hours) + " " + componentList[i].durationMinutes % 60 + " " + getString(R.string.mins)
                        }
                        duration2 = componentList[i].durationMinutes / 15
                        startTime2 = calendar.time
                        if (!binding.scFilterCycle.isChecked) {
                            isToggleClicked = false
                        }
                        setFilterTwoTime(componentList[i].hour, componentList[i].minute)
                        updateEndTime()
                        if (componentList[i].value.equals("ON", ignoreCase = true) || componentList[i].value.equals("OFF", ignoreCase = true)) {
                            setFilterTwoEnable(true)
                            isToggleClicked = true
                            binding.scFilterCycle.isChecked = true
                            updateEndTime()
                        } else {
                            isToggleClicked = false
                            binding.scFilterCycle.isChecked = false
                            setFilterTwoEnable(false)
                            binding.textViewFcOff.visibility = View.VISIBLE
                        }
                    }
                }
            } else {
                if (componentList[0].componentType == "FILTER") {
                    if (componentList[0].value.equals("OFF", ignoreCase = true)) {
                        binding.filterStatus.text = ""
                        binding.filterStatusValue.text = ""
                    } else if (componentList[0].value.equals("ON", ignoreCase = true)) {
                        binding.filterStatus.text = "F"
                        binding.filterStatusValue.text = "1"
                    }
                }
                if (componentList[0].componentType == "FILTER" && componentList[0].port == "0") {
                    val calendar = Calendar.getInstance()
                    calendar[Calendar.HOUR_OF_DAY] = componentList[0].hour
                    calendar[Calendar.MINUTE] = componentList[0].minute
                    if (componentList[0].durationMinutes < 60) {
                        binding.tvChooseTimer1.text = componentList[0].durationMinutes.toString() + " " + getString(R.string.mins)
                    } else if (componentList[0].durationMinutes % 60 == 0) {
                        if (componentList[0].durationMinutes / 60 == 1) binding.tvChooseTimer1.text = (componentList[0].durationMinutes / 60).toString() + " " + getString(R.string.hour) else binding.tvChooseTimer1.text = (componentList[0].durationMinutes / 60).toString() + " " + getString(R.string.hours)
                    } else {
                        binding.tvChooseTimer1.text = (componentList[0].durationMinutes / 60).toString() + " " + getString(R.string.hours) + " " + componentList[0].durationMinutes % 60 + " " + getString(R.string.mins)
                    }
                    duration1 = componentList[0].durationMinutes / 15
                    startTime1 = calendar.time
                    setFilterOneTime(componentList[0].hour, componentList[0].minute)
                    updateEndTime()
                }
                binding.fll2.visibility = View.GONE
                binding.filterCycleTxtstart2.text = "---"
                binding.tvChooseTimer2.text = "---"
                binding.filterCycleTxtend2.text = "---"
                if (binding.scFilterCycle.isChecked) {
                    isToggleClicked = false
                }
                binding.scFilterCycle.isChecked = false
                setFilterTwoEnable(false)
            }
    }

    private fun setFilterTwoEnable(b: Boolean) {
        if (b) {
            binding.filterCycleTxtstart2.isEnabled = true
            binding.filterCycleTxtend2.isEnabled = true
            binding.tvChooseTimer2.isEnabled = true
            binding.filterCycleTxtstart2.visibility = View.VISIBLE
            binding.filterCycleTxtend2.visibility = View.VISIBLE
            binding.tvChooseTimer2.visibility = View.VISIBLE
            binding.textViewFcOff.visibility = View.GONE
            updateEndTime()
        } else {
            binding.filterCycleTxtstart2.isEnabled = false
            binding.filterCycleTxtend2.isEnabled = false
            binding.tvChooseTimer2.isEnabled = false
            binding.filterCycleTxtstart2.visibility = View.INVISIBLE
            binding.filterCycleTxtend2.visibility = View.INVISIBLE
            binding.tvChooseTimer2.visibility = View.INVISIBLE
            binding.textViewFcOff.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            binding.tvChooseTimer1.text = data!!.extras!!.getString("timer")
            duration1 = data.extras!!.getInt("timerPosition")
        } else if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            binding.tvChooseTimer2.text = data!!.extras!!.getString("timer")
            duration2 = data.extras!!.getInt("timerPosition")
        }
        updateEndTime()
    }

    @SuppressLint("SetTextI18n")
    private fun updateEndTime() {
        val cal = Calendar.getInstance()
        cal.time = startTime1
        cal.add(Calendar.MINUTE, duration1 * 15)
        if (!is24Hour) binding.filterCycleTxtend1.text = getString(R.string.ends_at) + " " + Constants.sdf12.format(cal.time) else binding.filterCycleTxtend1.text = getString(R.string.ends_at) + " " + Constants.sdf24.format(cal.time)
        if (binding.scFilterCycle.isChecked) {
            cal.time = startTime2
            cal.add(Calendar.MINUTE, duration2 * 15)
            if (!is24Hour) binding.filterCycleTxtend2.text = getString(R.string.ends_at) + " " + Constants.sdf12.format(cal.time) else binding.filterCycleTxtend2.text = getString(R.string.ends_at) + " " + Constants.sdf24.format(cal.time)
        }
    }

    companion object {
        private const val TAG = "FilterCycleFragment"

        @JvmStatic
        fun newInstance(): FilterCycleFragment {
            return FilterCycleFragment()
        }
    }

    override fun getFragmentView(): Int = R.layout.fragment_filter_cycle
}