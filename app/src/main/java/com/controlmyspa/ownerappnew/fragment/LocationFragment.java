package com.controlmyspa.ownerappnew.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.service.SpaManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;

public class LocationFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private static final String TAG = "LocationFragment";
    private static final int ZOOM_LEVEL = 15;
    private static final int TILT_LEVEL = 0;
    private static final int BEARING_LEVEL = 0;
    //location related variables
    private static final int REQUEST_CHECK_SETTINGS = 123;
    private Button btnUpdateSpaLocation;
    private LocationCallback locationCallback;
    private GoogleApiClient googleApiClient;
    private String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    // not added
    Runnable runnable = () -> SpaManager.requestSaveSpaDetails(LocationFragment.this.getContext(), new RestAPICallback() {
        @Override
        public void onSuccess(String result) {
            if (getContext() != null) {
                // Toast.makeText(getContext(), R.string.success_update, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Updated spa location");
            }
        }

        @Override
        public void onFailure(String result) {
            if (getContext() != null) {
                //  Toast.makeText(getContext(), R.string.error_update, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failed to update spa location");
            }
        }
    });
    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private Handler handler_post_device_location;
    private double mCurrent_Latitude;
    private double mCurrent_Longitude;
    private int flag = 0;

    public LocationFragment() {
        // Required empty public constructor
    }

    public static LocationFragment newInstance() {
        return new LocationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*request permission*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            checkPermission();


        } else {
            displayLocationSettingsRequest(getActivity());
        }


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_location, container,
                false);
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        handler_post_device_location = new Handler(Looper.getMainLooper());

        btnUpdateSpaLocation = view.findViewById(R.id.btnUpdateSpaLocation);
        Button btn_my_spa = view.findViewById(R.id.btn_my_spa);
        ImageView iv_my_location = view.findViewById(R.id.iv_my_location);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                            mCurrent_Latitude = location.getLatitude();
                            mCurrent_Longitude = location.getLongitude();
                            SpaManager.m_device_Lat = location.getLatitude();
                            SpaManager.m_device_Lon = location.getLongitude();

                        }
                    });

        } catch (SecurityException e) {
            System.err.print("Security exception in onViewCreated " + LocationFragment.class.getCanonicalName());
        }

        /*update location button */
        btnUpdateSpaLocation.setOnClickListener(v -> {
            SpaManager.requestSaveSpaDetails(LocationFragment.this.getContext(), new RestAPICallback() {
                @Override
                public void onSuccess(String result) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), R.string.success_update, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Updated spa location");
                    }
                }

                @Override
                public void onFailure(String result) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), R.string.error_update, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Failed to update spa location");
                    }
                }
            });
            // code to update latest spa location
           /* SpaManager.requestUpdateSpaLocation(LocationFragment.this.getContext(),
                    SpaManager.m_Lat,
                    SpaManager.m_Lon, new RestAPICallback(){


                        @Override
                        public void onSuccess(String result) {
                            if (getContext() != null) {
                                Toast.makeText(getContext(), R.string.success_update, Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Updated spa location");


                            }
                        }

                        @Override
                        public void onFailure(String result) {
                            if (getContext() != null) {
                                Toast.makeText(getContext(), R.string.error_update, Toast.LENGTH_SHORT).show();

                            }
                            Log.d(TAG, "Failed to update spa location");
                        }
                    });*/
        });
        SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        map.getMapAsync(this);

        btn_my_spa.setOnClickListener(v -> {

            flag = 1;
            displayLocationSettingsRequest(getActivity());

            mMap.clear();

            LatLng my_spa_location = new LatLng(SpaManager.m_Lat, SpaManager.m_Lon);
            mMap.addMarker(new MarkerOptions()
                    .position(my_spa_location)
                    .title(getString(R.string.my_spa))
                    .alpha(0.7f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_))
                    .draggable(true));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(my_spa_location, ZOOM_LEVEL));

        });


        iv_my_location.setOnClickListener(v -> {

            flag = 0;
            displayLocationSettingsRequest(getActivity());


            mMap.clear();
            LatLng location = new LatLng(mCurrent_Latitude, mCurrent_Longitude);
            mMap.addMarker(new MarkerOptions()
                    .position(location).title(getString(R.string.My_Location))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_LEVEL));
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data

                    try {
                        Log.d(TAG, location.toString());
                        // ...
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };


    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onMapReady(final GoogleMap map) {


        MapsInitializer.initialize(getContext());
        mMap = map;
        try {
            map.setMyLocationEnabled(true);
        } catch (SecurityException ex) {
            Log.d(TAG, ex.toString());
        }

//showing my current location
        mMap.clear();
        LatLng location = new LatLng(mCurrent_Latitude, mCurrent_Longitude);
        map.addMarker(new MarkerOptions()
                .position(location)
                .title(getString(R.string.My_Location))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_LEVEL));

        mMap.setOnMapClickListener(latLng -> {


            mMap.clear();
            //setting spa location
            SpaManager.m_Lat = latLng.latitude;
            SpaManager.m_Lon = latLng.longitude;

            //showing my current location
            LatLng location1 = new LatLng(mCurrent_Latitude, mCurrent_Longitude);
            mMap.addMarker(new MarkerOptions()
                    .position(location1)
                    .title(getString(R.string.My_Location))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location1, ZOOM_LEVEL));

            // showing spa current location
            LatLng myspa_location = new LatLng(SpaManager.m_Lat, SpaManager.m_Lon);
            mMap.addMarker(new MarkerOptions()
                    .position(myspa_location)
                    .title(getString(R.string.my_spa))
                    .alpha(0.7f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_))
                    .draggable(true));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myspa_location, ZOOM_LEVEL));

        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {


            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                mMap.clear();
                flag = 1;

                SpaManager.m_Lat = marker.getPosition().latitude;
                SpaManager.m_Lon = marker.getPosition().longitude;
                //showing my current location
                LatLng location = new LatLng(mCurrent_Latitude, mCurrent_Longitude);
                mMap.addMarker(new MarkerOptions()
                        .position(location)
                        .title(getString(R.string.My_Location))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                /* mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12));*/

                // Showing spa current location
                LatLng myspa_location = new LatLng(SpaManager.m_Lat, SpaManager.m_Lon);
                mMap.addMarker(new MarkerOptions()
                        .position(myspa_location)
                        .title(getString(R.string.my_spa))
                        .alpha(0.7f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_))
                        .draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myspa_location, ZOOM_LEVEL));
            }
        });

    }

    private void displayLocationSettingsRequest(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        //  PendingResult<LocationSettingsResult > result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(getContext()).checkLocationSettings(builder.build());
        //new code based on getSettingsClient
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.

                //checking that location is present nt or not


                if (response.getLocationSettingsStates().isLocationPresent()) {
                    Log.d("Yes", "Present");
                    try {
                        fusedLocationClient.getLastLocation()
                                .addOnSuccessListener(getActivity(), location -> {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        SpaManager.m_device_Lat = location.getLatitude();
                                        SpaManager.m_device_Lon = location.getLongitude();
                                    }
                                });

                    } catch (SecurityException e) {
                        System.err.print("Security exception at line 314 " + LocationFragment.class.getCanonicalName());
                    }


                }

            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                    getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i(TAG, "User agreed to make required location settings changes.");

                    /*do something here permission is granted */
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i(TAG, "User chose not to make required location settings changes.");
                    Toast.makeText(getActivity(), getString(R.string.app_required_location_permission), Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (isGooglePlayServicesAvailable()) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        }
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(getContext());
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(getContext(), R.string.install_google_play_services,
                        Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat
                .checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            int permsRequestCode = 200;
            ActivityCompat.requestPermissions(getActivity(),
                    perms,
                    permsRequestCode);
        }
        displayLocationSettingsRequest(getActivity());

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            mCurrent_Latitude = location.getLatitude();
                            mCurrent_Longitude = location.getLongitude();

                            SpaManager.m_device_Lat = location.getLatitude();
                            SpaManager.m_device_Lon = location.getLongitude();
                            mMap.clear();
                            if (flag == 0) {
                                //showing my current location
                                LatLng currentLocation = new LatLng(mCurrent_Latitude,
                                        mCurrent_Longitude);
                                mMap.addMarker(new MarkerOptions()
                                        .position(currentLocation)
                                        .title(getString(R.string.My_Location))
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 12));

                            } else if (flag == 1) {
                                // Showing spa current location
                                mMap.clear();
                                LatLng myspa_location = new LatLng(SpaManager.m_Lat, SpaManager.m_Lon);
                                mMap.addMarker(new MarkerOptions()
                                        .position(myspa_location)
                                        .title(getString(R.string.my_spa))
                                        .alpha(0.7f)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_))
                                        .draggable(true));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myspa_location, 12));
                            }
                        }
                    });


        } catch (SecurityException exception) {
            System.err.print(" LocationFragment.java line no.549 " + exception);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
        try {
            fusedLocationClient.requestLocationUpdates(new LocationRequest(), locationCallback, null);

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        try {
            fusedLocationClient.removeLocationUpdates(locationCallback);

        } catch (SecurityException e) {
            e.printStackTrace();
        }

    }


}
