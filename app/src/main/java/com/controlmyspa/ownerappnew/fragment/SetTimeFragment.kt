package com.controlmyspa.ownerappnew.fragment

import android.app.Dialog
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.DialogUpdateSpaTimeBinding
import com.controlmyspa.ownerappnew.databinding.FragmentSetTimeBinding
import com.controlmyspa.ownerappnew.helper.baseCzDetails
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.utils.Constants
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SetTimeFragment : BaseFragment<FragmentSetTimeBinding>() {
    private var hour = 0
    private var minute = 0
    private var calendarTime = Date()
    private var mMilitary = false
    private var processingData = false

    override fun getFragmentView() = R.layout.fragment_set_time

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.data = true
        binding.scSpaTime.setOnClickListener {
            val cal = Calendar.getInstance()
            val currentLocalTime = cal.time
            val sdf: DateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val strDate = sdf.format(currentLocalTime)
            val strTime = Constants.sdf24.format(calendarTime)
            setDateTime(strDate, strTime, if (binding.scSpaTime.isChecked) "TRUE" else "FALSE")
            val calendarTemp = Calendar.getInstance()
            calendarTemp[Calendar.HOUR_OF_DAY] = hour
            calendarTemp[Calendar.MINUTE] = minute
            val setTimeTemp = calendarTemp.time
            if (binding.scSpaTime.isChecked) {
                binding.timeOfDayTime.text = Constants.sdf24.format(setTimeTemp)
            } else {
                binding.timeOfDayTime.text = Constants.sdf12.format(setTimeTemp)
            }
        }
        binding.btnDeviceTime.setOnClickListener { updateDialog() }

        baseCzDetails?.let { showData(it) }
    }

    private fun updateDialog() {
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val dialog = activity?.let { Dialog(it) }
        val dialogBinding: DialogUpdateSpaTimeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_update_spa_time, null, false)
        dialog?.setContentView(dialogBinding.root)
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog?.window?.attributes)
        layoutParams.width = size.x - size.x / 10
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog?.window?.attributes = layoutParams
        dialogBinding.btnCancel.setOnClickListener { dialog?.dismiss() }
        dialogBinding.btnUpdateSpaTime.setOnClickListener {
            val cal = Calendar.getInstance()
            val currentLocalTime = cal.time
            val sdf: DateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
            val strDate = sdf.format(currentLocalTime)
            cal[Calendar.HOUR_OF_DAY] = cal[Calendar.HOUR_OF_DAY]
            cal[Calendar.MINUTE] = cal[Calendar.MINUTE]
            calendarTime = cal.time
            val strTime = Constants.sdf24.format(calendarTime)
            setDateTime(strDate, strTime, if (mMilitary) "TRUE" else "FALSE")
            dialog?.dismiss()
        }
        dialog?.show()
    }

    private fun setDateTime(date: String, time: String, military: String) {
        processingData = true
        binding.timeOfDayProgressBar.visibility = View.VISIBLE
        SpaManager.requestSetTime(this@SetTimeFragment.context, date, time, military, object : RestAPICallback {
            override fun onSuccess(result: String) {
                needToUpdateAgain()
                binding.timeOfDayProgressBar.visibility = View.GONE
                if (binding.scSpaTime.isChecked) binding.timeOfDayTime.text = Constants.sdf24.format(calendarTime) else binding.timeOfDayTime.text = Constants.sdf12.format(calendarTime)
                Toast.makeText(this@SetTimeFragment.context, R.string.spa_clock_updated, Toast.LENGTH_LONG).show()
            }

            override fun onFailure(result: String) {
                needToUpdateAgain()
                binding.timeOfDayProgressBar.visibility = View.GONE
                Log.d(TAG, result)
                Toast.makeText(context, R.string.error_save,
                        Toast.LENGTH_LONG).show()
                SpaManager.authFailRedirectToRefreshAppData(requireContext())
            }
        })
    }

    private fun needToUpdateAgain() {
        Handler(Looper.getMainLooper()).postDelayed({
            processingData = false
        }, 5000)
    }

    override fun handleResults(czDetails: CzDetails) {
        baseCzDetails = czDetails
        baseCzDetails?.let { showData(it) }
    }

    private fun showData(czDetails: CzDetails) {
        if (!processingData) {
            binding.data = czDetails.currentState?.online ?: false
            mMilitary = czDetails.currentState?.military ?: false
            binding.scSpaTime.isChecked = mMilitary
            hour = czDetails.currentState?.hour ?: 0
            minute = czDetails.currentState?.minute ?: 0
            val calendar = Calendar.getInstance()
            calendar[Calendar.HOUR_OF_DAY] = hour
            calendar[Calendar.MINUTE] = minute
            calendarTime = calendar.time
            if (mMilitary) binding.timeOfDayTime.text = Constants.sdf24.format(calendarTime) else binding.timeOfDayTime.text = Constants.sdf12.format(calendarTime)
        }
    }

    override fun handleError(throwable: Throwable) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    override fun onSuccessUserData(result: String) {
    }

    override fun onFailedUserData(result: String) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    companion object {
        private const val TAG = "SetTimeFragment"

        @JvmStatic
        fun newInstance(): SetTimeFragment {
            return SetTimeFragment()
        }
    }
}