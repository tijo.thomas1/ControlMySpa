package com.controlmyspa.ownerappnew.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.api.*
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.FragNewTempBinding
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.model.*
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.TokenManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.util.*
import kotlin.math.roundToInt

private const val TAG = "NewTempFragment"

class NewTempFragment : BaseFragment<FragNewTempBinding>() {
    private var isSpaOnline: Boolean = true
    private var isCelsius = false
    private var isTempChanged: Boolean = true
    private var isFahrenheit = false
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var mSpaId = ""
    private var mIsSliderTouched = false
    private var mDesiredTemp = 0f

    private lateinit var currentSpaState: CurrentState
    override fun handleResults(czDetails: CzDetails) {
        if (!firebaseCrashlyticsCustomKeyAdded) {
            FirebaseCrashlytics.getInstance().setCustomKey("serialNumber", czDetails.serialNumber)
            firebaseCrashlyticsCustomKeyAdded = true
        }
        baseCzDetails = czDetails
        baseCzDetails?.let { showData(it) }
    }

    private lateinit var mSwipeListener: SwipeListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSwipeListener = activity as SwipeListener
    }

    private fun showData(czDetails: CzDetails) {
        try {
            binding.temperatureProgressBar.visibility = View.GONE
            isSpaOnline = czDetails.currentState?.online ?: false
            if (isTempChanged)
                if (isSpaOnline) {
                    isCelsius = czDetails.currentState?.celsius ?: false
                    czDetails.currentState?.let {
                        currentSpaState = it
                    }
                    mSpaId = czDetails.id
                    SpaManager.m_ID = mSpaId
                    SpaManager.m_CurrentTemp = currentSpaState.currentTemp?.toFloat()
                            ?: 50.0F
                    SpaManager.m_DesiredTemp = currentSpaState.desiredTemp?.toFloat()
                            ?: 50.0F
                    isCelsius = currentSpaState.celsius
                    binding.visible = true
                    updateFilterValues(currentSpaState.components)
                    binding.scDegree.isChecked = isCelsius
                    binding.lowHighSwitch.isChecked = currentSpaState.tempRange == "HIGH"
                    if (binding.viewMeter.height != 0)
                        updateTempData(currentSpaState.setupParams.lowRangeLow, currentSpaState.setupParams.lowRangeHigh,
                                currentSpaState.setupParams.highRangeLow, currentSpaState.setupParams.highRangeHigh)

                    updateTempUI(binding.scDegree.isChecked)
                    handleDegreeData()
                    binding.readyRestSwitch.isChecked = currentSpaState.runMode.equals("Ready", true)
                    updatePage(true)
                    if (currentSpaState.currentTemp?.toFloat() == 0.0F) {
                        binding.visible = false
                        binding.message = getString(R.string.your_spa_is_offline)
                    } else binding.visible = true
                    if (currentSpaState.desiredTemp?.toFloat() != 0F) {
                        binding.tvLowerLimit.text = convertTemperature(currentSpaState.setupParams.lowRangeLow.toString(), binding.scDegree.isChecked)
                        binding.tvUpperLimit.text = convertTemperature(currentSpaState.setupParams.highRangeHigh.toString(), binding.scDegree.isChecked)
                    } else {
                        binding.visible = false
                        binding.message = getString(R.string.your_spa_is_offline)
                    }

                    mSwipeListener.drawerUnLocked()

                } else {
                    binding.temperatureDescription.visibility = View.VISIBLE
                    binding.temperatureDescription.text = getString(R.string.your_spa_is_offline)
                    binding.visible = false
                    mSwipeListener.drawerLocked()
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun handleError(throwable: Throwable) {
        mSwipeListener.drawerLocked()
        binding.temperatureProgressBar.visibility = View.GONE
        binding.visible = false
        if (throwable is HttpException) {
            if (throwable.code() == 404) {
                binding.message = requireContext().getString(R.string.spa_not_found)
                updatePage(false)
            } else {
                binding.message = requireContext().getString(R.string.could_not_get_spa_info)
                SpaManager.authFailRedirectToRefreshAppData(requireContext())
                updatePage(false)
            }
            binding.visible = false
        } else {
            binding.message = requireContext().getString(R.string.could_not_get_spa_info)
            SpaManager.authFailRedirectToRefreshAppData(requireContext())
            updatePage(false)
        }
        needToUpdateAgain()
    }


    private fun updatePage(flag: Boolean) {
        binding.visible = flag
    }

    override fun onSuccessUserData(result: String) {}
    override fun onFailedUserData(result: String) {}

    override fun getFragmentView() = R.layout.frag_new_temp

    @SuppressLint("SetTextI18n")
    private fun updateFilterValues(components: MutableList<Components>) {
        val spaComponents: MutableList<Components> = ArrayList()
        for (i in components.indices) {
            if (components[i].componentType == "FILTER") {
                spaComponents.add(components[i])
            }
        }
        if (spaComponents.isNotEmpty())
            if (spaComponents[0].componentType == "FILTER" && spaComponents[1].componentType == "FILTER") {
                if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                    binding.tvAppState.text = """
                    F1 : ${getString(R.string.off)}
                    F2 : ${getString(R.string.off)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                    binding.tvAppState.text = """
                    F1 : ${getString(R.string.off)}
                    F2 : ${getString(R.string.on)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                    binding.tvAppState.text = """
                    F1 : ${getString(R.string.on)}
                    F2 : ${getString(R.string.off)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                    binding.tvAppState.text = """
                    F1 : ${getString(R.string.on)}
                    F2 : ${getString(R.string.on)}
                    """.trimIndent()
                } else {
                    binding.tvAppState.text = """
                    F1 : ${getString(R.string.off)}
                    F2 : ${getString(R.string.disabled)}
                    """.trimIndent()
                }
            }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.visible = false

        binding.scDegree.setOnClickListener {
            isTempChanged = false
            updateTempUI(binding.scDegree.isChecked)
            val apiInterface = ApiClient.getClient()?.create(ApiInterface::class.java)
            val jsonObject = JsonObject()
            val checked = binding.scDegree.isChecked
            val value = if (!checked)
                "FAHRENHEIT"
            else "CELSIUS"
            jsonObject.addProperty(DESIRED_STATE, value)
            val observable = apiInterface?.setTempDegreeMode(mSpaId, jsonObject, BEARER + " " + TokenManager.accessToken)
            val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                    ?.map { result: PowerState? -> result }
                    ?.subscribe({ handleDegreeData() }) { throwable: Throwable -> handleError(throwable) }
            disposable?.let { it1 -> compositeDisposable.add(it1) }

        }
        binding.lowHighSwitch.setOnClickListener {
            isTempChanged = false
            val apiInterface = ApiClient.getClient()?.create(ApiInterface::class.java)
            val jsonObject = JsonObject()
            val mode = if (binding.lowHighSwitch.isChecked) "HIGH" else "LOW"
            jsonObject.addProperty(DESIRED_STATE, mode)
            val observable = apiInterface?.setLowHighMode(mSpaId, jsonObject, BEARER + " " + TokenManager.accessToken)
            val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                    ?.map { result: PowerState? -> result }
                    ?.subscribe({ _: PowerState? -> handleLowHighMode() }) { throwable: Throwable -> handleError(throwable) }
            disposable?.let { it1 -> compositeDisposable.add(it1) }

        }
        binding.readyRestSwitch.setOnClickListener {
            isTempChanged = false
            val apiInterface = ApiClient.getClient()?.create(ApiInterface::class.java)
            val jsonObject = JsonObject()
            jsonObject.addProperty(ORIGINATOR_ID, mSpaId)
            val observable = apiInterface?.setHeaterModeToggle(mSpaId, jsonObject, BEARER + " " + TokenManager.accessToken)
            val disposable = observable?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread())
                    ?.map { result: PowerState? -> result }
                    ?.subscribe({ handleHeaterModeToggleData() }) { throwable: Throwable -> handleError(throwable) }
            disposable?.let { it1 -> compositeDisposable.add(it1) }
        }

        showAccountInfo()

        view.setOnTouchListener { _: View?, event: MotionEvent ->
            val x = event.x
            val y = event.y
            if (event.action == MotionEvent.ACTION_DOWN) {
                mSliderLeftX = binding.viewSlider.x
                mSliderTopY = binding.viewSlider.y
                if (x > mSliderLeftX && x < mSliderLeftX + mSliderWidth) {
                    if (y > mSliderTopY && y < mSliderTopY + mSliderHeight) {

                        val highRangeLow = baseCzDetails?.currentState?.setupParams?.highRangeLow
                        val lowRangeHigh = baseCzDetails?.currentState?.setupParams?.lowRangeHigh
                        val lowRangeLow = baseCzDetails?.currentState?.setupParams?.lowRangeLow
                        val highRangeHigh = baseCzDetails?.currentState?.setupParams?.highRangeHigh
                        lowRangeLow?.let {
                            lowRangeHigh?.let { it1 ->
                                highRangeLow?.let { it2 ->
                                    highRangeHigh?.let { it3 ->
                                        updateTempData(it, it1, it2, it3)
                                    }
                                }
                            }
                        }
                        mIsSliderTouched = true
                        mLastY = y
                    }
                }
            }
            if (mIsSliderTouched && event.action == MotionEvent.ACTION_MOVE) {
                isTempChanged = false
                val yDiff = y - mLastY
                var newTopY = mSliderTopY + yDiff
                if (newTopY < mCurrRangeMinY) {
                    newTopY = mCurrRangeMinY
                    mDesiredTemp = mRangeMaxTemp.toFloat()
                } else if (newTopY > mCurrRangeMaxY) {
                    newTopY = mCurrRangeMaxY
                    mDesiredTemp = mRangeMinTemp.toFloat()
                } else {
                    mLastY = y
                    for (i in mTempArray.indices) {
                        if (mSliderTopY > mTempSlotsMinYArray[i] && mSliderTopY < mTempSlotsMaxYArray[i]) {
                            mDesiredTemp = mTempArray[i]
                            break
                        }
                    }
                }

                binding.temperatureTxtSet.text = convertTemperature(mDesiredTemp.toString(), binding.scDegree.isChecked)

                mSliderTopY = newTopY
                binding.viewSlider.y = mSliderTopY
            }
            if (event.action == MotionEvent.ACTION_UP) {
                if (mIsSliderTouched) {
                    setDesiredTemp(mDesiredTemp)
                    mIsSliderTouched = false
                }
            }
            true
        }

        binding.temperatureProgressBar.visibility = View.VISIBLE

    }

    private fun updateTempData(lowRangeLow: Int, lowRangeHigh: Int, highRangeLow: Int, highRangeHigh: Int) {
        mSliderHeight = binding.viewSlider.height.toFloat()
        mCurrTempMarkerHeight = binding.viewSliderCurrentMarker.height.toFloat()
        mSliderWidth = binding.viewSlider.width.toFloat()
        mMinY = binding.viewMeter.y - (mSliderHeight / 2)
        mMaxY = binding.viewMeter.y + binding.viewMeter.height - (mSliderHeight / 2)
        mMeterRange = if (!binding.scDegree.isChecked)
            (highRangeHigh - lowRangeLow + 1)
        else
            (highRangeHigh - lowRangeLow + 7)
        mYRange = mMaxY - mMinY

        /**
         * @author Tijo Thomas
         * @since 18-03-2021
         * @description making the slots array to same value as in fahrenheit section for celsius also
         * */
        val tempMeterRange = highRangeHigh - lowRangeLow
        mUnitInY = mYRange / tempMeterRange

        if (binding.lowHighSwitch.isChecked) {
            mRangeMinTemp = highRangeLow
            mRangeMaxTemp = highRangeHigh
            mCurrRangeMaxY = mMinY + (highRangeHigh - mRangeMinTemp) * mUnitInY
            mCurrRangeMinY = mMinY
        } else {
            mRangeMinTemp = lowRangeLow
            mRangeMaxTemp = lowRangeHigh
            mCurrRangeMaxY = mMaxY
            mCurrRangeMinY = mMinY + (highRangeHigh - mRangeMaxTemp) * mUnitInY
        }
        /**
         * @author Tijo Thomas
         * @since 18-03-2021
         * @description changing value back to original unit value to work with celsius
         * */
        mUnitInY = mYRange / mMeterRange
        Log.e(TAG, "updateTempData: $mCurrRangeMaxY $mCurrRangeMinY ${binding.viewMeter.height} $mUnitInY $mMinY $mMaxY")
        mTempArray.clear()
        mTempSlotsMaxYArray.clear()
        mTempSlotsMinYArray.clear()

        for (i in 0 until mMeterRange) {
            if (!binding.scDegree.isChecked) {
                mTempSlotsMinYArray.add(mMaxY - mUnitInY * i - mUnitInY)
                mTempSlotsMaxYArray.add(mMaxY - mUnitInY * i)
                mTempArray.add((lowRangeLow + i).toFloat())
            } else {
                mTempSlotsMinYArray.add(mMaxY - mUnitInY * (i.toFloat()) - mUnitInY)
                mTempSlotsMaxYArray.add(mMaxY - mUnitInY * (i.toFloat()))
                val q = (0.9 * i)
                mTempArray.add((lowRangeLow + q).toFloat())
            }

            Log.i(TAG, "mTempSlotsMinYArray[$i] = ${mTempSlotsMinYArray[i]}")
            Log.i(TAG, "mTempSlotsMaxYArray[$i] = ${mTempSlotsMaxYArray[i]}")
            Log.i(TAG, "mTempArray[$i] = ${mTempArray[i]}")

        }
    }

    private fun updateTempUI(isCelsius: Boolean) {
        binding.tvLowerLimit.text = convertTemperature(currentSpaState.setupParams.lowRangeLow.toString(), isCelsius)
        binding.tvUpperLimit.text = convertTemperature(currentSpaState.setupParams.highRangeHigh.toString(), isCelsius)

        binding.temperatureTxtCurrent.text = currentSpaState.currentTemp?.let { convertTemperature(it, isCelsius) }
        binding.temperatureTxtSet.text = currentSpaState.desiredTemp?.let { convertTemperature(it, isCelsius) }
        updateSetTempPosition(currentSpaState.desiredTemp?.toFloat())
        updateCurrTempPosition(currentSpaState.currentTemp?.toFloat())
    }

    private fun updateSetTempPosition(temp: Float?) {
        for (i in 0 until mMeterRange) {
            if (temp?.roundToInt() == mTempArray[i].roundToInt()) {
                binding.viewSlider.y = mTempSlotsMinYArray[i] + mUnitInY / 2
            }
        }
    }

    private fun updateCurrTempPosition(temp: Float?) {
        Log.i(TAG, "updateCurrTempPosition ++++")
        Log.i(TAG, "binding.viewSlider.height = ${binding.viewSlider.height}")
        Log.i(TAG, "binding.viewSliderCurrentMarker.height = ${binding.viewSliderCurrentMarker.height}")
        for (i in 0 until mMeterRange) {
            if (temp == mTempArray[i]) {
                binding.viewSliderCurrentMarker.y = mTempSlotsMinYArray[i] + (mUnitInY / 2) - (mSliderHeight / 2) + (mCurrTempMarkerHeight / 2)
                binding.temperatureLayoutCurrent.y = mTempSlotsMinYArray[i] + mUnitInY / 2
            }
        }
    }

    private fun handleHeaterModeToggleData() {
        needToUpdateAgain()
    }

    private fun needToUpdateAgain() {
        Handler(Looper.getMainLooper()).postDelayed({
            isTempChanged = true
        }, 7000)
    }

    private fun handleLowHighMode() {
        val highRangeLow = baseCzDetails?.currentState?.setupParams?.highRangeLow
        val lowRangeHigh = baseCzDetails?.currentState?.setupParams?.lowRangeHigh
        val lowRangeLow = baseCzDetails?.currentState?.setupParams?.lowRangeLow
        val highRangeHigh = baseCzDetails?.currentState?.setupParams?.highRangeHigh
        lowRangeLow?.let { lowRangeHigh?.let { it1 -> highRangeLow?.let { it2 -> highRangeHigh?.let { it3 -> updateTempData(it, it1, it2, it3) } } } }
        updateTempUI(binding.scDegree.isChecked)
        needToUpdateAgain()
    }

    private fun handleDegreeData() {
        isFahrenheit = binding.scDegree.isChecked
        updateDegree(binding.scDegree.isChecked)
        needToUpdateAgain()
    }

    private fun updateDegree(flag: Boolean) {
        if (flag) {
            binding.tvLowerLimit.text = convertTemperature(currentSpaState.setupParams.lowRangeLow.toString(), binding.scDegree.isChecked)
            binding.tvUpperLimit.text = convertTemperature(currentSpaState.setupParams.highRangeHigh.toString(), binding.scDegree.isChecked)

            binding.temperatureTxtCurrent.text = currentSpaState.currentTemp?.let { convertTemperature(it, binding.scDegree.isChecked) }
            binding.temperatureTxtSet.text = currentSpaState.desiredTemp?.let { convertTemperature(it, binding.scDegree.isChecked) }
            updateSetTempPosition(currentSpaState.desiredTemp?.toFloat())
            updateCurrTempPosition(currentSpaState.currentTemp?.toFloat())

            if (!isSpaOnline) {
                binding.temperatureDescription.visibility = View.VISIBLE
                binding.temperatureDescription.text = getString(R.string.your_spa_is_offline)
                binding.visible = false
            }
        }
    }

    private fun showAccountInfo() {
        val apiService = ApiClient.getClient()?.create(ApiInterface::class.java)
        val whoAmI = apiService?.getWhoAmI(BEARER + " " + TokenManager.accessToken)
        whoAmI?.enqueue(object : Callback<AccountData?> {
            override fun onResponse(call: Call<AccountData?>, response: Response<AccountData?>) {
                accountData = response.body()
            }

            override fun onFailure(call: Call<AccountData?>, t: Throwable) {

            }
        })
    }

    private fun setDesiredTemp(temp: Float) {
        Log.d(TAG, "SetDesiredTemp: $temp")
        isTempChanged = false
        SpaManager.requestSetDesiredTemp(context, temp, mSpaId, object : RestAPICallback {
            @SuppressLint("SetTextI18n")
            override fun onSuccess(result: String) {
                binding.temperatureProgressBar.visibility = View.GONE
                Log.d(TAG, "SetDesiredTemp: Successful")
                needToUpdateAgain()
            }

            override fun onFailure(result: String) {
                binding.temperatureProgressBar.visibility = View.GONE
                needToUpdateAgain()
            }
        })
    }

}