package com.controlmyspa.ownerappnew.fragment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.SchedulePresetActivity
import com.controlmyspa.ownerappnew.adapter.TimezoneListAdapter
import com.controlmyspa.ownerappnew.databinding.DialogTimezoneBinding
import com.controlmyspa.ownerappnew.helper.TimezoneDataListener
import java.util.*
import kotlin.collections.ArrayList

class TimezoneDialogFragment
    : DialogFragment() {
    private lateinit var mListener: TimezoneDataListener
    private lateinit var binding: DialogTimezoneBinding
    private val mAdapter by lazy {
        TimezoneListAdapter(TimeZone.getAvailableIDs(),mListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_timezone, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
        binding.mClosePopUp.setOnClickListener {
            dismiss()
        }

        binding.mTimeZoneListView.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }

        binding.mTimeZoneListView.adapter = mAdapter
        binding.mSearchView.addTextChangedListener(textWatcher)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(searchedItem: CharSequence?, start: Int, before: Int, count: Int) {
            filterList(searchedItem)
        }
    }

    private fun filterList(searchedItem: CharSequence?) {
        val availableIDs = TimeZone.getAvailableIDs()
        val mList: MutableList<String> = ArrayList()
        for (i in availableIDs.indices) {
            if (availableIDs[i].contains(searchedItem.toString(), true)) {
                mList.add(availableIDs[i])
            }
        }
        mAdapter.filterData(mList.toTypedArray())
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SchedulePresetActivity) {
            mListener = context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement TimezoneDataListener")
        }
    }
}