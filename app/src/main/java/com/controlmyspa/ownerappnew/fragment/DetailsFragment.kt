package com.controlmyspa.ownerappnew.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.text.HtmlCompat
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.helper.BUNDLE_EMAIL_SETUP
import com.controlmyspa.ownerappnew.helper.baseCzDetails
import com.controlmyspa.ownerappnew.helper.convertTemperature
import com.controlmyspa.ownerappnew.helper.requestServiceMail
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.model.MailData
import com.controlmyspa.ownerappnew.service.SpaManager
import org.json.JSONObject
import java.util.*

class DetailsFragment : AccountFragment() {
    private var mailData: MailData? = null
    private var dealerEmail = ""
    override fun requestService() {
        getDealerData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseCzDetails?.let { showData(it) }
    }

    private fun showData(czDetails: CzDetails) {
        try {
            binding.data = czDetails
            val isCelsius = czDetails.currentState?.celsius?:false
            binding.currentTemp.text = czDetails.currentState?.currentTemp?.let { convertTemperature(it, isCelsius) }
            binding.setTemp.text = czDetails.currentState?.desiredTemp?.let { convertTemperature(it, isCelsius) }
            binding.sensorATemp.text = czDetails.currentState?.sensorATemp?.let { convertTemperature(it, isCelsius) }
            binding.sensorBTemp.text = czDetails.currentState?.sensorBTemp?.let { convertTemperature(it, isCelsius) }
            czDetails.currentState?.components?.let { updateFilterValues(it) }
            val dipSwitches = czDetails.currentState?.systemInfo?.dipSwitches
            var dipSwitchSettingsNumber = ""
            for (k in 0 until (dipSwitches?.count() ?: 0)) {
                dipSwitchSettingsNumber = if (dipSwitches?.get(k)?.on!!) {
                    dipSwitchSettingsNumber + "1"
                } else {
                    dipSwitchSettingsNumber + "0"
                }
            }
            binding.dipSwitchSettings.text = dipSwitchSettingsNumber

            val serialNumber = binding.spaSerialNumber.text.toString()
            val currentTemperature = binding.currentTemp.text.toString()
            val spaControllerPart = binding.spaControllerPart.text.toString()
            val setTemp = binding.setTemp.text.toString()
            val spaControllerSoftwareVersion = binding.spaControllerSfVersion.text.toString()
            val sensorATemp = binding.sensorATemp.text.toString()
            val cmsAccountSetupDate = binding.cmsAccountSetupDate.text.toString()
            val sensorBTemp = binding.sensorBTemp.text.toString()
            val runMode = binding.spaRunMode.text.toString()
            val dipSwitchSettings = binding.dipSwitchSettings.text.toString()
            val highLowRangeStatus = binding.highLowStatus.text.toString()
            val currentSetup = binding.currentSetup.text.toString()
            val filterCycle = binding.filterCycle.text.toString()
            val lastFaultLogEntry = binding.lastFaultLogEntry.text.toString()
            val firstName = binding.fragAcc.accountFirstName.text.toString()
            val lastName = binding.accountLastName.text.toString()
            val email = binding.fragAcc.accountEmail.text.toString()
            val phNo = binding.fragAcc.accountPhone.text.toString()
            val address1 = binding.fragAcc.accountAddress1.text.toString()
            val address2 = binding.fragAcc.accountAddress2.text.toString()
            val city = binding.fragAcc.accountCity.text.toString()
            val state = binding.fragAcc.accountState.text.toString()
            val zipCode = binding.fragAcc.accountZipcode.text.toString()
            val country = binding.fragAcc.accountCountry.text.toString()

            mailData = MailData(serialNumber, currentTemperature, spaControllerPart, setTemp, spaControllerSoftwareVersion, sensorATemp, cmsAccountSetupDate,
                    sensorBTemp, runMode, dipSwitchSettings, highLowRangeStatus, currentSetup
                    , filterCycle, lastFaultLogEntry, firstName, lastName, email, phNo, address1, address2, city, state, zipCode, country)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onFailedUserData(result: String) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    override fun onSuccessUserData(result: String) {
        val jsonObject = JSONObject(result)
        dealerEmail = jsonObject.getString("email")
        val mData = "<b>${getString(R.string.first_name)}</b> : ${mailData?.firstName}<br>" +
                "<b>${getString(R.string.last_name)}</b> : ${mailData?.lastName}<br>" +
                "<b>${getString(R.string.email)}</b> : ${mailData?.email}<br>" +
                "<b>${getString(R.string.phone_number)}</b> : ${mailData?.phNo}<br>" +
                "<b>${getString(R.string.address1)}</b> : ${mailData?.address1}<br>" +
                "<b>${getString(R.string.address2)}</b> : ${mailData?.address2}<br>" +
                "<b>${getString(R.string.city)}</b> : ${mailData?.city}<br>" +
                "<b>${getString(R.string.state)}</b> : ${mailData?.state}<br>" +
                "<b>${getString(R.string.zip_code)}</b> : ${mailData?.zipCode}<br>" +
                "<b>${getString(R.string.country)}</b> : ${mailData?.country}<br>" +
                "<b>${getString(R.string.serial_number)}</b> : ${mailData?.serialNumber}<br>" +
                "<b>${getString(R.string.current_temp)}</b> : ${mailData?.currentTemp}<br>" +
               /* "<b>${getString(R.string.spa_controller_part)}</b> : ${mailData?.spaControllerPart}<br>" +*/
                "<b>${getString(R.string.set_temp)}</b> : ${mailData?.setTemp}<br>" +
                "<b>${getString(R.string.spa_controller_software_version)}</b> : ${mailData?.spaControllerSoftwareVersion}<br>" +
                /*"<b>${getString(R.string.sensor_a_temp)}</b> : ${mailData?.sensorATemp}<br>" +*/
                "<b>${getString(R.string.cms_account_setup_date)}</b> : ${mailData?.cmsAccountSetupDate}<br>" +
               /* "<b>${getString(R.string.sensor_b_temp)}</b> : ${mailData?.sensorBTemp}<br>" +*/
                "<b>${getString(R.string.run_mode)}</b> : ${mailData?.runMode}<br>" +
                "<b>${getString(R.string.dipswitch_settings)}</b> : ${mailData?.dipSwitchSettings}<br>" +
                "<b>${getString(R.string.high_low_range_status)}</b> : ${mailData?.highLowRangeStatus}<br>" +
                "<b>${getString(R.string.current_setup)}</b> : ${mailData?.currentSetup}<br>" +
                "<b>${getString(R.string.filter_cycle)}</b> : ${mailData?.filterCycle}<br>" +
                "<b>${getString(R.string.last_fault_log_entry)}</b> : ${mailData?.lastFaultLogEntry}<br>"

        activity?.let {
            requestServiceMail(it, dealerEmail, HtmlCompat.fromHtml(mData, HtmlCompat.FROM_HTML_MODE_LEGACY))
        }
    }

    override fun handleResults(czDetails: CzDetails) {
        baseCzDetails = czDetails
        baseCzDetails?.let { showData(it) }
    }

    override fun handleErrorThrown(throwable: Throwable) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    companion object {
        @JvmStatic
        fun newInstance(): DetailsFragment {
            return DetailsFragment()
        }

        @JvmStatic
        fun newInstance(isToBeDone: Boolean): DetailsFragment {
            val frag = DetailsFragment()
            val bundle = Bundle()
            bundle.putBoolean(BUNDLE_EMAIL_SETUP, isToBeDone)
            frag.arguments = bundle
            return frag
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateFilterValues(components: MutableList<Components>) {
        val spaComponents: MutableList<Components> = ArrayList()
        for (i in components.indices) {
            if (components[i].componentType == "FILTER") {
                spaComponents.add(components[i])
            }
        }
        if (spaComponents[0].componentType == "FILTER" && spaComponents[1].componentType == "FILTER") {
            if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                binding.filterCycle.text = """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.off)}
                    """.trimIndent()
            } else if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                binding.filterCycle.text = """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.on)}
                    """.trimIndent()
            } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                binding.filterCycle.text = """
                    F1 : ${getString(R.string.on)} , F2 : ${getString(R.string.off)}
                    """.trimIndent()
            } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                binding.filterCycle.text = """
                    F1 : ${getString(R.string.on)} , F2 : ${getString(R.string.on)}
                    """.trimIndent()
            } else {
                binding.filterCycle.text = """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.disabled)}
                    """.trimIndent()
            }
        }
    }
}