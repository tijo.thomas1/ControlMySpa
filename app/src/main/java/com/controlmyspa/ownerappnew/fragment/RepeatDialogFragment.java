package com.controlmyspa.ownerappnew.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.FragmentRepeatDialogBinding;
import com.controlmyspa.ownerappnew.materialdatetimepicker.date.DatePickerDialog;
import com.controlmyspa.ownerappnew.model.SpaPreset;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RepeatDialogFragment extends DialogFragment {
    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    private static final String ARG_PARAM1 = "start_date";
    private static final String ARG_PARAM2 = "end_date";
    private static final String ARG_PARAM3 = "repeat_mode";

    private Date mStartDate;
    private Date mEndDate;
    private String mRepeatMode;
    private OnRepeatModeListener mListener;
    private FragmentRepeatDialogBinding binding;

    public RepeatDialogFragment() {
        // Required empty public constructor
    }


    public static RepeatDialogFragment newInstance(String param1, String param2, String param3) {
        RepeatDialogFragment fragment = new RepeatDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String strStartDate = getArguments().getString(ARG_PARAM1);
            String strEndDate = getArguments().getString(ARG_PARAM2);
            try {
                if (!TextUtils.isEmpty(strStartDate)) {
                    mStartDate = sdf.parse(strStartDate);
                }
                if (!TextUtils.isEmpty(strEndDate)) {
                    mEndDate = sdf.parse(strEndDate);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mRepeatMode = getArguments().getString(ARG_PARAM3);
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_repeat_dialog, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.array_repeat, R.layout.spinner_item_white);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.repeatSpinner.setAdapter(adapter);
        //spinner initial selection
        if (mRepeatMode != null) {
            switch (mRepeatMode) {
                case SpaPreset.RepeatMode.DAILY:
                    binding.repeatSpinner.setSelection(0);
                    break;
                case SpaPreset.RepeatMode.WEEKLY:
                    binding.repeatSpinner.setSelection(1);
                    break;
             /*   case SpaPreset.RepeatMode.MONTHLY:
                    binding.repeatSpinner.setSelection(2);
                    break;*/
                case SpaPreset.RepeatMode.NONE:
                    binding.repeatSpinner.setSelection(2);
                    break;
            }
        }

        if (mStartDate != null) {
            binding.repeatTextStart.setText(sdf.format(mStartDate));
        }
        if (mEndDate != null) {
            binding.repeatTextEnd.setText(sdf.format(mEndDate));
        }

        binding.repeatTextStart.setOnClickListener(v -> {
            final Calendar calendar = Calendar.getInstance();
            if (mStartDate != null) {
                calendar.setTime(mStartDate);
            }


            DatePickerDialog mDatePicker = DatePickerDialog.newInstance((view13, year, monthOfYear, dayOfMonth) -> {
                calendar.set(year, monthOfYear, dayOfMonth);
                mStartDate = calendar.getTime();
                binding.repeatTextStart.setText(sdf.format(mStartDate));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            mDatePicker.setAccentColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            mDatePicker.setTitle(getString(R.string.select_a_starting_date));
            mDatePicker.show(getChildFragmentManager(), "DatePickerDialog");

        });
        binding.repeatTextEnd.setOnClickListener(v -> {
            final Calendar calendar = Calendar.getInstance();
            if (mEndDate != null) {
                calendar.setTime(mEndDate);
            }

            DatePickerDialog mDatePicker = DatePickerDialog.newInstance((view13, year, monthOfYear, dayOfMonth) -> {
                calendar.set(year, monthOfYear, dayOfMonth);
                mEndDate = calendar.getTime();
                binding.repeatTextEnd.setText(sdf.format(mEndDate));

            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            mDatePicker.setAccentColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            mDatePicker.setTitle(getString(R.string.select_an_ending_date));
            mDatePicker.show(getChildFragmentManager(), "DatePickerDialog");
        });
        binding.repeatBtnCancel.setOnClickListener(v -> dismiss());
        binding.repeatBtnOk.setOnClickListener(v -> {
            String strStartDate = "";
            String strEndDate = "";
            if (mStartDate != null) {
                strStartDate = sdf.format(mStartDate);
            }
            if (mEndDate != null) {
                strEndDate = sdf.format(mEndDate);
            }
            if (mListener != null) {
                String strRepeat = binding.repeatSpinner.getSelectedItem().toString();
                mListener.onRepeatModeChange(strStartDate, strEndDate, strRepeat);
                dismiss();
            }
        });

    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRepeatModeListener) {
            mListener = (OnRepeatModeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRepeatModeListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRepeatModeListener {
        void onRepeatModeChange(String startDate, String endDate, String repeatMode);
    }
}
