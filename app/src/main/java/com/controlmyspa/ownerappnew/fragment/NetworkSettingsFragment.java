package com.controlmyspa.ownerappnew.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.FragmentNetworkSettingsBinding;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;


public class NetworkSettingsFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "NetworkSettings";
    private Boolean isConnected = false;
    private FragmentNetworkSettingsBinding binding;

    public NetworkSettingsFragment() {
        // Required empty public constructor
    }

    public static NetworkSettingsFragment newInstance() {
        return new NetworkSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment'
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_network_settings, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        binding.networkProgressBar.setIndeterminate(true);
        binding.networkProgressBar.setVisibility(View.GONE);

        binding.networkBtnTestconnection.setOnClickListener(v -> _getNetworkSettings());
        binding.networkBtnSave.setOnClickListener(v -> {
            //Instead of enable, disable of save button.
            if (!isConnected) {
                Toast.makeText(NetworkSettingsFragment.this.getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }
            _putNetworkSettings();

        });
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.array_config, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.spinnerIpv4.setAdapter(adapter);
        binding.spinnerIpv4.setOnItemSelectedListener(this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // An item was selected. You can retrieve the selected item using
        String str = (String) parent.getItemAtPosition(position);
        Log.d(TAG, str);
        if (str.equals("DHCP")) {
            binding.networkTxtIpaddress.setEnabled(false);
            binding.networkTxtSubnet.setEnabled(false);
            binding.networkTxtGateway.setEnabled(false);
        } else {
            binding.networkTxtIpaddress.setEnabled(true);
            binding.networkTxtSubnet.setEnabled(true);
            binding.networkTxtGateway.setEnabled(true);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void _getNetworkSettings() {
        binding.networkProgressBar.setVisibility(View.VISIBLE);
        NetworkManager.requestGetNetworkSettings(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.networkProgressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.success), Toast.LENGTH_LONG).show();
                isConnected = true;
                //binding.networkTxtSsid.setText(NetworkManager.m_SSID);
                binding.networkTxtIpaddress.setText(NetworkManager.m_IPAddress);
                binding.networkTxtSubnet.setText(NetworkManager.m_SubnetMask);
                binding.networkTxtGateway.setText(NetworkManager.m_Gateway);
                if (NetworkManager.m_DHCP) {
                    binding.spinnerIpv4.setSelection(1); //DHCP
                    binding.networkTxtIpaddress.setEnabled(false);
                    binding.networkTxtSubnet.setEnabled(false);
                    binding.networkTxtGateway.setEnabled(false);
                } else {
                    binding.spinnerIpv4.setSelection(0); //Manual
                    binding.networkTxtIpaddress.setEnabled(true);
                    binding.networkTxtSubnet.setEnabled(true);
                    binding.networkTxtGateway.setEnabled(true);
                }
            }

            @Override
            public void onFailure(String result) {

                binding.networkProgressBar.setVisibility(View.GONE);
                isConnected = false;
                if (getActivity() != null)
                    Toast.makeText(getActivity(), getString(R.string.please_follow_the_instructions_and_try_again),
                            Toast.LENGTH_LONG).show();
            }
        });
    }

    private void _putNetworkSettings() {

        String strIP = binding.networkTxtIpaddress.getText().toString();
        String strSubnetMask = binding.networkTxtSubnet.getText().toString();
        String strGateway = binding.networkTxtGateway.getText().toString();
        boolean isDHCP = (binding.spinnerIpv4.getSelectedItemPosition() == 1);
        if (!isDHCP) {
            if (!Utility.isValidIP(strIP)) {
                Toast.makeText(NetworkSettingsFragment.this.getContext(), getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strSubnetMask)) {
                Toast.makeText(NetworkSettingsFragment.this.getContext(), getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strGateway)) {
                Toast.makeText(NetworkSettingsFragment.this.getContext(), getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
        }

        NetworkManager.m_SSID = binding.networkTxtSsid.getText().toString();
        NetworkManager.m_Password = binding.networkTxtPassword.getText().toString();
        NetworkManager.m_IPAddress = strIP;
        NetworkManager.m_SubnetMask = strSubnetMask;
        NetworkManager.m_Gateway = strGateway;
        NetworkManager.m_DHCP = (binding.spinnerIpv4.getSelectedItemPosition() == 0);

        binding.networkProgressBar.setVisibility(View.VISIBLE);
        NetworkManager.requestPutNetworkSettings(NetworkSettingsFragment.this.getContext(), new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.networkProgressBar.setVisibility(View.GONE);
                Toast.makeText(NetworkSettingsFragment.this.getContext(), R.string.success_save, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String result) {
                binding.networkProgressBar.setVisibility(View.GONE);
                Toast.makeText(NetworkSettingsFragment.this.getContext(), R.string.error_save, Toast.LENGTH_LONG).show();
            }
        });

    }
}
