package com.controlmyspa.ownerappnew.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.text.HtmlCompat
import com.controlmyspa.ownerappnew.MainActivity
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.base.BaseFragment
import com.controlmyspa.ownerappnew.databinding.FragmentContactDealerBinding
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.model.AccountData
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.service.SpaManager
import org.json.JSONException
import org.json.JSONObject
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import java.util.*

@RuntimePermissions
class ContactDealerFragment : BaseFragment<FragmentContactDealerBinding>(), View.OnClickListener {
    private lateinit var swipeListener: SwipeListener
    override fun getFragmentView(): Int = R.layout.fragment_contact_dealer
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListener()
        getDealerData()
        binding.visible = true
    }

    private fun initClickListener() {
        binding.btnCallDealer.setOnClickListener(this)
        binding.btnEmailDealer.setOnClickListener(this)
    }

    override fun handleResults(czDetails: CzDetails) {

    }

    override fun handleError(throwable: Throwable) {
        SpaManager.authFailRedirectToRefreshAppData(requireContext())
    }

    override fun onSuccessUserData(result: String) {
        binding.visible = false
        showDealerInfo(result)
    }

    override fun onFailedUserData(result: String) {
        binding.visible = false
        Toast.makeText(context, R.string.error_update, Toast.LENGTH_SHORT).show()
    }

    private fun showDealerInfo(result: String) {
        try {
            val jsonObject = JSONObject(result)
            binding.dealerName.setText(jsonObject.getString("name"))
            binding.dealerLastname.setText("")
            binding.dealerEmail.text = jsonObject.getString("email")
            binding.dealerEmail.isSelected = true
            binding.dealerEmail.ellipsize = TextUtils.TruncateAt.MARQUEE
            binding.dealerEmail.isSingleLine = true
            binding.dealerPhone.setText(jsonObject.getString("phone"))
            val jsonObjAddress = jsonObject.getJSONObject("address")
            binding.dealerAddress1.setText(jsonObjAddress.getString("address1"))
            binding.dealerAddress2.setText(jsonObjAddress.getString("address2"))
            binding.dealerCity.setText(jsonObjAddress.getString("city"))
            binding.dealerState.setText(jsonObjAddress.getString("state"))
            binding.dealerZipcode.setText(jsonObjAddress.getString("zip"))
            binding.dealerCountry.setText(jsonObjAddress.getString("country"))
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    fun callDealer() {
        val phNo = binding.dealerPhone.text.toString()
        if (phNo.isNotEmpty()) {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:" + phNo.trim { it <= ' ' }.replace("-", ""))
            startActivity(callIntent)
        } else {
            Toast.makeText(activity, getString(R.string.phone_number_is_not_valid_or_unavailable),
                    Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        swipeListener = activity as MainActivity
    }

    private fun emailDealer() {
        baseCzDetails?.let { czDetails ->
            accountData?.let { accountData ->
                requestMail(czDetails, accountData)
            }
        }
        //swipeListener.requestServiceStarted()
    }

    private fun requestMail(czDetails: CzDetails, accountData: AccountData) {
        //val spaControllerPart = czDetails.serialNumber.subSequence(0, 8)
        val isCelsius = czDetails.currentState?.celsius?:false
        val currentTemp = czDetails.currentState?.currentTemp?.let { convertTemperature(it, isCelsius) }
        val setTemp = czDetails.currentState?.desiredTemp?.let { convertTemperature(it, isCelsius) }
        /*val sensorATemp = czDetails.currentState.sensorATemp?.let { convertTemperature(it, isCelsius) }
        val sensorBTemp = czDetails.currentState.sensorBTemp?.let { convertTemperature(it, isCelsius) }*/
        val lastFaultLogEntry = if (!czDetails.alerts.isNullOrEmpty()) czDetails.alerts[czDetails.alerts.count() - 1].shortDescription else ""
        val spaControllerSoftwareVersion = "${czDetails.currentState?.systemInfo?.controllerSoftwareVersion}"
        val dipSwitches = czDetails.currentState?.systemInfo?.dipSwitches
        var dipSwitchSettingsNumber = ""
        for (k in 0 until (dipSwitches?.count() ?: 0)) {
            dipSwitchSettingsNumber = if (dipSwitches?.get(k)?.on!!) {
                dipSwitchSettingsNumber + "1"
            } else {
                dipSwitchSettingsNumber + "0"
            }
        }

        val mData = "<b>${getString(R.string.first_name)}</b> : ${accountData.firstName?:""}<br>" +
                "<b>${getString(R.string.last_name)}</b> : ${accountData.lastName?:""}<br>" +
                "<b>${getString(R.string.email)}</b> : ${accountData.email?:""}<br>" +
                "<b>${getString(R.string.phone_number)}</b> : ${accountData.phone?:""}<br>" +
                "<b>${getString(R.string.address1)}</b> : ${accountData.address.address1?:""}<br>" +
                "<b>${getString(R.string.address2)}</b> : ${accountData.address.address2?:""}<br>" +
                "<b>${getString(R.string.city)}</b> : ${accountData.address.city?:""}<br>" +
                "<b>${getString(R.string.state)}</b> : ${accountData.address.state?:""}<br>" +
                "<b>${getString(R.string.zip_code)}</b> : ${accountData.address.zip?:""}<br>" +
                "<b>${getString(R.string.country)}</b> : ${accountData.address.country?:""}<br>" +
                "<b>${getString(R.string.serial_number)}</b> : ${czDetails.serialNumber}<br>" +
                "<b>${getString(R.string.current_temp)}</b> : ${currentTemp?:""}<br>" +
                /* "<b>${getString(R.string.spa_controller_part)}</b> : ${spaControllerPart}<br>" +*/
                "<b>${getString(R.string.set_temp)}</b> : ${setTemp?:""}<br>" +
                "<b>${getString(R.string.spa_controller_software_version)}</b> : ${spaControllerSoftwareVersion}<br>" +
                /* "<b>${getString(R.string.sensor_a_temp)}</b> : ${sensorATemp}<br>" +*/
                "<b>${getString(R.string.cms_account_setup_date)}</b> : ${formatDate(czDetails.salesDate)}<br>" +
                /* "<b>${getString(R.string.sensor_b_temp)}</b> : ${sensorBTemp}<br>" +*/
                "<b>${getString(R.string.run_mode)}</b> : ${czDetails.currentState?.runMode}<br>" +
                "<b>${getString(R.string.dipswitch_settings)}</b> : ${dipSwitchSettingsNumber}<br>" +
                "<b>${getString(R.string.high_low_range_status)}</b> : ${czDetails.currentState?.tempRange}<br>" +
                "<b>${getString(R.string.current_setup)}</b> : ${czDetails.currentState?.systemInfo?.currentSetup}<br>" +
                "<b>${getString(R.string.filter_cycle)}</b> : ${czDetails.currentState?.components?.let { updateFilterValues(it) }}<br>" +
                "<b>${getString(R.string.last_fault_log_entry)}</b> : ${lastFaultLogEntry}<br>"


        activity?.let { requestServiceMail(it, binding.dealerEmail.text.toString(), HtmlCompat.fromHtml(mData, HtmlCompat.FROM_HTML_MODE_LEGACY)) }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @SuppressLint("SetTextI18n")
    private fun updateFilterValues(components: MutableList<Components>): String {
        val spaComponents: MutableList<Components> = ArrayList()
        for (i in components.indices) {
            if (components[i].componentType == "FILTER") {
                spaComponents.add(components[i])
            }
        }
        if (spaComponents.isNotEmpty())
            if (spaComponents[0].componentType == "FILTER" && spaComponents[1].componentType == "FILTER") {
                if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                    return """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.off)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("OFF", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                    return """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.on)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("OFF", ignoreCase = true)) {
                    return """
                    F1 : ${getString(R.string.on)} , F2 : ${getString(R.string.off)}
                    """.trimIndent()
                } else if (spaComponents[0].value.equals("ON", ignoreCase = true) && spaComponents[1].value.equals("ON", ignoreCase = true)) {
                    return """
                    F1 : ${getString(R.string.on)} , F2 : ${getString(R.string.on)}
                    """.trimIndent()
                } else {
                    return """
                    F1 : ${getString(R.string.off)} , F2 : ${getString(R.string.disabled)}
                    """.trimIndent()
                }
            }

        return ""
    }


    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    fun callPermissionDenied() {
        Toast.makeText(activity, R.string.please_allow_to_make_this_call,
                Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Permission denied.")
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCallDealer -> callDealerWithPermissionCheck()
            R.id.btnEmailDealer -> emailDealer()
            else -> {
            }
        }
    }

    companion object {
        private const val TAG = "ContactDealerFragment"

        @JvmStatic
        fun newInstance(): ContactDealerFragment {
            return ContactDealerFragment()
        }
    }
}