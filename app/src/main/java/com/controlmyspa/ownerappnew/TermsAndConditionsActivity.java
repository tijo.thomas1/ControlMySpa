package com.controlmyspa.ownerappnew;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.databinding.ActivityTermsAndConditionsBinding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.service.UserManager;


public class TermsAndConditionsActivity extends AppCompatActivity {
    private static final String TAG = "TermsAndConditions";


    private ActivityTermsAndConditionsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_and_conditions);
        binding.activityTacProgressBar.setIndeterminate(true);
        binding.activityTacProgressBar.setVisibility(View.VISIBLE);
        UserManager.findMostRecentTAC(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.activityTacProgressBar.setVisibility(View.GONE);
                binding.activityTacTxttac.setText(HtmlCompat.fromHtml(result, HtmlCompat.FROM_HTML_MODE_LEGACY));
            }

            @Override
            public void onFailure(String result) {
                binding.activityTacProgressBar.setVisibility(View.GONE);
                binding.activityTacTxttac.setText(R.string.error_tac);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    public void onClickAgree(View view) {
        binding.activityTacProgressBar.setVisibility(View.VISIBLE);
        UserManager.agreeTAC(this, new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.activityTacProgressBar.setVisibility(View.GONE);
                Intent intent = new Intent(TermsAndConditionsActivity.this,
                        MainActivity.class);
                startActivity(intent);
                TermsAndConditionsActivity.this.finish();
            }

            @Override
            public void onFailure(String result) {
                Log.d(TAG, result);
                binding.activityTacProgressBar.setVisibility(View.GONE);
                Toast.makeText(TermsAndConditionsActivity.this, R.string.error_later, Toast.LENGTH_SHORT).show();
            }
        });

    }


}
