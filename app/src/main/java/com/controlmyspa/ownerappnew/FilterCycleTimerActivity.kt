package com.controlmyspa.ownerappnew

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.databinding.RowWifiListBinding
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.Utility

class FilterCycleTimerActivity : AppCompatActivity() {
    private var timerArray: List<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_cycle_timer)
        initControls()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    private fun initControls() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val tvHeader = findViewById<TextView>(R.id.tvHeader)
        tvHeader.text = resources.getString(R.string.choose_timer)
        // tvHeader.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        val lvChooseTimer: RecyclerView = findViewById(R.id.lvChooseTimer)
        lvChooseTimer.layoutManager = LinearLayoutManager(this)
        timerArray = mutableListOf(*resources.getStringArray(R.array.array_filter_cycle))
        val onItemClickSupport = object : OnItemClickSupport {
            override fun onItemClicked(position: Int, time: String) {
                val intent = Intent()
                intent.putExtra("timerPosition", position + 1)
                intent.putExtra("timer", time)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
        val timerAdapter = TimerAdapter(onItemClickSupport)
        lvChooseTimer.adapter = timerAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private interface OnItemClickSupport {
        fun onItemClicked(position: Int, time: String)
    }

    private inner class TimerAdapter(private val onItemClickSupport: OnItemClickSupport) : RecyclerView.Adapter<TimerAdapter.MyViewHolder>() {
        inner class MyViewHolder(val mViewBinding: RowWifiListBinding) : RecyclerView.ViewHolder(mViewBinding.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val mViewBinding: RowWifiListBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_wifi_list, parent, false)
            return MyViewHolder(mViewBinding)
        }

        override fun getItemCount(): Int = timerArray.count()

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.mViewBinding.mData = timerArray[position]
            holder.itemView.setOnClickListener {
                onItemClickSupport.onItemClicked(holder.adapterPosition, timerArray[holder.adapterPosition])
            }
        }
    }
}