package com.controlmyspa.ownerappnew.service;

import android.util.Log;

import androidx.annotation.Nullable;

import com.controlmyspa.ownerappnew.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.KeyStore;

import cz.msebera.android.httpclient.Header;

public class TokenManager {
    private static final String TAG = "TokenManager";
    public static String accessToken = "";
    //private static String refreshToken = "";
    public static String tokenEndpointURL = "";
    private static String mobileClientId = "";
    private static String mobileClientSecret = "";
    public static final AsyncHttpClient client = Constants.getUnsafeHttpClient();

    public static void getTokenEndpoint(final RestAPICallback callback) {
        client.get(Constants.ENTRY_URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, @Nullable Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    mobileClientId = response.getString("mobileClientId");
                    mobileClientSecret = response.getString("mobileClientSecret");
                    JSONObject t = response.getJSONObject("_links").getJSONObject("tokenEndpoint");
                    tokenEndpointURL = t.getString("href");
                    Log.d(TAG, "Got Token Endpoint.");
                    callback.onSuccess("success");

                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d(TAG, String.valueOf(statusCode));
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d(TAG, String.valueOf(statusCode));
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d(TAG, String.valueOf(statusCode));
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void getAccessToken(String username, String password,
                                      final RestAPICallback callback) {
        if (tokenEndpointURL.isEmpty()) {
            callback.onFailure("No URL");
            return;
        }

        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        client.setBasicAuth(mobileClientId, mobileClientSecret);
        client.setConnectTimeout(30000);

        RequestParams params = new RequestParams();
        params.put("grant_type", "password");
        params.put("scope", "openid user_name");
        params.put("username", username);
        params.put("password", password);

        // Below 1 line is added by manish

        client.post(tokenEndpointURL/*NewUrl*/, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    accessToken = response.getString("access_token");
                    //refreshToken = response.getString("refresh_token");
                    callback.onSuccess("success");
                    Log.d(TAG, accessToken);
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                Log.d(TAG, String.valueOf(statusCode));
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

}
