package com.controlmyspa.ownerappnew.service;

import static com.controlmyspa.ownerappnew.helper.AppConstantsKt.LOCK_PANEL;
import static com.controlmyspa.ownerappnew.helper.AppConstantsKt.UNLOCK_PANEL;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.HighLow;
import com.controlmyspa.ownerappnew.model.SpaComponent;
import com.controlmyspa.ownerappnew.model.SpaPreset;
import com.controlmyspa.ownerappnew.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class SpaManager {
    private static final String TAG = "SpaManager";
    private static String COUNTRY;
    public static String COUNTRY_CODE;
    public static String m_ID;
    public static String m_SerialNumber;
    //static String m_RegKey;
    public static Boolean mCelcius;
    public static String m_ProductName;
    public static String m_ProductModel;
    public static String m_OwnerName;
    public static final String CHROMAZONE = "CHROMAZON3";
    public static String dipSwitchSettingsNumber = "";
    public static int currentSetup = 0;
    public static String lastEntryFaultLogShortDescription = "";
    public static String salesDate = "";
    public static float lowRangeLow = 0;
    public static float lowRangeHigh = 0;
    public static float highRangeLow = 0;
    public static float highRangeHigh = 0;
    public static String sensorATemp = "";
    public static String sensorBTemp = "";

    //static Boolean m_Sold = true;
    //static Boolean m_Registered = true;
    //static String m_DealerID;
    //static String m_OemID;

    public static String m_ManufacturedDate;
    public static String m_RegistrationDate;
    public static String m_P2P_AP_SSID;
    public static String m_SalesAssociate;
    public static String m_TransactionID;

    public static int m_hour;
    public static int m_minute;
    public static boolean m_military;
    public static boolean m_online;

    public static String m_RunMode;
    public static float m_CurrentTemp = 0;
    public static float m_DesiredTemp = 0;
    public static String mTempRange = "";
    //SpaPreset list
    public static ArrayList<SpaPreset> m_PresetList = new ArrayList<>();
    //Spa location
    public static double m_Lat = 0.0;
    public static double m_Lon = 0.0;
    //device location
    public static double m_device_Lat = 0.0;
    public static double m_device_Lon = 0.0;
    public static String m_Location = "";
    public static ArrayList<SpaComponent> m_ComponentList = new ArrayList<>();
    private static int m_TargetDesiredTemp = 0;
    private static AsyncHttpClient client = Constants.getUnsafeHttpClient();

    private static String getEndpointForSetSpaLocation() {

        return Constants.BASE_URL + "/location/" + UserManager.strId;
    }

    private static String getEndpointForGetSpaLocation() {
        return Constants.BASE_URL + "/location/" + UserManager.strId;
    }


    private static String getEndpointForSetDesiredTemp(String spaId) {
        return Constants.BASE_URL + "/control/" + spaId + "/setDesiredTemp";
    }

    private static String getEndpointForSetFilterCycleIntervals() {
        return Constants.BASE_URL + "/control/" + m_ID + "/setFilterCycleIntervals";
    }

    private static String getEndpointForSetfilterCycleIntervalsSchedule() {
        return Constants.BASE_URL + "/control/" + m_ID + "/setFilterCycleIntervalsSchedule";
    }

    private static String getEndpointForSetControlState(String componentType) {
        return Constants.BASE_URL + "/control/" + m_ID + "/set" + componentType + "State";
    }

    private static String getEndpointForRunSpaRecipe(String id) {
        return Constants.BASE_URL + "/spas/" + m_ID + "/recipes/" + id + "/run";
    }

    private static String getEndpointForSpaDetails() {
        return Constants.BASE_URL + "/spas/" + m_ID;

    }

    private static String getEndpointForSpaSetTime() {
        return Constants.BASE_URL + "/control/" + m_ID + "/setTime";
    }

    private static String getEndpointForGetSpaRecipes() {
        return Constants.BASE_URL + "/spas/" + m_ID + "/recipes";
    }


    private static String getEndpointForUpdateSpaRecipe(String presetID) {
        return Constants.BASE_URL + "/spas/" + m_ID + "/recipes/" + presetID;
    }

    private static String getEndpointForDeleteSpaRecipe(String presetID) {
        return Constants.BASE_URL + "/spas/" + m_ID + "/recipes/" + presetID;
    }

    /*api to get country code and lat-lng */
    private static String getEndPointForCountryCode() {
        return Constants.CURRENT_COUNTRY_NAME_URL;
    }

    public static void init() {
        m_ID = "";
        m_SerialNumber = "";
        m_ProductName = "";
        m_ProductModel = "";
        m_OwnerName = "";

        m_ManufacturedDate = "";
        m_RegistrationDate = "";
        m_P2P_AP_SSID = "";
        m_SalesAssociate = "";
        m_TransactionID = "";

        m_RunMode = "";
        m_CurrentTemp = 0;
        m_TargetDesiredTemp = 0;
        m_DesiredTemp = 0;

        m_PresetList.clear();

        //Spa location
        m_Lat = 0.0;
        m_Lon = 0.0;

        //device location
        m_device_Lat = 0.0;
        m_device_Lon = 0.0;
        m_Location = "";

        m_ComponentList.clear();
    }

    public static void requestRunSpaRecipe(String recipeID, final RestAPICallback callback) {
        String strUrl = getEndpointForRunSpaRecipe(recipeID);
        client.post(strUrl, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, response.toString());
                callback.onSuccess("success");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void requestSetDesiredTemp(Context context, float desiredTemp, String spaId, final RestAPICallback callback) {
        String url = getEndpointForSetDesiredTemp(spaId);
        Log.d(TAG, "API URL : " + url);
        // m_TargetDesiredTemp = desiredTemp;
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        JSONObject jsonParams = new JSONObject();
        try {
            double temp = Double.parseDouble(String.valueOf(desiredTemp));
            jsonParams.put("desiredTemp", temp);
            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");

            client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, response.toString());
                    try {
                        JSONObject obj = response.getJSONObject("values");
                        m_DesiredTemp = Float.parseFloat(obj.getString("DESIREDTEMP"));
                    } catch (JSONException e) {
                        callback.onFailure(e.toString());
                        return;
                    }
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                    Log.d(TAG, "Failure");
                    callback.onFailure("fail");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, responseString);
                    callback.onFailure(responseString);
                }
            });
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }

    }

    public static void requestSetFilterCycleIntervals(Context context, int componentIndex, final RestAPICallback callback) {
        String interval;
        SpaComponent component = m_ComponentList.get(componentIndex);
        if (component.targetValue.equals(SpaComponent.ControlState.ON)) {
            interval = "2"; //30 minutes
        } else {
            interval = "0"; //OFF
        }
        String strUrl = getEndpointForSetFilterCycleIntervals();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("intervalNumber", interval);
            jsonParams.put("deviceNumber", component.deviceNumber);
            jsonParams.put("originatorId", "optional-filtercycle");
            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            _requestCommon(context, strUrl, entity, callback);
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }

    public static void requestSetFilterCycleToggle(Context context, String toggle, final RestAPICallback callback) {

        String strUrl = getEndpointForSetFilterCycleIntervals();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("intervalNumber", toggle);
            jsonParams.put("deviceNumber", "1");
            jsonParams.put("originatorId", "optional-filtercycle");
            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            _requestCommon(context, strUrl, entity, callback);
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }

    public static void requestSetFilterCycleIntervalsSchedule(Context context, int deviceNumber,
                                                              String scheduledTime, int interval,
                                                              final RestAPICallback callback) {
        String strUrl = getEndpointForSetfilterCycleIntervalsSchedule();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("intervalNumber", interval);
            jsonParams.put("deviceNumber", deviceNumber);
            jsonParams.put("time", scheduledTime);
            jsonParams.put("originatorId", "optional-filtercycle");

            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            _requestCommon(context, strUrl, entity, callback);
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }

    public static void requestSetControlState(Context context, int componentIndex,
                                              ArrayList<SpaComponent> m_ComponentList,
                                              final RestAPICallbackForControl callback) {

        String strType;
        SpaComponent component = m_ComponentList.get(componentIndex);
        switch (component.componentType) {
            case "PUMP":
                strType = "Jet";
                break;
            case "BLOWER":
                strType = "Blower";
                break;
            case "LIGHT":
                strType = "Light";
                break;
            case "MISTER":
                strType = "Mister";
                break;
            case "AUX":
                strType = "Aux";
                break;
            case "MICROSILK":
                strType = "Microsilk";
                break;
            case "CIRCULATION_PUMP":
                strType = "CircPump";
                break;
            case "OZONE":
                strType = "Ozone";
                break;
            default:
                return;
        }
        String strUrl = getEndpointForSetControlState(strType);
        Log.d(TAG, strUrl);
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceNumber", component.deviceNumber);
            jsonParams.put("desiredState", component.targetValue);
            jsonParams.put("originatorId", TokenManager.accessToken);

            Log.d(TAG, jsonParams.toString());
            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            // _requestCommon(context, strUrl, entity, callback);
            _requestCommon_new(context, strUrl, entity, callback);
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }

    public static void requestFindByUsername(final Context context, final RestAPICallback callback) {
        String url = Constants.BASE_URL + "/spas/search/findByUsername";
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        RequestParams params = new RequestParams();
        params.put("username", UserManager.strUsername);
        client.get(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    if (response.has("location")) {
                        JSONArray arrLocation = response.getJSONArray("location");
                        if (arrLocation.length() != 0) {
                            m_Lon = arrLocation.getDouble(0);
                            m_Lat = arrLocation.getDouble(1);
                        }
                    }

                    if (response.has("productName")) {
                        m_ProductName = response.getString("productName");
                    }
                    if (response.has("model")) {
                        m_ProductModel = response.getString("model");
                    }

                    m_OwnerName = response.getJSONObject("owner").getString("fullName");
                    m_SerialNumber = response.getString("serialNumber");

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    if (response.has("registrationDate")) {
                        m_RegistrationDate = response.getString("registrationDate");
                        Date dt = sdf.parse(m_RegistrationDate);
                        if (dt != null) {
                            m_RegistrationDate = sdf1.format(dt);
                        }
                    }

                    if (response.has("manufacturedDate")) {
                        m_ManufacturedDate = response.getString("manufacturedDate");
                        Date dt = sdf.parse(m_ManufacturedDate);
                        if (dt != null) {
                            m_ManufacturedDate = sdf1.format(dt);
                        }
                    }

                    if (response.has("p2pAPSSID")) {
                        m_P2P_AP_SSID = response.getString("p2pAPSSID");
                    }

                    if (response.has("associate")) {
                        m_SalesAssociate = response.getJSONObject("associate").getString("fullName");
                    }


                    if (response.has("transactionCode")) {
                        m_TransactionID = response.getString("transactionCode");
                    }

                    m_ID = response.getString("_id");
                    m_online = response.getBoolean("online");
                    salesDate = response.getString("salesDate");
                    if (response.has("alerts")) {
                        JSONArray alerts = response.getJSONArray("alerts");
                        lastEntryFaultLogShortDescription = alerts.getJSONObject(alerts.length() - 1).getString("shortDescription");
                    }

                    JSONObject currentState = response.getJSONObject("currentState");

                    m_hour = currentState.getInt("hour");
                    m_minute = currentState.getInt("minute");
                    m_military = currentState.getBoolean("military");
                    mCelcius = currentState.getBoolean("celsius");
                    m_RunMode = currentState.getString("runMode");
                    if (currentState.has("currentTemp")) {
                        m_CurrentTemp = (float) Double.parseDouble(currentState.getString("currentTemp"));
                        Log.e("SpaManager", m_CurrentTemp + "");
                    }
                    if (currentState.has("sensorATemp"))
                        sensorATemp = currentState.getString("sensorATemp");
                    if (currentState.has("sensorBTemp"))
                        sensorBTemp = currentState.getString("sensorBTemp");
                    // BY Manish Dutta
                    if (currentState.has("targetDesiredTemp")) {
                        m_TargetDesiredTemp = (int) Double.parseDouble(currentState.getString("targetDesiredTemp"));
                    }

                    // App says No spa Info because of "desiredTemp" field. That is why we kept this condition.
                    // If there is no data in Desired Temp. It will show 1111.
                    if (currentState.has("desiredTemp") && (!currentState.getString("desiredTemp").equals(""))) {
                        m_DesiredTemp = (float) Double.parseDouble(currentState.getString("desiredTemp"));
                    } else {
                        m_DesiredTemp = 0;
                    }
                    mTempRange = currentState.getString("tempRange");
                    JSONObject systemInfo = currentState.getJSONObject("systemInfo");
                    currentSetup = systemInfo.getInt("currentSetup");
                    try {
                        JSONArray dipSwitchArray = systemInfo.getJSONArray("dipSwitches");
                        dipSwitchSettingsNumber = "";
                        for (int k = 0; k < dipSwitchArray.length(); k++) {
                            if (dipSwitchArray.getJSONObject(k).getBoolean("on")) {
                                dipSwitchSettingsNumber = dipSwitchSettingsNumber + "1";
                            } else {
                                dipSwitchSettingsNumber = dipSwitchSettingsNumber + "0";
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    // BY Manish Dutta
                    m_ComponentList.clear();

                    JSONObject setupParams = currentState.getJSONObject("setupParams");
                    lowRangeLow = setupParams.getInt("lowRangeLow");
                    lowRangeHigh = setupParams.getInt("lowRangeHigh");
                    highRangeLow = setupParams.getInt("highRangeLow");
                    highRangeHigh = setupParams.getInt("highRangeHigh");
                    JSONArray components = currentState.getJSONArray("components");

                    for (int i = 0; i < components.length(); i++) {

                        JSONObject component = components.getJSONObject(i);
                        if (component.has("availableValues")) {
                            //If registeredTimestamp exists, display.
                            if (!component.getString("componentType").equals("OZONE"))
                                if (component.has("registeredTimestamp")) {
                                    SpaComponent ctemp = new SpaComponent();

                                    if (component.has("port")) {
                                        ctemp.deviceNumber = component.getString("port");
                                    } else {
                                        ctemp.deviceNumber = ""; //Default value for deviceNumber for escaping null pointer exception
                                    }

                                    String name = component.getString("name");
                                    String[] split = name.split("_");
                                    boolean splitted = false;
                                    if (split.length > 1) {
                                        splitted = true;
                                    }
                                    if (ctemp.deviceNumber.equals("")) {
                                        if (splitted)
                                            ctemp.name = split[0] + " " + split[1];
                                        else ctemp.name = name;
                                    } else {
                                        if (splitted)
                                            ctemp.name = split[0] + " " + split[1] + " " + (Integer.parseInt(ctemp.deviceNumber) + 1);
                                        else
                                            ctemp.name = name + " " + (Integer.parseInt(ctemp.deviceNumber) + 1);
                                    }
                                    //------

                                    if (component.has("hour")) {
                                        ctemp.m_hour = component.getInt("hour");
                                    }

                                    if (component.has("minute")) {
                                        ctemp.m_minute = component.getInt("minute");
                                    }

                                    if (component.has("durationMinutes")) {
                                        ctemp.m_durationMinutes = component.getInt("durationMinutes");
                                    }

                                    ctemp.componentType = component.getString("componentType");
                                    ctemp.value = component.getString("value");
                                    if (component.has("targetValue")) {
                                        ctemp.targetValue = component.getString("targetValue");
                                    }

                                    JSONArray availableValues = component.getJSONArray("availableValues");
                                    for (int j = 0; j < availableValues.length(); j++) {
                                        ctemp.availableValues.add(availableValues.getString(j));
                                    }
                                    switch (ctemp.componentType) {
                                        case "PUMP":
                                            ctemp.sortOrder = 1;
                                            break;
                                        case "LIGHT":
                                            ctemp.sortOrder = 2;
                                            break;
                                        default:
                                            ctemp.sortOrder = 3;
                                    }
                                    m_ComponentList.add(ctemp);
                                }
                        }
                    }
                    //m_components sort by type
                    Collections.sort(m_ComponentList, (o1, o2) -> o1.sortOrder - o2.sortOrder);

                    SpaComponent panelComponent = new SpaComponent();
                    panelComponent.componentType = context.getString(R.string.CMS_panel_name);
                    panelComponent.name = context.getString(R.string.CMS_panel_name);
                    boolean isPanelLocked = currentState.getBoolean("panelLock");
                    panelComponent.value = isPanelLocked ? LOCK_PANEL : UNLOCK_PANEL;
                    m_ComponentList.add(panelComponent);

                    String primaryTZLStatus = currentState.getString("primaryTZLStatus");
                    if (primaryTZLStatus.equalsIgnoreCase("TZL_CONNECTED")) {
                        SpaComponent spaComponent = new SpaComponent();
                        spaComponent.componentType = CHROMAZONE;
                        spaComponent.name = CHROMAZONE;
                        spaComponent.value = context.getString(R.string.lighting);
                        m_ComponentList.add(spaComponent);
                    }
                    callback.onSuccess("success");
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {

                callback.onFailure(String.valueOf(statusCode));

                //session timeout
                authFailRedirectToRefreshAppData(context);
                //authFailDialog(context);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                callback.onFailure(String.valueOf(statusCode));
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                 /*Commented By Manish to enable Session Timed Out feature as it is in iOS.
                   When this api fails, it should say "session timed out" and let the user
                   to the login screen.*/
             /*   if (statusCode == 404) {
                    //Spa not found
                    callback.onFailure("404");
                } else {
                    callback.onFailure(responseString);
                }
                            if (statusCode == 404) {
                                //Spa not found
                                callback.onFailure("404");
                            } else {
                                authFailDialog(context);
                            }*/

                //session timeout
                authFailRedirectToRefreshAppData(context);
                //authFailDialog(context);
            }
        });
    }

    /*new method to redirect created by Ashish Verma*/
    public static void authFailRedirectToRefreshAppData(final Context context) {
        final String strUsername = UserManager.strUsername;
        final String strPassword = UserManager.strPassword;

        TokenManager.getTokenEndpoint(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {

                TokenManager.getAccessToken(strUsername, strPassword, new RestAPICallback() {
                    @Override
                    public void onSuccess(String result) {
                        UserManager.getWhoAmI(new RestAPICallback() {
                            @Override
                            public void onSuccess(String result) {

                                Log.d(TAG, result);
                                if (UserManager.strDeviceToken != null) {

                                    if (UserManager.strDeviceToken.length() > 0) {
                                        UserManager.setDeviceToken(context, new RestAPICallback() {
                                            @Override
                                            public void onSuccess(String result) {
                                                Log.d(TAG, "device token set success:" + result);
                                            }

                                            @Override
                                            public void onFailure(String result) {
                                                Log.d(TAG, "device token failed:" + result);
                                            }
                                        });
                                    } else {
                                        Log.d(TAG, "device token length is 0");
                                    }
                                } else {
                                    Log.d(TAG, "device token is null");
                                }

                                UserManager.findCurrentUserAgreement(new RestAPICallback() {
                                    @SuppressLint("CommitPrefEdits")
                                    @Override
                                    public void onSuccess(String result) {
                                        Log.d(TAG, result);

                                    }

                                    @Override
                                    public void onFailure(String result) {

                                    }
                                });


                                UserManager.getOEMUrl(UserManager.strOemLogoUrl, new RestAPICallback1() {
                                    @SuppressLint("CommitPrefEdits")
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        InputStream inputStream = new ByteArrayInputStream(bytes);
                                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);


                                        File myDir = new File(Environment.getExternalStorageDirectory().toString() + "/ControlMySpa");
                                        if (!myDir.exists()) {
                                            myDir.mkdirs();
                                        }
                                        File file = new File(myDir, "img_oem.png");
                                        if (file.exists()) {
                                            file.delete();
                                        }
                                        try {
                                            FileOutputStream out = new FileOutputStream(file);
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                                            out.flush();
                                            out.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(String s) {

                                    }
                                });


                            }

                            @Override
                            public void onFailure(String result) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(String result) {

                    }
                });
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }

    private static void authFailDialog(final Context context) {
        String message = "Session Timed Out";
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(context.getResources().getString(R.string.app_name));
        adb.setCancelable(false);
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            Intent intent = LoginActivity.getStartIntent((Activity) context);
            intent.putExtra("isSessionTimedOut", true);
            context.startActivity(intent);
            ((Activity) context).finish();
        });
        if (!((Activity) context).isFinishing() && message.length() > 0) {
            adb.show();
        }
    }


    private static void _requestCommon(Context context, String url, StringEntity params, final RestAPICallback callback) {
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        Log.e(TAG, params.toString());
        client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("DATA", "" + response);
                callback.onSuccess("success");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    private static void _requestCommon_new(Context context, String url, StringEntity params, final RestAPICallbackForControl callback) {
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e(TAG, "SUCCESS " + response.toString());
                callback.onSuccess("success", response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.e(TAG, "ERROR " + response.toString());
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.e(TAG, "ERROR " + responseString);
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }


    public static void requestSaveSpaDetails(Context context, final RestAPICallback callback) {
        String url = getEndpointForSpaDetails();
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        JSONObject jsonParams = new JSONObject();
        try {
            JSONArray arr = new JSONArray();
            arr.put(m_Lon);
            arr.put(m_Lat);
            jsonParams.put("location", arr);
            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");

            client.patch(context, url, params, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, response.toString());
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                    Log.d(TAG, "Failure");
                    callback.onFailure("fail");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, responseString);
                    callback.onFailure(responseString);
                }
            });
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }

    }

    /*created on March 15
     *sending location to server
     * */


    public static void requestToGetSpaLocation(Context context, final RestAPICallback callback) {

        String url = getEndpointForGetSpaLocation();

        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {

                    callback.onSuccess("success");
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }
        });


    }

    public static void requestUpdateSpaLocation(Context context, Double spaLat, Double spaLng,
                                                final RestAPICallback callback) {
        String url = getEndpointForSetSpaLocation();
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        JSONObject jsonParams = new JSONObject();
        try {

            jsonParams.put("lat", String.valueOf(spaLat));
            jsonParams.put("lng", String.valueOf(spaLng));

            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");
            client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d(TAG, response.toString());
                    callback.onSuccess(response.toString() + response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    callback.onFailure(String.valueOf(statusCode) + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    callback.onFailure(statusCode + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    callback.onFailure(String.valueOf(statusCode) + errorResponse);

                }
            });

        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }


    public static void requestSetTime(Context context, String strDate, String strTime, String timeFormat,
                                      final RestAPICallback callback) {
        String url = getEndpointForSpaSetTime();
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("date", strDate);
            jsonParams.put("time", strTime);
            jsonParams.put("military_format", timeFormat);
            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");

            client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, response.toString());
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                    Log.d(TAG, "Failure");
                    callback.onFailure("fail");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, responseString);
                    callback.onFailure(responseString);
                }
            });
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }

    }

    //Preset APIs
    public static void requestToGetCurrentCountryCode(final Context context, final RestAPICallback callback) {
        String url = getEndPointForCountryCode();
        Log.d(TAG, "API URL : " + url);
        // client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, @Nullable Header[] headers, JSONObject response) {
                try {
                    /*
                     * German "DE"
                     * French "FR"
                     * Italian "IT"
                     * Sweden  "SE"
                     * English "EN"
                     * */

                    Log.d(TAG, response.toString());
                    String countryCode = response.getString("countryCode");
                    COUNTRY = response.getString("country");


                    callback.onSuccess(countryCode);
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, Throwable throwable, JSONArray errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }


    //Preset APIs
    public static void requestGetSpaPresets(final RestAPICallback callback) {
        String url = getEndpointForGetSpaRecipes();
        Log.e(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e(TAG, response.toString());
                m_PresetList.clear();
                try {
                    if (response.has("_embedded")) {
                        JSONArray presets = response.getJSONObject("_embedded").getJSONArray("recipeDToes");
                        SpaPreset.getPresetList(m_PresetList, presets);
                    }
                    callback.onSuccess("success");
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d(TAG, errorResponse.toString());
                callback.onFailure(errorResponse.toString());
            }
        });
    }


    public static void requestSaveSpaPreset(Context context, final SpaPreset preset, final RestAPICallback callback) {
        String url = getEndpointForGetSpaRecipes();
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        try {
            JSONObject jsonPreset = new JSONObject();
            /*JSONObject filter0 = new JSONObject();
            filter0.put("intervalNumber", preset.mFilter1Interval);
            filter0.put("deviceNumber", 0);
            jsonPreset.put("FILTER_0", filter0);

            JSONObject filter1 = new JSONObject();
            filter1.put("intervalNumber", preset.mFilter2Interval);
            filter1.put("deviceNumber", 1);
            jsonPreset.put("FILTER_1", filter1);*/
            DecimalFormat df = new DecimalFormat("#.##");
            double temp = Double.parseDouble(df.format(preset.temperature));
            JSONObject jsonHeater = new JSONObject();
            jsonHeater.put("desiredTemp", temp);
            jsonPreset.put("HEATER", jsonHeater);
            jsonPreset.put("TOGGLE_HEATER_MODE", new JSONObject().put("desiredState", preset.mRunMode));
            jsonPreset.put("TEMP_RANGE", new JSONObject().put("desiredState", preset.mTempRange == HighLow.LOW ? "LOW" : "HIGH"));
            JSONObject jsonSchedule = new JSONObject();
            jsonSchedule.put("cronExpression", preset.strCronExpression);
            //jsonSchedule.put("durationMinutes", preset.durationMinutes);
            jsonSchedule.put("startDate", preset.strStartDate);
            jsonSchedule.put("endDate", preset.strEndDate);
            jsonSchedule.put("timeZone", preset.strTimeZone);
            jsonSchedule.put("enabled", preset.bEnabled);

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("name", preset.strName);
            jsonParams.put("notes", "This is a preset.");
            jsonParams.put("settings", jsonPreset);
            jsonParams.put("schedule", jsonSchedule);
            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");
            Log.d(TAG, jsonParams.toString());

            client.post(context, url, params, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, response.toString());
                    try {
                        preset.strId = response.getString("_id");
                        m_PresetList.add(preset);
                        callback.onSuccess("success");
                    } catch (JSONException e) {
                        callback.onFailure(e.toString());
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                    if (statusCode == 409) {
                        // handle 409 specific error here, which means Recipes reached maximum.
                        callback.onFailure("Max");
                    } else {
                        Log.d(TAG, responseString);
                        callback.onFailure(responseString);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

            });
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }
    }

    public static void requestUpdateSpaPreset(Context context, final SpaPreset preset, final RestAPICallback callback) {
        String url = getEndpointForUpdateSpaRecipe(preset.strId);
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        try {
            JSONObject jsonPreset = new JSONObject();
             /*JSONObject filter0 = new JSONObject();
            filter0.put("intervalNumber", preset.mFilter1Interval);
            filter0.put("deviceNumber", 0);
            jsonPreset.put("FILTER_0", filter0);

            JSONObject filter1 = new JSONObject();
            filter1.put("intervalNumber", preset.mFilter2Interval);
            filter1.put("deviceNumber", 1);
            jsonPreset.put("FILTER_1", filter1);*/
            DecimalFormat df = new DecimalFormat("#.##");
            double temp = Double.parseDouble(df.format(preset.temperature));
            JSONObject jsonHeater = new JSONObject();
            jsonHeater.put("desiredTemp", temp);
            jsonPreset.put("HEATER", jsonHeater);
            jsonPreset.put("TOGGLE_HEATER_MODE", new JSONObject().put("desiredState", preset.mRunMode));
            jsonPreset.put("TEMP_RANGE", new JSONObject().put("desiredState", preset.mTempRange == HighLow.LOW ? "LOW" : "HIGH"));
            JSONObject jsonSchedule = new JSONObject();
            jsonSchedule.put("cronExpression", preset.strCronExpression);
            // jsonSchedule.put("durationMinutes", preset.durationMinutes);
            jsonSchedule.put("startDate", preset.strStartDate);
            jsonSchedule.put("endDate", preset.strEndDate);
            jsonSchedule.put("timeZone", preset.strTimeZone);
            jsonSchedule.put("enabled", preset.bEnabled);

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("name", preset.strName);
            jsonParams.put("spaId", m_ID);
            jsonParams.put("notes", "This is a preset.");
            jsonParams.put("settings", jsonPreset);
            jsonParams.put("schedule", jsonSchedule);
            StringEntity params = new StringEntity(jsonParams.toString(), "UTF-8");
            Log.d(TAG, jsonParams.toString());

            client.put(context, url, params, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, response.toString());
                    callback.onSuccess("success " + response.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    callback.onFailure(String.valueOf(statusCode));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    callback.onFailure(responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

            });
        } catch (Exception e) {
            callback.onFailure(e.toString());
        }

    }

    public static void requestDeleteSpaPreset(final int index, final RestAPICallback callback) {
        String presetID = m_PresetList.get(index).strId;
        String url = getEndpointForDeleteSpaRecipe(presetID);
        Log.d(TAG, "API URL : " + url);
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        client.delete(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, response.toString());
                if (response.has("error")) {
                    try {
                        String error = response.getString("error");
                        callback.onFailure(error);
                    } catch (JSONException e) {
                        callback.onFailure(e.toString());
                    }
                } else {
                    m_PresetList.remove(index);
                    callback.onSuccess("success");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }
        });
    }
}
