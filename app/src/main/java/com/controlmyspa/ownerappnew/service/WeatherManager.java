package com.controlmyspa.ownerappnew.service;

import android.util.Log;

import com.controlmyspa.ownerappnew.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by polaris on 11/2/16.
 */

public class WeatherManager {
    private static final String TAG = "WeatherManager";

//    public static Double lat;
//    public static Double lon;

    public static String name = "";
    public static String weather_main = "";
    private static String weather_description = "";
    public static String weather_icon = "";

    public static double temperature = 0;
    private static double pressure = 0;
    private static double humidity = 0;
    private static double wind_speed = 0;
    private static double wind_degree = 0;


    private static String weatherEndpointURL = Constants.WEATHER_URL + "/weather";
    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void getWeather(double lat, double lon, final RestAPICallback callback) {
        RequestParams params = new RequestParams();
        params.put("APPID", Constants.WEATHER_API_KEY);
        params.put("lat", lat);
        params.put("lon", lon);

        client.get(weatherEndpointURL, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    int code = response.getInt("cod");
                    if (code == 404) {
                        callback.onFailure("Didn't get the weather");
                        return;
                    }
                    name = response.getString("name");

                    JSONArray weather = response.getJSONArray("weather");
                    weather_main = weather.getJSONObject(0).getString("main");
                    weather_description = weather.getJSONObject(0).getString("description");
                    weather_icon = weather.getJSONObject(0).getString("icon");

                    JSONObject main = response.getJSONObject("main");
                    temperature = main.getDouble("temp");
                    pressure = main.getDouble("pressure");
                    humidity = main.getDouble("humidity");

                    JSONObject wind = response.getJSONObject("wind");
                    wind_speed = wind.getDouble("speed");
                    if (wind.has("deg")) {
                        wind_degree = wind.getDouble("deg");
                    }

                    Log.d(TAG, "Weather:" + weather_main);
                    callback.onSuccess("success");
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }
        });
    }
}
