package com.controlmyspa.ownerappnew.service;

import org.json.JSONObject;

public interface RestAPICallbackForControl {

    void onSuccess(String result, JSONObject response);

    void onFailure(String result);
}
