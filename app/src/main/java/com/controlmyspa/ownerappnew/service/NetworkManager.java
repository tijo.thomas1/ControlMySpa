package com.controlmyspa.ownerappnew.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.helper.PrefManager;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class NetworkManager {
    private static final String TAG = "NetworkManager";
    public static String m_SSID = "";
    public static String m_Password = "";
    public static String m_IPAddress = "";
    public static String m_SubnetMask = "";
    public static String m_Gateway = "";
    public static Boolean m_DHCP = false;
    public static Boolean m_EthernetPluggedIn = false;

    private static AsyncHttpClient client = Constants.getUnsafeHttpClient();

    public static void requestGetNetworkSettings(final RestAPICallback callback) {
        String strURL = Constants.NETWORK_SETTINGS_URL;
        Log.d(TAG, "API URL : " + strURL);

        client.get(strURL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.has("wifi")) {

                        JSONObject wifi = response.getJSONObject("wifi");
                        m_SSID = wifi.getString("ssid");
                        m_Password = wifi.getString("password");
                    }

                    m_EthernetPluggedIn = response.getBoolean("ethernetPluggedIn");
                    if (!response.isNull("ethernet")) {

                        JSONObject ethernet = response.getJSONObject("ethernet");
                        m_IPAddress = ethernet.getString("ipAddress");
                        m_SubnetMask = ethernet.getString("netmask");
                        m_Gateway = ethernet.getString("gateway");
                        m_DHCP = ethernet.getBoolean("dhcp");
                    }
                    callback.onSuccess("success");

                } catch (Exception e) {

                    callback.onFailure(e.toString() + response);
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void registerUserToSpa(final RestAPICallback callback) {
        String strURL = Constants.REGISTER_ENTRY_URL;


        client.get(strURL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {

                    if (response.getString("serialNumber").equals("null") || TextUtils.isEmpty(response.getString("serialNumber"))) {
                        callback.onFailure("Failure");
                    } else {
                        String serialNumber = response.getString("serialNumber");

                        callback.onSuccess(serialNumber);
                    }


                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void isCmsCodeValid(String cmsCode, final RestAPICallback callback) {
        String strURL = Constants.AUTO_BUILD_URL + "verify?cmsCode=" + cmsCode;
        Log.d(TAG, "API URL : " + strURL);

        client.get(strURL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    callback.onSuccess("Success");

                } catch (Exception e) {

                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void verifyEmail(Context context, String email, final RestAPICallback callback) {
        String strURL = Constants.AUTO_BUILD_URL + "validateUsername";


        JSONObject jsonParams = new JSONObject();
        try {

            jsonParams.put("username", email);

            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            client.post(context, strURL, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {


                    callback.onFailure(errorResponse == null ? "Failure" : errorResponse.toString());
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestGetWiFiList(final RestAPICallback callback) {
        String strURL = Constants.NETWORK_WIFI_URL;


        client.get(strURL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    callback.onSuccess(response.toString());

                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void requestPutNetworkSettings(Context context, final RestAPICallback callback) {
        String strURL = Constants.NETWORK_SETTINGS_URL;


        JSONObject jsonParams = new JSONObject();
        try {
            JSONObject ethernet = new JSONObject();
            ethernet.put("ipAddress", m_IPAddress);
            ethernet.put("netmask", m_SubnetMask);
            ethernet.put("gateway", m_Gateway);
            ethernet.put("dhcp", m_DHCP);
            jsonParams.put("ethernet", ethernet);

            if (!m_SSID.isEmpty()) {
                JSONObject wifi = new JSONObject();
                wifi.put("ssid", m_SSID);
                wifi.put("password", m_Password);
                jsonParams.put("wifi", wifi);
            }

            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            client.post(context, strURL, entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d(TAG, String.format("Status code: %d", statusCode));
                    callback.onFailure(String.valueOf(statusCode));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDealerData(Context context, final RestAPICallback callback) {
        client.setTimeout(16 * 1000);
        PrefManager pref = new PrefManager(context);
        String strURL = Constants.AUTO_BUILD_URL + "spa";
        Log.d(TAG, "API URL : " + strURL);

        DealerData dealerData = OwnerApplication.getInstance().getDealerData();
        JSONObject jsonParams = new JSONObject();

        try {
            jsonParams.put("cmsCode", dealerData.getCmsCode());
            jsonParams.put("serialNumber", pref.getSerialNumber());
            jsonParams.put("demo", dealerData.isDemo());

            JSONObject user = new JSONObject();
            user.put("lastName", dealerData.getLastName());
            user.put("firstName", dealerData.getFirstName());
            user.put("phone", dealerData.getPhone());
            user.put("email", dealerData.getEmail());
            user.put("password", dealerData.getPassword());

            JSONObject address = new JSONObject();
            address.put("address1", dealerData.getAddress1());
            address.put("address2", dealerData.getAddress2());
            address.put("city", dealerData.getCity());
            address.put("state", dealerData.getState());
            address.put("country", dealerData.getCountry());
            address.put("zip", dealerData.getZip());

            user.put("address", address);

            JSONArray roles = new JSONArray();
            if (dealerData.isDealerAccount())
                roles.put("DEALER");
            else roles.put("OWNER");

            user.put("roles", roles);
            jsonParams.put("user", user);

            StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
            client.post(context, strURL, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.d(TAG, "T&C agreement failed.");
                    callback.onFailure(errorResponse == null ? "Failure" : errorResponse.toString());
                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d(TAG, "T&C agreement failed.");
                    callback.onFailure(responseString == null ? "Failure" : responseString);
                }


                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d(TAG, "T&C agreement failed.");
                    callback.onFailure(errorResponse == null ? "Failure" : errorResponse.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
