package com.controlmyspa.ownerappnew.service;

/**
 * Created by polaris on 10/18/16.
 */

public interface RestAPICallback1 {
    void onSuccess(byte[] bytes);

    void onFailure(String s);
}
