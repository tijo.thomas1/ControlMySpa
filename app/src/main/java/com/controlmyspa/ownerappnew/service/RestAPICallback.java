package com.controlmyspa.ownerappnew.service;


public interface RestAPICallback {
    void onSuccess(String result);

    void onFailure(String result);
}
