package com.controlmyspa.ownerappnew.service;

import static com.controlmyspa.ownerappnew.utils.Constants.BASE_URL;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.controlmyspa.ownerappnew.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class UserManager {
    private static final String TAG = "UserManager";

    public static String strUsername;
    public static String strPassword;
    public static String strDeviceToken = "";

    public static String strFirstName;
    public static String strLastName;
    public static String strEmail;
    public static String strId;
    public static String strPhone;
    public static String strAddress1;
    public static String strAddress2;

    public static String strCity;
    public static String strZipcode;
    public static String strState;
    public static String strCountry;

    public static String strDealerId = "";
    public static String strOemId;
    public static String strOemLogoUrl;
    private static ArrayList<String> arrRoles = new ArrayList<>();
    private static String strTACVersion;
    private static String strTACText;
    private static AsyncHttpClient client = Constants.getUnsafeHttpClient();

    private static String getEndpointForUpdateAccount() {
        return BASE_URL + "/user-registration/" + strId;
    }

    private static String getEndpointForSetDeviceToken() {
        return BASE_URL + "/user-registration/" + strId + "/setDeviceToken";
    }

    public static void init() {
        strUsername = "";
        strPassword = "";
        strFirstName = "";
        strLastName = "";
        strEmail = "";
        strId = "";
        strPhone = "";
        strAddress1 = "";
        strAddress2 = "";
        strCity = "";
        strZipcode = "";
        strState = "";
        strCountry = "";
        strDealerId = "";
        strOemId = "";
        arrRoles.clear();
        strTACVersion = "";
        strTACText = "";
    }

    public static void getWhoAmI(final RestAPICallback callback) {
        String url = BASE_URL + "/auth/whoami";

        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        client.setConnectTimeout(30000);
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    strUsername = response.getString("username");
                    strFirstName = response.getString("firstName");
                    strLastName = response.getString("lastName");
                    strEmail = response.getString("email");
                    strId = response.getString("_id");
                    if (response.has("phone"))
                        strPhone = response.getString("phone");
                    JSONObject address = response.getJSONObject("address");

                    if (address.has("address1")) {
                        strAddress1 = address.getString("address1");
                    }
                    if (address.has("address2")) {
                        strAddress2 = address.getString("address2");
                    }
                    if (address.has("city")) {
                        strCity = address.getString("city");
                    }
                    if (address.has("zip")) {
                        strZipcode = address.getString("zip");
                    }
                    if (address.has("state")) {
                        strState = address.getString("state");
                    }

                    strCountry = address.getString("country");
                    if (response.has("dealerId")) {
                        strDealerId = response.getString("dealerId");
                    }

                    if (response.has("oemId")) {
                        strOemId = response.getString("oemId");
                    }
                    if (response.has("roles")) {
                        arrRoles.clear();
                        JSONArray roles = response.getJSONArray("roles");
                        for (int i = 0; i < roles.length(); i++) {
                            arrRoles.add(roles.getString(i));
                        }
                    }

                    if (response.has("_links")) {
                        JSONObject links = response.getJSONObject("_links");

                        if (links.has("logo")) {
                            JSONObject logo = links.getJSONObject("logo");
                            strOemLogoUrl = logo.getString("href");
                        }
                    }

                    callback.onSuccess("success");
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(String.valueOf(statusCode));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(String.valueOf(statusCode));
            }
        });
    }

    public static void findCurrentUserAgreement(final RestAPICallback callback) {
        String url = BASE_URL + "/tac/search/findCurrentUserAgreement";


        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        RequestParams params = new RequestParams();
        params.put("userId", strId);

        client.get(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String strDateAgreed = response.getString("dateAgreed");
                    Log.d(TAG, strDateAgreed);
                    callback.onSuccess("success");
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                callback.onFailure(responseString);
            }
        });
    }

    public static void getOEMData(final RestAPICallback callback) {
        String url = BASE_URL + "/oems/" + strOemId;

        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        RequestParams params = new RequestParams();


        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    //Log.d(TAG, strDateAgreed);
                    callback.onSuccess(response.toString());
                } catch (Exception e) {

                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                callback.onFailure(responseString);
            }
        });
    }

    public static void getOEMUrl(String strUrl, final RestAPICallback1 callback) {


        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        RequestParams params = new RequestParams();
        //params.put("userId", strId);

        client.get(strUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, @Nullable Header[] headers, byte[] responseBody) {
                try {

                    callback.onSuccess(responseBody);
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, @Nullable Header[] headers, byte[] responseBody,
                                  Throwable error) {
                callback.onFailure(error.toString());
            }
        });

            /*client.get(strUrl, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        //Log.d(TAG, strDateAgreed);
                        callback.onSuccess(response.toString());
                    } catch (Exception e) {
                        Log.d(TAG, e.toString());
                        callback.onFailure(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, responseString);
                    callback.onFailure(responseString);
                }
            });*/
    }

    public static void findMostRecentTAC(final RestAPICallback callback) {
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        String endpointForFindMostRecentTAC = BASE_URL + "/tac/search/findMostRecent";
        client.get(endpointForFindMostRecentTAC, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    strTACText = response.getString("text");
                    strTACVersion = response.getString("version");
                    callback.onSuccess(strTACText);
                } catch (Exception e) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                callback.onFailure(responseString);
            }
        });
    }

    public static void agreeTAC(Context context, final RestAPICallback callback) {
        String url = BASE_URL + "/tac/agree";


        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("userId", strId);
            jsonParams.put("version", strTACVersion);
            StringEntity entity = new StringEntity(jsonParams.toString());
            client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

            client.post(context, url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String strDateAgreed = response.getString("dateAgreed");
                        Log.d(TAG, strDateAgreed);
                        callback.onSuccess("success");
                    } catch (Exception e) {
                        callback.onFailure(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    callback.onFailure(String.valueOf(statusCode));
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setDeviceToken(Context context, final RestAPICallback callback) {
        String url = getEndpointForSetDeviceToken();


        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceToken", strDeviceToken);
            jsonParams.put("deviceType", "ANDROID");
            StringEntity entity = new StringEntity(jsonParams.toString());
            client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

            client.post(context, url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        callback.onSuccess("success");
                    } catch (Exception e) {
                        callback.onFailure(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    throwable.printStackTrace();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    throwable.printStackTrace();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    throwable.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setDeviceLocation(Context context, final RestAPICallback callback) {
        String url = BASE_URL + "/user-registration/" + strId + "/setDeviceLocation";
        Log.d(TAG, "API URL : " + url);

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("lat", String.valueOf(SpaManager.m_Lat));
            jsonParams.put("lng", String.valueOf(SpaManager.m_Lon));
            StringEntity entity = new StringEntity(jsonParams.toString());
            client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

            client.post(context, url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {


                        callback.onSuccess("success");
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                        callback.onFailure(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONObject errorResponse) {
                    callback.onFailure(String.valueOf(statusCode));
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void requestUpdateAccount(Context context, final RestAPICallback callback) {
        String url = getEndpointForUpdateAccount();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("firstName", strFirstName);
            jsonParams.put("lastName", strLastName);

            JSONObject addressJSON = new JSONObject();
            addressJSON.put("address1", strAddress1);
            addressJSON.put("address2", strAddress2);
            addressJSON.put("city", strCity);
            addressJSON.put("state", strState);
            addressJSON.put("country", strCountry);
            addressJSON.put("zip", strZipcode);
            jsonParams.put("address", addressJSON);

            jsonParams.put("phone", strPhone);
            jsonParams.put("dealerId", strDealerId);
            jsonParams.put("oemId", strOemId);
            jsonParams.put("email", strEmail);
            jsonParams.put("spaId", SpaManager.m_ID);

            JSONArray roles = new JSONArray(arrRoles);
            jsonParams.put("roles", roles);
            jsonParams.put("username", strUsername);

            StringEntity entity = new StringEntity(jsonParams.toString());
            Log.e("DETAILS", "" + jsonParams.toString());

            client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
            client.put(context, url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onSuccess("success");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONObject response) {
                    callback.onFailure(String.valueOf(statusCode));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                    callback.onFailure(responseString);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestUnregister(final RestAPICallback callback) {

        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);
        String endpointForUnregister = BASE_URL + "/registerSpaToUser";
        client.delete(endpointForUnregister, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onSuccess(strTACText);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }
        });
    }

    public static void getDealerInfo(final RestAPICallback callback) {
        client.addHeader("Authorization", "Bearer " + TokenManager.accessToken);

        String url = BASE_URL + "/dealers/" + UserManager.strDealerId;
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onSuccess(response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d(TAG, responseString);
                callback.onFailure(responseString);
            }
        });
    }
}
