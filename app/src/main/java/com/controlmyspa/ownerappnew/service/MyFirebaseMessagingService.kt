package com.controlmyspa.ownerappnew.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.controlmyspa.ownerappnew.MainActivity
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.helper.SPA_STATE_UPDATED
import com.controlmyspa.ownerappnew.utils.Constants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

/**
 * Created by polaris on 1/25/17.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var payload: String = ""
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        // TODO: Implement this method to send any registration to your app's servers.
        UserManager.strDeviceToken = token

        Handler(Looper.getMainLooper()).post {
            UserManager.setDeviceToken(this, object : RestAPICallback {
                override fun onSuccess(result: String) {
                    Log.d(TAG, "successfully saved: $result")
                }

                override fun onFailure(result: String) {
                    Log.d(TAG, "device token server registration failed: $result")
                }
            })
        }

        saveTokenToPrefs(token)
        Log.d(" Firebase token", token)
    }

    private fun saveTokenToPrefs(_token: String) {
        // Access Shared Preferences
        val preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
        val editor = preferences.edit()
        // Save to SharedPreferences
        editor.putString(Constants.PREF_TOKEN, _token)
        editor.apply()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            val message = remoteMessage.data["message"]
            message?.let { msg ->
                if (msg.contains(SPA_STATE_UPDATED)) {
                    val intent = Intent(SPA_STATE_UPDATED)
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                }
            }
        }
        remoteMessage.notification?.let { notification ->
            Log.d(TAG, "Message Notification Body: " + notification.body)
            payload = notification.body.toString()
            sendNotification()
        }
    }

    private fun sendNotification() {
        val requestCode = Random().nextInt(8999) + 1000
        Log.d(TAG, "NOTIFICATION_ID = $requestCode")
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        /*new builder*/
        /**@author TIJO THOMAS
         * @issue Targeting S+ (version 31 and above) requires that one of FLAG_IMMUTABLE or FLAG_MUTABLE
         * be specified when creating a PendingIntent
         * @since 28-12-2021*/
        val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        } else
            PendingIntent.FLAG_UPDATE_CURRENT
        val pendingIntent = PendingIntent.getActivity(
            this, requestCode, intent,
            flags
        )
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val channelId = "Default"
        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setStyle(NotificationCompat.BigTextStyle().bigText(payload))
            .setContentTitle(resources.getString(R.string.app_name))
            .setContentText(payload)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mBuilder.setSmallIcon(R.drawable.ic_trans_notification)
        mBuilder.color = ContextCompat.getColor(this, R.color.colorPrimary)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId, "Default channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher)
        }
        manager.notify(requestCode, mBuilder.build())
    }

    companion object {
        // Here we need to implement the Push Notification Code.
        private const val TAG = "AppFireBaseService"
    }
}