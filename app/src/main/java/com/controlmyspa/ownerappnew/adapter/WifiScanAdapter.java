package com.controlmyspa.ownerappnew.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.model.WifiList;

import java.util.List;

public class WifiScanAdapter extends RecyclerView.Adapter<WifiScanAdapter.MyView> {

    /**
     * AP Channel bandwidth is 20 MHZ
     */
    private static final int CHANNEL_WIDTH_20MHZ = 0;
    /**
     * AP Channel bandwidth is 40 MHZ
     */
    private static final int CHANNEL_WIDTH_40MHZ = 1;
    /**
     * AP Channel bandwidth is 80 MHZ
     */
    private static final int CHANNEL_WIDTH_80MHZ = 2;
    /**
     * AP Channel bandwidth is 160 MHZ
     */
    private static final int CHANNEL_WIDTH_160MHZ = 3;
    /**
     * AP Channel bandwidth is 160 MHZ, but 80MHZ + 80MHZ
     */
    private static final int CHANNEL_WIDTH_80MHZ_PLUS_MHZ = 4;
    private List<WifiList> wifiList;
    private Context context;
    private OnItemClickListeners onItemClickListeners;


    public WifiScanAdapter(Context context, List<WifiList> wifiList, OnItemClickListeners listeners) {

        this.context = context;
        this.wifiList = wifiList;
        this.onItemClickListeners = listeners;

    }

    @NonNull
    @Override
    public MyView onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.wifi_list, viewGroup, false);

        final MyView viewHolder = new MyView(view);
        view.setOnClickListener(v -> onItemClickListeners.onItemClick(viewHolder.getAdapterPosition(), v, wifiList));


        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyView myView, int position) {

        /*setting all recyclerview items */
        WifiList device = wifiList.get(position);
        myView.vName.setText(device.getName());
        myView.vName.setSelected(true);
        myView.bSSID.setText(device.getBssid());
        myView.signalStrength.setText(device.getCapabilities());

        myView.signalStrength.setSelected(true);


        myView.ch_bandWidth.setText(getChannelBandWidth(device.getChannelWidth()));

        myView.tvIsConnected.setText(device.isConnectedWith() ? myView.itemView.getContext().getString(R.string.connected) : "");


        myView.dbm.setText(device.getLevel() + " dBm");

        int level = WifiManager.calculateSignalLevel(device.getLevel(), 5);

        if (level >= 4) {
            myView.img_strength.setImageResource(isNetworkOpenType(device.getCapabilities()) ?
                    R.drawable.ic_signal_wifi_4_bar_black_24dp : R.drawable.ic_signal_wifi_4_bar_lock_black_24dp);

        } else if (level == 3) {
            myView.img_strength.setImageResource(isNetworkOpenType(device.getCapabilities()) ?
                    R.drawable.ic_signal_wifi_3_bar_black_24dp : R.drawable.ic_signal_wifi_3_bar_lock_black_24dp);

        } else if (level == 2) {
            myView.img_strength.setImageResource(isNetworkOpenType(device.getCapabilities()) ?
                    R.drawable.ic_signal_wifi_2_bar_black_24dp : R.drawable.ic_signal_wifi_2_bar_lock_black_24dp);
        } else if (level == 1) {
            myView.img_strength.setImageResource(isNetworkOpenType(device.getCapabilities()) ?
                    R.drawable.ic_signal_wifi_1_bar_black_24dp : R.drawable.ic_signal_wifi_1_bar_lock_black_24dp);
        } else if (level == 0) {
            myView.img_strength.setImageResource(isNetworkOpenType(device.getCapabilities()) ?
                    R.drawable.ic_signal_wifi_0_bar_black_24dp : R.drawable.ic_signal_wifi_0_bar_black_24dp);
        }


    }

    @Override
    public int getItemCount() {
        return wifiList.size();
    }


    private String getChannelBandWidth(int channelWidth) {

        switch (channelWidth) {
            case CHANNEL_WIDTH_20MHZ:
                return "20 MHz";
            case CHANNEL_WIDTH_40MHZ:
                return "40 MHz";
            case CHANNEL_WIDTH_80MHZ:
                return "80 MHz";
            case CHANNEL_WIDTH_160MHZ:
            case CHANNEL_WIDTH_80MHZ_PLUS_MHZ:
                return "160 MHz";
            default:
                throw new IllegalArgumentException(
                        "translateScanResultChannelWidth: bad " + channelWidth);
        }
    }

    private Boolean isNetworkOpenType(String capabilities) {

        // WPA or WPA2 Network
        // Open Network
        if (capabilities.toUpperCase().contains("WEP")) {
            // WEP Network
            return false;
        } else return !capabilities.toUpperCase().contains("WPA")
                && !capabilities.toUpperCase().contains("WPA2");

    }

    public interface OnItemClickListeners {

        void onItemClick(int position, View v, List<WifiList> list);
    }

    static class MyView extends RecyclerView.ViewHolder {

        private ImageView img_strength;

        private TextView vName,
                signalStrength,
                bSSID,
                ch_bandWidth,
                tvIsConnected,
                dbm;

        MyView(@NonNull View itemView) {
            super(itemView);
            vName = itemView.findViewById(R.id.ssid_name);
            bSSID = itemView.findViewById(R.id.bssid);
            img_strength = itemView.findViewById(R.id.Wifilogo);
            signalStrength = itemView.findViewById(R.id.capabilities);
            ch_bandWidth = itemView.findViewById(R.id.ch_bandWdth);
            tvIsConnected = itemView.findViewById(R.id.tv_isconnected);
            dbm = itemView.findViewById(R.id.dbm);

        }
    }
}


