package com.controlmyspa.ownerappnew.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.ListItemPresetBinding
import com.controlmyspa.ownerappnew.model.SpaPreset
import java.util.*

class PresetListAdapter(private val items: ArrayList<SpaPreset>, private val listener: OnPresetActionListener) : RecyclerView.Adapter<PresetListAdapter.MyViewHolder>() {
    class MyViewHolder(val binding: ListItemPresetBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: ListItemPresetBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item_preset, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int = items.count()

    interface OnPresetActionListener {
        fun onPlay(position: Int)
        fun onDelete(position: Int)
        fun onSchedule(position: Int)
        fun onEdit(position: Int)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.presetListBtndelete.setOnClickListener { listener.onDelete(holder.adapterPosition) }
        holder.binding.presetListBtnedit.setOnClickListener { listener.onEdit(holder.adapterPosition) }
        holder.binding.presetListBtnplay.setOnClickListener { listener.onPlay(holder.adapterPosition) }
        holder.binding.presetListBtnschedule.setOnClickListener { listener.onSchedule(holder.adapterPosition) }
        holder.binding.data = items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}