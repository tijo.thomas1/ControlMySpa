package com.controlmyspa.ownerappnew.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.model.SpaComponent;
import com.controlmyspa.ownerappnew.model.SpaPreset;
import com.controlmyspa.ownerappnew.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EditPresetAdapter extends BaseAdapter {
    private static final Map<String, Integer> mapStatusResource = new HashMap<>();

    static {
        mapStatusResource.put("ON", R.drawable.ctrl_on);
        mapStatusResource.put("OFF", R.drawable.ctrl_off);
        mapStatusResource.put("DISABLED", R.drawable.ctrl_off);
        mapStatusResource.put("LOW", R.drawable.ctrl_low);
        mapStatusResource.put("MED", R.drawable.ctrl_med);
        mapStatusResource.put("HIGH", R.drawable.ctrl_hi);
    }

    private LayoutInflater mInflater;
    private SpaPreset mPreset;
    private ArrayList<SpaComponent> mDataSource;
    private boolean isCelsius;
    private TextView txtTemperature;

    public EditPresetAdapter(Context context, SpaPreset preset, boolean isCelsius) {
        mPreset = preset;
        mDataSource = preset.arrComponents;
        this.isCelsius = isCelsius;

        Collections.sort(mDataSource, (result1, result2) -> result1.componentType.compareTo(result2.componentType));
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0)
            return null;
        else
            return mDataSource.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private double roundHalf(double number) {
        double diff = number - (int) number;
        if (diff < 0.25) return (int) number;
        else if (diff < 0.75) return (int) number + 0.5;
        else return (int) number + 1;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //Temperature row
        if (position == 0) {
            convertView = mInflater.inflate(R.layout.list_item_edit_preset_temperature, parent, false);
            ImageButton btnPlus = convertView.findViewById(R.id.edit_preset_list_temperature_btnplus);
            ImageButton btnMinus = convertView.findViewById(R.id.edit_preset_list_temperature_btnminus);
            txtTemperature = convertView.findViewById(R.id.edit_preset_list_temperature);
            Log.e("isCelsius", "" + isCelsius);
            if (isCelsius)
                txtTemperature.setText(String.format(Locale.ENGLISH, "%.1f°", roundHalf(((mPreset.temperature) - 32) / 1.8)));
            else
                txtTemperature.setText(String.format(Locale.ENGLISH, "%f°", mPreset.temperature));

            btnPlus.setOnClickListener(v -> {
                Log.e("isCelsius", "" + isCelsius);
                if (mPreset.temperature != Constants.MAX_TEMPERATURE) {
                    mPreset.temperature += 1;
                    String format;
                    if (isCelsius) {
                        format = String.format(Locale.ENGLISH, "%.1f°", roundHalf(((mPreset.temperature) - 32) / 1.8));
                        txtTemperature.setText(format);
                        Log.e("isCelsius", "" + isCelsius + " " + format);
                    } else {
                        format = String.format(Locale.ENGLISH, "%f°", mPreset.temperature);
                        txtTemperature.setText(format);
                        Log.e("isCelsius", "" + isCelsius + " " + format);
                    }
                    notifyDataSetChanged();
                }

            });
            btnMinus.setOnClickListener(v -> {
                Log.e("isCelsius", "" + isCelsius);
                if (mPreset.temperature != Constants.MIN_TEMPERATURE) {
                    mPreset.temperature -= 1;
                    String format;
                    if (isCelsius) {
                        format = String.format(Locale.ENGLISH, "%.1f°", roundHalf(((mPreset.temperature) - 32) / 1.8));
                        txtTemperature.setText(format);
                        Log.e("isCelsius", "" + isCelsius + " " + format);
                    } else {
                        format = String.format(Locale.ENGLISH, "%d°", mPreset.temperature);
                        txtTemperature.setText(format);
                        Log.e("isCelsius", "" + isCelsius + " " + format);
                    }
                    notifyDataSetChanged();
                }
            });
            return convertView;
        }
        ViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            convertView = mInflater.inflate(R.layout.list_item_edit_preset, parent, false);
            holder = new ViewHolder();
            holder.txtName = convertView.findViewById(R.id.edit_preset_list_name);
            holder.btnPlus = convertView.findViewById(R.id.edit_preset_list_btnplus);
            holder.btnMinus = convertView.findViewById(R.id.edit_preset_list_btnminus);
            holder.imgState = convertView.findViewById(R.id.edit_preset_list_imgstate);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TextView txtName = holder.txtName;
        ImageButton btnPlus = holder.btnPlus;
        ImageButton btnMinus = holder.btnMinus;
        final ImageView imgState = holder.imgState;

        final SpaComponent component = (SpaComponent) getItem(position);
        btnPlus.setOnClickListener(v -> {
            if (component.availableValues.size() == 0) {
                return;
            }
            int statusIndex = component.availableValues.indexOf(component.value);
            statusIndex += 1;
            if (statusIndex >= component.availableValues.size()) {
                statusIndex -= component.availableValues.size();
            }
            if (component.componentType.equalsIgnoreCase("FILTER")) {
                statusIndex = component.availableValues.indexOf(component.value);
                statusIndex += 1;
                if (statusIndex >= 2) {
                    statusIndex -= 2;
                }
            }

            String value = component.availableValues.get(statusIndex);
            imgState.setImageResource(mapStatusResource.get(value));
            component.value = value;
        });
        btnMinus.setOnClickListener(v -> {
            if (component.availableValues.size() == 0) {
                return;
            }
            int statusIndex = component.availableValues.indexOf(component.value);
            statusIndex -= 1;
            if (statusIndex < 0) {
                statusIndex += component.availableValues.size();
            }
            if (component.componentType.equalsIgnoreCase("FILTER")) {
                statusIndex = component.availableValues.indexOf(component.value);
                statusIndex -= 1;
                if (statusIndex < 0) {
                    statusIndex += 2;
                }
            }
            String value = component.availableValues.get(statusIndex);
            imgState.setImageResource(mapStatusResource.get(value));
            component.value = value;
        });


        if (component.name == null)//if name is null, set component type as name
        {
            txtName.setText(component.componentType);
        } else {
            txtName.setText(component.name);
        }

        if (component.availableValues.size() != 0) {
            imgState.setImageResource(mapStatusResource.get(component.value));
        } else {
            //component without registeredTimeStamp property
            Log.d("TODO", "No registeredTimeStamp: set off by default");
            imgState.setImageResource(mapStatusResource.get("OFF"));
            btnMinus.setEnabled(false);
            btnPlus.setEnabled(false);
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView txtName;
        ImageButton btnPlus;
        ImageButton btnMinus;
        ImageView imgState;
    }

}
