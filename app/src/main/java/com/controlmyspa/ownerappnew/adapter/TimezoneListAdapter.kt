package com.controlmyspa.ownerappnew.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.AdapterTimezoneListBinding
import com.controlmyspa.ownerappnew.helper.TimezoneDataListener

class TimezoneListAdapter(private var mTimeZoneList: Array<String>, private val mListener: TimezoneDataListener) : RecyclerView.Adapter<TimezoneListAdapter.MyViewHolder>() {
    class MyViewHolder(val binding: AdapterTimezoneListBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: AdapterTimezoneListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_timezone_list, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int = mTimeZoneList.count()

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.mTextView.text = mTimeZoneList[position]

        holder.itemView.setOnClickListener {
            mListener.onTimezoneChanged(mTimeZoneList[holder.adapterPosition])
        }
    }

    fun filterData(tempIds: Array<String>) {
        this.mTimeZoneList = tempIds
        notifyDataSetChanged()
    }
}