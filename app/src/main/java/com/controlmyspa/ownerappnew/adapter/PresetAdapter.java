package com.controlmyspa.ownerappnew.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.model.SpaPreset;

import java.util.ArrayList;

public class PresetAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<SpaPreset> mDataSource;
    private OnPresetActionListener mListener;

    public PresetAdapter(Context context, ArrayList<SpaPreset> items, OnPresetActionListener listener) {
        mContext = context;
        mDataSource = items;
        mListener = listener;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /**
     * Sets the image button to the given state and grays-out the icon.
     *
     * @param enabled   The state of the button
     * @param item      The button item to modify
     * @param iconResId The button's icon ID
     */
    private static void setImageButtonEnabled(Context ctxt, boolean enabled,
                                              ImageButton item, int iconResId) {
        item.setEnabled(enabled);
        Drawable originalIcon = ContextCompat.getDrawable(ctxt, iconResId);
        Drawable icon = enabled ? originalIcon : convertDrawableToGrayScale(originalIcon);
        item.setImageDrawable(icon);
    }

    /**
     * Mutates and applies a filter that converts the given drawable to a Gray
     * image. This method may be used to simulate the color of disable icons in
     * Honeycomb's ActionBar.
     *
     * @return a mutated version of the given drawable with a color filter applied.
     */
    private static Drawable convertDrawableToGrayScale(Drawable drawable) {
        if (drawable == null)
            return null;

        Drawable res = drawable.mutate();
        res.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        return res;
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_preset, parent, false);
            holder = new ViewHolder();
            holder.txtName = convertView.findViewById(R.id.preset_list_name);
            holder.btnPlay = convertView.findViewById(R.id.preset_list_btnplay);
            holder.btnSchedule = convertView.findViewById(R.id.preset_list_btnschedule);
            holder.btnEdit = convertView.findViewById(R.id.preset_list_btnedit);
            holder.btnDelete = convertView.findViewById(R.id.preset_list_btndelete);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TextView txtName = holder.txtName;
        ImageButton btnPlay = holder.btnPlay;
        ImageButton btnSchedule = holder.btnSchedule;
        ImageButton btnEdit = holder.btnEdit;
        ImageButton btnDelete = holder.btnDelete;

        btnPlay.setOnClickListener(v -> mListener.onPlay(position));
        btnSchedule.setOnClickListener(v -> mListener.onSchedule(position));
        btnEdit.setOnClickListener(v -> mListener.onEdit(position));
        btnDelete.setOnClickListener(v -> mListener.onDelete(position));

        SpaPreset preset = (SpaPreset) getItem(position);
        txtName.setText(preset.strName);

        //Disable Edit, Delete if it's system preset. for example: turn off spa
        if (preset.strName.equals("Turn Off Spa")) {
            setImageButtonEnabled(mContext, false, btnEdit, R.drawable.ic_action_edit);
            setImageButtonEnabled(mContext, false, btnDelete, R.drawable.ic_action_delete);
        } else {
            setImageButtonEnabled(mContext, true, btnEdit, R.drawable.ic_action_edit);
            setImageButtonEnabled(mContext, true, btnDelete, R.drawable.ic_action_delete);
        }

        return convertView;
    }

    public interface OnPresetActionListener {
        void onPlay(int position);

        void onDelete(int position);

        void onSchedule(int position);

        void onEdit(int position);
    }

    private static class ViewHolder {
        TextView txtName;
        ImageButton btnPlay;
        ImageButton btnEdit;
        ImageButton btnSchedule;
        ImageButton btnDelete;
    }
}
