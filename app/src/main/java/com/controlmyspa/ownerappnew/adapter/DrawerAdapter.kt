package com.controlmyspa.ownerappnew.adapter

import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.AdapterDrawerBinding
import com.controlmyspa.ownerappnew.helper.KeyBoardHideListener

class DrawerAdapter(private val mIconsArray: TypedArray, private val mTitleArray: Array<String>, private val keyBoardHideListener: KeyBoardHideListener) : RecyclerView.Adapter<DrawerAdapter.MyViewHolder>() {

    private var selectedItemPosition = 0

    class MyViewHolder(val binding: AdapterDrawerBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: AdapterDrawerBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_drawer, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.mIconView.setImageDrawable(mIconsArray.getDrawable(position))
        holder.binding.mTitleView.text = mTitleArray[position]
        if (position == selectedItemPosition) {
            holder.itemView.setBackgroundResource(android.R.drawable.screen_background_dark_transparent)
        } else holder.itemView.setBackgroundResource(R.drawable.side_nav_bar)


        holder.itemView.setOnClickListener {
            keyBoardHideListener.onClickedDrawerItem(holder.adapterPosition)
        }
    }

    override fun getItemCount(): Int = mTitleArray.count()

     fun selectedItemPosition(position: Int) {
        this.selectedItemPosition = position
        notifyDataSetChanged()
    }
}