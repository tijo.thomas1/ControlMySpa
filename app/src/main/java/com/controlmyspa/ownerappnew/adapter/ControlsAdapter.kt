package com.controlmyspa.ownerappnew.adapter

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.RecyclerviewItemRowBinding
import com.controlmyspa.ownerappnew.helper.CHROMAZONE
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.SpaComponent

private const val TAG = "ControlsAdapter"

class ControlsAdapter(private val mListener: OnControlClickListener, private val dataList: MutableList<Components>) : RecyclerView.Adapter<ControlsAdapter.MyViewHolder>() {
    class MyViewHolder(val binding: RecyclerviewItemRowBinding) : RecyclerView.ViewHolder(binding.root)

    private var isTouchableFlag = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: RecyclerviewItemRowBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.recyclerview_item_row, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int = dataList.count()

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.component = dataList[position]
        holder.itemView.setOnClickListener {
            val spaComponent = dataList.getOrNull(holder.adapterPosition)
            spaComponent?.let {
                when (it.name) {
                    CHROMAZONE -> mListener.openCzPage()
                    holder.itemView.context.getString(R.string.CMS_panel_name) -> {
                        if (!isTouchableFlag) return@setOnClickListener
                        mListener.setPanelLockMode(spaComponent.value, holder)
                    }
                    else -> {
                        if (!isTouchableFlag) return@setOnClickListener
                        Log.d("RecyclerView", "CLICK: " + holder.adapterPosition)
                        if (spaComponent.componentType == "CIRCULATION_PUMP") {
                            Toast.makeText(holder.itemView.context, holder.itemView.context.getString(R.string.read_only), Toast.LENGTH_LONG).show()
                            Log.d(TAG, "read-only")
                    } else {
                        when (spaComponent.value) {
                            SpaComponent.ControlState.OFF -> when {
                                spaComponent.availableValues.contains("ON") -> {
                                    spaComponent.targetValue = SpaComponent.ControlState.ON
                                }
                                spaComponent.availableValues.contains("LOW") -> {
                                    spaComponent.targetValue = SpaComponent.ControlState.LOW
                                }
                                spaComponent.availableValues.contains("MED") -> {
                                    spaComponent.targetValue = SpaComponent.ControlState.MED
                                }
                                else -> {
                                    spaComponent.targetValue = SpaComponent.ControlState.HIGH
                                }
                            }
                            SpaComponent.ControlState.LOW -> if (spaComponent.availableValues.contains("MED")) {
                                spaComponent.targetValue = SpaComponent.ControlState.MED
                            } else if (spaComponent.availableValues.contains("HIGH") || spaComponent.availableValues.contains("HI")) {
                                spaComponent.targetValue = SpaComponent.ControlState.HIGH
                            } else {
                                spaComponent.targetValue = SpaComponent.ControlState.OFF
                            }
                            SpaComponent.ControlState.MED -> if (spaComponent.availableValues.contains("HIGH")) {
                                spaComponent.targetValue = SpaComponent.ControlState.HIGH
                            } else {
                                spaComponent.targetValue = SpaComponent.ControlState.OFF
                            }
                            SpaComponent.ControlState.ON, SpaComponent.ControlState.HIGH -> if (spaComponent.availableValues.contains("OFF")) {
                                spaComponent.targetValue = SpaComponent.ControlState.OFF
                            } else if (spaComponent.availableValues.contains("LOW")) {
                                spaComponent.targetValue = SpaComponent.ControlState.LOW
                            }
                            else -> {

                            }
                        }
                        spaComponent.value = spaComponent.targetValue
                        holder.binding.component = spaComponent
                            //Set Wait Animation
                            Glide.with(holder.itemView.context).asGif().fitCenter()
                                    .load(Uri.parse("file:///android_asset/loader.gif"))
                                    .signature(ObjectKey(System.currentTimeMillis().toString()))
                                    .into(holder.binding.rvImage)
                            isSomethingInProgress(true)
                            mListener.setControlState(holder.adapterPosition, holder, spaComponent)
                        }
                    }
                }
            }
        }
    }

    fun isSomethingInProgress(isInProgress: Boolean) {
        this.isTouchableFlag = !isInProgress
    }

    fun getIsSomethingInProgress(): Boolean = this.isTouchableFlag

    interface OnControlClickListener {
        fun setControlState(position: Int, holder: MyViewHolder, spaComponent: Components)
        fun setPanelLockMode(state: String, holder: MyViewHolder)
        fun openCzPage()
    }

}