package com.controlmyspa.ownerappnew.adapter

import android.bluetooth.le.ScanResult
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.AdapterNewDeviceListBinding
import com.controlmyspa.ownerappnew.helper.OnItemClickedListener

class BleDeviceAdapter(private val scanResultList: MutableList<ScanResult>, private val clickListener: OnItemClickedListener) : RecyclerView.Adapter<BleDeviceAdapter.MyViewHolder>() {
    class MyViewHolder(val binding: AdapterNewDeviceListBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: AdapterNewDeviceListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_new_device_list, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int = scanResultList.count()

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.spaSerialNumber.text = scanResultList[position].scanRecord?.deviceName
        holder.itemView.setOnClickListener {
            clickListener.onItemClicked(scanResultList[holder.adapterPosition])
        }
    }
}