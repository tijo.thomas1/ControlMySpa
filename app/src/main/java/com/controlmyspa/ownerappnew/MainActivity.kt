package com.controlmyspa.ownerappnew

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.controlmyspa.ownerappnew.adapter.DrawerAdapter
import com.controlmyspa.ownerappnew.base.BaseActivity
import com.controlmyspa.ownerappnew.databinding.ActivityMainBinding
import com.controlmyspa.ownerappnew.fragment.*
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.UserManager
import com.controlmyspa.ownerappnew.utils.Constants
import com.github.pwittchen.swipe.library.rx2.Swipe
import com.github.pwittchen.swipe.library.rx2.SwipeEvent
import com.google.android.material.navigation.NavigationView
import com.google.android.play.core.review.ReviewManagerFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, KeyBoardHideListener, SwipeListener {
    private lateinit var mBinding: ActivityMainBinding
    private var mSwipe: Swipe = Swipe(300, 350)
    private val mCompositeDisposableSwipe = CompositeDisposable()
    private var mAdapter: DrawerAdapter? = null
    private var countTempPageEntry: Int = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(mBinding.appBarMain.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val disposable = mSwipe.observe()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { swipeEvent: SwipeEvent -> handleSwipe(swipeEvent) }
        mCompositeDisposableSwipe.add(disposable)
        val toggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
                this, mBinding.drawerLayout, mBinding.appBarMain.toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                mBinding.appBarMain.mainView.translationX = slideOffset * drawerView.width
                mBinding.drawerLayout.bringChildToFront(drawerView)
                mBinding.drawerLayout.requestLayout()
                mBinding.drawerLayout.setScrimColor(Color.TRANSPARENT)
                if (slideOffset == 1f) hideKeyboard()
            }
        }
        mBinding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        mBinding.navView.setNavigationItemSelectedListener(this)

        // Setting isLogout Flag to false, so that when user kills the app and reopens it. It should login automatically.
        val preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
        val loginPrefsEditor = preferences.edit()
        loginPrefsEditor.putBoolean(Constants.PREF_IS_LOGOUT, false)
        loginPrefsEditor.apply()


        /*Dummy call for Controls screen*/
        SpaManager.requestFindByUsername(this, object : RestAPICallback {
            override fun onSuccess(result: String?) {
            }

            override fun onFailure(result: String?) {
            }

        })

        val mDrawerTitleList = resources?.getStringArray(R.array.mDrawerTitleArray)
        val mDrawerIconsList = resources?.obtainTypedArray(R.array.mDrawerIconsArray)
        mAdapter = mDrawerTitleList?.let { mDrawerIconsList?.let { itIcon -> DrawerAdapter(itIcon, it, this) } }
        mBinding.mDrawerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mAdapter
        }
        //Set initial Fragment
        // mBinding.navView.setCheckedItem(R.id.nav_temperature)
        // onNavigationItemSelected(mBinding.navView.menu.findItem(R.id.nav_temperature))
        onClickedDrawerItem(0)

    }

    private fun handleSwipe(swipeEvent: SwipeEvent) {
        Log.e(TAG, "" + visibleFragment)
        when (swipeEvent) {
            SwipeEvent.SWIPED_LEFT -> {
                if (visibleFragment.toString().contains("NewControlsFragment")) onClickedDrawerItem(0)
            }
            SwipeEvent.SWIPED_RIGHT -> {
                if (visibleFragment.toString().contains("NewTempFragment")) onClickedDrawerItem(1)
            }
            else -> {

            }
        }
    }

    private val visibleFragment: Fragment?
        get() {
            val fragmentManager = this@MainActivity.supportFragmentManager
            val fragments = fragmentManager.fragments
            for (fragment in fragments) {
                if (fragment != null && fragment.isVisible) return fragment
            }
            return null
        }

    override fun onDestroy() {
        mCompositeDisposableSwipe.dispose()
        super.onDestroy()
    }

    override fun onBackPressed() {

        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (countTempPageEntry > 4)
                requestReviewFromUser(false)
            else super.onBackPressed()
        }
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) view = View(this)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun positionNameWithLanguageId(): String {
        return when (Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)) {
            "fr", "it", "de" -> {
                getString(R.string.whos_spa) + " " + org.apache.commons.text.WordUtils.capitalize(UserManager.strFirstName)
            }
            else -> {
                org.apache.commons.text.WordUtils.capitalize(UserManager.strFirstName) + " " + getString(R.string.whos_spa)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        hideKeyboard()
        val fragment: Fragment
        val fragmentManager = supportFragmentManager
        when (item.itemId) {
            R.id.nav_temperature -> {
                fragment = NewTempFragment()
                if (!TextUtils.isEmpty(UserManager.strFirstName))
                    mBinding.appBarMain.tvHeader.text = positionNameWithLanguageId()
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_controls -> {
                fragment = NewControlsFragment()
                mBinding.appBarMain.tvHeader.setText(R.string.control_my_spa_navigation)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_account -> {
                fragment = DetailsFragment.newInstance()
                mBinding.appBarMain.tvHeader.setText(R.string.spa_account_settings)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_presets -> {
                fragment = PresetsFragment.newInstance()
                mBinding.appBarMain.tvHeader.setText(R.string.schedules)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_time -> {
                fragment = SetTimeFragment.newInstance()
                mBinding.appBarMain.tvHeader.setText(R.string.your_spa_clock)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_filter_cycle -> {
                fragment = FilterCycleFragment.newInstance()
                mBinding.appBarMain.tvHeader.setText(R.string.filter_cycle)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_contact_your_dealer -> {
                fragment = ContactDealerFragment.newInstance()
                mBinding.appBarMain.tvHeader.setText(R.string.contact_us)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit()
            }
            R.id.nav_privacy_policy -> {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PRIVACY_POLICY))
                startActivity(intent)
                return false
            }
            R.id.nav_logout -> {
                onLogOut()
                return true
            }
        }
        mBinding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    private fun onLogOut() {
        UserManager.strDeviceToken = ""
        baseCzDetails = null
        accountData = null
        UserManager.setDeviceToken(this, object : RestAPICallback {
            override fun onSuccess(result: String) {}
            override fun onFailure(result: String) {}
        })
        UserManager.init()
        SpaManager.init()
        val loginPreferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
        val loginPrefsEditor = loginPreferences.edit()
        loginPrefsEditor.putBoolean(Constants.PREF_IS_LOGOUT, true)
        loginPrefsEditor.apply()
        val intent = LoginActivity.getStartIntent(this)
        startActivity(intent)
        finish()
    }

    override fun bluetoothInitialised(hasBluetooth: Boolean) {}
    override fun locationPermissionGranted() {}
    override fun signInRequestSuccess() {}
    override fun hideKeyBoardIfNeeded() {
        hideKeyboard()
    }

    override fun openLoginPage() {
        val intent = LoginActivity.getStartIntent(this)
        startActivity(intent)
        finish()
    }

    override fun onClickedDrawerItem(position: Int) {
        hideKeyboard()
        val fragmentManager = supportFragmentManager
        if (position != 7)
            mAdapter?.selectedItemPosition(position)
        when (position) {
            0 -> {
                countTempPageEntry++
                if (!TextUtils.isEmpty(UserManager.strFirstName))
                    mBinding.appBarMain.tvHeader.text = positionNameWithLanguageId()
                fragmentManager.beginTransaction().replace(R.id.content_main, NewTempFragment()).commit()
            }
            1 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.control_my_spa_navigation)
                fragmentManager.beginTransaction().replace(R.id.content_main, NewControlsFragment()).commit()
            }
            2 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.spa_account_settings)
                fragmentManager.beginTransaction().replace(R.id.content_main, DetailsFragment.newInstance()).commit()
            }
            3 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.schedules)
                fragmentManager.beginTransaction().replace(R.id.content_main, PresetsFragment.newInstance()).commit()
            }
            4 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.your_spa_clock)
                fragmentManager.beginTransaction().replace(R.id.content_main, SetTimeFragment.newInstance()).commit()
            }
            5 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.filter_cycle)
                fragmentManager.beginTransaction().replace(R.id.content_main, FilterCycleFragment.newInstance()).commit()
            }
            6 -> {
                mBinding.appBarMain.tvHeader.setText(R.string.contact_us)
                fragmentManager.beginTransaction().replace(R.id.content_main, ContactDealerFragment.newInstance()).commit()
            }
            7 -> {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PRIVACY_POLICY))
                startActivity(intent)
            }
            8 -> {
                requestReviewFromUser(true)
            }
        }
        mBinding.drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun onChangeTemperatureFragment() {
        if (!mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.navView.setCheckedItem(R.id.nav_temperature)
            onNavigationItemSelected(mBinding.navView.menu.findItem(R.id.nav_temperature))
        }
    }

    override fun onChangeControlsFragment() {
        if (!mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.navView.setCheckedItem(R.id.nav_controls)
            onNavigationItemSelected(mBinding.navView.menu.findItem(R.id.nav_controls))
        }
    }

    override fun drawerLocked() {
        //mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        //mBinding.navView.isEnabled = false
    }

    override fun drawerUnLocked() {
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        mBinding.navView.isEnabled = true
    }

    override fun requestServiceStarted() {
        showMailProgress()
        supportFragmentManager.beginTransaction().replace(R.id.content_main, DetailsFragment.newInstance(true)).commit()
    }

    override fun requestServiceCompleted() {
        mBinding.navView.setCheckedItem(R.id.nav_contact_your_dealer)
        onNavigationItemSelected(mBinding.navView.menu.findItem(R.id.nav_contact_your_dealer))
        hideMailProgress()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        mSwipe.dispatchTouchEvent(ev)
        return super.dispatchTouchEvent(ev)
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    private fun requestReviewFromUser(isLogOut: Boolean) {
        val manager = ReviewManagerFactory.create(this)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { rq ->
            if (rq.isSuccessful) {
                Log.e("REVIEW", "ReviewInfo")
                // We got the ReviewInfo object
                val reviewInfo = rq.result
                val flow = manager.launchReviewFlow(this, reviewInfo)
                flow.addOnCompleteListener {
                    Log.e("REVIEW", "$it")
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                    if (!isLogOut)
                        super.onBackPressed()
                    else onLogOut()
                }
            } else {
                if (!isLogOut)
                    super.onBackPressed()
                else onLogOut()
            }
        }
    }
}