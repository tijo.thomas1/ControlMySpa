package com.controlmyspa.ownerappnew.gatewayultra;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSetup3Binding;
import com.controlmyspa.ownerappnew.helper.AppConstantsKt;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.AccountSetupActivity;
import com.controlmyspa.ownerappnew.utils.Constants;

public class NoSpaDetectedActivity extends AppCompatActivity {
    private ActivityGwuSetup3Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_setup3);
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        extraData();
        if (LoginActivity.signal_strength) {
            binding.textView5.setVisibility(View.GONE);
            binding.btnManualSetup.setText(getString(R.string.cancel));
        }
        binding.btnHelp.setOnClickListener(view -> {
            Intent intent = new Intent(this, SetupHelpActivity.class);
            startActivity(intent);
        });
        binding.btnManualSetup.setOnClickListener(view -> {
            if (!LoginActivity.signal_strength) {
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();
                loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, false);
                loginPrefsEditor.apply();
                Intent intent = new Intent(this, AccountSetupActivity.class);
                startActivity(intent);
                finish();
            } else {
                startActivity(LoginActivity.getStartIntent(this));
            }
        });
        binding.btnTryAgain.setOnClickListener(view -> onBackPressed());
    }
    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }
    @SuppressLint("SetTextI18n")
    private void extraData() {
        boolean isMultipleTimes = getIntent().getBooleanExtra(AppConstantsKt.IS_MULTIPLE, false);
        if (isMultipleTimes) {
            binding.textView6.setText(getString(R.string.no_spa_detected) + "\n" + getString(R.string.multiple_failure));
            binding.textView7.setText(getString(R.string.multiple_failure_msg));
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
