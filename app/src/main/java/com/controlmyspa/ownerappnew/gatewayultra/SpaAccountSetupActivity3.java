package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSetupAccount3Binding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.PasswordStrengthCalculator;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

import org.json.JSONException;
import org.json.JSONObject;

public class SpaAccountSetupActivity3 extends AppCompatActivity implements View.OnClickListener {

    AlertDialog.Builder builder;

    private DealerData dealerData;
    private ActivityGwuSetupAccount3Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_setup_account3);
        setSupportActionBar(binding.mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initViews();
        PasswordStrengthCalculator passwordStrengthCalculator = new PasswordStrengthCalculator();
        binding.edtpassword.addTextChangedListener(passwordStrengthCalculator);
        passwordStrengthCalculator.getLowerCase().observe(this, integer -> displayPasswordSuggestions(integer, binding.textView9));
        passwordStrengthCalculator.getUpperCase().observe(this, integer -> displayPasswordSuggestions(integer, binding.textView9));
        passwordStrengthCalculator.getDigit().observe(this, integer -> displayPasswordSuggestions(integer, binding.editText2));
        passwordStrengthCalculator.getSpecialChar().observe(this, integer -> displayPasswordSuggestions(integer, binding.editText3));
        passwordStrengthCalculator.getMinLength().observe(this, integer -> displayPasswordSuggestions(integer, binding.editText1));
    }

    private void displayPasswordSuggestions(int value, TextView textView) {
        if (value == 1) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_done, 0, 0, 0);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }
    }

    private void initViews() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        binding.btnNext.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);
        builder = new AlertDialog.Builder(this);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_next:
                String password = binding.edtpassword.getText().toString().trim();
                final String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{7,}$";

                if (password.matches(pattern)) {
                    dealerData.setPassword(binding.edtpassword.getText().toString().trim());
                    sendDealerData();

                } else {
                    builder.setMessage(getString(R.string.password_did_not_meet_rules))
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, (dialog, id) -> dialog.cancel());
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                break;

            case R.id.btn_cancel:
                Intent intent = LoginActivity.getStartIntent(this);
                startActivity(intent);
                finish();

                break;
            default:
                break;
        }
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setCancelable(false);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    private void sendDealerData() {
        binding.pbConnect.setVisibility(View.VISIBLE);
        NetworkManager.saveDealerData(SpaAccountSetupActivity3.this, new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.pbConnect.setVisibility(View.GONE);
                Intent intent = new Intent(SpaAccountSetupActivity3.this, SpaAccountSetupActivity4.class);
                intent.putExtra("password", binding.edtpassword.getText().toString());
                startActivity(intent);
            }

            @Override
            public void onFailure(String result) {
                binding.pbConnect.setVisibility(View.GONE);
                if (!result.equals("Failure")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        showOkDialog(jsonObject.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}



