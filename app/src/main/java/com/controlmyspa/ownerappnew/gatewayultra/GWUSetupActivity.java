package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.BleScannerActivity;
import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSetup1Binding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

public class GWUSetupActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityGwuSetup1Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_setup1);
        initViews();
    }

    private void initViews() {
        binding.btnNext.setOnClickListener(this);
        if (LoginActivity.signal_strength) binding.textView5.setVisibility(View.GONE);
    }

    public void onBackPressed(View view) {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        if (view.getId() == R.id.btn_next) {
            intent = new Intent(this, BleScannerActivity.class);
            startActivity(intent);
            finish();
        }
    }
}

