package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;

public class AccountSetupSuccessful extends AppCompatActivity implements View.OnClickListener {
    Button btnHome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gwu_account_setup_successful);
        initViews();
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initViews() {
        btnHome = findViewById(R.id.btn_home);
        btnHome.setOnClickListener(this);
        if (OwnerApplication.getInstance().getIsDirectSetup()) {
            btnHome.setText(getString(R.string.home));
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_home) {
            if (OwnerApplication.getInstance().getIsDirectSetup()) {
                OwnerApplication.getInstance().setIsDirectSetup(false);
                Intent intent = LoginActivity.getStartIntent(this);
                startActivity(intent);
            } else {
                OwnerApplication.getInstance().setNormalSetup(true);
                Intent intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                startActivity(intent);
            }
        }
    }
}