package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuMultiplespasDetectedBinding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;

public class MultipleSpasDetectedActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityGwuMultiplespasDetectedBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_multiplespas_detected);
        setSupportActionBar(binding.mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initViews();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initViews() {

        binding.btnCancel.setOnClickListener(this);
        binding.spaId.setOnClickListener(this);
        binding.spaId1.setOnClickListener(this);
        binding.spaId2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.btn_cancel:
                onBackPressed();
                break;

            case R.id.spa_id:
            case R.id.spa_id1:
            case R.id.spa_id2:
                OwnerApplication.getInstance().setSpaSetup(true);
                OwnerApplication.getInstance().setNormalSetup(false);
                OwnerApplication.getInstance().setDealer(false);
                intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                startActivity(intent);

                break;

            default:
                break;
        }
    }
}
