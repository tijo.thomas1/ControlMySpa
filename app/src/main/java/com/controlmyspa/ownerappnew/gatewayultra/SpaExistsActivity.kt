package com.controlmyspa.ownerappnew.gatewayultra

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.LoginActivity
import com.controlmyspa.ownerappnew.OwnerApplication
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.ActivitySpaExistsBinding
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.PrefManager
import com.controlmyspa.ownerappnew.helper.Utility
import com.controlmyspa.ownerappnew.utils.Constants

class SpaExistsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySpaExistsBinding
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_spa_exists)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val pref = PrefManager(this)
        val serialNumber = pref.getSerialNumber()
        binding.spaSerialNumber.text = "${getString(R.string.gateway_serial_number)}\n$serialNumber"

        binding.btnCancel.setOnClickListener {
            startActivity(LoginActivity.getStartIntent(this))
        }
        binding.btnForgot.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW,
                    Uri.parse(Constants.FORGOT_PASSWORD_URL))
            startActivity(browserIntent)
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
