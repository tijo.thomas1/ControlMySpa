package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.BleScannerActivity;
import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSetupHelp2Binding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

public class SetupHelpActivity2 extends AppCompatActivity implements View.OnClickListener {
    private ActivityGwuSetupHelp2Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_setup_help2);
        setSupportActionBar(binding.mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initViews();
    }

    private void initViews() {
        binding.btnNext.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);

        if (LoginActivity.signal_strength) {
            binding.textView5.setText(getString(R.string.help));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.btn_next:
                intent = new Intent(this, BleScannerActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.btn_cancel:
                startActivity(LoginActivity.getStartIntent(this));
                finish();
                break;

            default:
                break;
        }
    }
}
