package com.controlmyspa.ownerappnew.gatewayultra

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.OwnerApplication
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSetupBinding
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.Utility
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class BluetoothRequiredActivity : AppCompatActivity(), View.OnClickListener {
    private val binding: ActivityGwuSetupBinding by lazy {
        DataBindingUtil.setContentView(
            this,
            R.layout.activity_gwu_setup
        )
    }
    private val bluetoothManager by lazy { getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager }
    private val bleAdapter by lazy { bluetoothManager.adapter }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(binding.mToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        initViews()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    @RequiresApi(Build.VERSION_CODES.S)
    @NeedsPermission(android.Manifest.permission.BLUETOOTH_CONNECT)
    fun bluetoothConnect() {
        requestBleAllowPopup()
    }

    private fun requestBleAllowPopup() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
    }

    private fun initViews() {
        binding.btnNext.setOnClickListener(this)
        if (bleAdapter.isEnabled) {
            binding.switch1.isChecked = true
            val intent = Intent(this, GWUSetupActivity::class.java)
            startActivity(intent)
            finish()
        }
        binding.switch1.setOnCheckedChangeListener { _: CompoundButton?, _: Boolean ->
            if (!bleAdapter.isEnabled) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    bluetoothConnectWithPermissionCheck()
                } else requestBleAllowPopup()
            } else {
                bleAdapter.disable()
                Toast.makeText(
                    applicationContext,
                    getString(R.string.ble_turned_off),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            binding.switch1.isChecked = true
            Toast.makeText(applicationContext, getString(R.string.ble_turned_on), Toast.LENGTH_LONG)
                .show()
        } else binding.switch1.isChecked = false
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(
            MyContextWrapper.wrap(
                newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)
            )
        )
    }

    fun on(v: View?) {
        if (!bleAdapter.isEnabled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                bluetoothConnectWithPermissionCheck()
            } else requestBleAllowPopup()
        } else {
            bleAdapter.disable()
            Toast.makeText(
                applicationContext,
                getString(R.string.ble_turned_off),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onClick(view: View) {
        val intent: Intent
        if (view.id == R.id.btn_next) {
            if (bleAdapter.isEnabled) {
                intent = Intent(this, GWUSetupActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.turn_on_ble),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    companion object {
        private const val REQUEST_ENABLE_BT = 1001
    }
}