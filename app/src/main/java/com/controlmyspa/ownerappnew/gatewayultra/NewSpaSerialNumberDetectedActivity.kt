package com.controlmyspa.ownerappnew.gatewayultra

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.base.BaseActivity
import com.controlmyspa.ownerappnew.LoginActivity
import com.controlmyspa.ownerappnew.OwnerApplication
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.api.*
import com.controlmyspa.ownerappnew.databinding.ActivityNewSpaSerialNumberDetectedBinding
import com.controlmyspa.ownerappnew.helper.INTENT_SERIAL_NO
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.PrefManager
import com.controlmyspa.ownerappnew.helper.Utility
import com.controlmyspa.ownerappnew.model.DealerData
import com.controlmyspa.ownerappnew.model.ValidateUsername
import com.controlmyspa.ownerappnew.spasetup.cmscode.CmsCodeSetupOne
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewSpaSerialNumberDetectedActivity : BaseActivity() {
    override fun bluetoothInitialised(hasBluetooth: Boolean) {

    }

    override fun signInRequestSuccess() {

    }

    override fun locationPermissionGranted() {

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    private var mDealerData: DealerData? = null
    var serialNumber: String = ""
    private lateinit var binding: ActivityNewSpaSerialNumberDetectedBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_spa_serial_number_detected)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mDealerData = OwnerApplication.getInstance().dealerData

        binding.btnNo.setOnClickListener {
            val intent = LoginActivity.getStartIntent(this)
            startActivity(intent)
            finish()
        }
        binding.btnYes.setOnClickListener {
            checkSerialNumberAccountExists(true)
        }
        binding.btnSpaSalespersonDealer.setOnClickListener {
            checkSerialNumberAccountExists(false)
        }
        serialNumber = intent?.getStringExtra(INTENT_SERIAL_NO).toString()
        val pref = PrefManager(this)
        pref.addSerialNumber(serialNumber)

        binding.spaSerialNumber.text = "${getString(R.string.gateway_serial_number)}\n$serialNumber"
    }

    private fun checkSerialNumberAccountExists(bool: Boolean) {
        binding.progressBar.visibility = View.VISIBLE
        val apiService = ApiClient.getClient()?.create(ApiInterface::class.java)
        val jsonObject = JsonObject()
        jsonObject.addProperty(SERIAL, serialNumber)
        val responseCall: Call<ValidateUsername>? = apiService?.checkSpaExists(SPA_VERIFY, jsonObject)
        responseCall?.enqueue(object : Callback<ValidateUsername> {
            override fun onFailure(call: Call<ValidateUsername>, t: Throwable) {
                binding.progressBar.visibility = View.GONE
                Snackbar.make(binding.root, t.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ValidateUsername>, response: Response<ValidateUsername>) {
                binding.progressBar.visibility = View.GONE
                if (response.isSuccessful) {
                    val validateUsername = response.body()
                    if (validateUsername?.message?.equals(SPA_EXISTS_MSG)!!) {
                        openSpaExistsActivity()
                    } else {
                        OwnerApplication.getInstance().isDirectSetup = true
                        if (bool) {
                            mDealerData?.isDealerAccount = false
                            OwnerApplication.getInstance().setIsDemo(false)
                            startCmsCodeActivity()
                        } else {
                            OwnerApplication.getInstance().setIsDemo(true)
                            mDealerData?.isDealerAccount = true
                            startActivity(Intent(this@NewSpaSerialNumberDetectedActivity,
                                    CmsCodeSetupOne::class.java))
                        }
                    }
                } else {
                    if (bool) {
                        mDealerData?.isDealerAccount = false
                        OwnerApplication.getInstance().setIsDemo(false)
                        startCmsCodeActivity()
                    } else {
                        mDealerData?.isDealerAccount = true
                        OwnerApplication.getInstance().setIsDemo(true)
                        startActivity(Intent(this@NewSpaSerialNumberDetectedActivity,
                                CmsCodeSetupOne::class.java))
                    }
                }
            }

        })

    }

    private fun openSpaExistsActivity() {
        val intent = Intent(this@NewSpaSerialNumberDetectedActivity, SpaExistsActivity::class.java)
        startActivity(intent)
    }

    private fun startCmsCodeActivity() {
        val intent = Intent(this, CmsCodeSetupOne::class.java)
        intent.putExtra("fromnewspa", "fromnewspa")
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            setResult(Activity.RESULT_OK)
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
