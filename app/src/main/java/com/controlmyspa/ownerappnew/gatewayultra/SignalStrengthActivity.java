package com.controlmyspa.ownerappnew.gatewayultra;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.base.BaseActivity;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuSignalStrengthBinding;
import com.controlmyspa.ownerappnew.helper.AppConstantsKt;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

import static com.controlmyspa.ownerappnew.helper.AppConstantsKt.INTENT_SERIAL_NO;

public class SignalStrengthActivity extends BaseActivity implements View.OnClickListener {
    private ActivityGwuSignalStrengthBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_signal_strength);
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        initViews();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {
        binding.signalStrength.setOnClickListener(this);
        binding.internetSignal.setOnClickListener(this);
        binding.btnHome.setOnClickListener(this);

        int cloudConnected = getIntent().getIntExtra(AppConstantsKt.INTENT_CLOUD_CONNECTED, -1);
        int signalStrength = getIntent().getIntExtra(AppConstantsKt.INTENT_SIGNAL_STRENGTH, -1);
        if (cloudConnected == 1) {
            binding.setInternetSignalDrawable(ContextCompat.getDrawable(this, R.drawable.a));
            binding.tvNetsignalText.setText(getString(R.string.connected));
        } else {
            binding.setInternetSignalDrawable(ContextCompat.getDrawable(this, R.drawable.b));
            binding.tvNetsignalText.setText(getString(R.string.not_connected));
            binding.btnHelp.setVisibility(View.GONE);
        }

        binding.btnHelp.setOnClickListener(v -> {

        });
        binding.btnSignalHelp.setOnClickListener(v -> {

        });
        switch (signalStrength) {
            case 0: {
                binding.signalStrength.setImageResource(R.drawable.b);
                binding.tvBluetoothText.setText(getString(R.string.not_connected));
                binding.btnSignalHelp.setVisibility(View.GONE);
            }
            break;
            case 1: {
                binding.signalStrength.setImageResource(R.drawable.poor);
                binding.tvBluetoothText.setText(getString(R.string.poor));
                binding.btnSignalHelp.setVisibility(View.GONE);
            }
            break;
            case 2: {
                binding.signalStrength.setImageResource(R.drawable.fair);
                binding.tvBluetoothText.setText(getString(R.string.fair));
            }
            break;
            case 3: {
                binding.signalStrength.setImageResource(R.drawable.good);
                binding.tvBluetoothText.setText(getString(R.string.good));
            }
            break;
            case 4: {
                binding.signalStrength.setImageResource(R.drawable.very_good);
                binding.tvBluetoothText.setText(getString(R.string.very_good));
            }
            break;
            case 5: {
                binding.signalStrength.setImageResource(R.drawable.exelent);
                binding.tvBluetoothText.setText(getString(R.string.excellent));
            }
            break;
        }

        String serialNumber = getIntent().getStringExtra(INTENT_SERIAL_NO);
        TextView spaSerial = findViewById(R.id.tv_spaid);
        spaSerial.setText(spaSerial.getText() + serialNumber);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(Activity.RESULT_OK);
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_home: {
                startActivity(LoginActivity.getStartIntent(this));
            }
            break;
            case R.id.btn_cancel:
                finish();
            case R.id.internet_signal:
            case R.id.signal_strength:
            default:
                break;
        }
    }

    @Override
    public void bluetoothInitialised(boolean hasBluetooth) {

    }

    @Override
    public void locationPermissionGranted() {

    }

    @Override
    public void signInRequestSuccess() {

    }
}

