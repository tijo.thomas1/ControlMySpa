package com.controlmyspa.ownerappnew.gatewayultra;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;

public class SpaAccountSetupActivity4 extends AppCompatActivity implements View.OnClickListener {
    Button btnNext, btnCancel;
    TextView txtUserid, txtPassword;
    String string = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gwu_setup_account4);
        initViews();
    }
    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {
        DealerData dealerData = OwnerApplication.getInstance().getDealerData();
        btnNext = findViewById(R.id.btn_next);
        btnCancel = findViewById(R.id.btn_cancel);
        txtUserid = findViewById(R.id.userid);
        txtPassword = findViewById(R.id.password);
        btnNext.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            string = bundle.getString("password");
        }
        if (string != null) {
            txtPassword.setText(getString(R.string.password) + " : " + string);
        }

        txtUserid.setText(getString(R.string.user_id) + dealerData.getEmail());
    }

    public void onBackPressed(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.btn_next:
                intent = new Intent(this, AccountSetupSuccessful.class);
                startActivity(intent);
                break;

            case R.id.btn_cancel:
                startActivity(LoginActivity.getStartIntent(this));
                finish();
                break;

            default:
                break;
        }
    }
}

