package com.controlmyspa.ownerappnew.gatewayultra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityGwuShowroomAccount3Binding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.PasswordStrengthCalculator;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

import org.json.JSONException;
import org.json.JSONObject;

public class ShowroomSpaAccountActivity3 extends AppCompatActivity implements View.OnClickListener {
    AlertDialog.Builder builder;
    private DealerData dealerData;

    private ActivityGwuShowroomAccount3Binding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_gwu_showroom_account3);
        initviews();
        dealerData = OwnerApplication.getInstance().getDealerData();

        mBinding.textView5.setText(dealerData.isDemo() ? getString(R.string.spa_temporary_account) : getString(R.string.account_setup));

        PasswordStrengthCalculator passwordStrengthCalculator = new PasswordStrengthCalculator();
        mBinding.edtpassword.addTextChangedListener(passwordStrengthCalculator);
        passwordStrengthCalculator.getLowerCase().observe(this, integer -> displayPasswordSuggestions(integer, mBinding.textView9));
        passwordStrengthCalculator.getUpperCase().observe(this, integer -> displayPasswordSuggestions(integer, mBinding.textView9));
        passwordStrengthCalculator.getDigit().observe(this, integer -> displayPasswordSuggestions(integer, mBinding.editText2));
        passwordStrengthCalculator.getSpecialChar().observe(this, integer -> displayPasswordSuggestions(integer, mBinding.editText3));
        passwordStrengthCalculator.getMinLength().observe(this, integer -> displayPasswordSuggestions(integer, mBinding.editText1));
    }

    private void displayPasswordSuggestions(int value, TextView textView) {
        if (value == 1) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_done, 0, 0, 0);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }
    }

    private void initviews() {
        mBinding.btnNext.setOnClickListener(this);
        mBinding.btnCancel.setOnClickListener(this);
        builder = new AlertDialog.Builder(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    public void onBackPressed(View view) {
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_next: {
                String password = mBinding.edtpassword.getText().toString().trim();
                final String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{7,}$";

                if (password.matches(pattern)) {
                    dealerData.setPassword(password);
                    sendDealerData();
                } else {
                    builder.setMessage(getString(R.string.password_did_not_meet_rules))
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, (dialog, id) -> dialog.cancel());

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    // alert.setTitle("AlertDialogExample");
                    alert.show();
                }
            }
            break;

            case R.id.btn_cancel:

                finish();

                break;

            default:
                break;
        }
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    private void sendDealerData() {
        mBinding.pbConnect.setVisibility(View.VISIBLE);
        //  btnConfirm.setEnabled(false);
        NetworkManager.saveDealerData(ShowroomSpaAccountActivity3.this, new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                mBinding.pbConnect.setVisibility(View.GONE);
                // btnConfirm.setEnabled(true);
               /* Intent intent = new Intent(SpaAccountSetupActivity3.this, SpaDealerSetupFive.class);
                startActivity(intent);*/
                Intent intent = new Intent(ShowroomSpaAccountActivity3.this, SpaAccountSetupActivity4.class);
                intent.putExtra("password", mBinding.edtpassword.getText().toString());
                startActivity(intent);
            }

            @Override
            public void onFailure(String result) {
                mBinding.pbConnect.setVisibility(View.GONE);
                //  btnConfirm.setEnabled(true);
                //showOkDialog(result);
                if (!result.equals("Failure")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        showOkDialog(jsonObject.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }  //showOkDialog("");

            }
        });
    }
}
