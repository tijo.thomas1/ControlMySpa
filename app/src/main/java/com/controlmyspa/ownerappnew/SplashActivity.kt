package com.controlmyspa.ownerappnew

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.controlmyspa.ownerappnew.databinding.ActivitySplashNewBinding
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.service.UserManager
import com.controlmyspa.ownerappnew.utils.Constants
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import java.io.File
import java.util.*


class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashNewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_new)
        currentCountryCode
        initControls()
        deviceFCMToken
    }

    /* Storing Country code by calling api */
    private val currentCountryCode: Unit
        get() { /* Storing Country code by calling api */
            SpaManager.requestToGetCurrentCountryCode(applicationContext, object : RestAPICallback {
                override fun onSuccess(result: String) {
                    try {
                        val sharedPreferences = getSharedPreferences(Constants.PREFS_LANGUAGE_STORE,
                                Context.MODE_PRIVATE)
                        val editor = sharedPreferences.edit()
                        val code = sharedPreferences.getString(Constants.PREF_COUNTRY_CODE, "")
                        val countryCode = if (TextUtils.isEmpty(code)) result.toLowerCase(Locale.ENGLISH) else code?.toLowerCase(Locale.ENGLISH)
                        when {
                            countryCode.equals("DE", ignoreCase = true) -> {
                                editor.putString(Constants.PREF_COUNTRY_CODE, countryCode)
                                editor.apply()
                            }
                            countryCode.equals("FR", ignoreCase = true) -> {
                                editor.putString(Constants.PREF_COUNTRY_CODE, countryCode)
                                editor.apply()
                            }
                            countryCode.equals("IT", ignoreCase = true) -> {
                                editor.putString(Constants.PREF_COUNTRY_CODE, countryCode)
                                editor.apply()
                            }
                            countryCode.equals("SE", ignoreCase = true) -> {
                                editor.putString(Constants.PREF_COUNTRY_CODE, countryCode)
                                editor.apply()
                            }
                            else -> { /* If no language code is matching with given languages ,
                                    set it english as a default language
                                    * */
                                editor.putString(Constants.PREF_COUNTRY_CODE, "en")
                                editor.apply()
                            }
                        }
                        SpaManager.COUNTRY_CODE = sharedPreferences.getString(Constants.PREF_COUNTRY_CODE, "")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(result: String) {
                }
            })
        }

    //finish firebase on complete
    private val deviceFCMToken: Unit
        get() { //finish firebase on complete
            FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener { task: Task<InstanceIdResult?> ->
                        if (!task.isSuccessful) {
                            Log.e(TAG, "getInstanceId failed", task.exception)
                            val sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
                            UserManager.strDeviceToken = sharedPreferences.getString(Constants.PREF_TOKEN,
                                    null)
                            return@addOnCompleteListener
                        }
                        val token = task.result?.token
                        Log.e(TAG, "token received:$token")
                        UserManager.strDeviceToken = token
                    }
        }

    private fun initControls() {
        val loadAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein)
        val file = File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "control_my_spa_dealer.png"
        )
        if (file.exists()) {
            val bmOptions = BitmapFactory.Options()
            val bitmap = BitmapFactory.decodeFile(file.absolutePath, bmOptions)
            Glide.with(this).load(bitmap).into(binding.ivAppIcon)
            binding.ivAppIcon.startAnimation(loadAnimation)
            Handler(Looper.getMainLooper()).postDelayed({
                val intent = LoginActivity.getStartIntent(this)
                startActivity(intent)
                finish()
            }, 4000)
        } else {
            binding.ivAppIcon.startAnimation(loadAnimation)
            Handler(Looper.getMainLooper()).postDelayed({
                val intent = LoginActivity.getStartIntent(this)
                startActivity(intent)
                finish()
            }, 4000)
        }
    }

    companion object {
        private const val TAG = "SplashActivity"
    }
}