package com.controlmyspa.ownerappnew.spasetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.VerifyEmailActivity;

public class NewSpaOwnerActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_spa_owner);
        initControls();
    }

    private void initControls() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        Button btnWelcome = findViewById(R.id.btnWelcome);
        btnWelcome.setOnClickListener(this);
        Button btnCmsCode = findViewById(R.id.btnCmsCode);
        btnCmsCode.setOnClickListener(this);
        Button btnNone = findViewById(R.id.btnNone);
        btnNone.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


    @Override
    public void onClick(View view) {

        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnWelcome:
                intent = new Intent(this, VerifyEmailActivity.class);
                break;
            case R.id.btnCmsCode:
                intent = new Intent(this, SetupOptionsActivity.class);
                break;
            case R.id.btnNone:
                intent = new Intent(this, NoneOfTheAboveActivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
    }


}
