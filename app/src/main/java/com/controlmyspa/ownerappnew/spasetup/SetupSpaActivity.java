package com.controlmyspa.ownerappnew.spasetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

public class SetupSpaActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_spa);
        initControls();
    }

    private void initControls() {
        Button btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent;

        if (view.getId() == R.id.btnClose) {
            intent = LoginActivity.getStartIntent(this);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }
}
