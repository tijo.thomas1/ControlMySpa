package com.controlmyspa.ownerappnew.spasetup.cmscode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.api.ApiClient;
import com.controlmyspa.ownerappnew.api.ApiInterface;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.model.ValidateUsername;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CmsCodeSetupTwo extends AppCompatActivity implements View.OnClickListener {

    String string = "";
    private EditText etFName;
    private EditText etLName;
    private EditText etEmail;
    private DealerData dealerData;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms_code_setup_two);
        initControls();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progressBar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeadText = findViewById(R.id.tvHeadText);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            string = bundle.getString("fromnewspa");
        }
        if (string != null) {
            if (string.equals("fromnewspa")) {
                tvHeadText.setText(getString(R.string.spa_account_setup));
            } else {
                tvHeadText.setText(getString(R.string.spa_temporary_account));
            }
        }

        if (dealerData.isDemo()) {
            tvHeadText.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeadText.setText(getString(R.string.spa_account_setup));
        }


        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        etFName = findViewById(R.id.etFName);
        etLName = findViewById(R.id.etLName);
        etEmail = findViewById(R.id.etEmail);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        String email = etEmail.getText().toString().trim();
        if (etFName.getText().toString().equals("")) {
            Toast.makeText(CmsCodeSetupTwo.this, R.string.please_enter_your_first_name, Toast.LENGTH_LONG).show();
        } else if (etLName.getText().toString().equals("")) {
            Toast.makeText(CmsCodeSetupTwo.this, R.string.please_enter_your_last_name, Toast.LENGTH_LONG).show();
        } else if (etEmail.getText().toString().equals("")) {
            Toast.makeText(CmsCodeSetupTwo.this, R.string.please_enter_your_email_address, Toast.LENGTH_LONG).show();
        } else if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            dealerData.setFirstName(etFName.getText().toString().trim());
            dealerData.setLastName(etLName.getText().toString().trim());
            validateEmail();
        } else {
            showOkDialog(getString(R.string.enter_valid_email));
        }
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        // adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()) {

            case R.id.btnNext:
                validate();
                break;

            case R.id.btnCancel:
                intent = LoginActivity.getStartIntent(this);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }


    private void validateEmail() {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = Objects.requireNonNull(ApiClient.INSTANCE.getClient()).create(ApiInterface.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", etEmail.getText().toString());
        Call<ValidateUsername> validateUsernameCall = apiInterface.getValidatedData(jsonObject);
        validateUsernameCall.enqueue(new Callback<ValidateUsername>() {
            @Override
            public void onResponse(@NonNull Call<ValidateUsername> call, @NonNull Response<ValidateUsername> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ValidateUsername validateUsername = response.body();
                    if (validateUsername != null) {
                        if (validateUsername.getMessage().contains("valid")) {
                            showOkDialog(getString(R.string.this_email_address_is_already_in_use_with_controlmyspa_please_choose_another_email));
                        } else {
                            sendDealSetupPage();
                        }
                    }

                } else {
                    try {
                        String dataError = null;
                        if (response.errorBody() != null) {
                            dataError = response.errorBody().string();
                        }
                        Gson gson = new Gson();
                        ValidateUsername validate = gson.fromJson(dataError, new TypeToken<ValidateUsername>() {
                        }.getType());
                        if (validate != null) {
                            if (validate.getMessage().contains("not")) {
                                sendDealSetupPage();
                            }
                        }
                        Log.e("ERROR", "ERROR " + validate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateUsername> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                showOkDialog(getString(R.string.this_email_address_is_already_in_use_with_controlmyspa_please_choose_another_email));
            }
        });


    }

    private void sendDealSetupPage() {
        dealerData.setEmail(etEmail.getText().toString().trim());
        Intent intent = new Intent(CmsCodeSetupTwo.this, CmsCodeSetupThree.class);
        if (string.equals("fromnewspa")) {
            intent.putExtra("fromnewspa", "fromnewspa");
        }
        startActivity(intent);
    }


}
