package com.controlmyspa.ownerappnew.spasetup.spadealer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;

import cz.msebera.android.httpclient.util.TextUtils;

public class SpaDealerSetupTwo extends AppCompatActivity implements View.OnClickListener {

    private EditText etFName;
    private EditText etLName;
    private EditText etEmail;
    private DealerData dealerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spa_dealer_setup_two);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeadText = findViewById(R.id.tvHeadText);
        String welcomeMsg = getString(R.string.welcome_to_controlmyspa_delearsetup) + getString(R.string.spa_temporary_account);
        tvHeadText.setText(welcomeMsg);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(this);
        etFName = findViewById(R.id.etFName);
        etLName = findViewById(R.id.etLName);
        etEmail = findViewById(R.id.etEmail);

    }

    private void validate() {
        if (TextUtils.isEmpty(etFName.getText().toString())) {
            Toast.makeText(SpaDealerSetupTwo.this, R.string.please_enter_your_first_name, Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(etLName.getText().toString())) {
            Toast.makeText(SpaDealerSetupTwo.this, R.string.please_enter_your_last_name, Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Toast.makeText(SpaDealerSetupTwo.this, R.string.please_enter_your_email_address, Toast.LENGTH_LONG).show();
        } else if (!Utility.isValidEmail(etEmail.getText().toString())) {
            Toast.makeText(SpaDealerSetupTwo.this, R.string.enter_valid_email, Toast.LENGTH_LONG).show();
        } else {
            dealerData.setFirstName(etFName.getText().toString().trim());
            dealerData.setLastName(etLName.getText().toString().trim());
            dealerData.setEmail(etEmail.getText().toString().trim());
            Intent intent = new Intent(SpaDealerSetupTwo.this, SpaDealerSetupThree.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnNext:
                validate();
                break;
            case R.id.btnHelp:
            default:
                break;
        }
    }

}
