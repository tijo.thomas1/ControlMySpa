package com.controlmyspa.ownerappnew.spasetup.cmscode;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.spasetup.spadealer.SpaDealerSetupFive;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class CmsCodeSetupFour extends AppCompatActivity implements View.OnClickListener {

    private Button btnConfirm;
    private ProgressBar pbConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms_code_setup_four);
        initControls();
    }

    private void initControls() {
        DealerData dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        btnConfirm = findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);
        Button btnGoBack = findViewById(R.id.btnGoBack);
        btnGoBack.setOnClickListener(this);
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(this);
        TextView tvEmail = findViewById(R.id.tvEmail);
        tvEmail.setText(dealerData.getEmail());
        pbConnect = findViewById(R.id.pbConnect);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    private void sendDealerData() {
        pbConnect.setVisibility(View.VISIBLE);
        btnConfirm.setEnabled(false);
        NetworkManager.saveDealerData(CmsCodeSetupFour.this, new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                pbConnect.setVisibility(View.GONE);
                btnConfirm.setEnabled(true);
                Intent intent = new Intent(CmsCodeSetupFour.this, SpaDealerSetupFive.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(String result) {
                pbConnect.setVisibility(View.GONE);
                btnConfirm.setEnabled(true);
                //showOkDialog(result);
                if (!result.equals("Failure")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        showOkDialog(jsonObject.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }  //showOkDialog("");

            }
        });
    }

    public boolean isInternetEnable() {

        NetworkInfo activeNetworkInfo = ((ConnectivityManager)
                Objects.requireNonNull(this.getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        } else return activeNetworkInfo.isConnectedOrConnecting();

    }

    @Override
    public void onClick(View view) {

        Intent intent;

        switch (view.getId()) {

            case R.id.btnConfirm:
                if (isInternetEnable()) {

                    runOnUiThread(this::sendDealerData);
                } else {
                    showOkDialog(getString(R.string.check_internet_connection));
                }
                /*intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();*/
                break;

            case R.id.btnGoBack:

                intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.btnHelp:
                break;

            default:
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }
}
