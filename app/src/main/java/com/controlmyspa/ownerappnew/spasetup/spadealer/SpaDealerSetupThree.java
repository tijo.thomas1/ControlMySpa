package com.controlmyspa.ownerappnew.spasetup.spadealer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;

public class SpaDealerSetupThree extends AppCompatActivity implements View.OnClickListener {

    private EditText etAddress1,etAddress2;
    private EditText etCity;
    private EditText etState;
    private EditText etPinCode;
    private EditText etCountry;
    private EditText etPhone;
    private DealerData dealerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spa_dealer_setup_three);
        initControls();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeadText = findViewById(R.id.tvHeadText);
        String welcomeMsg = getString(R.string.welcome_to_controlmyspa_delearsetup) + getString(R.string.spa_temporary_account);
        tvHeadText.setText(welcomeMsg);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(this);
        etAddress1 = findViewById(R.id.etAddress1);
         etAddress2 = findViewById(R.id.etAddress2);
        etCity = findViewById(R.id.etCity);
        etState = findViewById(R.id.etState);
        etPinCode = findViewById(R.id.etPinCode);
        etCountry = findViewById(R.id.etCountry);
        etPhone = findViewById(R.id.etPhone);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        if (etAddress1.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_your_address, Toast.LENGTH_LONG).show();
        } else if (etCity.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_your_city, Toast.LENGTH_LONG).show();
        } else if (etState.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_your_state, Toast.LENGTH_LONG).show();
        } else if (etPinCode.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_the_pin_code, Toast.LENGTH_LONG).show();
        } else if (etCountry.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_the_country, Toast.LENGTH_LONG).show();
        } else if (etPhone.getText().toString().equals("")) {
            Toast.makeText(SpaDealerSetupThree.this, R.string.please_enter_the_phone, Toast.LENGTH_LONG).show();
        } else {
            dealerData.setAddress1(etAddress1.getText().toString().trim());
            dealerData.setAddress2(etAddress2.getText().toString().trim().equals("") ? "" : etAddress2.getText().toString().trim());
            dealerData.setCity(etCity.getText().toString().trim());
            dealerData.setState(etState.getText().toString().trim());
            dealerData.setZip(etPinCode.getText().toString().trim());
            dealerData.setCountry(etCountry.getText().toString().trim());
            dealerData.setPhone(etPhone.getText().toString().trim());

            Intent intent = new Intent(this, SpaDealerSetupFour.class);
            startActivityForResult(intent, 101);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnNext:

                validate();

                break;

            case R.id.btnHelp:

            default:
                break;
        }
    }

}
