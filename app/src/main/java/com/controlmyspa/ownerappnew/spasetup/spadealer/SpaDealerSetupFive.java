package com.controlmyspa.ownerappnew.spasetup.spadealer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;

public class SpaDealerSetupFive extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spa_dealer_setup_five);
        initControls();
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvStatus = findViewById(R.id.tvStatus);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        /*Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("status_flag")) {
            if (bundle.getInt("status_flag") == 1) {
                tvStatus.setText(getString(R.string.dealer_setup_success));
            } else {
                tvStatus.setText(getString(R.string.dealer_setup_fail));
            }
        }*/
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnNext) {
            OwnerApplication.getInstance().setNormalSetup(true);
            Intent intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
            startActivity(intent);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

}
