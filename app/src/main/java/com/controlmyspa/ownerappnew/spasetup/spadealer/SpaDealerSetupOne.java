package com.controlmyspa.ownerappnew.spasetup.spadealer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

import java.util.Objects;

public class SpaDealerSetupOne extends AppCompatActivity implements View.OnClickListener {

    // private String dealerText = ;
    private EditText etCmsCode;
    private ProgressBar pbCmsCode;
    private DealerData dealerData;
    private boolean isDashAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spa_dealer_setup_one);
        initControls();
    }

    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeadText = findViewById(R.id.tvHeadText);
        String welcomeMsg = getString(R.string.welcome_to_controlmyspa_delearsetup) + getString(R.string.spa_temporary_account);
        tvHeadText.setText(welcomeMsg);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(this);
        etCmsCode = findViewById(R.id.etCmsCode);
        etCmsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 4) {
                    isDashAvailable = true;
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    if (isDashAvailable) {
                        isDashAvailable = false;
                        etCmsCode.setText(etCmsCode.getText().toString().substring(0, etCmsCode.getText().toString().length() - 1));
                        etCmsCode.setSelection(etCmsCode.getText().toString().length());
                    } else {
                        etCmsCode.setText(s + "-");
                        etCmsCode.setSelection(s.toString().length() + 1);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pbCmsCode = findViewById(R.id.pbCmsCode);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


    private void isCmsCodeValid() {
        NetworkManager.isCmsCodeValid(etCmsCode.getText().toString().trim().replace("'", "''"),
                new RestAPICallback() {
                    @Override
                    public void onSuccess(String result) {
                        pbCmsCode.setVisibility(View.GONE);
                        dealerData.setCmsCode(etCmsCode.getText().toString().trim());
                        Intent intent = new Intent(SpaDealerSetupOne.this, SpaDealerSetupTwo.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(String result) {
                        pbCmsCode.setVisibility(View.GONE);
                        showOkDialog(getResources().getString(R.string.your_cms_code_isnot_valid));
                    }
                });
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    public boolean isInternetEnable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) Objects.requireNonNull(this.getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void validate() {
        if (etCmsCode.getText().toString().trim().equals("")) {
            Toast.makeText(SpaDealerSetupOne.this, R.string.please_enter_your_cms_code, Toast.LENGTH_LONG).show();
        } else {
            pbCmsCode.setVisibility(View.VISIBLE);
            if (isInternetEnable()) {

                runOnUiThread(this::isCmsCodeValid);
            } else {
                pbCmsCode.setVisibility(View.GONE);
                showOkDialog(getResources().getString(R.string.your_device_is_not_connected_the_cloud_server));
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnNext:

                validate();
                break;

            case R.id.btnHelp:

            default:
                break;
        }
    }
}
