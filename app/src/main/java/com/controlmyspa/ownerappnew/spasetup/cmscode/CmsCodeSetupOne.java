package com.controlmyspa.ownerappnew.spasetup.cmscode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.api.ApiClient;
import com.controlmyspa.ownerappnew.api.ApiInterface;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.model.ValidateUsername;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CmsCodeSetupOne extends AppCompatActivity implements View.OnClickListener {

    private String string = "";
    private EditText etCmsCode;
    private ProgressBar pbCmsCode;
    private DealerData dealerData;
    private boolean isDashAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms_code_setup_one);
        initControls();
    }

    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeadText = findViewById(R.id.tvHeadText);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            string = bundle.getString("fromnewspa");
        }
        if (string != null) {
            if (string.equals("fromnewspa")) {
                tvHeadText.setText(getString(R.string.spa_account_setup));
            } else {
                tvHeadText.setText(getString(R.string.spa_temporary_account));
            }
        }

        if (dealerData.isDemo()) {
            tvHeadText.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeadText.setText(getString(R.string.spa_account_setup));
        }

        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        etCmsCode = findViewById(R.id.etCmsCode);
        etCmsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 4) {
                    isDashAvailable = true;
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    if (isDashAvailable) {
                        isDashAvailable = false;
                        etCmsCode.setText(etCmsCode.getText().toString().substring(0, etCmsCode.getText().toString().length() - 1));
                        etCmsCode.setSelection(etCmsCode.getText().toString().length());
                    } else {
                        etCmsCode.setText(s + "-");
                        etCmsCode.setSelection(s.toString().length() + 1);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pbCmsCode = findViewById(R.id.pbCmsCode);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void isCmsCodeValid() {
        pbCmsCode.setVisibility(View.VISIBLE);
        String cmsCode = etCmsCode.getText().toString().trim();
        ApiInterface apiService = Objects.requireNonNull(ApiClient.INSTANCE.getClient()).create(ApiInterface.class);
        Call<ValidateUsername> call = apiService.checkCmsCode(cmsCode);
        call.enqueue(new Callback<ValidateUsername>() {
            @Override
            public void onResponse(@NonNull Call<ValidateUsername> call, @NonNull Response<ValidateUsername> response) {
                pbCmsCode.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    dealerData.setCmsCode(cmsCode);
                    Intent intent = new Intent(CmsCodeSetupOne.this, CmsCodeSetupTwo.class);
                    if (string.equals("fromnewspa")) {
                        intent.putExtra("fromnewspa", "fromnewspa");
                    }
                    startActivity(intent);
                } else
                    showOkDialog(getResources().getString(R.string.Your_CMS_Code_is_Not_Valid_Please_Check_It_and_Try_Again));
            }

            @Override
            public void onFailure(@NonNull Call<ValidateUsername> call, @NonNull Throwable t) {
                pbCmsCode.setVisibility(View.GONE);
                showOkDialog(getResources().getString(R.string.Your_CMS_Code_is_Not_Valid_Please_Check_It_and_Try_Again));
            }
        });


       /* NetworkManager.isCmsCodeValid(etCmsCode.getText().toString().trim().replace("'", "''"),
                new RestAPICallback() {
                    @Override
                    public void onSuccess(String result) {
                        pbCmsCode.setVisibility(View.GONE);
                        dealerData.setCmsCode(etCmsCode.getText().toString().trim());
                        dealerData.setDemo(false);
                        if (string.equals("fromnewspa")) {
                            Intent intent = new Intent(CmsCodeSetupOne.this, CmsCodeSetupTwo.class);
                            intent.putExtra("fromnewspa", "fromnewspa");
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(CmsCodeSetupOne.this, CmsCodeSetupTwo.class);
                            startActivity(intent);
                        }

                    }

                    @Override
                    public void onFailure(String result) {
                        pbCmsCode.setVisibility(View.GONE);
                        showOkDialog(getResources().getString(R.string.Your_CMS_Code_is_Not_Valid_Please_Check_It_and_Try_Again));

                    }
                });*/
    }

    private void validate() {
        if (etCmsCode.getText().toString().trim().equals("")) {
            Toast.makeText(CmsCodeSetupOne.this, R.string.please_enter_your_cms_code, Toast.LENGTH_LONG).show();
        } else {
            pbCmsCode.setVisibility(View.VISIBLE);
            if (isInternetEnable()) {
                runOnUiThread(this::isCmsCodeValid);
            } else {
                pbCmsCode.setVisibility(View.GONE);
                showOkDialog(getResources().getString(R.string.your_device_is_not_connected_the_cloud_server));
            }
        }
    }

    public boolean isInternetEnable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) Objects.requireNonNull(this.getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.btnNext:

                validate();
                break;

            case R.id.btnCancel:
                intent = LoginActivity.getStartIntent(this);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }
}
