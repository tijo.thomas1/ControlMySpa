package com.controlmyspa.ownerappnew.spasetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.networksettings.GetCloseToSpaWithin10Feet;
import com.controlmyspa.ownerappnew.networksettings.VerifyEmailActivity;
import com.controlmyspa.ownerappnew.spasetup.spadealer.DealerWarningActivity;

public class SetupOptionsActivity extends AppCompatActivity implements View.OnClickListener {
    private DealerData mDealerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_options);
        initControls();
    }

    private void initControls() {
        mDealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        Button btnPreviousSpa = findViewById(R.id.btnPreviousSpa);
        btnPreviousSpa.setOnClickListener(this);
        Button btnNewOwner = findViewById(R.id.btnNewOwner);
        btnNewOwner.setOnClickListener(this);
        Button btnNewDealer = findViewById(R.id.btnNewDealer);
        btnNewDealer.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {

        Intent intent;

        switch (view.getId()) {
            case R.id.btnPreviousSpa:
                OwnerApplication.getInstance().setCmsCodeEmpty();
                mDealerData.setDealerAccount(false);
                intent = new Intent(this, VerifyEmailActivity.class);
                startActivity(intent);
                break;

            case R.id.btnNewOwner:
                OwnerApplication.getInstance().setSpaSetup(true);
                OwnerApplication.getInstance().setNormalSetup(false);
                OwnerApplication.getInstance().setDealer(false);
                OwnerApplication.getInstance().setIsDemo(false);
                mDealerData.setDealerAccount(false);
                intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                startActivity(intent);
                break;

            case R.id.btnNewDealer:
                OwnerApplication.getInstance().setSpaSetup(false);
                OwnerApplication.getInstance().setNormalSetup(false);
                OwnerApplication.getInstance().setDealer(true);
                OwnerApplication.getInstance().setIsDemo(true);
                mDealerData.setDealerAccount(true);
                intent = new Intent(this, DealerWarningActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


}
