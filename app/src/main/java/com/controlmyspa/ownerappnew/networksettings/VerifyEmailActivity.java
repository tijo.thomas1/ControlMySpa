package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

import static com.controlmyspa.ownerappnew.helper.Utility.isInternetConnectivity;

public class VerifyEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail;
    private ProgressBar pbEmail;
    private int errorCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        etEmail = findViewById(R.id.etEmail);
        pbEmail = findViewById(R.id.pbEmail);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    private void validate() {
        if (etEmail.getText().toString().equals("")) {
            showOkDialog(getResources().getString(R.string.please_enter_your_email_address));
        } else if (!Utility.isValidEmail(etEmail.getText().toString().trim())) {
            showOkDialog(getResources().getString(R.string.plz_enter_valid_email_with_format));
        } else {
            pbEmail.setVisibility(View.VISIBLE);
            if (isInternetConnectivity(getBaseContext())) {
                validateEmail();
            } else {
                errorCode = 1;
                goToNext();
            }
        }
    }

    private void validateEmail() {
        NetworkManager.verifyEmail(this, etEmail.getText().toString().trim().replace("'", "''"),
                new RestAPICallback() {
                    @Override
                    public void onSuccess(String result) {
                        errorCode = 3;
                        goToNext();
                    }

                    @Override
                    public void onFailure(String result) {

                        if (result.equals("Failure")) {
                            errorCode = 1;
                        } else {
                            errorCode = 2;
                        }
                        goToNext();
                    }
                });
    }

    private void goToNext() {
        pbEmail.setVisibility(View.GONE);
        Intent intent = new Intent(this, VerificationMessageActivity.class);
        intent.putExtra("errorCode", errorCode);
        startActivity(intent);
        if (errorCode != 1)
            finish();
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnNext) {
            validate();
        }
    }

}
