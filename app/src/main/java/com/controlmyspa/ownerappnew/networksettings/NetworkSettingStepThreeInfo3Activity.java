package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.spasetup.cmscode.CmsCodeSetupOne;
import com.controlmyspa.ownerappnew.spasetup.spadealer.SpaDealerSetupOne;

import java.util.Objects;

public class NetworkSettingStepThreeInfo3Activity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_three_info3);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount=findViewById(R.id.tvHeaderAccount);
        if (OwnerApplication.getInstance().getDealerData().isDemo()) {
            tvHeaderAccount.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeaderAccount.setText(getString(R.string.spa_account_setup));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isInternetEnable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) Objects.requireNonNull(this.getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void goToErrorPage() {
        Intent intent = new Intent(NetworkSettingStepThreeInfo3Activity.this,
                NetworkSettingStepThreeInfo4Activity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnNext) {
            if (isInternetEnable()) {
                runOnUiThread(() -> {
                    Intent intent;
                    if (OwnerApplication.getInstance().isSpaSetup()) {
                        intent = new Intent(NetworkSettingStepThreeInfo3Activity.this,
                                CmsCodeSetupOne.class);
                    } else {
                        intent = new Intent(NetworkSettingStepThreeInfo3Activity.this,
                                SpaDealerSetupOne.class);
                    }
                    startActivity(intent);
                });
            } else {
                goToErrorPage();
            }
        }
    }
}