package com.controlmyspa.ownerappnew.networksettings;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.adapter.WifiScanAdapter;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.WifiList;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SearchWifiNetworksActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SearchWifiNetworksActivity.class.getCanonicalName();
    private static final int REQUEST_CHECK_SETTINGS = 123;
    final Handler handler_scan = new Handler(Looper.getMainLooper());
    /*  permission user tv_connected_network_namehas to give */
    String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    int permsRequestCode = 200;
    private WifiManager wifiManager;
    private RecyclerView listView;
    private Button buttonScan;
    private List<ScanResult> results;
    private ArrayList<WifiList> arrayList = new ArrayList<>();
    private WifiScanAdapter adapter;
    private Switch tb_mob_wifi;
    private RelativeLayout wifi_details;
    private ProgressBar progressBar;
    private TextView tv_connected_network_name;
    BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifiManager.getScanResults();
            unregisterReceiver(this);
            if (results.size() > 0) {
                arrayList.clear();
                setdataToAdapter();
            } else {
                setProgressVisibility(false);
            }

        }
    };
    private Runnable runnableScan = new Runnable() {
        @Override
        public void run() {
            if (results.size() > 0) {
                arrayList.clear();
                setdataToAdapter();
            }
            //scanWifi();


            tv_connected_network_name.setText(getSssidOfConnectedNetwork() == null ? "" : getString(R.string.connected) + " : " + getSssidOfConnectedNetwork());

            setProgressVisibility(false);
            handler_scan.postDelayed(this, 5000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_one_info);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        Button btnHelp = findViewById(R.id.btnHelp);
        btnHelp.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
        if (OwnerApplication.getInstance().getDealerData().isDemo()) {
            tvHeaderAccount.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeaderAccount.setText(getString(R.string.spa_account_setup));
        }
        wifi_details = findViewById(R.id.wifi_details);
        buttonScan = findViewById(R.id.scanBtn);
        tb_mob_wifi = findViewById(R.id.tb_mob_wifi);
        listView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progressBar);
        /*wifi list visibility gone */
        wifi_details.setVisibility(View.GONE);
        tv_connected_network_name = findViewById(R.id.tv_connected_network_name);

        /*recyclerview layout */
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        listView.setLayoutManager(mLayoutManager);

        tb_mob_wifi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                // The toggle is enabled
                enableWifiView(true);
            } else {
                // The toggle is disabled
                enableWifiView(false);
            }

        });


        buttonScan.setOnClickListener(view -> scanWifi());


        /*request permission*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            doWifiNetworkSearch();
        }
    }

    private void enableWifiView(boolean b) {
        if (b) {
            wifi_details.setVisibility(View.VISIBLE);

            if (!wifiManager.isWifiEnabled())
                wifiManager.setWifiEnabled(true);

            buttonScan.setVisibility(View.VISIBLE);
            scanWifi();

        } else {
            if (wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(false);
            }
            wifi_details.setVisibility(View.GONE);
            buttonScan.setVisibility(View.GONE);

        }
    }

    private void doWifiNetworkSearch() {


        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(this, R.string.plz_check_your_wifi, Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        adapter = new WifiScanAdapter(this, arrayList, (position, v, list) -> {

            /*checking if network is open ,device will connect without opening dialog box. */


            if (position < list.size())
                /*To prevent from ArrayIndexOutOfBoundException */ {

                if (isNetworkOpenType(list.get(position).getCapabilities()))
                    /*connection with open network ( no need password )*/ {

                    connectOpenWifiNetwork(wifiManager, list.get(position).getName());

                } else /*with password so ask user to enter password */ {

                    connectToWifi(list.get(position).getName(), "controlmyspa");
                    // openPasswordDialog( position, v, list );
                }
            }

        });
        listView.setAdapter(adapter);


        scanWifi();
    }

    private String getSssidOfConnectedNetwork() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        if (isConnected) {
            // Do whatever
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo.getNetworkId() == -1) {
                return null; // Not connected to an access point
            }
            return wifiInfo.getSSID();
        }


        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler_scan.removeCallbacks(runnableScan);
    }

    private void scanWifi() {

        if (wifi_details.getVisibility() == View.VISIBLE)
            setProgressVisibility(true);

        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
    }

    private void setdataToAdapter() {

        /*set text of connected network  */
        tv_connected_network_name.setText(getSssidOfConnectedNetwork() == null ? "" : getString(R.string.connected) + " : " + getSssidOfConnectedNetwork());

        /*get SSID of connected wifi */
        String bssid = isConnectedWithWiFi();

        boolean isConnectedWith;

        for (ScanResult scanResult : results) {


            if (scanResult.SSID.startsWith("CMS_SPA_")) {
                isConnectedWith = bssid != null && bssid.equals(scanResult.BSSID);
                int chanwidth = 0;
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        chanwidth = scanResult.channelWidth;
                    }
                } catch (Exception e) {
                    chanwidth = 0;
                }
                WifiList list = new WifiList(scanResult.SSID, scanResult.BSSID, scanResult.level, scanResult.frequency,
                        chanwidth, scanResult.capabilities, isConnectedWith);
                arrayList.add(list);
            }

        }
        /* progressbar hide */
        setProgressVisibility(false);
        adapter.notifyDataSetChanged();

    }

    private void setProgressVisibility(boolean hasToVisible) {

        if (hasToVisible) {
            /*coded to disable touch functionality when progress bar is visible */
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.VISIBLE);

        } else {
            /*code to enable touch functionality when progress bar is invisible*/
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {
        setProgressVisibility(false);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            case R.id.help:
                Utility.helpDialog(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                Intent intent = new Intent(this, LetTestConnectionActivity.class);
                startActivity(intent);
                break;

            case R.id.btnHelp:
            default:
                break;
        }
    }


    private String isConnectedWithWiFi() {

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        if (isConnected) {
            // Do whatever
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo.getNetworkId() == -1) {
                return null; // Not connected to an access point
            }
            return wifiInfo.getBSSID();
        }

        return null;
    }


    /* Check wifi network type [ open, wep, wpa ... ]*/
    private Boolean isNetworkOpenType(String capabilities) {

        // WPA or WPA2 Network
        // Open Network
        if (capabilities.toUpperCase().contains("WEP")) {
            // WEP Network
            return false;
        } else return !capabilities.toUpperCase().contains("WPA")
                && !capabilities.toUpperCase().contains("WPAwifiManager2");

    }


    /* Connect with selected wifi network from list of wifi signals */
    private void connectToWifi(final String networkSSID, final String networkPassword) {

        setProgressVisibility(true);

        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = String.format("\"%s\"", networkSSID);
        conf.preSharedKey = String.format("\"%s\"", networkPassword);
        int netId = wifiManager.addNetwork(conf);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
        /* refresh list after 4 second */
        handler_scan.postDelayed(runnableScan, 4000);
    }

    /* connect with open wifi network */

    private void connectOpenWifiNetwork(WifiManager wifiManager, String networkSSID) {
        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = "\"" + networkSSID + "\"";
        wc.hiddenSSID = true;
        wc.priority = 0xBADBAD;
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        int id = wifiManager.addNetwork(wc);
        wifiManager.disconnect();
        wifiManager.enableNetwork(id, true);
        wifiManager.reconnect();
    }

    @Override
    public void onRequestPermissionsResult(final int permsRequestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {

        if (permsRequestCode == 200) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]
                    == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the

                displayLocationSettingsRequest(getBaseContext());
            } else {
                checkPermission();
            }
        }


    }

    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat
                .checkSelfPermission(getBaseContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    perms,
                    permsRequestCode);
        }
        displayLocationSettingsRequest(this);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:

                    tb_mob_wifi.setClickable(true);
                    doWifiNetworkSearch();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        tb_mob_wifi.setClickable(false);
                        status.startResolutionForResult(SearchWifiNetworksActivity.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                    break;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    tb_mob_wifi.setClickable(true);
                    doWifiNetworkSearch();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i(TAG, "User chose not to make required location settings changes.");
                    Toast.makeText(getApplicationContext(), getString(R.string.app_required_location_permission), Toast.LENGTH_LONG).show();
                    break;
            }
        }


    }
}