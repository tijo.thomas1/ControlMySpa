package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

public class HelpScreenConnectSpaToWifi extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_two);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button btnYes = findViewById(R.id.btnYes);
        btnYes.setOnClickListener(this);
        Button btnNeedHelp = findViewById(R.id.btnNeedHelp);
        btnNeedHelp.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        /*SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        Intent intent = null;

        if (sharedPreferences.getBoolean(Constants.PREF_MAIN_PAGE, false)) {
            SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();
            loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, false);
            loginPrefsEditor.apply();
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnYes:
                // New Code
                Intent intent = new Intent(this, LetTestConnectionActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.btnNeedHelp:

            default:
                break;
        }
    }
}
