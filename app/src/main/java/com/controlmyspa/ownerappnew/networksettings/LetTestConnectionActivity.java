package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.PrefManager;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.utils.Constants;
import com.ligl.android.widget.iosdialog.IOSDialog;

public class LetTestConnectionActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean isTestConnectionSuccess;
    private ProgressBar pbTestConnection;
    //private TextView tvResult;
    //private Button btnAction;
    private Button btnTestConnection;
    private SharedPreferences preferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private DealerData dealerData;
    private PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_let_us_test_connection);
        pref = new PrefManager(this);
        initControls();
    }

    private void initControls() {
        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pbTestConnection = findViewById(R.id.pbTestConnection);
        /*tvResult = findViewById(R.id.tvResult);
        btnAction = findViewById(R.id.btnAction);
        btnAction.setOnClickListener(this);*/
        btnTestConnection = findViewById(R.id.btnTestConnection);
        btnTestConnection.setOnClickListener(this);
        preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
        if (OwnerApplication.getInstance().getDealerData().isDemo()) {
            tvHeaderAccount.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeaderAccount.setText(getString(R.string.spa_account_setup));
        }
    }

    private void registerUserToSpa() {
        NetworkManager.registerUserToSpa(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                showDialog(getString(R.string.connection_successful));
                dealerData.setSerialNumber(result);
                pref.addSerialNumber(result);
                loginPrefsEditor = preferences.edit();
                loginPrefsEditor.putString(Constants.SERIAL_NUMBER, result);
                loginPrefsEditor.apply();
            }

            @Override
            public void onFailure(String result) {
                pbTestConnection.setVisibility(View.GONE);
                isTestConnectionSuccess = false;
                //isTestConnectionSuccess = true;
                showDialog(getString(R.string.connection_not_successful));

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog(final String message) {
        if (isTestConnectionSuccess) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
            adb.setTitle(getResources().getString(R.string.app_name));
            adb.setMessage(message);
            adb.setCancelable(false);
            adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                Intent intent;
                if (OwnerApplication.getInstance().isNormalSetup()) {
                    intent = new Intent(LetTestConnectionActivity.this,
                            NetworkSettingStepFourActivity.class);
                } else {
                    intent = new Intent(LetTestConnectionActivity.this,
                            NetworkSettingStepThreeInfo1Activity.class);
                }

                startActivity(intent);
                finish();

            });
            if (!this.isFinishing() && message.length() > 0) {
                adb.show();
            }
        } else {
            if (!this.isFinishing() && message.length() > 0) {
                new IOSDialog.Builder(this)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.retry, (dialog, which) -> getNetworkConnectionInfo())
                        .setNegativeButton(R.string.cancel, (dialog, which) -> finish()).show();
            }
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnTestConnection:

                getNetworkConnectionInfo();
                break;

            case R.id.btnAction:
                Intent intent;

                if (isTestConnectionSuccess) {
                    if (OwnerApplication.getInstance().isNormalSetup()) {
                        intent = new Intent(this, NetworkSettingStepFourActivity.class);
                    } else {
                        intent = new Intent(this, NetworkSettingStepThreeInfo1Activity.class);
                    }
                } else {
                    intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                }
                startActivity(intent);
                finish();
                break;

            default:
                break;


        }
    }

    private void getNetworkConnectionInfo() {

        pbTestConnection.setVisibility(View.VISIBLE);
        btnTestConnection.setVisibility(View.GONE);
        btnTestConnection.setTextColor(ContextCompat.getColor(this,
                R.color.colorAccent));
        NetworkManager.requestGetNetworkSettings(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                //btnTestConnection.setVisibility(View.VISIBLE);
                btnTestConnection.setEnabled(false);
                pbTestConnection.setVisibility(View.GONE);
                isTestConnectionSuccess = true;

                if (!OwnerApplication.getInstance().isNormalSetup()) {
                    registerUserToSpa();
                } else {
                    showDialog(getString(R.string.connection_successful));
                }
            }

            @Override
            public void onFailure(String result) {
                //btnTestConnection.setVisibility(View.VISIBLE);
                btnTestConnection.setEnabled(false);
                pbTestConnection.setVisibility(View.GONE);
                isTestConnectionSuccess = false;
                // isTestConnectionSuccess = true;
                showDialog(getString(R.string.connection_not_successful));
            }
        });
    }
}
