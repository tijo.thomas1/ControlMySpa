package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.databinding.ActivityNetworkSettingStepSixBinding;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

public class NetworkSettingStepSixActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "NetworkSettingStepSix";

    private ActivityNetworkSettingStepSixBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_network_setting_step_six);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {
        setSupportActionBar(binding.rowToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.array_config, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.spinnerIpv4.setAdapter(adapter);
        binding.spinnerIpv4.setOnItemSelectedListener(this);
        binding.spinnerIpv4.setSelection(1);
        binding.rowToolbar.tvHeader.setText(getString(R.string.control_myspa));
        if (OwnerApplication.getInstance().isNormalSetup()) {
            binding.tvHeaderWiFi.setVisibility(View.VISIBLE);
            binding.tvHeaderAccount.setVisibility(View.GONE);
        } else {
            binding.tvHeaderWiFi.setVisibility(View.GONE);
            binding.tvHeaderAccount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // An item was selected. You can retrieve the selected item using
        String str = (String) parent.getItemAtPosition(position);
        Log.d(TAG, str);
        if (str.equals("DHCP")) {
            binding.networkTxtIpaddress.setEnabled(false);
            binding.networkTxtSubnet.setEnabled(false);
            binding.networkTxtGateway.setEnabled(false);
        } else {
            binding.networkTxtIpaddress.setEnabled(true);
            binding.networkTxtSubnet.setEnabled(true);
            binding.networkTxtGateway.setEnabled(true);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onSaveSettings(View view) {
        final Intent intent = new Intent(this, CongratulationYourSpaConnection.class);
        String strIP = binding.networkTxtIpaddress.getText().toString();
        String strSubnetMask = binding.networkTxtSubnet.getText().toString();
        String strGateway = binding.networkTxtGateway.getText().toString();
        boolean isDHCP = (binding.spinnerIpv4.getSelectedItemPosition() == 1);
        if (!isDHCP) {
            if (!Utility.isValidIP(strIP)) {
                Toast.makeText(this, getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strSubnetMask)) {
                Toast.makeText(this, getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strGateway)) {
                Toast.makeText(this, getString(R.string.invalid_ip_address), Toast.LENGTH_LONG).show();
                return;
            }
        }

        NetworkManager.m_SSID = binding.networkTxtSsid.getText().toString();
        NetworkManager.m_Password = binding.networkTxtPassword.getText().toString();
        NetworkManager.m_IPAddress = strIP;
        NetworkManager.m_SubnetMask = strSubnetMask;
        NetworkManager.m_Gateway = strGateway;
        NetworkManager.m_DHCP = (binding.spinnerIpv4.getSelectedItemPosition() == 0);

        binding.pbConnect.setVisibility(View.VISIBLE);
        NetworkManager.requestPutNetworkSettings(NetworkSettingStepSixActivity.this, new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                binding.pbConnect.setVisibility(View.GONE);
                intent.putExtra("status_flag", 1);
                startActivity(intent);
                //Toast.makeText(NetworkSettingStepSixActivity.this, R.string.success_save, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String result) {
                binding.pbConnect.setVisibility(View.GONE);
                intent.putExtra("status_flag", 0);
                startActivity(intent);
                //Toast.makeText(NetworkSettingStepSixActivity.this, R.string.error_save, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        Intent intent = null;

        if (sharedPreferences.getBoolean(Constants.PREF_MAIN_PAGE, false)){
            SharedPreferences.Editor loginPrefsEditor = sharedPreferences.edit();
            loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, false);
            loginPrefsEditor.apply();
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
