package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.api.ApiClient;
import com.controlmyspa.ownerappnew.api.ApiInterface;
import com.controlmyspa.ownerappnew.databinding.ActivityNetworkSettingStepThreeInfo1Binding;
import com.controlmyspa.ownerappnew.gatewayultra.SpaExistsActivity;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.PrefManager;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.ValidateUsername;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.controlmyspa.ownerappnew.api.ApiConstantsKt.SERIAL;
import static com.controlmyspa.ownerappnew.api.ApiConstantsKt.SPA_EXISTS_MSG;
import static com.controlmyspa.ownerappnew.api.ApiConstantsKt.SPA_VERIFY;

public class NetworkSettingStepThreeInfo1Activity extends AppCompatActivity implements View.OnClickListener {
    private ActivityNetworkSettingStepThreeInfo1Binding binding;
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_network_setting_step_three_info1);
        prefManager = new PrefManager(this);
        initControls();
    }

    private void initControls() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        if (OwnerApplication.getInstance().getDealerData().isDemo()) {
            binding.tvHeaderAccount.setText(getString(R.string.spa_temporary_account));
        } else {
            binding.tvHeaderAccount.setText(getString(R.string.spa_account_setup));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkSerialNumberAccountExists() {
        binding.progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = Objects.requireNonNull(ApiClient.INSTANCE.getClient()).create(ApiInterface.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(SERIAL, prefManager.getSerialNumber());
        Call<ValidateUsername> responseCall = apiService.checkSpaExists(SPA_VERIFY, jsonObject);
        responseCall.enqueue(new Callback<ValidateUsername>() {
            @Override
            public void onResponse(@NotNull Call<ValidateUsername> call, @NotNull Response<ValidateUsername> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ValidateUsername validateUsername = response.body();
                    if (validateUsername != null) {
                        if (validateUsername.getMessage().equals(SPA_EXISTS_MSG)) {
                            openSpaExistsActivity();
                        } else {
                            Intent intent = new Intent(NetworkSettingStepThreeInfo1Activity.this, NetworkSettingStepThreeInfo2Activity.class);
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(NetworkSettingStepThreeInfo1Activity.this, NetworkSettingStepThreeInfo2Activity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ValidateUsername> call, @NotNull Throwable t) {
                binding.progressBar.setVisibility(View.GONE);
                Snackbar.make(binding.getRoot(), Objects.requireNonNull(t.getMessage()), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void openSpaExistsActivity() {
        Intent intent = new Intent(this, SpaExistsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnNext) {
            new AlertDialog.Builder(this).setMessage(getString(R.string.please_confirm_that_your_phone_tablet_is_connected_to_the_internet_and_is_not_connected_to_spa_wi_fi))
                    .setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {
                        if (Utility.isInternetConnectivity(this))
                            checkSerialNumberAccountExists();
                        else
                            Toast.makeText(this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                    }).create().show();
        }
    }

}