package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NetworkSettingStepFourActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView lvWiFiList;
    private WiFiAdapter wiFiAdapter;
    private List<String> wifiScanList = null;
    private ProgressBar pbWifi;
    private SwipeRefreshLayout btn_refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_four);

        btn_refresh = findViewById(R.id.btn_refresh);
        initControls();
        btn_refresh.setOnClickListener(v -> {

        });

        btn_refresh.setOnRefreshListener(() -> {
            pbWifi.setVisibility(View.VISIBLE);
            btn_refresh.setRefreshing(true);
            getAllWiFiList();
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pbWifi = findViewById(R.id.pbWifi);
        wifiScanList = new ArrayList<>();
        lvWiFiList = findViewById(R.id.lvWiFiList);
        wiFiAdapter = new WiFiAdapter();
        lvWiFiList.setAdapter(wiFiAdapter);

        lvWiFiList.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(NetworkSettingStepFourActivity.this,
                    SelectWifiFromSpaGatewayWifiList.class);
            intent.putExtra("wifiName", wifiScanList.get(i).replace("\"", ""));
            startActivity(intent);

        });


        pbWifi.setVisibility(View.VISIBLE);
        lvWiFiList.setVisibility(View.GONE);
        Button btnAction = findViewById(R.id.btnAction);
        btnAction.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
        getAllWiFiList();

        //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 7000);
    }

    public void getAllWiFiList() {

        NetworkManager.requestGetWiFiList(new RestAPICallback() {
            @Override
            public void onSuccess(String result) {
                wifiScanList.clear();

                pbWifi.setVisibility(View.GONE);
                btn_refresh.setRefreshing(false);
                lvWiFiList.setVisibility(View.VISIBLE);

                try {
                    JSONArray jsonArray = new JSONArray(result);

                    if (jsonArray.length() == 0) {

                        Toast.makeText(NetworkSettingStepFourActivity.this, R.string.no_wifi_found, Toast.LENGTH_LONG).show();
                    } else {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            if ((Float.parseFloat(jsonObject.getString("frequency")) > 2.4) && (Float.parseFloat(jsonObject.getString("frequency")) <= 2.5)) {
                                wifiScanList.add(jsonObject.getString("essid"));
                            }
                        }
                        wiFiAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String result) {
                pbWifi.setVisibility(View.GONE);
                // Toast.makeText(NetworkSettingStepFourActivity.this,"Result"+result,Toast.LENGTH_LONG).show();
                btn_refresh.setRefreshing(false);

                Toast.makeText(NetworkSettingStepFourActivity.this, R.string.please_check_your_device_is_connected_wit_SPA,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnAction) {
            Intent intent = new Intent(this, NetworkSettingStepSixActivity.class);
            startActivity(intent);
        }
    }

    /*class WifiScanReceiver extends BroadcastReceiver {

        public void onReceive(Context c, Intent intent) {
            wifiScanList = wifiManager.getScanResults();

            for (int i = 0; i < wifiScanList.size(); i++) {
                if (!(wifiScanList.get(i).frequency > 2400 && wifiScanList.get(i).frequency < 2500)) {
                    wifiScanList.remove(i--);
                }
            }

            pbWifi.setVisibility(View.GONE);
            lvWiFiList.setVisibility(View.VISIBLE);

            wiFiAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 7000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifiScanReceiver = new WifiScanReceiver();
                    wifiManager.startScan();
                    pbWifi.setVisibility(View.VISIBLE);
                    lvWiFiList.setVisibility(View.GONE);
                } else {
                    // permission denied, boo!
                    Toast.makeText(this, "Please allow to display all the wifi available near to you.", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Permission denied.");
                }
            }
        }
    }*/

    private class WiFiAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return wifiScanList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            ViewHolder viewHolder = new ViewHolder();
            LayoutInflater inflater = getLayoutInflater();

            if (convertView == null) {

                convertView = inflater.inflate(R.layout.row_wifi_list, viewGroup, false);
                convertView.setTag(viewHolder);

                viewHolder.tvWiFi = convertView.findViewById(R.id.tvWiFi);

            } else {

                viewHolder = (ViewHolder) convertView.getTag();

            }

            viewHolder.tvWiFi.setText(wifiScanList.get(i).replace("\"", ""));

            return convertView;
        }

        class ViewHolder {
            TextView tvWiFi;
        }
    }
}
