package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.spasetup.NewSpaOwnerActivity;

public class VerificationMessageActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_message);
        initControls();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        Button btnTryAgain = findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(this);
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        Button btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        TextView tvErrorInternet = findViewById(R.id.tvErrorInternet);
        TextView tvErrorGMail = findViewById(R.id.tvErrorGMail);
        TextView tvSuccess = findViewById(R.id.tvSuccess);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("errorCode")) {

            switch (bundle.getInt("errorCode")) {
                case 1:
                    tvErrorInternet.setVisibility(View.VISIBLE);
                    tvErrorGMail.setVisibility(View.GONE);
                    tvSuccess.setVisibility(View.GONE);
                    btnTryAgain.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnNext.setVisibility(View.GONE);
                    btnRegister.setVisibility(View.GONE);
                    break;
                case 2:
                    tvErrorInternet.setVisibility(View.GONE);
                    tvErrorGMail.setVisibility(View.VISIBLE);
                    tvSuccess.setVisibility(View.GONE);
                    btnTryAgain.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnNext.setVisibility(View.GONE);
                    btnRegister.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    tvErrorInternet.setVisibility(View.GONE);
                    tvErrorGMail.setVisibility(View.GONE);
                    tvSuccess.setVisibility(View.VISIBLE);
                    btnTryAgain.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.GONE);
                    btnNext.setVisibility(View.VISIBLE);
                    btnRegister.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.btnTryAgain:

                onBackPressed();
                break;
            case R.id.btnCancel:

                intent = LoginActivity.getStartIntent(this);
                startActivity(intent);
                finish();
                break;
            case R.id.btnRegister:

                intent = new Intent(this, NewSpaOwnerActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btnNext:

                intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}