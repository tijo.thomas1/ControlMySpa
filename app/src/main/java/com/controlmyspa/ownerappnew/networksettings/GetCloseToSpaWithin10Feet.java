package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;

public class GetCloseToSpaWithin10Feet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_one);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
        if (OwnerApplication.getInstance().getDealerData().isDemo()) {
            tvHeaderAccount.setText(getString(R.string.spa_temporary_account));
        } else {
            tvHeaderAccount.setText(getString(R.string.spa_account_setup));
        }
    }

    public void onClickNext(View view) {
        // New Code
        Intent intent = new Intent(this, SearchWifiNetworksActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
