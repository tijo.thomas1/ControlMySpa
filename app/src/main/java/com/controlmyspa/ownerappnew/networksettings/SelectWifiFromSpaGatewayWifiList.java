package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.service.NetworkManager;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.google.android.material.textfield.TextInputEditText;

public class SelectWifiFromSpaGatewayWifiList extends AppCompatActivity implements View.OnClickListener {

    private EditText etSSID;
    private TextInputEditText etPassword;
    private ProgressBar pbConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_five);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        etSSID = findViewById(R.id.etSSID);
        etPassword = findViewById(R.id.etPassword);
        etPassword.requestFocus();
        pbConnect = findViewById(R.id.pbConnect);
        Button btnSaveSetting = findViewById(R.id.btnSaveSetting);
        btnSaveSetting.setOnClickListener(this);
        Button btnAdvanceSetting = findViewById(R.id.btnAdvanceSetting);
        btnAdvanceSetting.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("wifiName")) {
            etSSID.setText(bundle.getString("wifiName"));
        }

    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {/*// app icon in action bar clicked; goto parent activity.
                onBackPressed();*/
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveSetting:


                final Intent intent = new Intent(this, CongratulationYourSpaConnection.class);
        /*Boolean isDHCP = (spinnerIPV4.getSelectedItemPosition() == 1);
        if (!isDHCP) {
            if (!Utility.isValidIP(strIP)) {
                Toast.makeText(this, "Invalid IP address.", Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strSubnetMask)) {
                Toast.makeText(this, "Invalid IP address.", Toast.LENGTH_LONG).show();
                return;
            }
            if (!Utility.isValidIP(strGateway)) {
                Toast.makeText(this, "Invalid IP address.", Toast.LENGTH_LONG).show();
                return;
            }
        }*/

                NetworkManager.m_SSID = etSSID.getText().toString();
                NetworkManager.m_Password = etPassword.getText().toString();
        /*NetworkManager.m_IPAddress = NetworkManager.m_IPAddress;
        NetworkManager.m_SubnetMask = NetworkManager.m_SubnetMask;
        NetworkManager.m_Gateway = NetworkManager.m_Gateway;*/
                NetworkManager.m_DHCP = false;

                //progressBar.setVisibility(View.VISIBLE);

                /*Before calling api checking password is empty  or not */
                if (NetworkManager.m_Password != null && NetworkManager.m_Password.length() >= 8) {
                    pbConnect.setVisibility(View.VISIBLE);
                    NetworkManager.requestPutNetworkSettings(this, new RestAPICallback() {
                        @Override
                        public void onSuccess(String result) {
                            pbConnect.setVisibility(View.GONE);
                            intent.putExtra("status_flag", 1);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onFailure(String result) {
                            pbConnect.setVisibility(View.GONE);
                            intent.putExtra("status_flag", 0);
                            startActivity(intent);
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(SelectWifiFromSpaGatewayWifiList.this, R.string.please_enter_valid_password,
                            Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.btnAdvanceSetting:
                Intent intent1 = new Intent(this, NetworkSettingStepSixActivity.class);
                startActivity(intent1);
                finish();
                break;

            default:
                break;

        }
    }
}
