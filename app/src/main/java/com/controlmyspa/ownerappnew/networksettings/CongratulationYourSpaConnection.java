package com.controlmyspa.ownerappnew.networksettings;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.MainActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.fragment.LocationFragment;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.model.DealerData;
import com.controlmyspa.ownerappnew.service.RestAPICallback;
import com.controlmyspa.ownerappnew.service.SpaManager;
import com.controlmyspa.ownerappnew.service.UserManager;
import com.controlmyspa.ownerappnew.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.Task;

public class CongratulationYourSpaConnection extends AppCompatActivity
        implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = CongratulationYourSpaConnection.class.getSimpleName();
    //location related variables
    private static final int REQUEST_CHECK_SETTINGS = 123;
    GoogleApiClient googleApiClient;
    String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    //runnable method to send device current location periodically after 5 minutes
    Runnable runnable = () -> {
        /*code to update latest spa location */
        SpaManager.requestUpdateSpaLocation(CongratulationYourSpaConnection.this,
                SpaManager.m_device_Lat,
                SpaManager.m_device_Lon, new RestAPICallback() {


                    @Override
                    public void onSuccess(String result) {
                        Toast.makeText(CongratulationYourSpaConnection.this, R.string.success_update, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Updated spa location");
                    }

                    @Override
                    public void onFailure(String result) {
                        Toast.makeText(CongratulationYourSpaConnection.this, R.string.error_update, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Failed to update spa location");
                    }
                });

    };
    private boolean connectionStatus;
    private DealerData dealerData;
    private WifiManager wifiManager;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private Handler handler_post_device_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_seven);


        initControls();

        updateSpaDeviceLocation();
    }

    private void updateSpaDeviceLocation() {
        handler_post_device_location = new Handler(Looper.getMainLooper());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                            SpaManager.m_device_Lat = location.getLatitude();
                            SpaManager.m_device_Lon = location.getLongitude();

                            Bundle bundle = getIntent().getExtras();

                            if (bundle != null && bundle.containsKey("status_flag") && bundle.getInt("status_flag") == 1 && UserManager.strId != null) {
                                handler_post_device_location.post(runnable);
                            }


                        }
                    });

        } catch (SecurityException e) {
            System.err.print("Security exception in onViewCreated " + LocationFragment.class.getCanonicalName());
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        dealerData = OwnerApplication.getInstance().getDealerData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
        TextView tvHeaderAccount = findViewById(R.id.tvHeaderAccount);
        TextView tvHeaderWiFi = findViewById(R.id.tvHeaderWiFi);
        if (OwnerApplication.getInstance().isNormalSetup()) {
            tvHeaderWiFi.setVisibility(View.VISIBLE);
            tvHeaderAccount.setVisibility(View.GONE);
        } else {
            tvHeaderWiFi.setVisibility(View.GONE);
            tvHeaderAccount.setVisibility(View.VISIBLE);
        }
        //tvStatus = findViewById(R.id.tvStatus);
        //private TextView tvStatus;
        TextView tvStatusSuccess = findViewById(R.id.tvStatusSuccess);
        TextView tvStatusFail = findViewById(R.id.tvStatusFail);
        Button btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        Button btnTryAgain = findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(this);
        ProgressBar pbSettings = findViewById(R.id.pbSettings);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("status_flag")) {
            if (bundle.getInt("status_flag") == 1) {

                tvStatusSuccess.setVisibility(View.VISIBLE);
                tvStatusFail.setVisibility(View.GONE);
                //tvStatus.setText(getString(R.string.connection_status_true));
                connectionStatus = true;
                btnTryAgain.setVisibility(View.GONE);
                btnClose.setText(getString(R.string.close));

                //wifiManager.setWifiEnabled(false);


            } else {

                //tvStatus.setText(getString(R.string.connection_status_false));
                tvStatusSuccess.setVisibility(View.GONE);
                tvStatusFail.setVisibility(View.VISIBLE);
                //btnClose.setText(getResources().getString(R.string.go_to_step_1));
                connectionStatus = false;
                btnTryAgain.setVisibility(View.VISIBLE);
                btnClose.setText(getString(R.string.cancel));
                //pbSettings.setVisibility(View.GONE);
            }
        }
        if (OwnerApplication.getInstance().isNormalSetup()) {
            pbSettings.setVisibility(View.GONE);
            btnClose.setEnabled(true);
        } else {
            //sendDealerData();
            btnClose.setEnabled(false);
        }

        /*request permission*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            checkPermission();


        } else {
            displayLocationSettingsRequest(CongratulationYourSpaConnection.this);
        }
    }

    @Override
    public void onBackPressed() {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        Intent intent;
        if (sharedPreferences.getBoolean(Constants.PREF_MAIN_PAGE, false)) {
            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else {
            intent = LoginActivity.getStartIntent(this);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.btnClose:

                if (connectionStatus) {
                    SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                    if (sharedPreferences.getBoolean(Constants.PREF_MAIN_PAGE, false)) {
                        intent = new Intent(this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    } else {
                        intent = LoginActivity.getStartIntent(this);
                    }
                    startActivity(intent);
                    finish();

                } else {
                    intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }


                break;

            case R.id.btnTryAgain:

                intent = new Intent(this, GetCloseToSpaWithin10Feet.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler_post_device_location.removeCallbacks(runnable);
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }

    }

    private void displayLocationSettingsRequest(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(CongratulationYourSpaConnection.this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        //  PendingResult<LocationSettingsResult > result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        //new code based on getSettingsClient
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.

                //checking that location is present nt or not


                if (response.getLocationSettingsStates().isLocationPresent()) {
                    Log.d("Yes", "Present");
                    try {
                        fusedLocationClient.getLastLocation()
                                .addOnSuccessListener(CongratulationYourSpaConnection.this,
                                        location -> {
                                            // Got last known location. In some rare situations this can be null.
                                            if (location != null) {
                                                // Logic to handle location object


                                                SpaManager.m_device_Lat = location.getLatitude();
                                                SpaManager.m_device_Lon = location.getLongitude();

                                                if (getIntent().getExtras().getInt("status_flag") == 1 && UserManager.strId != null)
                                                    handler_post_device_location.post(runnable);

                                            }
                                        });

                    } catch (SecurityException e) {
                        System.err.print("Security exception at line 314 " +
                                LocationFragment.class.getCanonicalName());
                    }


                }

            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                    CongratulationYourSpaConnection.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                            SpaManager.m_device_Lat = location.getLatitude();
                            SpaManager.m_device_Lon = location.getLongitude();

                            if (getIntent().getExtras().getInt("status_flag") == 1 && UserManager.strId != null)
                                handler_post_device_location.post(runnable);
                        }
                    });


        } catch (SecurityException exception) {
            System.err.print(" LocationFragment.java line no.549 " + exception);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(CongratulationYourSpaConnection.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat
                .checkSelfPermission(CongratulationYourSpaConnection.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            int permsRequestCode = 200;
            ActivityCompat.requestPermissions(CongratulationYourSpaConnection.this,
                    perms,
                    permsRequestCode);
        }
        displayLocationSettingsRequest(CongratulationYourSpaConnection.this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i(TAG, "User agreed to make required location settings changes.");

                    /*do something here permission is granted */
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i(TAG, "User chose not to make required location settings changes.");
                    Toast.makeText(CongratulationYourSpaConnection.this, R.string.app_required_location_permission, Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
