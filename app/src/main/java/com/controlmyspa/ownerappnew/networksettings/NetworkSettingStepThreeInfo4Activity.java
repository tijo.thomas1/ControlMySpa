package com.controlmyspa.ownerappnew.networksettings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.controlmyspa.ownerappnew.LoginActivity;
import com.controlmyspa.ownerappnew.MainActivity;
import com.controlmyspa.ownerappnew.OwnerApplication;
import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.helper.MyContextWrapper;
import com.controlmyspa.ownerappnew.helper.Utility;
import com.controlmyspa.ownerappnew.spasetup.cmscode.CmsCodeSetupOne;
import com.controlmyspa.ownerappnew.spasetup.spadealer.SpaDealerSetupOne;
import com.controlmyspa.ownerappnew.utils.Constants;

public class NetworkSettingStepThreeInfo4Activity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting_step_three_info4);
        initControls();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().getApplicationContext())));
    }

    private void initControls() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button btnTryAgain = findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(this);
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(getString(R.string.control_myspa));
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {// app icon in action bar clicked; goto parent activity.
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isInternetEnable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(this.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton(R.string.ok, (dialogInterface, i) -> {

        });
        if (!this.isFinishing() && message.length() > 0) {
            adb.show();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnTryAgain:

                if (isInternetEnable()) {

                    runOnUiThread(() -> {
                        Intent intent;
                        if (OwnerApplication.getInstance().isSpaSetup()) {
                            intent = new Intent(NetworkSettingStepThreeInfo4Activity.this, CmsCodeSetupOne.class);
                        } else {
                            intent = new Intent(NetworkSettingStepThreeInfo4Activity.this, SpaDealerSetupOne.class);
                        }
                        startActivity(intent);
                    });
                } else {
                    showOkDialog(getString(R.string.check_internet_connection));
                }
                break;

            case R.id.btnCancel:

                SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                Intent intent;

                if (sharedPreferences.getBoolean(Constants.PREF_MAIN_PAGE, false)) {

                    intent = new Intent(this, MainActivity.class);
                } else {
                    intent = LoginActivity.getStartIntent(this);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }
}