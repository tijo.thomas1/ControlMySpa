package com.controlmyspa.ownerappnew;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.controlmyspa.ownerappnew.model.DealerData;

/**
 * Created by polaris on 1/20/17.
 */

public class OwnerApplication extends Application {

    static OwnerApplication ownerApplication;
    private DealerData dealerData;
    private boolean isDealer = false;
    private boolean isNormalSetup = true;
    private boolean isSpaSetup = false;
    private boolean isDirectSetup = false;

    public static OwnerApplication getInstance() {
        return ownerApplication;
    }

    public boolean getIsDirectSetup() {
        return isDirectSetup;
    }

    public void setIsDirectSetup(boolean isDirect) {
        this.isDirectSetup = isDirect;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        ownerApplication = this;

        dealerData = new DealerData();
    }

    public DealerData getDealerData() {
        return dealerData;
    }


    public void setCmsCodeEmpty() {
        dealerData.setCmsCode("");
    }

    public boolean getDealer() {
        return isDealer;
    }

    public void setDealer(boolean isDealer) {
        this.isDealer = isDealer;
    }

    public boolean isNormalSetup() {
        return isNormalSetup;
    }

    public void setNormalSetup(boolean normalSetup) {
        isNormalSetup = normalSetup;
    }

    public void setIsDemo(boolean isDemo) {
        dealerData.setDemo(isDemo);
    }

    public boolean isSpaSetup() {
        return isSpaSetup;
    }

    public void setSpaSetup(boolean spaSetup) {
        isSpaSetup = spaSetup;
    }
}
