package com.controlmyspa.ownerappnew

import android.app.Activity
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.controlmyspa.ownerappnew.adapter.BleDeviceAdapter
import com.controlmyspa.ownerappnew.base.BaseActivity
import com.controlmyspa.ownerappnew.databinding.ActivityNewMainBinding
import com.controlmyspa.ownerappnew.gatewayultra.NewSpaSerialNumberDetectedActivity
import com.controlmyspa.ownerappnew.gatewayultra.NoSpaDetectedActivity
import com.controlmyspa.ownerappnew.gatewayultra.SignalStrengthActivity
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.networksettings.AccountSetupActivity
import com.controlmyspa.ownerappnew.utils.Constants
import com.google.android.material.snackbar.Snackbar
import cz.msebera.android.httpclient.util.TextUtils

class BleScannerActivity : BaseActivity(), OnItemClickedListener {
    override fun onItemClicked(scanResult: ScanResult) {
        if (connectGatt != null) {
            connectGatt?.disconnect()
            connectGatt?.close()
            connectGatt = null
        }
        scanLeDevice(false)
        connectGatt = scanResult.device.connectGatt(this, false, bluetoothGattCallback)

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(
            MyContextWrapper.wrap(
                newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)
            )
        )
    }

    private var serialNumber: String = ""
    private var signalStrength: Int = -1
    private var cloudConnected: Int = -1
    private var counter: Int = 0
    private var scanResultList: MutableList<ScanResult> = ArrayList()
    private lateinit var deviceListAdapter: BleDeviceAdapter
    private var mBleScanner: BluetoothLeScanner? = null
    private var isBackground = false
    private var isSignalStrengthCharaCalled = false
    private val macHashMap: MutableMap<String, String> = HashMap()
    override fun bluetoothInitialised(hasBluetooth: Boolean) {
        scanLeDevice(hasBluetooth)
    }

    override fun signInRequestSuccess() {

    }

    override fun locationPermissionGranted() {
        initialiseBluetooth()
    }

    override fun onPause() {
        isBackground = true
        mBleScanner?.stopScan(mLeScanCallback)
        connectGatt?.close()
        connectGatt = null
        super.onPause()
    }

    override fun onResume() {
        serialNumber = ""
        signalStrength = -1
        cloudConnected = -1
        isBackground = false
        Log.e("RESULT", "" + scanResultList.count())
        if (scanResultList.isEmpty()) {
            Handler(Looper.getMainLooper()).postDelayed({
                if (!mScanning) {
                    scanLeDevice(true)
                }
            }, 2000)
        } else showRecyclerView()
        super.onResume()
    }

    override fun onDestroy() {
        isBackground = true
        connectGatt?.close()
        connectGatt = null
        super.onDestroy()
    }

    private lateinit var binding: ActivityNewMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_main)
        setUpToolBar()
        setUpRecyclerView()
        scanResultList.clear()
        mBleScanner = null
        setUpDeviceAdapter()
        if (LoginActivity.signal_strength) {
            binding.txtSetup.visibility = View.GONE
            binding.btnManualSetup.text = getString(R.string.cancel)
        }

        binding.btnManualSetup.setOnClickListener {
            if (!LoginActivity.signal_strength) {
                val sharedPreferences =
                    getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
                val loginPrefsEditor = sharedPreferences.edit()
                loginPrefsEditor.putBoolean(Constants.PREF_MAIN_PAGE, false)
                loginPrefsEditor.apply()
                OwnerApplication.getInstance().setIsDemo(false)
                val intent = Intent(this@BleScannerActivity, AccountSetupActivity::class.java)
                startActivity(intent)
                finish()
            } else startActivity(LoginActivity.getStartIntent(this))
        }

        /**@author TIJO THOMAS
         * @since 28-12-2021
         * @issue java.lang.SecurityException: Need android.permission.BLUETOOTH_SCAN permission for
         * android.content.AttributionSource@749ee67a: GattService registerScanner*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            isBlePermissionRequiredWithAndroidVersionS()
        } else
            isPermissionGranted()
    }

    private var pStatus: Float = 0f
    private fun setUpProgress() {
        binding.progressBarNew.progress = 0f
        Thread {
            while (pStatus < 100f) {
                pStatus += 10f
                Handler(Looper.getMainLooper()).post {
                    binding.progressBarNew.progress = pStatus
                }
                try {
                    Thread.sleep(1000)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }.start()
    }

    private fun setUpDeviceAdapter() {
        deviceListAdapter = BleDeviceAdapter(scanResultList, this)
        binding.recyclerView.adapter = deviceListAdapter
    }

    private fun setUpRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.setHasFixedSize(true)
    }

    private fun setUpToolBar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private var mScanning: Boolean = false
    private fun scanLeDevice(enable: Boolean) {
        if (mBleScanner == null)
            mBleScanner = mBluetoothAdapter?.bluetoothLeScanner
        if (enable) {
            Handler(Looper.getMainLooper()).post {
                pStatus = 0f
                setUpProgress()
            }

            mScanning = true
            Snackbar.make(
                binding.root,
                getString(R.string.please_wait_while_searching_ble_device),
                Snackbar.LENGTH_LONG
            ).show()
            counter++
            binding.progressBarNew.visibility = View.VISIBLE
            /** Stops scanning after a pre-defined scan period.*/
            Handler(Looper.getMainLooper()).postDelayed({
                mScanning = false
                mBleScanner?.stopScan(mLeScanCallback)
                val isMultiple: Boolean = counter > 5
                if (scanResultList.isEmpty())
                    if ((!isBackground && scanResultList.isEmpty()) || (!isBackground && TextUtils.isEmpty(
                            serialNumber
                        ))
                    ) {
                        val intent = Intent(this, NoSpaDetectedActivity::class.java)
                        intent.putExtra(IS_MULTIPLE, isMultiple)
                        startActivityForResult(intent, NO_SPA_DETECTED)
                    }
            }, SCAN_PERIOD)
            val scanFilter = ScanFilter.Builder().build()
            val settings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build()
            mBleScanner?.startScan(listOf(scanFilter), settings, mLeScanCallback)
        } else {
            mScanning = false
            mBleScanner?.stopScan(mLeScanCallback)
        }
    }

    private val mLeScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            try {
                if (result != null)
                    if (result.scanRecord != null) {
                        if (result.scanRecord?.deviceName?.startsWith("CMS")!!) {
                            if (TextUtils.isEmpty(macHashMap[result.device.address])) {
                                macHashMap[result.device.address] = result.device.address
                                scanResultList.add(result)
                                deviceListAdapter.notifyDataSetChanged()
                                Handler(Looper.getMainLooper()).postDelayed({
                                    if (scanResultList.count() == 1) {
                                        onItemClicked(scanResultList[0])
                                    } else showRecyclerView()
                                }, 6000)
                            }
                        }
                    }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        override fun onScanFailed(errorCode: Int) {
            Log.d("Bluetooth Scan", "Scan Failed $errorCode")
            /* var error = ""
             when (errorCode) {
                 SCAN_FAILED_ALREADY_STARTED -> {
                     error = "SCAN_FAILED_ALREADY_STARTED"
                 }
                 SCAN_FAILED_APPLICATION_REGISTRATION_FAILED -> {
                     error = "SCAN_FAILED_APPLICATION_REGISTRATION_FAILED"
                 }
                 SCAN_FAILED_FEATURE_UNSUPPORTED -> {
                     error = "SCAN_FAILED_FEATURE_UNSUPPORTED"
                 }
                 SCAN_FAILED_INTERNAL_ERROR -> {
                     error = "SCAN_FAILED_INTERNAL_ERROR"
                 }
             }

             Snackbar.make(binding.root, "Sorry an error occurred to find the cms device check if it is ON or OFF $error ", Snackbar.LENGTH_LONG).show()*/
        }
    }
    private val bluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS)
                gatt?.discoverServices()
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (gatt?.getService(DEVICE_INFORMATION_SERVICE) != null) {
                    val service = gatt.getService(DEVICE_INFORMATION_SERVICE)
                    val characteristic =
                        service?.getCharacteristic(DEVICE_INFORMATION_SERIAL_NUMBER_CHARACTERISTIC)
                    gatt.readCharacteristic(characteristic)
                    gatt.setCharacteristicNotification(characteristic, true)
                }

                if (gatt?.getService(ULTRA_SERVICE) != null) {
                    val service = gatt.getService(ULTRA_SERVICE)
                    val characteristic = service?.getCharacteristic(STATUS_CHARACTERISTIC)
                    gatt.readCharacteristic(characteristic)
                    gatt.setCharacteristicNotification(characteristic, true)
                }

            }
        }


        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (characteristic?.uuid.toString()
                        .equals(STATUS_CHARACTERISTIC.toString(), ignoreCase = true)
                ) {
                    signalStrength = characteristic?.value?.get(0)?.toInt()!!
                    val rfLinkUp = characteristic.value?.get(1)?.toInt()
                    val hostHasIp = characteristic.value?.get(2)?.toInt()
                    val cloudDnsOk = characteristic.value?.get(3)?.toInt()
                    cloudConnected = characteristic.value?.get(4)?.toInt()!!
                    val errorCode = characteristic.value?.get(5)?.toInt()
                    val rs485Rx = characteristic.value?.get(6)?.toInt()
                    val rs485LinkUp = characteristic.value?.get(7)?.toInt()
                    Log.d(
                        "Bluetooth Scan",
                        "onCharacteristicRead   $signalStrength $rfLinkUp $hostHasIp" +
                                "$cloudConnected $cloudDnsOk $errorCode $rs485Rx $rs485LinkUp"
                    )
                }

                if (characteristic?.uuid.toString().equals(
                        DEVICE_INFORMATION_SERIAL_NUMBER_CHARACTERISTIC.toString(),
                        ignoreCase = true
                    )
                ) {
                    serialNumber = characteristic?.getStringValue(0).toString()
                    Log.d("Bluetooth Scan", "onCharacteristicRead  $serialNumber")
                }

                if (LoginActivity.signal_strength) {
                    if (!isSignalStrengthCharaCalled) {
                        isSignalStrengthCharaCalled = true
                        val characteristicValue = gatt?.getService(ULTRA_SERVICE)
                            ?.getCharacteristic(STATUS_CHARACTERISTIC)
                        gatt?.readCharacteristic(characteristicValue)
                        gatt?.setCharacteristicNotification(characteristicValue, true)
                    } else {
                        if (!TextUtils.isEmpty(serialNumber) && signalStrength != -1 && cloudConnected != -1) {
                            gatt?.disconnect()
                            val intent =
                                Intent(this@BleScannerActivity, SignalStrengthActivity::class.java)
                            intent.putExtra(INTENT_SERIAL_NO, serialNumber)
                            intent.putExtra(INTENT_SIGNAL_STRENGTH, signalStrength)
                            intent.putExtra(INTENT_CLOUD_CONNECTED, cloudConnected)
                            startActivityForResult(intent, SPA_DETECTION_DONE)
                            isSignalStrengthCharaCalled = false
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(serialNumber) && !isBackground) {
                        gatt?.disconnect()
                        val intent = Intent(
                            this@BleScannerActivity,
                            NewSpaSerialNumberDetectedActivity::class.java
                        )
                        intent.putExtra(INTENT_SERIAL_NO, serialNumber)
                        startActivityForResult(intent, SPA_DETECTION_DONE)
                    }
                }

            }
        }
    }

    private fun showRecyclerView() {
        binding.recyclerView.visibility = View.VISIBLE
        binding.progressBarNew.visibility = View.GONE
        binding.txtSetup.visibility = View.GONE
        binding.txtSpa.text = getString(R.string.please_choose_one)
        scanLeDevice(false)
    }

    /*  private fun hideRecyclerView() {
          binding.recyclerView.visibility = View.GONE
          binding.progressBarNew.visibility = View.VISIBLE
          if (LoginActivity.signal_strength)
              binding.txtSetup.visibility = View.GONE
          else binding.txtSetup.visibility = View.VISIBLE
          binding.txtSpa.text = getString(R.string.now_please_wait_a_few_seconds_while_we_detect_your_spas_bluetooth_signal)
      }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NO_SPA_DETECTED && resultCode == Activity.RESULT_OK) {
            scanLeDevice(true)
        } else if (requestCode == SPA_DETECTION_DONE && resultCode == Activity.RESULT_OK) {
            if (scanResultList.count() > 1) {
                showRecyclerView()
            } else onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}