package com.controlmyspa.ownerappnew.model;


import java.io.Serializable;
import java.util.ArrayList;

public class SpaComponent implements Serializable {
    public String name;
    public String deviceNumber;
    public String componentType;
    public String value = ControlState.ON;
    public ArrayList<String> availableValues = new ArrayList<>();
    public int m_hour = 0;
    public int m_minute = 0;
    public int m_durationMinutes = 0;
    public int sortOrder = 0;
    public String targetValue = ControlState.OFF;
    public interface ControlState  {
        String ON = "ON";
        String OFF = "OFF";
        String LOW = "LOW";
        String MED = "MED";
        String HIGH = "HIGH";
        String HI = "HI";
    }
}
