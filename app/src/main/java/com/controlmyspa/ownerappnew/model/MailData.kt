package com.controlmyspa.ownerappnew.model

data class MailData(
        var serialNumber: String,
        var currentTemp: String,
        var spaControllerPart: String,
        var setTemp: String,
        var spaControllerSoftwareVersion: String,
        var sensorATemp: String,
        var cmsAccountSetupDate: String,
        var sensorBTemp: String,
        var runMode: String,
        var dipSwitchSettings: String,
        var highLowRangeStatus: String,
        var currentSetup: String,
        var filterCycle: String,
        var lastFaultLogEntry: String,
        var firstName: String,
        var lastName: String,
        var email: String,
        var phNo: String,
        var address1: String,
        var address2: String,
        var city: String,
        var state: String,
        var zipCode: String,
        var country: String
)