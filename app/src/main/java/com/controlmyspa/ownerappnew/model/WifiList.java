package com.controlmyspa.ownerappnew.model;

public class WifiList {

    private String name;
    private String bssid;
    private String strength;
    private int level;
    private int frequency;
    private int channelWidth;
    private String capabilities;
    private boolean isConnectedWith;


    public WifiList(String name, String bssid, int level, int frequency, int channelWidth, String capabilities,
                    boolean isConnectedWith) {
        this.name = name;
        this.bssid = bssid;
        this.level = level;
        this.frequency = frequency;
        this.channelWidth = channelWidth;
        this.capabilities = capabilities;
        this.isConnectedWith = isConnectedWith;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getChannelWidth() {
        return channelWidth;
    }

    public void setChannelWidth(int channelWidth) {
        this.channelWidth = channelWidth;
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }

    public boolean isConnectedWith() {
        return isConnectedWith;
    }

    public void setConnectedWith(boolean connectedWith) {
        isConnectedWith = connectedWith;
    }
}
