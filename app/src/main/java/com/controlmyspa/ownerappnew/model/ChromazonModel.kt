package com.controlmyspa.ownerappnew.model

import com.google.gson.annotations.SerializedName

data class PowerState(
        @SerializedName("_id")
        var id: String,
        var spaId: String,
        var requestTypeId: Int,
        var originatorId: String,
        var sentTimestamp: String,
        var values: Values,
        var metadata: Metadata
)

data class Values(
        @SerializedName("DESIREDSTATE")
        var desiredState: String,
        @SerializedName("TZL_ZONE")
        var tzlZone: String?,
        @SerializedName("TZL_GROUP")
        var tzlGroup: String?,
        @SerializedName("DESIREDTEMP")
        var desiredTemp: String?
)

data class Metadata(
        @SerializedName("Requested By")
        var requestedBy: String,
        @SerializedName("Via")
        var via: String
)


data class CzDetails(
        @SerializedName("_id")
        var id: String,
        var serialNumber: String,
        var productName: String,
        var model: String,
        var dealerId: String,
        var associate: Associate,
        var oemId: String,
        var templateId: String,
        var currentState: CurrentState?,
        var salesDate: String,
        var registrationDate: String,
        var manufacturedDate: String,
        var p2pAPSSID: String,
        var buildNumber: String,
        var sold: String,
        var tzlstate: TzlState,
        var alerts: MutableList<Alerts>
)

data class Alerts(
        var shortDescription: String
)

data class TzlState(
        var updateTimestamp: String,
        var tzlLightStatus: TzlLightStatus,
        var tzlConfiguration: TzlConfiguration,
        var tzlColorSettings: TzlColorSettings
)

data class TzlColorSettings(
        var tzlColors: MutableList<TzlColors>
)

data class TzlColors(
        var colorId: Int,
        var red: Int,
        var green: Int,
        var blue: Int,
        var secondary: Boolean
)

data class TzlConfiguration(
        var tzlZoneFunctions: MutableList<TzlZoneFunctions>,
        var tzlGroupStates: MutableList<TzlGroupStates>
)

data class TzlGroupStates(
        var groupId: String,
        var state: String
)

data class TzlZoneFunctions(
        var zoneId: String,
        var zoneFunction: String
)

data class TzlLightStatus(
        var tzlZones: MutableList<TzlZones>
)

data class TzlZones(
        var zoneId: String,
        var state: String,
        var zoneName: String,
        var intensity: Int,
        var speed: Int,
        var red: Int,
        var green: Int,
        var blue: Int
)

data class CurrentState(
        var runMode: String,
        var desiredTemp: String?,
        var targetDesiredTemp: String,
        var currentTemp: String?,
        var controllerType: String,
        var errorCode: Int,
        var shouldShowAlert: Boolean,
        var cleanupCycle: Boolean,
        var uplinkTimestamp: String,
        var staleTimestamp: String,
        var heaterMode: String,
        var hour: Int,
        var minute: Int,
        var online: Boolean,
        var celsius: Boolean,
        var demoMode: Boolean,
        var timeNotSet: Boolean,
        var settingsLock: Boolean,
        var spaOverheatDisabled: Boolean,
        var bluetoothStatus: String,
        var updateIntervalSeconds: Int,
        var wifiUpdateIntervalSeconds: Int,
        var components: MutableList<Components>,
        var setupParams: SetupParams,
        var systemInfo: SystemInfo,
        var wifiConnectionHealth: String,
        var ethernetPluggedIn: Boolean,
        var rs485ConnectionActive: Boolean,
        var rs485AcquiredAddress: Int,
        var messageSeverity: Int,
        var uiCode: Int,
        var uiSubCode: Int,
        var invert: Boolean,
        var allSegsOn: Boolean,
        var panelLock: Boolean,
        var military: Boolean,
        var tempRange: String?,
        var primingMode: Boolean,
        var soundAlarm: Boolean,
        var repeat: Boolean,
        var panelMode: String,
        var swimSpaMode: String,
        var swimSpaModeChanging: Boolean,
        var heaterCooling: Boolean,
        var latchingMessage: Boolean,
        var lightCycle: Boolean,
        var elapsedTimeDisplay: Boolean,
        var tvLiftState: Int,
        var specialTimeouts: Boolean,
        var stirring: Boolean,
        var ecoMode: Boolean,
        var soakMode: Boolean,
        var overrangeEnabled: Boolean,
        var heatExternallyDisabled: Boolean,
        var testMode: Boolean,
        var tempLock: Boolean,
        var primaryTZLStatus: String,
        var secondaryTZLStatus: String,
        var alertState: String,
        var abdisplay: Boolean,
        var sensorATemp: String?,
        var sensorBTemp: String?
)

data class SystemInfo(
        var heaterPower: Int,
        var mfrSSID: Int,
        var modelSSID: Int,
        var versionSSID: Int,
        var swSignature: Long,
        var currentSetup: Int,
        var lastUpdateTimestamp: String,
        var dipSwitches: MutableList<DipSwitches>?,
        var controllerSoftwareVersion: String
)

data class DipSwitches(
        var on: Boolean
)

data class SetupParams(
        var lowRangeLow: Int,
        var lowRangeHigh: Int,
        var highRangeLow: Int,
        var highRangeHigh: Int,
        var gfciEnabled: Boolean,
        var drainModeEnabled: Boolean,
        var lastUpdateTimestamp: String
)

data class Components(
        var componentType: String,
        var materialType: String,
        var port: String,
        var value: String,
        var availableValues: MutableList<String>,
        var targetValue: String,
        var name: String,
        var registeredTimestamp: String,
        var hour: Int,
        var minute: Int,
        var durationMinutes: Int,
        var serialNumber: String,
        var componentId: String,
        @SerializedName("_links")
        var links: Links?,
        var sortOrder: Int
)

data class Links(
        @SerializedName("component")
        var linkComponent: LinkComponent
)

data class LinkComponent(
        var href: String
)

data class Associate(
        @SerializedName("_id")
        var id: String,
        var lastName: String,
        var firstName: String,
        var active: Boolean,
        var fullName: String
)