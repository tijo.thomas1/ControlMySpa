package com.controlmyspa.ownerappnew.model;

import com.controlmyspa.ownerappnew.helper.HighLow;
import com.controlmyspa.ownerappnew.service.SpaManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class SpaPreset implements Serializable {
    public String strId;
    public String strName;
    private String strNotes;
    //public Date creationDate = new Date();
    public float temperature = 50;
    //Scheduling data
    public String strStartDate = "";
    public String strEndDate = "";
    public String strMode = RepeatMode.NONE;
    public String strCronExpression;
    public String strTimeZone = "";
    public long durationMinutes = TimeUnit.DAYS.convert(1, TimeUnit.MINUTES);
    public boolean bEnabled = false;
    public boolean isRunning = false;
    public String mRunMode = "";
    public String mFilter1Interval = "";
    public String mFilter2Interval = "";
    public HighLow mTempRange = HighLow.LOW;

    public ArrayList<SpaComponent> arrComponents = new ArrayList<>();

    public static void getPresetList(ArrayList<SpaPreset> presetList, JSONArray presets) {
        presetList.clear();
        try {
            for (int i = 0; i < presets.length(); i++) {
                JSONObject obj = presets.getJSONObject(i);
                SpaPreset preset = new SpaPreset();
                preset.strId = obj.getString("_id");
                if (obj.has("name")) {
                    preset.strName = obj.getString("name");
                }
                if (obj.has("notes")) {
                    preset.strNotes = obj.getString("notes");
                }
                //Parsing components settings
                JSONObject components = obj.getJSONObject("settings");
                if (components.has("TOGGLE_HEATER_MODE"))
                    preset.mRunMode = components.getJSONObject("TOGGLE_HEATER_MODE").getString("desiredState");
                if (components.has("TEMP_RANGE"))
                    preset.mTempRange = components.getJSONObject("TEMP_RANGE").getString("desiredState").equals("LOW") ? HighLow.LOW : HighLow.HIGH;
                if (components.has("SCHEDULE_FILTER_1"))
                    preset.mFilter2Interval = components.getJSONObject("SCHEDULE_FILTER_1").getString("intervalNumber");
                if (components.has("SCHEDULE_FILTER_0"))
                    preset.mFilter1Interval = components.getJSONObject("SCHEDULE_FILTER_0").getString("intervalNumber");

                if (components.has("FILTER_1"))
                    preset.mFilter2Interval = components.getJSONObject("FILTER_1").getString("intervalNumber");
                if (components.has("FILTER_0"))
                    preset.mFilter1Interval = components.getJSONObject("FILTER_0").getString("intervalNumber");

                Iterator<String> iter = components.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        JSONObject value = (JSONObject) components.get(key);

                        if (value.length() == 0) { //Blank data
                            continue;
                        }
                        String[] typeArr = key.split("_");
                        SpaComponent ctemp = new SpaComponent();
                        //                            case "FILTER":
                        //                            case "SCHEDULE_FILTER":
                        //                                if (value.getInt("intervalNumber") == 0) {
                        //                                    ctemp.value = SpaComponent.ControlState.OFF;
                        //
                        //                                } else {
                        //                                    ctemp.value = SpaComponent.ControlState.ON;
                        //                                }
                        //                                break;
                        // ctemp.value = value.getString("desiredState");
                        if ("HEATER".equals(typeArr[0])) {
                           String temperature = value.getString("desiredTemp");
                            String floatTemp = temperature.replace(",", ".");
                            preset.temperature = Float.parseFloat(floatTemp);
                            // continue;
                        }
//                        if (value.has("deviceNumber")) {
//                            ctemp.deviceNumber = value.getString("deviceNumber");
//                        } else {
//                            ctemp.deviceNumber = "";
//                        }

                        ctemp.componentType = typeArr[0];

                        //Search and Set component name and availableValues.
                        for (int j = 0; j < SpaManager.m_ComponentList.size(); j++) {
                            SpaComponent component = SpaManager.m_ComponentList.get(j);
                            if (component.componentType.equals(typeArr[0])) {
                                if (component.deviceNumber.equals(ctemp.deviceNumber)) {
                                    ctemp.name = component.name;
                                    ctemp.availableValues.addAll(component.availableValues);
                                    break;
                                }
                            }
                        }
                        preset.arrComponents.add(ctemp);

                        preset.isRunning = obj.getBoolean("running");

                    } catch (JSONException e) {
                        // Something went wrong!
                        e.printStackTrace();
                    }
                }
                //Parsing Scheduling
                if (obj.has("schedule")) {
                    JSONObject schedule = obj.getJSONObject("schedule");
                    //print(schedule)
                    if (schedule.has("cronExpression")) {
                        preset.strCronExpression = schedule.getString("cronExpression");
                    }
                    preset.bEnabled = schedule.getBoolean("enabled");
                    if (schedule.has("timeZone")) {
                        preset.strTimeZone = schedule.getString("timeZone");
                    }
                    if (schedule.has("startDate")) {
                        preset.strStartDate = schedule.getString("startDate");
                    }
                    if (schedule.has("endDate")) {
                        preset.strEndDate = schedule.getString("endDate");
                    }
                    if (schedule.has("durationMinutes")) {
                        preset.durationMinutes = schedule.getInt("durationMinutes");
                    }
                }
                presetList.add(preset);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface RepeatMode {
        String NONE = "None";
        String DAILY = "Daily";
        String WEEKLY = "Weekly";
        String MONTHLY = "Monthly";
    }

}
