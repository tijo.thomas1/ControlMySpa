package com.controlmyspa.ownerappnew.model

data class ValidateUsername(
        var status: Int, var message: String
)