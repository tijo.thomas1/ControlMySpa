package com.controlmyspa.ownerappnew.model

data class AccountData(var lastName: String?, var firstName: String?, var phone: String?, var email: String?, var address: Address)

data class Address(var address1: String?, var address2: String?, var city: String?, var state: String?, var country: String?, var zip: String?)