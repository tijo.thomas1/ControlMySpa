package com.controlmyspa.ownerappnew.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.Spanned
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.controlmyspa.ownerappnew.R
import com.controlmyspa.ownerappnew.model.AccountData
import com.controlmyspa.ownerappnew.model.Components
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.model.SpaComponent
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.truncate

const val SCAN_PERIOD = 10000L
val STATUS_CHARACTERISTIC: UUID = UUID.fromString("29cfdf92-dda0-4ab6-95e8-8d82b323783b")
val ULTRA_SERVICE: UUID = UUID.fromString("6d6c23e9-cf57-4706-b7ae-a74936862f82")
val DEVICE_INFORMATION_SERIAL_NUMBER_CHARACTERISTIC: UUID = UUID.fromString("00002A25-0000-1000-8000-00805f9b34fb")
val DEVICE_INFORMATION_SERVICE: UUID = UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb")

const val NO_SPA_DETECTED = 101
const val SPA_DETECTION_DONE = 202
const val INTENT_SERIAL_NO = "serial_number"
const val INTENT_SIGNAL_STRENGTH = "signal_strength"
const val INTENT_CLOUD_CONNECTED = "cloud_connected"
const val IS_MULTIPLE = "is_multiple"
const val PREF_NAME = "ControlMySpa"
const val PRIVATE_MODE = 0
const val PREF_SERIAL_NUMBER = "serial_number"
const val PREF_MAIL_DATA = "mailing_data"
const val LOCK_PANEL = "LOCK_PANEL"
const val UNLOCK_PANEL = "UNLOCK_PANEL"
const val BUNDLE_EMAIL_SETUP = "email"
private const val FAHRENHEIT_TO_CELSIUS_CONSTANT: Float = 5.0f / 9.0f
private const val CELSIUS_TO_FAHRENHEIT_TO_CONSTANT: Float = 1.8f //9.0f / 5.0f
private const val FAHRENHEIT_TO_CELSIUS_PAYLOAD_CONSTANT = 32.0f
const val SEEK_DELTA = 50F
var baseCzDetails: CzDetails? = null
var accountData: AccountData? = null
const val SPA_STATE_UPDATED = "SPA_STATE_UPDATED"
const val TZL_CONNECTED = "TZL_CONNECTED"
const val CHROMAZONE = "CHROMAZON3"

const val SCHEDULE_INTENT_REQUEST_CODE = 1234


var firebaseCrashlyticsCustomKeyAdded: Boolean = false

/*For Temp Page */
var mLastY = 0f
var mSliderLeftX = 0f
var mSliderTopY = 0f
var mSliderWidth = 0f
var mSliderHeight = 0f
var mMaxY = 0f
var mCurrRangeMaxY = mMaxY
var mMinY = 0f
var mCurrRangeMinY = mMinY
var mTempSlotsMaxYArray = ArrayList<Float>()
var mTempSlotsMinYArray = ArrayList<Float>()
var mTempArray = ArrayList<Float>()
var mRangeMaxTemp = 0
var mRangeMinTemp = 0
var mMeterRange = 0
var mYRange = 0f
var mUnitInY = 0f
var mCurrTempMarkerHeight = 0f


@BindingAdapter("stringText")
fun setDataAsString(mEditText: EditText, value: String?) {
    value?.let { mEditText.setText(it) }
}

@BindingAdapter("stringText")
fun setDataAsString(mEditText: EditText, value: Int?) {
    value?.toString().let { mEditText.setText(it) }
}

@BindingAdapter("stringText")
fun setDataAsString(mTextView: TextView, value: String?) {
    value?.let { mTextView.text = it }
}

@BindingAdapter("stringText")
fun setDataAsString(mTextView: TextView, value: Int?) {
    value?.toString().let { mTextView.text = it }
}

@BindingAdapter("stringText")
fun setDataAsString(mTextView: TextView, value: CharSequence?) {
    value?.toString().let { mTextView.text = value.toString() }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("control_mode_name")
fun setControlsModeName(mTextView: TextView, component: Components) {
    mTextView.text = if (TextUtils.isEmpty(component.port))
        component.name.replace("_", " ").toUpperCase(Locale.ENGLISH)
    else component.name.replace("_", " ").toUpperCase(Locale.ENGLISH) + " ${component.port.toInt() + 1}"

    if (component.value == mTextView.context.getString(R.string.lighting) || component.componentType == "CIRCULATION_PUMP") {
        mTextView.isSelected = true
        mTextView.setSingleLine()
        mTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
    }
    mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
}

fun changeLocalisedValueForControlsValue(value: String, mContext: Context): String {
    return when (value) {
        SpaComponent.ControlState.HI, SpaComponent.ControlState.HIGH -> mContext.getString(R.string.high).toUpperCase(Locale.ENGLISH)
        SpaComponent.ControlState.LOW -> mContext.getString(R.string.low).toUpperCase(Locale.ENGLISH)
        SpaComponent.ControlState.OFF -> mContext.getString(R.string.off)
        SpaComponent.ControlState.ON -> mContext.getString(R.string.on)
        else -> value
    }
}

@BindingAdapter("control_mode")
fun setControlsMode(mTextView: TextView, component: Components) {
    mTextView.text = changeLocalisedValueForControlsValue(component.value, mTextView.context)
    if (component.componentType == "LIGHT" || component.componentType == "BLOWER") {
        mTextView.text = if (component.value == SpaComponent.ControlState.HI || component.value == SpaComponent.ControlState.HIGH)
            changeLocalisedValueForControlsValue(SpaComponent.ControlState.ON, mTextView.context)
        else changeLocalisedValueForControlsValue(SpaComponent.ControlState.OFF, mTextView.context)
    }

    when (component.value) {
        mTextView.context.getString(R.string.lighting) -> {
            mTextView.text = component.value.toUpperCase(Locale.ENGLISH)
            //mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
        }
        LOCK_PANEL -> mTextView.text = mTextView.context.getString(R.string.CMS_panel_lock)
        UNLOCK_PANEL -> mTextView.text = mTextView.context.getString(R.string.CMS_panel_unlock)
    }

}

@BindingAdapter("control_images")
fun setControlsImages(mImageView: ImageView, component: Components) {
    when (component.value) {
        SpaComponent.ControlState.ON -> {
            mImageView.setImageResource(R.drawable.ctrl_on)
        }
        SpaComponent.ControlState.LOW ->
            mImageView.setImageResource(R.drawable.ctrl_low)
        SpaComponent.ControlState.MED ->
            mImageView.setImageResource(R.drawable.ctrl_med)
        SpaComponent.ControlState.HI -> {
            if (component.componentType == "LIGHT" || component.componentType == "BLOWER") {
                mImageView.setImageResource(R.drawable.ctrl_on)
            } else {
                mImageView.setImageResource(R.drawable.ctrl_hi)
            }
        }
        SpaComponent.ControlState.HIGH -> {
            if (component.componentType == "LIGHT" || component.componentType == "BLOWER") {
                mImageView.setImageResource(R.drawable.ctrl_on)
            } else {
                mImageView.setImageResource(R.drawable.ctrl_hi)
            }
        }
        SpaComponent.ControlState.OFF ->
            mImageView.setImageResource(R.drawable.ctrl_off)
        mImageView.context.getString(R.string.lighting) -> {
            mImageView.setImageResource(R.drawable.tz_lighting)
        }
        LOCK_PANEL -> {
            mImageView.setImageResource(R.drawable.ic_lock)
            mImageView.layoutParams.height = 150
            mImageView.layoutParams.width = 150
        }
        UNLOCK_PANEL -> {
            mImageView.setImageResource(R.drawable.ic_unlock)
            mImageView.layoutParams.height = 150
            mImageView.layoutParams.width = 150
        }
    }
}

/*fun convertTemperature(temp: String, isCelsius: Boolean): String {
    var floatedValue: Float? = temp.toFloatOrNull()
    if (floatedValue == null) {
        floatedValue = ("$temp.0").toFloat()
    }
    Log.e("TEMPERATURE ROUNDED", "CELSIUS : ${roundToHalf((floatedValue - FAHRENHEIT_TO_CELSIUS_PAYLOAD_CONSTANT) * FAHRENHEIT_TO_CELSIUS_CONSTANT)} FAHRENHEIT : $temp")
    return if (isCelsius) String.format(Locale.ENGLISH, "%.1f", roundToHalf((floatedValue - FAHRENHEIT_TO_CELSIUS_PAYLOAD_CONSTANT) * FAHRENHEIT_TO_CELSIUS_CONSTANT)) + "°"
    else "${Integer.parseInt(String.format("%.0f", floatedValue))}°"
}

fun roundToHalf(value: Float): Float = (value * 2).roundToLong() / 2.0f*/

fun celsiusToFahrenheit(c: Float): Double {
    return (c * 1.8) + 32
}

fun convertTemperature(f: String, isCelsius: Boolean): String {
    Log.e("QWERTYR", f)
    var floatedValue: Float? = f.toFloatOrNull()
    if (floatedValue == null) {
        floatedValue = ("$f.0").toFloat()
    }
    /*(roundToHalf((floatedValue - FAHRENHEIT_TO_CELSIUS_PAYLOAD_CONSTANT) * FAHRENHEIT_TO_CELSIUS_CONSTANT).toDouble())*/
    return if (isCelsius) String.format(Locale.ENGLISH, "%.1f°", roundHalf(((floatedValue - 32) / 1.8)))
    else "${Integer.parseInt(String.format("%.0f", floatedValue))}°"
}

fun roundToHalf(f: Float): Float {
    Log.e("QWERTYR", "$f")
    return (f * 2) / 2.0f
}

fun roundHalf(number: Double): Double {
    val diff = number - number.toInt()
    return if (diff < 0.25) number.toInt().toDouble() else if (diff < 0.75) number.toInt() + 0.5 else (number.toInt() + 1).toDouble()
}

fun roundUp(value: Double, toNearest: Double): Double { // For X.0 or X.5
    //Log.e("QWERTY", "$value ${abs(value - truncate(value))}")

    val tempCheck = abs(value - truncate(value))
    /*if (tempCheck == 0.50 && tempCheck == 0.00)
        return value*/
    if (tempCheck in 0.51..0.78) { // 1.64
        return floor(value / toNearest) * toNearest
    }
    if (tempCheck in 0.89..0.99999999999) { // 1.89
        return ceil(value / toNearest) * toNearest
    }
    if (tempCheck in 0.00..0.45) { // 1.11
        return floor(value / toNearest) * toNearest
    }
    if (tempCheck in 0.34..0.50) { // 1.45
        return ceil(value / toNearest) * toNearest
    }
    print("value ===============>$value")
    return floor(value / toNearest) * toNearest
}

@BindingAdapter("formatDate")
fun formatDateFromServer(mTextView: TextView, dateString: String?) {
    val fmt = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ", Locale.ENGLISH)
    val date: Date? = dateString?.let { fmt.parse(it) }
    val fmtOut = SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH)
    mTextView.text = date?.let { fmtOut.format(it) }
}

@BindingAdapter("marquee")
fun setMarquee(mTextView: TextView, value: Boolean) {
    mTextView.isSelected = value
    mTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
    mTextView.isSingleLine = value
}

@BindingAdapter("imageDrawable")
fun setImageDrawable(mImageView: ImageView, drawable: Drawable) {
    Glide.with(mImageView.context).asDrawable().load(drawable).into(mImageView)
}

fun requestServiceMail(mContext: Context, email: String, message: Spanned) {
    val subject = mContext.getString(R.string.request_service)
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "message/rfc822"
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
    intent.putExtra(Intent.EXTRA_TEXT, message)
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.setPackage("com.google.android.gm")
    if (intent.resolveActivity(mContext.packageManager) != null) mContext.startActivity(intent) else Toast.makeText(mContext, mContext.getString(R.string.gmail_app_is_not_installed),
            Toast.LENGTH_SHORT).show()
}

fun formatDate(dateString: String?): String? {
    val fmt = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ", Locale.ENGLISH)
    val date: Date? = dateString?.let { fmt.parse(it) }
    val fmtOut = SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH)
    return date?.let { fmtOut.format(it) }
}