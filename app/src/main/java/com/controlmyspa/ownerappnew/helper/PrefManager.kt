package com.controlmyspa.ownerappnew.helper

import android.content.Context
import android.content.SharedPreferences

class PrefManager(context: Context) {
    private val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
    private val editor: SharedPreferences.Editor = pref.edit()


    fun addSerialNumber(serialNumber: String) {
        editor.putString(PREF_SERIAL_NUMBER, serialNumber).apply()
    }

    fun getSerialNumber(): String? {
        return pref.getString(PREF_SERIAL_NUMBER, "")
    }


    fun addMailData(data: String) {
        editor.putString(PREF_MAIL_DATA, data).apply()
    }

    fun getMailData(): String? {
        return pref.getString(PREF_MAIL_DATA, "")
    }

}