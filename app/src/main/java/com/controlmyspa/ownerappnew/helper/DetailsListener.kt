package com.controlmyspa.ownerappnew.helper

import com.controlmyspa.ownerappnew.model.CzDetails

interface DetailsListener {
    fun onDataLoaded(czDetails: CzDetails)
}