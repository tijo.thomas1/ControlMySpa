package com.controlmyspa.ownerappnew.helper

import android.graphics.Color
import com.controlmyspa.ownerappnew.R


enum class PasswordStrength(var msg: Int, var color: Int) {
    MIN7CHAR(R.string.must_contain_at_least_7_characters, Color.parseColor("#61ad85")),
    UPPER_OR_LOWER(R.string.must_contain_at_least_1_letter_a_z_and_a_z, Color.parseColor("#4d8a6a")),
    NUMBER(R.string.must_contain_at_least_1_number_0_9, Color.parseColor("#3a674f")),
    SPECIAL_CHAR(R.string.must_contain_at_least_1_special_character, Color.parseColor("#264535"));

    companion object {
        private const val MIN_LENGTH = 7
        private const val MAX_LENGTH = 15
        fun calculate(password: String): PasswordStrength {
            var score = 0
            // boolean indicating if password has an upper case
            var upper = false
            // boolean indicating if password has a lower case
            var lower = false
            // boolean indicating if password has at least one digit
            var digit = false
            // boolean indicating if password has at least one special char
            var specialChar = false
            for (element in password) {
                if (!specialChar && !Character.isLetterOrDigit(element)) {
                    score= 3
                    specialChar = true
                } else {
                    if (!digit && Character.isDigit(element)) {
                        score=2
                        digit = true
                    } else {
                        if (!upper || !lower) {
                            if (Character.isUpperCase(element)) {
                                upper = true
                            } else {
                                lower = true
                            }
                            if (upper && lower) {
                                score=1
                            }
                        }
                    }
                }
            }
            val length = password.length
            if (length > MAX_LENGTH) {
                score++
            } else if (length < MIN_LENGTH) {
                score = 0
            }
            when (score) {
                0 -> return MIN7CHAR
                1 -> return UPPER_OR_LOWER
                2 -> return NUMBER
                3 -> return SPECIAL_CHAR
                else -> {
                }
            }
            return SPECIAL_CHAR
        }
    }
}