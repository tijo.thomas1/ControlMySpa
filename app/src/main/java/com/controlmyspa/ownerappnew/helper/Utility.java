package com.controlmyspa.ownerappnew.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import com.controlmyspa.ownerappnew.R;
import com.controlmyspa.ownerappnew.utils.Constants;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;

public class Utility {
    private static final String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static boolean isValidIP(final String ip) {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isInternetConnectivity(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null;
    }

    public static boolean isHavingNetworkConnectivity() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection)(new URL("https://www.google.com").openConnection());
            httpURLConnection.setRequestProperty("User-Agent", "Test");
            httpURLConnection.setRequestProperty("Connection", "close");
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.connect();
            return (httpURLConnection.getResponseCode() == 200);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void helpDialog(Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context,
                R.style.AlertDialogHelpScreen));
        // Get the layout inflater
        LayoutInflater inflater = context.getLayoutInflater();
        // Inflate and set the layout for the dialog
        final View dialogView = inflater.inflate(R.layout.help_layout, null);
        builder.setView(dialogView);
        AlertDialog dialog = builder.create();
        //transparent theme
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//style
        dialog.show();
    }

    public static String getSelectedLanguageId(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_LANGUAGE_STORE, MODE_PRIVATE);
        Log.d("Lng", preferences.getString(Constants.PREF_COUNTRY_CODE, ""));
        return preferences.getString(Constants.PREF_COUNTRY_CODE, "");
    }

}
