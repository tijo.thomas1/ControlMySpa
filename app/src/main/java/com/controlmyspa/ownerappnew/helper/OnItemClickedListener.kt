package com.controlmyspa.ownerappnew.helper

import android.bluetooth.le.ScanResult

interface OnItemClickedListener {
    fun onItemClicked(scanResult: ScanResult)
}