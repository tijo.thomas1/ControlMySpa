package com.controlmyspa.ownerappnew.helper

interface TimezoneDataListener {
    fun onTimezoneChanged(
            timezoneId: String
    )
}