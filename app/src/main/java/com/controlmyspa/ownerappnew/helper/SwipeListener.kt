package com.controlmyspa.ownerappnew.helper

interface SwipeListener {
    fun onChangeTemperatureFragment()
    fun onChangeControlsFragment()
    fun drawerLocked()
    fun drawerUnLocked()
    fun requestServiceStarted()
    fun requestServiceCompleted()
}