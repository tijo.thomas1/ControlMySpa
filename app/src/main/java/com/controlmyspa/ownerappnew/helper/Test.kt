package com.controlmyspa.ownerappnew.helper


val italian = arrayOf(
        "Nuovo? Connect My Spa",
        "Control My Spa",
        "Contattaci",
        "Spa",
        "Orari",
        "Impostazioni spa e impostazioni account",
        "Schermata Home",
        "Nome utente",
        "Parola d'ordine",
        "Ricorda",
        "Imposta temp",
        "Temp. Attuale",
        "Pannello",
        "BLOCCATO",
        "SBLOCCATO",
        "OFF",
        "ON",
        "ALTA",
        "BASSA",
        "Numero seriale gateway CMS",
        "Numero dello Spa Controller",
        "Versione software Spa controller",
        "Impostazioni Dip Switch",
        "Setup attuale #",
        "Ciclo di filtrazione",
        "Ultima voce nel registro errori",
        "Sensore temp A",
        "Sensore temp B",
        "Pronta",
        "Riposo",
        "DISABILITATA",
        "Necessaria Manutenzone",
        "Aggiorna account",
        "Aggiungere nuovo",
        "Modalità",
        "Range",
        "Salva",
        "Orologio della tua Spa",
        "Ora attuale Spa",
        "Aggiorna ora Spa",
        "Contattaci",
        "Durata",
        "Ripetizioni",
        "Il mio nuovo programma Spa",
        "Annulla",
        "Ok",
        "Inserisci un nome",
        "Nome",
        "Se non si dispone di questo codice, contattare il venditore",
        "Non hai programmi",
        "Iniziare alle",
        "Aggiornamento in corso",
        "NO, TORNA INDIETRO",
        "Spegnere",
        "In funzione",
        "Termini e condizioni"
)

val swedish = arrayOf(
        "Ny? Anslut mitt spa",
        "Control My Spa",
        "Kontakta oss",
        "spa",
        "scheman",
        "Spa-inställningar och kontoinställningar",
        "Hemskärm",
        "Användarnamn",
        "Lösenord",
        "Kom ihåg mig",
        "Sätta temperatur",
        "Nuvarande temp",
        "Panel",
        "LÅST",
        "OLÅST",
        "AV",
        "PÅ",
        "HÖG",
        "LÅG",
        "CMS Gateway Serienummer",
        "Spa kontroller part #",
        "Spa kontroller mjukvaruversion",
        "Hög / låg status",
        "Dipswitch-inställningar",
        "Nuvarande inställningsnummer",
        "Filtercykel",
        "Sista posten i felloggen",
        "Sensor A temp",
        "Sensor B temp",
        "Redo",
        "Resten",
        "INAKTIVERAD",
        "Begär service",
        "Uppdatera konto",
        "Lägg till ny",
        "Mode",
        "Räckvidd",
        "Spara",
        "Din spa-klocka",
        "Aktuell tid på Spa",
        "Uppdatera Spa Time",
        "Ring oss",
        "Varaktighet",
        "upprepningar",
        "Mitt nya spa-schema",
        "Avbryt",
        "Ok",
        "Ange ett namn.",
        "namn",
        "Om du inte har denna kod, vänligen kontakta inköpsställe",
        "Du har inga scheman",
        "Börja på",
        "Uppdatering pågår",
        "Nej, gå tillbaka",
        "Stäng av",
        "Runs",
        "Villkor"
)
val german = arrayOf(
        "Neu? Mein Spa verbinden",
        "Control My Spa",
        "Kontaktiere uns",
        "Das Spa von",
        "Zeitpläne",
        "Spa-Einstellungen und Kontoeinstellungen",
        "Startbildschirm",
        "Benutzername",
        "Passwort",
        "In Erinnerung behalten",
        "Temp. einstellen",
        "Aktuelle Temp.",
        "Panel",
        "GESPERRT",
        "ENTSPERRT",
        "AUS",
        "AN",
        "HOCH",
        "NIEDRIG",
        "CMS Gateway Seriennummer",
        "Spa Controller Art.Nummer",
        "Spa Controller Software Version",
        "High / Low Range Status",
        "Dipschalter-Einstellungen",
        "Aktuelle Setup-Nr.",
        "Filterzyklus",
        "Letzter Eintrag im Fehlerprotokoll",
        "Sensor A Temp",
        "Sensor B Temp",
        "Bereit",
        "Ruhestand",
        "DEAKTIVIERT",
        "Service anfordern",
        "Konto aktualisieren",
        "Neue hinzufügen",
        "Modus",
        "Reichweite",
        "speichern",
        "Ihre Spa-Uhr",
        "Aktuelle Zeit im Spa",
        "Spa-Zeit aktualisieren",
        "Rufen Sie uns an",
        "Dauer",
        "Wiederholungen",
        "Mein neuer Spa-Zeitplan",
        "Löschen",
        "OK",
        "Name eintippen",
        "Name",
        "Wenn Sie diesen Code nicht haben, wenden Sie sich bitte an Ihre Verkaufsstelle.",
        "Sie haben keine Zeitpläne",
        "Anfangen um",
        "Update läuft",
        "NEIN, zurückgehen",
        "Ausschalten",
        "Läuft",
        "Geschäftsbedingungen"
)
val french = arrayOf(
        "Nouveau? Brancher My Spa",
        "Controler My Spa",
        "Nous contacter",
        "Le spa de",
        "Des horaires",
        "Paramètres du spa et paramètres du compte",
        "Écran d'accueil",
        "Nom d'utilisateur",
        "Mot de passe",
        "Rappelez-vous en",
        "Régler la température",
        "Temp actuelle",
        "Panneau",
        "FERMÉ À CLÉ",
        "DÉVERROUILLÉ",
        "ETEINT",
        "ALLUMÉ",
        "HAUTE",
        "BASSE",
        "CMS Gateway # de série",
        "N ° de pièce du contrôleur de spa",
        "Version du logiciel du contrôleur de spa",
        "Statut de plage haute / basse",
        "Paramètres du commutateur DIP",
        "Configuration actuelle #",
        "Cycle de filtrage",
        "Dernière entrée dans le journal des défauts",
        "Capteur A temp",
        "Capteur B temp",
        "Prêt",
        "Au Repos",
        "DÉSACTIVÉE",
        "Appel Technicien",
        "Compte mis à jour",
        "Ajouter un nouveau",
        "Mode",
        "Intervalle",
        "Sauvegarder",
        "Votre horloge de spa",
        "Heure actuelle du  Spa",
        "Mettre à jour l'heure du spa",
        "Appelez-nous",
        "Durée",
        "Répètitions",
        "Mon nouveau programme de spa",
        "Annuler",
        "D'accord",
        "Entrez un nom.",
        "Nom",
        "Si vous ne disposez pas de ce code, veuillez contacter votre lieu d'achat.",
        "Vous n'avez pas de programme",
        "Commencer à",
        "Mise à jour en cours",
        "NON, RETOUR",
        "Eteindre",
        "Fonctionne",
        "Termes et conditions"
)
/*var stringDataToBeUpdatedItalian = "<string name=\"new_connect_my_spa\">${italian[0]}</string>" +
        "<string name=\"control_my_spa_navigation\">${italian[1]}</string>" +
        "<string name=\"contact_us\">${italian[2]}</string>" +
        "<string name=\"whos_spa\">${italian[3]}</string>" +
        "<string name=\"schedules\">${italian[4]}</string>" +
        "<string name=\"spa_account_settings\">${italian[5]}</string>" +
        "<string name=\"home\">${italian[6]}</string>" +
        "<string name=\"username\">${italian[7]}</string>" +
        "<string name=\"password\">${italian[8]}</string>" +
        "<string name=\"remember_me\">${italian[9]}</string>" +
        "<string name=\"set_temp\">${italian[10]}</string>" +
        "<string name=\"current_temp\">${italian[11]}</string>" +
        "<string name=\"CMS_panel_name\">${italian[12]}</string>" +
        "<string name=\"CMS_panel_lock\">${italian[13]}</string>" +
        "<string name=\"CMS_panel_unlock\">${italian[14]}</string>" +
        "<string name=\"off\">${italian[15]}</string>" +
        "<string name=\"on\">${italian[16]}</string>" +
        "<string name=\"high\">${italian[17]}</string>" +
        "<string name=\"low\">${italian[18]}</string>" +
        "<string name=\"cms_gateway_serial\">${italian[19]}</string>" +
        "<string name=\"spa_controller_part\">${italian[20]}</string>" +
        "<string name=\"spa_controller_software_version\">${italian[21]}</string>" +
        "<string name=\"high_low_range_status\">${italian[22]}</string>" +
        "<string name=\"dipswitch_settings\">${italian[23]}</string>" +
        "<string name=\"current_setup\">${italian[24]}</string>" +
        "<string name=\"filter_cycle\">${italian[25]}</string>" +
        "<string name=\"last_fault_log_entry\">${italian[26]}</string>" +
        "<string name=\"sensor_a_temp\">${italian[27]}</string>" +
        "<string name=\"sensor_b_temp\">${italian[28]}</string>" +
        "<string name=\"ready\">${italian[29]}</string>" +
        "<string name=\"rest\">${italian[30]}</string>" +
        "<string name=\"disabled\">${italian[31]}</string>" +
        "<string name=\"request_service\">${italian[32]}</string>" +
        "<string name=\"update_account\">${italian[33]}</string>" +
        "<string name=\"add_new\">${italian[34]}</string>" +
        "<string name=\"mode\">${italian[35]} :</string>" +
        "<string name=\"range\">${italian[36]}</string>" +
        "<string name=\"save\">${italian[37]}</string>" +
        "<string name=\"your_spa_clock\">${italian[38]}</string>" +
        "<string name=\"current_time_on_spa\">${italian[39]}</string>" +
        "<string name=\"update_spa_time\">${italian[40]}</string>" +
        "<string name=\"call_us\">${italian[41]}</string>" +
        "<string name=\"duration\">${italian[42]}</string>" +
        "<string name=\"repeats\">${italian[43]}:</string>" +
        "<string name=\"my_new_spa_schedule\">${italian[44]}</string>" +
        "<string name=\"cancel\">${italian[45]}</string>" +
        "<string name=\"ok\">${italian[46]}</string>" +
        "<string name=\"enter_a_name\">${italian[47]}</string>" +
        "<string name=\"name\">${italian[48]}:</string>" +
        "<string name=\"if_you_do_not_have_this_code_please_contact_your_place_of_purchase\">${italian[49]}</string>" +
        "<string name=\"no_schedules\">${italian[50]}</string>" +
        "<string name=\"starts_at\">${italian[51]}</string>" +
        "<string name=\"update_in_progress\">${italian[52]}…</string>" +
        "<string name=\"no_go_back\">${italian[53]}</string>" +
        "<string name=\"close\">${italian[54]}</string>" +
        "<string name=\"runs\">${italian[55]}</string>" +
        "<string name=\"terms_and_conditions\">${italian[56]}</string>"*/

var stringDataToBeUpdatedFrench = "<string name=\"new_connect_my_spa\">${french[0]}</string>" +
        "<string name=\"control_my_spa_navigation\">${french[1]}</string>" +
        "<string name=\"contact_us\">${french[2]}</string>" +
        "<string name=\"whos_spa\">${french[3]}</string>" +
        "<string name=\"schedules\">${french[4]}</string>" +
        "<string name=\"spa_account_settings\">${french[5]}</string>" +
        "<string name=\"home\">${french[6]}</string>" +
        "<string name=\"username\">${french[7]}</string>" +
        "<string name=\"password\">${french[8]}</string>" +
        "<string name=\"remember_me\">${french[9]}</string>" +
        "<string name=\"set_temp\">${french[10]}</string>" +
        "<string name=\"current_temp\">${french[11]}</string>" +
        "<string name=\"CMS_panel_name\">${french[12]}</string>" +
        "<string name=\"CMS_panel_lock\">${french[13]}</string>" +
        "<string name=\"CMS_panel_unlock\">${french[14]}</string>" +
        "<string name=\"off\">${french[15]}</string>" +
        "<string name=\"on\">${french[16]}</string>" +
        "<string name=\"high\">${french[17]}</string>" +
        "<string name=\"low\">${french[18]}</string>" +
        "<string name=\"cms_gateway_serial\">${french[19]}</string>" +
        "<string name=\"spa_controller_part\">${french[20]}</string>" +
        "<string name=\"spa_controller_software_version\">${french[21]}</string>" +
        "<string name=\"high_low_range_status\">${french[22]}</string>" +
        "<string name=\"dipswitch_settings\">${french[23]}</string>" +
        "<string name=\"current_setup\">${french[24]}</string>" +
        "<string name=\"filter_cycle\">${french[25]}</string>" +
        "<string name=\"last_fault_log_entry\">${french[26]}</string>" +
        "<string name=\"sensor_a_temp\">${french[27]}</string>" +
        "<string name=\"sensor_b_temp\">${french[28]}</string>" +
        "<string name=\"ready\">${french[29]}</string>" +
        "<string name=\"rest\">${french[30]}</string>" +
        "<string name=\"disabled\">${french[31]}</string>" +
        "<string name=\"request_service\">${french[32]}</string>" +
        "<string name=\"update_account\">${french[33]}</string>" +
        "<string name=\"add_new\">${french[34]}</string>" +
        "<string name=\"mode\">${french[35]} :</string>" +
        "<string name=\"range\">${french[36]}</string>" +
        "<string name=\"save\">${french[37]}</string>" +
        "<string name=\"your_spa_clock\">${french[38]}</string>" +
        "<string name=\"current_time_on_spa\">${french[39]}</string>" +
        "<string name=\"update_spa_time\">${french[40]}</string>" +
        "<string name=\"call_us\">${french[41]}</string>" +
        "<string name=\"duration\">${french[42]}</string>" +
        "<string name=\"repeats\">${french[43]}:</string>" +
        "<string name=\"my_new_spa_schedule\">${french[44]}</string>" +
        "<string name=\"cancel\">${french[45]}</string>" +
        "<string name=\"ok\">${french[46]}</string>" +
        "<string name=\"enter_a_name\">${french[47]}</string>" +
        "<string name=\"name\">${french[48]}:</string>" +
        "<string name=\"if_you_do_not_have_this_code_please_contact_your_place_of_purchase\">${french[49]}</string>" +
        "<string name=\"no_schedules\">${french[50]}</string>" +
        "<string name=\"starts_at\">${french[51]}</string>" +
        "<string name=\"update_in_progress\">${french[52]}…</string>" +
        "<string name=\"no_go_back\">${french[53]}</string>" +
        "<string name=\"close\">${french[54]}</string>" +
        "<string name=\"runs\">${french[55]}</string>" +
        "<string name=\"terms_and_conditions\">${french[56]}</string>"


var stringDataToBeUpdatedGerman = "<string name=\"new_connect_my_spa\">${german[0]}</string>" +
        "<string name=\"control_my_spa_navigation\">${german[1]}</string>" +
        "<string name=\"contact_us\">${german[2]}</string>" +
        "<string name=\"whos_spa\">${german[3]}</string>" +
        "<string name=\"schedules\">${german[4]}</string>" +
        "<string name=\"spa_account_settings\">${german[5]}</string>" +
        "<string name=\"home\">${german[6]}</string>" +
        "<string name=\"username\">${german[7]}</string>" +
        "<string name=\"password\">${german[8]}</string>" +
        "<string name=\"remember_me\">${german[9]}</string>" +
        "<string name=\"set_temp\">${german[10]}</string>" +
        "<string name=\"current_temp\">${german[11]}</string>" +
        "<string name=\"CMS_panel_name\">${german[12]}</string>" +
        "<string name=\"CMS_panel_lock\">${german[13]}</string>" +
        "<string name=\"CMS_panel_unlock\">${german[14]}</string>" +
        "<string name=\"off\">${german[15]}</string>" +
        "<string name=\"on\">${german[16]}</string>" +
        "<string name=\"high\">${german[17]}</string>" +
        "<string name=\"low\">${german[18]}</string>" +
        "<string name=\"cms_gateway_serial\">${german[19]}</string>" +
        "<string name=\"spa_controller_part\">${german[20]}</string>" +
        "<string name=\"spa_controller_software_version\">${german[21]}</string>" +
        "<string name=\"high_low_range_status\">${german[22]}</string>" +
        "<string name=\"dipswitch_settings\">${german[23]}</string>" +
        "<string name=\"current_setup\">${german[24]}</string>" +
        "<string name=\"filter_cycle\">${german[25]}</string>" +
        "<string name=\"last_fault_log_entry\">${german[26]}</string>" +
        "<string name=\"sensor_a_temp\">${german[27]}</string>" +
        "<string name=\"sensor_b_temp\">${german[28]}</string>" +
        "<string name=\"ready\">${german[29]}</string>" +
        "<string name=\"rest\">${german[30]}</string>" +
        "<string name=\"disabled\">${german[31]}</string>" +
        "<string name=\"request_service\">${german[32]}</string>" +
        "<string name=\"update_account\">${german[33]}</string>" +
        "<string name=\"add_new\">${german[34]}</string>" +
        "<string name=\"mode\">${german[35]} :</string>" +
        "<string name=\"range\">${german[36]}</string>" +
        "<string name=\"save\">${german[37]}</string>" +
        "<string name=\"your_spa_clock\">${german[38]}</string>" +
        "<string name=\"current_time_on_spa\">${german[39]}</string>" +
        "<string name=\"update_spa_time\">${german[40]}</string>" +
        "<string name=\"call_us\">${german[41]}</string>" +
        "<string name=\"duration\">${german[42]}</string>" +
        "<string name=\"repeats\">${german[43]}:</string>" +
        "<string name=\"my_new_spa_schedule\">${german[44]}</string>" +
        "<string name=\"cancel\">${german[45]}</string>" +
        "<string name=\"ok\">${german[46]}</string>" +
        "<string name=\"enter_a_name\">${german[47]}</string>" +
        "<string name=\"name\">${german[48]}:</string>" +
        "<string name=\"if_you_do_not_have_this_code_please_contact_your_place_of_purchase\">${german[49]}</string>" +
        "<string name=\"no_schedules\">${german[50]}</string>" +
        "<string name=\"starts_at\">${german[51]}</string>" +
        "<string name=\"update_in_progress\">${german[52]}…</string>" +
        "<string name=\"no_go_back\">${german[53]}</string>" +
        "<string name=\"close\">${german[54]}</string>" +
        "<string name=\"runs\">${german[55]}</string>" +
        "<string name=\"terms_and_conditions\">${german[56]}</string>"

var stringDataToBeUpdatedSwedish = "<string name=\"new_connect_my_spa\">${swedish[0]}</string>" +
        "<string name=\"control_my_spa_navigation\">${swedish[1]}</string>" +
        "<string name=\"contact_us\">${swedish[2]}</string>" +
        "<string name=\"whos_spa\">${swedish[3]}</string>" +
        "<string name=\"schedules\">${swedish[4]}</string>" +
        "<string name=\"spa_account_settings\">${swedish[5]}</string>" +
        "<string name=\"home\">${swedish[6]}</string>" +
        "<string name=\"username\">${swedish[7]}</string>" +
        "<string name=\"password\">${swedish[8]}</string>" +
        "<string name=\"remember_me\">${swedish[9]}</string>" +
        "<string name=\"set_temp\">${swedish[10]}</string>" +
        "<string name=\"current_temp\">${swedish[11]}</string>" +
        "<string name=\"CMS_panel_name\">${swedish[12]}</string>" +
        "<string name=\"CMS_panel_lock\">${swedish[13]}</string>" +
        "<string name=\"CMS_panel_unlock\">${swedish[14]}</string>" +
        "<string name=\"off\">${swedish[15]}</string>" +
        "<string name=\"on\">${swedish[16]}</string>" +
        "<string name=\"high\">${swedish[17]}</string>" +
        "<string name=\"low\">${swedish[18]}</string>" +
        "<string name=\"cms_gateway_serial\">${swedish[19]}</string>" +
        "<string name=\"spa_controller_part\">${swedish[20]}</string>" +
        "<string name=\"spa_controller_software_version\">${swedish[21]}</string>" +
        "<string name=\"high_low_range_status\">${swedish[22]}</string>" +
        "<string name=\"dipswitch_settings\">${swedish[23]}</string>" +
        "<string name=\"current_setup\">${swedish[24]}</string>" +
        "<string name=\"filter_cycle\">${swedish[25]}</string>" +
        "<string name=\"last_fault_log_entry\">${swedish[26]}</string>" +
        "<string name=\"sensor_a_temp\">${swedish[27]}</string>" +
        "<string name=\"sensor_b_temp\">${swedish[28]}</string>" +
        "<string name=\"ready\">${swedish[29]}</string>" +
        "<string name=\"rest\">${swedish[30]}</string>" +
        "<string name=\"disabled\">${swedish[31]}</string>" +
        "<string name=\"request_service\">${swedish[32]}</string>" +
        "<string name=\"update_account\">${swedish[33]}</string>" +
        "<string name=\"add_new\">${swedish[34]}</string>" +
        "<string name=\"mode\">${swedish[35]} :</string>" +
        "<string name=\"range\">${swedish[36]}</string>" +
        "<string name=\"save\">${swedish[37]}</string>" +
        "<string name=\"your_spa_clock\">${swedish[38]}</string>" +
        "<string name=\"current_time_on_spa\">${swedish[39]}</string>" +
        "<string name=\"update_spa_time\">${swedish[40]}</string>" +
        "<string name=\"call_us\">${swedish[41]}</string>" +
        "<string name=\"duration\">${swedish[42]}</string>" +
        "<string name=\"repeats\">${swedish[43]}:</string>" +
        "<string name=\"my_new_spa_schedule\">${swedish[44]}</string>" +
        "<string name=\"cancel\">${swedish[45]}</string>" +
        "<string name=\"ok\">${swedish[46]}</string>" +
        "<string name=\"enter_a_name\">${swedish[47]}</string>" +
        "<string name=\"name\">${swedish[48]}:</string>" +
        "<string name=\"if_you_do_not_have_this_code_please_contact_your_place_of_purchase\">${swedish[49]}</string>" +
        "<string name=\"no_schedules\">${swedish[50]}</string>" +
        "<string name=\"starts_at\">${swedish[51]}</string>" +
        "<string name=\"update_in_progress\">${swedish[52]}…</string>" +
        "<string name=\"no_go_back\">${swedish[53]}</string>" +
        "<string name=\"close\">${swedish[54]}</string>" +
        "<string name=\"runs\">${swedish[55]}</string>" +
        "<string name=\"terms_and_conditions\">${swedish[56]}</string>"



