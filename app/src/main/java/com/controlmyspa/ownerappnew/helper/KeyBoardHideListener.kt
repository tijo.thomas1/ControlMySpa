package com.controlmyspa.ownerappnew.helper

interface KeyBoardHideListener {
    fun hideKeyBoardIfNeeded()
    fun openLoginPage()
    fun onClickedDrawerItem(position: Int)
}