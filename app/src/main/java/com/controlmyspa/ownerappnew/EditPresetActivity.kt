package com.controlmyspa.ownerappnew

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.controlmyspa.ownerappnew.databinding.ActivityEditPresetBinding
import com.controlmyspa.ownerappnew.helper.*
import com.controlmyspa.ownerappnew.model.SpaPreset
import com.controlmyspa.ownerappnew.service.SpaManager
import java.util.*
import kotlin.collections.ArrayList

class EditPresetActivity : AppCompatActivity() {
    private var strTitle: String? = null
    private var index = -1
    private var preset: SpaPreset = SpaPreset()
    private lateinit var binding: ActivityEditPresetBinding
    private val hourArray: MutableList<Int> = ArrayList()
    private val repeatUpdateHandler: Handler = Handler(Looper.getMainLooper())
    private var hours: Int = 0
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private val lowRangeLow: Float by lazy {
        baseCzDetails?.currentState?.setupParams?.lowRangeLow?.toFloat() ?: 50.toFloat()
    }
    private val lowRangeHigh: Float by lazy {
        baseCzDetails?.currentState?.setupParams?.lowRangeHigh?.toFloat() ?: 99.toFloat()
    }
    private val highRangeLow: Float by lazy {
        baseCzDetails?.currentState?.setupParams?.highRangeLow?.toFloat() ?: 80.toFloat()
    }
    private val highRangeHigh: Float by lazy {
        baseCzDetails?.currentState?.setupParams?.highRangeHigh?.toFloat() ?: 104.toFloat()
    }

    private val isCelsius by lazy { baseCzDetails?.currentState?.celsius }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_preset)
        setSupportActionBar(binding.mToolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //Get the index of preset for editing or -1 for new preset
        val extras = intent.extras
        if (extras != null) {
            index = extras.getInt("index")
            strTitle = extras.getString("name")
        }
        if (!TextUtils.isEmpty(strTitle)) {
            binding.editPresetTxtname.text = strTitle
        }
        loadData()

        while (hourArray.count() != 24) {
            hours = +1
            hourArray.add(hours)
        }
        /*preset.mFilter1Interval?.let {
            binding.filterHours.text = (it.toFloat().toInt() / 4).toString()
        }
        preset.mFilter2Interval?.let {
            binding.filterHours2.text = (it.toFloat().toInt() / 4).toString()
        }*/
        binding.btnNegativeFilter.setOnClickListener {
            val toInt = binding.filterHours.text.toString().toInt()
            if (toInt != 1) {
                binding.filterHours.text = (toInt - 1).toString()
                preset.mFilter1Interval = ((toInt - 1) * 4).toString()
            }
        }

        binding.btnPositiveFilter.setOnClickListener {
            val toInt = binding.filterHours.text.toString().toInt()
            if (toInt != hourArray.count()) {
                binding.filterHours.text = (toInt + 1).toString()
                preset.mFilter1Interval = ((toInt + 1) * 4).toString()
            }
        }
        binding.btnNegativeFilter2.setOnClickListener {
            val toInt = binding.filterHours2.text.toString().toInt()
            if (toInt != 1) {
                binding.filterHours2.text = (toInt - 1).toString()
                preset.mFilter2Interval = ((toInt - 1) * 4).toString()
            }
        }

        binding.btnPositiveFilter2.setOnClickListener {
            val toInt = binding.filterHours2.text.toString().toInt()
            if (toInt != hourArray.count()) {
                binding.filterHours2.text = (toInt + 1).toString()
                preset.mFilter2Interval = ((toInt + 1) * 4).toString()
            }
        }

        isCelsius?.let { changeTemperature(it) }

        when (preset.mTempRange) {
            HighLow.LOW -> {
                preset.mTempRange = HighLow.LOW
                binding.low.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
                binding.hi.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
            }
            else -> {
                preset.mTempRange = HighLow.HIGH
                binding.low.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
                binding.hi.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
            }
        }

        /*For Low High*/
        binding.low.setOnClickListener {
            preset.temperature = lowRangeLow
            preset.mTempRange = HighLow.LOW
            binding.low.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
            binding.hi.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
            isCelsius?.let { it1 -> changeTemperature(it1) }
        }
        binding.hi.setOnClickListener {
            preset.temperature = highRangeHigh
            preset.mTempRange = HighLow.HIGH
            binding.low.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
            binding.hi.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
            isCelsius?.let { it1 -> changeTemperature(it1) }
        }
        /*For Temp*/
        binding.btnNegative.setOnClickListener {
            decrementTempValues()
        }
        /*  binding.btnPositive.setOnLongClickListener {
              repeatUpdateHandler.post(incrementTempRunnable)
              false
          }
          binding.btnNegative.setOnLongClickListener {
              repeatUpdateHandler.post(decrementTempRunnable)
              false
          }*/

        binding.btnPositive.setOnClickListener {
            incrementTempValues()
        }

        restReadySetup()

        binding.ready.setOnClickListener {
            preset.mRunMode = "Ready"
            binding.ready.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
            binding.rest.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
        }
        binding.rest.setOnClickListener {
            preset.mRunMode = "Rest"
            binding.ready.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
            binding.rest.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
        }

        /*NEXT BUTTON*/
        binding.editPresetBtnsave.setOnClickListener {
            val intent = Intent(this@EditPresetActivity,
                    SchedulePresetActivity::class.java)
            intent.putExtra("preset", preset)
            intent.putExtra("index", index)
            startActivityForResult(intent, SCHEDULE_INTENT_REQUEST_CODE)
            /* binding.editPresetProgressBar.visibility = View.VISIBLE
             if (index == -1) {
                 SpaManager.requestSaveSpaPreset(this, preset, object : RestAPICallback {
                     override fun onSuccess(result: String) {
                         binding.editPresetProgressBar.visibility = View.GONE
                         onBackPressed()
                     }

                     override fun onFailure(result: String) {
                         binding.editPresetProgressBar.visibility = View.GONE
                         Log.d(TAG, result)
                         if (result == "Max") {
                             Toast.makeText(this@EditPresetActivity,
                                     R.string.already_has_themaximum_number_of_preset,
                                     Toast.LENGTH_SHORT).show()
                         } else {
                             Toast.makeText(this@EditPresetActivity,
                                     R.string.error_save,
                                     Toast.LENGTH_SHORT).show()
                         }
                     }
                 })
             } else {
                 SpaManager.requestUpdateSpaPreset(this, index, object : RestAPICallback {
                     override fun onSuccess(result: String) {
                         binding.editPresetProgressBar.visibility = View.GONE
                         Toast.makeText(this@EditPresetActivity,
                                 R.string.success_save,
                                 Toast.LENGTH_SHORT).show()
                         onBackPressed()
                     }

                     override fun onFailure(result: String) {
                         binding.editPresetProgressBar.visibility = View.GONE
                         Log.d(TAG, result)
                         Toast.makeText(this@EditPresetActivity, R.string.error_save,
                                 Toast.LENGTH_SHORT).show()
                     }
                 })
             }*/
        }
    }

    private fun decrementTempValues() {
        if (preset.mTempRange == HighLow.LOW) {
            /*LOW*/
            if (preset.temperature > lowRangeLow) {
                isCelsius?.let { celsius ->
                    if (celsius) {
                        preset.temperature -= 0.9f
                    } else preset.temperature -= 1
                    val convertedTemperature = convertTemperature(preset.temperature.toString(), celsius)
                    binding.presetTemperature.text = convertedTemperature
                    Log.e("isCelsius", "$celsius $convertedTemperature")
                }
            }
        } else {
            /*HIGH*/
            if (preset.temperature > highRangeLow) {
                isCelsius?.let { celsius ->
                    if (celsius) {
                        preset.temperature -= 0.9f
                    } else preset.temperature -= 1
                    val convertedTemperature = convertTemperature(preset.temperature.toString(), celsius)
                    binding.presetTemperature.text = convertedTemperature
                    Log.e("isCelsius", "$celsius $convertedTemperature")
                }
            }
        }
    }

    private fun incrementTempValues() {
        if (preset.mTempRange == HighLow.LOW) {
            if (preset.temperature < lowRangeHigh - 0.9) {
                Log.e("temperature", "${preset.temperature} $lowRangeHigh")
                isCelsius?.let { celsius ->
                    if (celsius) {
                        preset.temperature += 0.9f
                    } else preset.temperature += 1
                    val convertedTemperature = convertTemperature(preset.temperature.toString(), celsius)
                    binding.presetTemperature.text = convertedTemperature
                    Log.e("isCelsius", "$celsius $convertedTemperature")
                }
            }
        } else {
            if (preset.temperature < highRangeHigh - 0.9) {
                isCelsius?.let { celsius ->
                    if (celsius) {
                        preset.temperature += 0.9f
                    } else preset.temperature += 1
                    val convertedTemperature = convertTemperature(preset.temperature.toString(), celsius)
                    binding.presetTemperature.text = convertedTemperature
                    Log.e("isCelsius", "$celsius $convertedTemperature")
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SCHEDULE_INTENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }

    private fun changeTemperature(celsius: Boolean) {
        val convertedTemperature = convertTemperature(preset.temperature.toString(), celsius)
        binding.presetTemperature.text = convertedTemperature
    }


    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    private fun loadData() {
        if (index != -1) {
            if (index < SpaManager.m_PresetList.count()) {
                preset = SpaManager.m_PresetList[index]
                binding.editPresetTxtname.text = preset.strName
            } else {
                Toast.makeText(this, getString(R.string.error_later), Toast.LENGTH_LONG).show()
                onBackPressed()
            }
            return
        }

        preset = SpaPreset()
        preset.strName = strTitle
        baseCzDetails?.let {
            preset.temperature = it.currentState?.desiredTemp?.toFloat() ?: lowRangeLow
            preset.mRunMode = if (it.currentState?.runMode.equals("READY", true)) "READY" else "REST"
            preset.mTempRange = if (it.currentState?.tempRange == "HIGH") HighLow.HIGH else HighLow.LOW
        }

        preset.mFilter2Interval = (1 * 4).toString()
        preset.mFilter1Interval = (1 * 4).toString()
        restReadySetup()

    }

    private fun restReadySetup() {
        /*For Ready Rest*/
        if (preset.mRunMode.equals("Ready", true)) {
            binding.ready.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
            binding.rest.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
        } else {
            binding.ready.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
            binding.rest.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray))
        }
    }

    private val incrementTempRunnable = object : Runnable {
        override fun run() {
            repeatUpdateHandler.removeCallbacks(this)
            if (binding.btnPositive.isPressed) {
                incrementTempValues()
                repeatUpdateHandler.postDelayed(this, 50)
            }
        }
    }

    private val decrementTempRunnable = object : Runnable {
        override fun run() {
            repeatUpdateHandler.removeCallbacks(this)
            if (binding.btnNegative.isPressed) {
                decrementTempValues()
                repeatUpdateHandler.postDelayed(this, 50)
            }
        }
    }
}