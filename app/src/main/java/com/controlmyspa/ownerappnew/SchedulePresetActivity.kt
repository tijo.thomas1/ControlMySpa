package com.controlmyspa.ownerappnew

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.controlmyspa.ownerappnew.databinding.ActivitySchedulePresetBinding
import com.controlmyspa.ownerappnew.fragment.RepeatDialogFragment
import com.controlmyspa.ownerappnew.fragment.RepeatDialogFragment.OnRepeatModeListener
import com.controlmyspa.ownerappnew.fragment.TimezoneDialogFragment
import com.controlmyspa.ownerappnew.helper.MyContextWrapper
import com.controlmyspa.ownerappnew.helper.TimezoneDataListener
import com.controlmyspa.ownerappnew.helper.Utility
import com.controlmyspa.ownerappnew.helper.baseCzDetails
import com.controlmyspa.ownerappnew.materialdatetimepicker.time.TimePickerDialog
import com.controlmyspa.ownerappnew.model.SpaPreset
import com.controlmyspa.ownerappnew.service.RestAPICallback
import com.controlmyspa.ownerappnew.service.SpaManager
import com.controlmyspa.ownerappnew.utils.Constants
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class SchedulePresetActivity : AppCompatActivity(), OnRepeatModeListener, TimezoneDataListener {
    private var index = -1
    private var preset: SpaPreset = SpaPreset()
    private var strStartDate: String = ""
    private var strEndDate: String = ""
    private var strRepeatMode: String = ""
    private lateinit var binding: ActivitySchedulePresetBinding
    private var dataRepeat: Array<String>? = null
    private var dTime: Date = Date()
    private var setTime = Date()
    private var isMilitary = false

    private val mTimeZoneFragment by lazy {
        TimezoneDialogFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_schedule_preset)
        setSupportActionBar(binding.mToolBar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        baseCzDetails?.let {
            isMilitary = it.currentState?.military == true
        }
        binding.schedulePresetProgressBar.isIndeterminate = true
        binding.schedulePresetProgressBar.visibility = View.GONE
        dataRepeat = resources.getStringArray(R.array.array_repeat)
        val adapter = ArrayAdapter.createFromResource(this,
                R.array.days_duration, R.layout.spinner_item_white)
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        // Apply the adapter to the spinner
        binding.schedulePresetSpinnerduration.adapter = adapter
//exception case
        binding.schedulePresetCalendarview.setDateSelected(Calendar.getInstance(), true)


        binding.schedulePresetTxtstart.setOnClickListener {
            val calendar = Calendar.getInstance()
            calendar.time = dTime
            val hour = calendar[Calendar.HOUR_OF_DAY]
            val minute = calendar[Calendar.MINUTE]
            val dpd = TimePickerDialog.newInstance(
                    { _: TimePickerDialog?, hourOfDay: Int, min: Int, _: Int ->
                        calendar[Calendar.HOUR_OF_DAY] = hourOfDay
                        calendar[Calendar.MINUTE] = min
                        setTime = calendar.time
                        binding.schedulePresetTxtstart.text = if (isMilitary) sdf24.format(setTime) else sdf12.format(setTime)
                    },
                    hour,
                    minute, isMilitary
            )
            dpd.isCancelable = false
            //dpd.setCancelText(R.string.cancel);
            dpd.accentColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
            dpd.title = getString(R.string.starts_at)
            dpd.show(supportFragmentManager, "TimePickerDialog1")
        }
        binding.schedulePresetTxtrepeat.setOnClickListener {
            val fragment: DialogFragment = RepeatDialogFragment.newInstance(strStartDate, strEndDate, strRepeatMode)
            fragment.isCancelable = false
            fragment.show(supportFragmentManager, "dialog")
        }
        binding.schedulePresetCalendarview.state().edit().setMinimumDate(Calendar.getInstance()).commit()
        binding.schedulePresetCalendarview.showOtherDates = MaterialCalendarView.SHOW_ALL

        binding.btnSave.setOnClickListener {
            setParams()
            binding.schedulePresetProgressBar.visibility = View.VISIBLE
            if (index == -1) {
                SpaManager.requestSaveSpaPreset(this, preset, object : RestAPICallback {
                    override fun onSuccess(result: String) {
                        binding.schedulePresetProgressBar.visibility = View.GONE
                        setResult(Activity.RESULT_OK)
                        onBackPressed()
                    }

                    override fun onFailure(result: String) {
                        binding.schedulePresetProgressBar.visibility = View.GONE
                        Log.d(TAG, result)
                        if (result == "Max") {
                            Toast.makeText(this@SchedulePresetActivity,
                                    R.string.already_has_themaximum_number_of_preset,
                                    Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this@SchedulePresetActivity,
                                    R.string.error_save,
                                    Toast.LENGTH_SHORT).show()
                        }
                    }
                })
            } else {
                SpaManager.requestUpdateSpaPreset(this, preset, object : RestAPICallback {
                    override fun onSuccess(result: String) {
                        Log.d(TAG, result)
                        binding.schedulePresetProgressBar.visibility = View.GONE
                        Toast.makeText(this@SchedulePresetActivity, R.string.success_save, Toast.LENGTH_SHORT).show()
                        setResult(Activity.RESULT_OK)
                        onBackPressed()
                    }

                    override fun onFailure(result: String) {
                        binding.schedulePresetProgressBar.visibility = View.GONE
                        Log.d(TAG, result)
                        Toast.makeText(this@SchedulePresetActivity, R.string.error_save, Toast.LENGTH_SHORT).show()
                    }
                })
            }
        }
        binding.mTimeZoneTxtView.setOnClickListener {
            mTimeZoneFragment.show(supportFragmentManager, "TimeZoneDialog")
        }

        loadData()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase,
                Utility.getSelectedLanguageId(OwnerApplication.getInstance().applicationContext)))
    }

    @SuppressLint("SetTextI18n")
    private fun loadData() {
        index = intent.getIntExtra("index", -1)
        preset = intent.getSerializableExtra("preset") as SpaPreset
        binding.schedulePresetTxtname.text = preset.strName
        if (TimeUnit.MINUTES.toDays(preset.durationMinutes).toInt() == 0) {
            preset.durationMinutes = TimeUnit.DAYS.toMinutes(1.toLong())
            Log.e("DURATION", preset.durationMinutes.toString())
        }

        binding.schedulePresetChkenable.isChecked = preset.bEnabled
        if (preset.strCronExpression != null) { //New schedule or Not
            val params = preset.strCronExpression!!.split(" ").toTypedArray()
            //Current Date
            val calendar = Calendar.getInstance()
            val cDay = calendar[Calendar.DAY_OF_MONTH]
            val cMonth = calendar[Calendar.MONTH]
            val cDayOfWeek = calendar[Calendar.DAY_OF_WEEK]
            val cYear = calendar[Calendar.YEAR]
            /*minute, hour,day, month, dayOfWeek, year*/
            val minute = params[1]
            val hour = params[2]
            val day = params[3]
            val month = if (params[4] == "*" || params[4] == "?") {
                params[4]
            } else {
                (params[4].toInt() - 1).toString()
            }
            val dayOfWeek = if (params[5] == "*" || params[5] == "?") {
                params[5]
            } else (params[5].toInt() + 1).toString()
            val newday: Int
            var newmonth: Int
            val newyear: Int
            if (month == "*" && day == "*" && dayOfWeek == "?") { //Daily
                strRepeatMode = SpaPreset.RepeatMode.DAILY
                binding.schedulePresetCalendarview.setDateSelected(Calendar.getInstance(), true)
            } else if (month == "*" && day == "?") {
                strRepeatMode = SpaPreset.RepeatMode.WEEKLY
                newday = if (cDayOfWeek > dayOfWeek.toInt()) {
                    dayOfWeek.toInt() + 7 - cDayOfWeek
                } else {
                    dayOfWeek.toInt() - cDayOfWeek
                }
                calendar.add(Calendar.DAY_OF_YEAR, newday)
                binding.schedulePresetCalendarview.setDateSelected(calendar, true)
            } else if (month == "*" && dayOfWeek == "?") {
                strRepeatMode = SpaPreset.RepeatMode.MONTHLY
                newmonth = cMonth
                newday = day.toInt()
                newyear = cYear
                if (cDay > day.toInt()) {
                    newmonth = cMonth + 1
                }
                calendar[newyear, newmonth] = newday
                binding.schedulePresetCalendarview.setDateSelected(calendar, true)
            } else {
                strRepeatMode = SpaPreset.RepeatMode.NONE
                newmonth = month.toInt()
                newday = day.toInt()
                newyear = cYear

                calendar[newyear, newmonth] = newday
                binding.schedulePresetCalendarview.setDateSelected(calendar, true)
            }
            val repeatMode = when (strRepeatMode) {
                SpaPreset.RepeatMode.DAILY -> {
                    dataRepeat?.get(0) ?: ""
                }
                SpaPreset.RepeatMode.WEEKLY -> {
                    dataRepeat?.get(1) ?: ""
                }
                SpaPreset.RepeatMode.NONE -> {
                    dataRepeat?.get(2) ?: ""
                }
                else -> ""
            }
            binding.schedulePresetTxtrepeat.text = repeatMode
            //Set Start time.
            calendar[Calendar.HOUR_OF_DAY] = hour.toInt()
            calendar[Calendar.MINUTE] = minute.toInt()
            dTime = calendar.time
            val strTime = if (isMilitary) sdf24.format(dTime) else sdf12.format(dTime)
            binding.schedulePresetTxtstart.text = strTime
            when (TimeUnit.MINUTES.toDays(preset.durationMinutes).toInt()) {
                1 -> binding.schedulePresetSpinnerduration.setSelection(0)
                2 -> binding.schedulePresetSpinnerduration.setSelection(1)
                3 -> binding.schedulePresetSpinnerduration.setSelection(2)
                7 -> binding.schedulePresetSpinnerduration.setSelection(3)
                else -> binding.schedulePresetSpinnerduration.setSelection(0)
            }
            Log.e("DURATION", TimeUnit.MINUTES.toDays(preset.durationMinutes).toString())
            //Set StartDate, EndDate
            if (preset.strStartDate != "" && preset.strEndDate != "") {
                strStartDate = preset.strStartDate?.substring(0, 10).toString()
                strEndDate = preset.strEndDate?.substring(0, 10).toString()
            }
            binding.mTimeZoneTxtView.text = preset.strTimeZone
        } else {
            binding.schedulePresetCalendarview.setDateSelected(Calendar.getInstance(), true)
            strRepeatMode = SpaPreset.RepeatMode.NONE
            binding.schedulePresetTxtrepeat.text = dataRepeat?.get(2) ?: ""
            binding.schedulePresetTxtstart.text = if (isMilitary) sdf24.format(setTime) else sdf12.format(setTime)
            val mTimeZoneId = TimeZone.getDefault().id
            preset.strTimeZone = mTimeZoneId
            binding.mTimeZoneTxtView.text = TimeZone.getDefault().id
        }
    }

    override fun onRepeatModeChange(startDate: String, endDate: String, repeatMode: String) {
        strStartDate = if (startDate.isEmpty()) {
            val calendar = Calendar.getInstance()
            Constants.sdf.format(calendar.time)
        } else {
            startDate
        }
        strEndDate = endDate
        strRepeatMode = repeatMode
        binding.schedulePresetTxtrepeat.text = repeatMode
    }

    private fun setParams() {
        val calendar = Calendar.getInstance()
        preset.strMode = when (strRepeatMode) {
            dataRepeat?.get(0) -> SpaPreset.RepeatMode.DAILY
            dataRepeat?.get(1) -> SpaPreset.RepeatMode.WEEKLY
            else -> SpaPreset.RepeatMode.NONE
        }
        preset.strStartDate = Constants.sdf.format(calendar.time)
        //preset!!.strEndDate = strEndDate
        preset.bEnabled = true
        val strStartTime = binding.schedulePresetTxtstart.text.toString()
        try {
            val startTime = if (isMilitary) sdf24.parse(strStartTime) else sdf12.parse(strStartTime)
            if (startTime != null) {
                calendar.time = startTime
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val hour = calendar[Calendar.HOUR_OF_DAY]
        val minute = calendar[Calendar.MINUTE]
        calendar.time = binding.schedulePresetCalendarview.selectedDate.date
        val day = calendar[Calendar.DAY_OF_MONTH]
        val month = calendar[Calendar.MONTH]
        val year = calendar[Calendar.YEAR]
        val dayOfWeek = calendar[Calendar.DAY_OF_WEEK]
        val strDuration = binding.schedulePresetSpinnerduration.selectedItem.toString()
        val temp = strDuration.split(" ").toTypedArray()
        val convertedMinutes = TimeUnit.DAYS.toMinutes(temp[0].toLong())
        preset.durationMinutes = convertedMinutes
        var expression = ""
        when (preset.strMode) {
            SpaPreset.RepeatMode.WEEKLY -> expression = String.format(Locale.ENGLISH, "0 %d %d ? * %d %d", minute, hour, dayOfWeek - 1, year)
            SpaPreset.RepeatMode.NONE -> expression = String.format(Locale.ENGLISH, "0 %d %d %d %d ? %d", minute, hour,
                    day, month + 1, year)
            SpaPreset.RepeatMode.DAILY -> expression = String.format(Locale.ENGLISH, "0 %d %d * * ? %d", minute, hour, year)
        }
        preset.strCronExpression = expression
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }


    companion object {
        private const val TAG = "SchedulePreset"
        private val sdf12: DateFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        private val sdf24: DateFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
    }

    override fun onTimezoneChanged(timezoneId: String) {
        mTimeZoneFragment.dismiss()
        preset.strTimeZone = timezoneId
        binding.mTimeZoneTxtView.text = timezoneId
    }
}
