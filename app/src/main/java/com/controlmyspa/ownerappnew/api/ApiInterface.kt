package com.controlmyspa.ownerappnew.api

import com.controlmyspa.ownerappnew.model.AccountData
import com.controlmyspa.ownerappnew.model.CzDetails
import com.controlmyspa.ownerappnew.model.PowerState
import com.controlmyspa.ownerappnew.model.ValidateUsername
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @POST(VALIDATE_USERNAME)
    fun getValidatedData(@Body body: JsonObject): Call<ValidateUsername>

    @POST(POWER_STATE_CHROMAZON)
    fun setPowerState(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(CZ_INTENSITY)
    fun setCzIntensity(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @GET(SPA_DETAILS)
    fun getSpaDetails(@Query(USERNAME) userName: String, @Header(HEADER) header: String): Observable<CzDetails>

    @GET(CMS_CODE_CHECK)
    fun checkCmsCode(@Query(CMS_CODE) cmsCode: String): Call<ValidateUsername>

    @POST(SET_ZONE)
    fun setState(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(SET_SPEED)
    fun setSpeed(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(SET_COLOR)
    fun setColor(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST
    fun checkSpaExists(@Url fullUrl: String, @Body body: JsonObject): Call<ValidateUsername>

    @GET(WHO_AM_I)
    fun getWhoAmI(@Header(HEADER) header: String): Call<AccountData>

    @POST(TEMP_DEGREE_MODE)
    fun setTempDegreeMode(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(TOGGLE_HEATER_MODE)
    fun setHeaterModeToggle(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(PANEL_LOCK)
    fun setPanelLockMode(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(LOW_HIGH)
    fun setLowHighMode(@Path(SPA_ID) spaId: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>

    @POST(SET_CONTROLS_STATE)
    fun setControlsState(@Path(SPA_ID) spaId: String, @Path(COMPONENT_TYPE) componentType: String, @Body body: JsonObject, @Header(HEADER) header: String): Observable<PowerState>
}