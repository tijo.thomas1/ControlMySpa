package com.controlmyspa.ownerappnew.api

import com.controlmyspa.ownerappnew.utils.Constants

const val BASE_URL = "${Constants.BASE_URL}/"
const val VALIDATE_USERNAME = "autoBuild/validateUsername"
const val SPA_ID = "spaId"
const val AUTO_BUILD_URL = Constants.AUTO_BUILD_URL
const val SPA_EXISTS_MSG = "Spa exists."
const val SERIAL = "serial"
const val SPA_VALIDATE = "validateSpa"
const val SPA_VERIFY = "$AUTO_BUILD_URL$SPA_VALIDATE"
const val CMS_CODE = "cmsCode"
const val CMS_CODE_VERIFY = "verify"
const val CMS_CODE_CHECK = "$AUTO_BUILD_URL$CMS_CODE_VERIFY"
const val POWER_STATE_CHROMAZON = "control/{$SPA_ID}/tzl/setPower"
const val DESIRED_STATE = "desiredState"
const val USERNAME = "username"
const val HEADER = "Authorization"
const val BEARER = "Bearer"
const val SPA_DETAILS = "spas/search/findByUsername"

/*tzlZone: accepted values 1,2,3,4,5,6 (4...6 when secondary chromazone is connected)
desiredState: accepted values 0,1,2,3,4,5,6,7,8*/
const val CZ_INTENSITY = "control/{$SPA_ID}/tzl/setIntensity"
const val TZL_ZONE = "tzlZone"
const val TZL_GROUP = "tzlGroup"

/*tzlZone: accepted values 1,2,3,4,5,6 (4...6 when secondary chromazone is connected)
desiredState: accepted values PARTY, RELAX, WHEEL*/
const val SET_ZONE = "control/{$SPA_ID}/tzl/setState"

/*set speed
* tzlZone: accepted values 1,2,3,4,5,6 (4...6 when secondary chromazone is connected)
desiredState: accepted values 0,1,2,3,4,5*/
const val SET_COLOR = "control/{$SPA_ID}/tzl/setColor"

const val SET_SPEED = "control/{$SPA_ID}/tzl/setSpeed"

const val WHO_AM_I = "/mobile/auth/whoami"

const val TEMP_DEGREE_MODE = "control/{$SPA_ID}/setTempMode"

const val TOGGLE_HEATER_MODE = "control/{$SPA_ID}/toggleHeaterMode"

const val ORIGINATOR_ID = "originatorId"

/*Lock Unlock Panel*/
const val PANEL_LOCK = "control/{$SPA_ID}/setPanel"

/*Low High Temp*/
const val LOW_HIGH = "control/{$SPA_ID}/setTempRange"

/*Controls*/
const val COMPONENT_TYPE = "componentType"
const val SET_CONTROLS_STATE = "control/{$SPA_ID}/set{$COMPONENT_TYPE}State"

